﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AppAndroid
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            

        }

        public void RedimensionarGuardarImagen(String fullFilePath, int size)
        {
            //System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            System.Drawing.Image image = System.Drawing.Image.FromFile(fullFilePath.Replace("_640.jpg", ".jpg").Replace("_320.jpg", ".jpg"), true);
            Size newSize = CalculateDimensions(image.Size, size);
            try
            {
                using (System.Drawing.Bitmap tempImage = new System.Drawing.Bitmap(image, newSize))
                {
                    //tempImage.Save(HttpContext.Current.Server.MapPath(fullFilePath), System.Drawing.Imaging.ImageFormat.Jpeg);
                    tempImage.Save(fullFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);

                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("DEBUG::LoadImages()::Error attempting to create image::" + e.Message);
            }
        }

        public static Size CalculateDimensions(Size oldSize, int targetSize)
        {
            Size newSize = new Size();
            if (oldSize.Height > oldSize.Width)
            {
                newSize.Width = (int)(oldSize.Width * ((float)targetSize / (float)oldSize.Height));
                newSize.Height = targetSize;
            }
            else
            {
                newSize.Width = targetSize;
                newSize.Height = (int)(oldSize.Height * ((float)targetSize / (float)oldSize.Width));
            }
            return newSize;
        }
    }
}
