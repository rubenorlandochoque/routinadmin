﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;
using AppAndroid.Master;
namespace WebService.WebService
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        [WebMethod]
        public string SincronizarRuteos(string jsonData)
        {
            var jss = new JavaScriptSerializer();

            dynamic data = jss.Deserialize<dynamic>(jsonData);
            DataTable dt = new DataTable();
            dt.Columns.Add("CodRuteo", typeof(String));
            dt.Columns.Add("FechaRegistro", typeof(String));
            

            try
            {
                String respuesta = "";

                foreach (var item in data)
                {
                    Database db = DatabaseFactory.CreateDatabase("sinc");
                    string stSqlCommand = "dbo.RUT_AddRuteo";
                    DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
                    db.AddOutParameter(dbCommand, "@Update", DbType.DateTime, 200);
                   
                    db.AddInParameter(dbCommand, "@CodProducto", DbType.String, item["CodProducto"]);
                    db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, item["CodRepositor"]);
                    db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, item["CodPuntoVenta"]);
                    db.AddInParameter(dbCommand, "@Observacion", DbType.String, item["Observacion"]);
                    db.AddInParameter(dbCommand, "@FechaCreacion", DbType.DateTime, Convert.ToDateTime(item["FechaCreacion"]));
                    db.AddInParameter(dbCommand, "@Coordenadas", DbType.String, item["Coordenadas"]);
                    db.AddInParameter(dbCommand, "@Precio", DbType.Double, Convert.ToDouble(item["Precio"]));
                    db.AddInParameter(dbCommand, "@Imagen", DbType.String, item["Imagen"]);
                    db.AddInParameter(dbCommand, "@Rango", DbType.String, item["Rango"]);
                    db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, item["CodSucursal"]);
                    db.ExecuteNonQuery(dbCommand);
                    DateTime dtime = Convert.ToDateTime(db.GetParameterValue(dbCommand, "@Update"));
                    
                    DataRow row = dt.NewRow();
                    row["CodRuteo"] = item["CodRuteo"];

                    string dia = ((DateTime)dtime).Day.ToString().PadLeft(2, '0');
                    string mes = ((DateTime)dtime).Month.ToString().PadLeft(2, '0');
                    string anio = ((DateTime)dtime).Year.ToString();
                    string hora = ((DateTime)dtime).Hour.ToString().PadLeft(2, '0');
                    string minuto = ((DateTime)dtime).Minute.ToString().PadLeft(2, '0');
                    string segundo = ((DateTime)dtime).Second.ToString().PadLeft(2, '0');

                    row["FechaRegistro"] = anio + "-" + mes + "-" + dia + " " + hora + ":" + minuto + ":" + segundo;
                   
                    dt.Rows.Add(row);
                }
                return JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            catch (Exception e)
            {
                return "false" + e.Message.ToString();
            }

        }


        [WebMethod]
        public string SyncRuteoFiles(String nameFile, byte[] file)
        {
            // the byte array argument contains the content of the file
            // the string argument contains the name and extension
            // of the file passed in the byte array
            String pathFile = "";
            try
            {
                //pathFile = "C:/ruteo/fotos";
                pathFile = Sesion.getURLFotosServer();
                MemoryStream ms = new MemoryStream(file);
                FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(pathFile + "/" + nameFile), FileMode.Create);
                ms.WriteTo(fs);
                ms.Close();
                fs.Close();
                fs.Dispose();
                return "true";


            }
            catch (Exception ex)
            {
                // return the error message if the operation fails
                return ex.Message.ToString();
            }
        }


        [WebMethod]
        public string SyncFiles(String nameFile, byte[] file, String codNovedad)
        {
            // the byte array argument contains the content of the file
            // the string argument contains the name and extension
            // of the file passed in the byte array
            String pathFile = "";
            try
            {
                //pathFile = "C:/ruteo/fotos";
                pathFile = Sesion.getURLFotosServer();    
                MemoryStream ms = new MemoryStream(file);
                FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(pathFile + "/" + nameFile), FileMode.Create);
                ms.WriteTo(fs);
                ms.Close();
                fs.Close();
                fs.Dispose();


                Database db = DatabaseFactory.CreateDatabase("sinc");
                string stSqlCommand = "dbo.NOV_AddFotoNovedades";
                DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
                db.AddInParameter(dbCommand, "@CodNovedad", DbType.String, codNovedad);
                db.AddInParameter(dbCommand, "@Imagen", DbType.String, nameFile);
                
                db.ExecuteNonQuery(dbCommand);

                return "true";


            }
            catch (Exception ex)
            {
                // return the error message if the operation fails
                return ex.Message.ToString();
            }
        }


        [WebMethod]
        public string SincronizarNovedades(string jsonData)
        {
            var jss = new JavaScriptSerializer();

            dynamic data = jss.Deserialize<dynamic>(jsonData);
            DataTable dt = new DataTable();
            dt.Columns.Add("CodNovedad", typeof(String));
            dt.Columns.Add("FechaRegistro", typeof(String));
            dt.Columns.Add("CodNewNovedad", typeof(String));

            try
            {
                String respuesta = "";

                foreach (var item in data)
                {
                    Database db = DatabaseFactory.CreateDatabase("sinc");
                    string stSqlCommand = "dbo.NOV_AddNovedad";
                    DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
                    db.AddOutParameter(dbCommand, "@Update", DbType.DateTime, 200);
                    db.AddOutParameter(dbCommand, "@CodNewNovedad", DbType.String, 200);
                    db.AddInParameter(dbCommand, "@CodNovedad", DbType.String, item["CodNovedad"]);
                    db.AddInParameter(dbCommand, "@CodProducto", DbType.String, item["CodProducto"]);
                    db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, item["CodRepositor"]);
                    db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, item["CodPuntoVenta"]);
                    db.AddInParameter(dbCommand, "@CodTipoNovedad", DbType.String, item["CodTipoNovedad"]);
                    db.AddInParameter(dbCommand, "@Observacion", DbType.String, item["Observacion"]);
                    db.AddInParameter(dbCommand, "@FechaCreacion", DbType.DateTime, Convert.ToDateTime(item["FechaCreacion"]));
                    db.AddInParameter(dbCommand, "@Coodenadas", DbType.String, item["Coordenadas"]);
                    db.AddInParameter(dbCommand, "@Precio", DbType.Double, Convert.ToDouble(item["Precio"]));
                    db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, item["CodSucursal"]);
                    try
                    {
                        db.AddInParameter(dbCommand, "@FechaElaboracion", DbType.DateTime, Convert.ToDateTime(item["FechaElaboracion"]));
                    }
                    catch (Exception e) {
                        db.AddInParameter(dbCommand, "@FechaElaboracion", DbType.DateTime, DBNull.Value);
                    }
                    
                    db.AddInParameter(dbCommand, "@HoraElaboracion", DbType.String, Convert.ToString(item["HoraElaboracion"]));
                    db.AddInParameter(dbCommand, "@NumLote", DbType.String, Convert.ToString(item["NumLote"]));
                    db.AddInParameter(dbCommand, "@Cantidad", DbType.Int32, Convert.ToInt32(item["Cantidad"]));
                    db.AddInParameter(dbCommand, "@Rango", DbType.Int32, Convert.ToInt32(item["Rango"]));
                    
                    db.ExecuteNonQuery(dbCommand);
                    DateTime dtime = Convert.ToDateTime(db.GetParameterValue(dbCommand, "@Update"));
                    String newNovedad = Convert.ToString(db.GetParameterValue(dbCommand, "@CodNewNovedad"));
                    DataRow row = dt.NewRow();
                    row["CodNovedad"] = item["CodNovedad"];

                    string dia = ((DateTime)dtime).Day.ToString().PadLeft(2, '0');
                    string mes = ((DateTime)dtime).Month.ToString().PadLeft(2, '0');
                    string anio = ((DateTime)dtime).Year.ToString();
                    string hora = ((DateTime)dtime).Hour.ToString().PadLeft(2, '0');
                    string minuto = ((DateTime)dtime).Minute.ToString().PadLeft(2, '0');
                    string segundo = ((DateTime)dtime).Second.ToString().PadLeft(2, '0');

                    row["FechaRegistro"] = anio + "-" + mes + "-" + dia + " " + hora + ":" + minuto + ":" + segundo;
                    row["CodNewNovedad"] = newNovedad;
                    dt.Rows.Add(row);
                }
                return JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            catch (Exception e)
            {
                return "false" + e.Message.ToString();
            }

        }

        [WebMethod]
        public string SincronizarTracking(string jsonData)
        {
            var jss = new JavaScriptSerializer();

            dynamic data = jss.Deserialize<dynamic>(jsonData);
            DataTable dt = new DataTable();
            dt.Columns.Add("CodTracking", typeof(String));
            dt.Columns.Add("FechaRegistro", typeof(String));

            try
            {
                String respuesta = "";

                foreach (var item in data)
                {
                    Database db = DatabaseFactory.CreateDatabase("sinc");
                    string stSqlCommand = "dbo.TRK_AddTraking";
                    DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
                    db.AddOutParameter(dbCommand, "@Update", DbType.DateTime, 200);
                    db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, item["CodUsuario"]);
                    db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, item["CodPuntoVenta"]);
                    db.AddInParameter(dbCommand, "@Coordenadas", DbType.String, item["Coordenadas"]);
                    db.AddInParameter(dbCommand, "@FechaCaptura", DbType.DateTime, Convert.ToDateTime(item["FechaCaptura"]));
                    db.AddInParameter(dbCommand, "@Rango", DbType.Int32, Convert.ToInt32(item["Rango"]));
                    db.AddInParameter(dbCommand, "@IMEI", DbType.String, item["IMEI"]);
                    db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, item["CodSucursal"]);

                    db.ExecuteNonQuery(dbCommand);
                    DateTime dtime = Convert.ToDateTime(db.GetParameterValue(dbCommand, "@Update"));
                    DataRow row = dt.NewRow();
                    row["CodTracking"] = item["CodTracking"];

                    string dia = ((DateTime)dtime).Day.ToString().PadLeft(2, '0');
                    string mes = ((DateTime)dtime).Month.ToString().PadLeft(2, '0');
                    string anio = ((DateTime)dtime).Year.ToString();
                    string hora = ((DateTime)dtime).Hour.ToString().PadLeft(2, '0');
                    string minuto = ((DateTime)dtime).Minute.ToString().PadLeft(2, '0');
                    string segundo = ((DateTime)dtime).Second.ToString().PadLeft(2, '0');

                    row["FechaRegistro"] = anio + "-" + mes + "-" + dia + " " + hora + ":" + minuto + ":" + segundo;
                    dt.Rows.Add(row);
                }
                return JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            catch (Exception e)
            {
                return "false" + e.Message.ToString();
            }

        }


        [WebMethod]
        public string SincronizarUsuarios()
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("sinc");

                DataTable dts = new DataTable();
                dts.Columns.Add("CodUsuario", typeof(String));
                dts.Columns.Add("Usuario", typeof(String));
                dts.Columns.Add("pass", typeof(String));
                dts.Columns.Add("CodPuntoVenta", typeof(String));
                dts.Columns.Add("PuntoVenta", typeof(String));
                dts.Columns.Add("CodTipoUsuario", typeof(String));
                dts.Columns.Add("CodSucursal", typeof(String));
                dts.Columns.Add("SincronizarTracking", typeof(int));

                string stSqlCommand = "[dbo].[REP_getRepositoresActivos]";
                DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
                DataTable dt = null;
                dt = db.ExecuteDataSet(dbCommand).Tables[0];

                foreach (DataRow rowf in dt.Rows)
                {
                    DataRow row = dts.NewRow();
                    row["CodUsuario"] = rowf["CodUsuario"];
                    row["Usuario"] = rowf["Usuario"];
                    row["pass"] = (BitConverter.ToString((Byte[])rowf["pass"])).Replace("-", "");
                    row["CodPuntoVenta"] = rowf["CodPuntoVenta"];
                    row["PuntoVenta"] = rowf["PuntoVenta"];
                    row["CodTipoUsuario"] = rowf["CodTipoUsuario"];
                    row["CodSucursal"] = rowf["CodSucursal"];
                    row["SincronizarTracking"] = rowf["SincronizarTracking"];

                    dts.Rows.Add(row);
                }

                /*configuracion global de tracking*/
                DataTable dtconfigtracking = null;
                dtconfigtracking = db.ExecuteDataSet(dbCommand).Tables[1];

                Dictionary<string, DataTable> returndata = new Dictionary<string, DataTable>();
                returndata.Add("datosusuario", dts);
                returndata.Add("datosconfiguracion", dtconfigtracking);

                return JsonConvert.SerializeObject(returndata, Formatting.Indented);

            }
            catch (Exception e)
            {
                return "false" + e.Message.ToString();
            }

        }

        [WebMethod]
        public string SincronizarProductos(String fechaUltAct)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("sinc");

                string stSqlCommand = "[dbo].[SYNC_SincronizarArtMobile]";
                DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
                db.AddInParameter(dbCommand, "@fechaUltAct", DbType.String, fechaUltAct);
                DataTable dt = null;
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                return JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            catch (Exception e)
            {
                return "false" + e.Message.ToString();
            }

        }


        [WebMethod]
        public string SincronizarDatos(string novedades, string ruteos, String fechaUltAct)
        {
            try
            {

                /*sincronizacion de novedades*/
                var jss = new JavaScriptSerializer();
                dynamic datanovedades = jss.Deserialize<dynamic>(novedades);
                DataTable dtnovedades = new DataTable();
                dtnovedades.Columns.Add("CodNovedad", typeof(String));
                dtnovedades.Columns.Add("FechaRegistro", typeof(String));
                dtnovedades.Columns.Add("CodNewNovedad", typeof(String));

                foreach (var item in datanovedades)
                {
                    Database db = DatabaseFactory.CreateDatabase("sinc");
                    string stSqlCommand = "dbo.NOV_AddNovedad";
                    DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

                    db.AddOutParameter(dbCommand, "@Update", DbType.DateTime, 200);
                    db.AddOutParameter(dbCommand, "@CodNewNovedad", DbType.String, 200);
                    db.AddInParameter(dbCommand, "@CodNovedad", DbType.String, item["CodNovedad"]);
                    db.AddInParameter(dbCommand, "@CodProducto", DbType.String, item["CodProducto"]);
                    db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, item["CodRepositor"]);
                    db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, item["CodPuntoVenta"]);
                    db.AddInParameter(dbCommand, "@CodTipoNovedad", DbType.String, item["CodTipoNovedad"]);
                    db.AddInParameter(dbCommand, "@Observacion", DbType.String, item["Observacion"]);
                    db.AddInParameter(dbCommand, "@FechaCreacion", DbType.DateTime, Convert.ToDateTime(item["FechaCreacion"]));
                    db.AddInParameter(dbCommand, "@Coodenadas", DbType.String, item["Coordenadas"]);
                    db.AddInParameter(dbCommand, "@Precio", DbType.Double, Convert.ToDouble(item["Precio"]));
                    db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, item["CodSucursal"]);
                    try
                    {
                        db.AddInParameter(dbCommand, "@FechaElaboracion", DbType.DateTime, Convert.ToDateTime(item["FechaElaboracion"]));
                    }
                    catch (Exception e)
                    {
                        db.AddInParameter(dbCommand, "@FechaElaboracion", DbType.DateTime, DBNull.Value);
                    }

                    db.AddInParameter(dbCommand, "@HoraElaboracion", DbType.String, Convert.ToString(item["HoraElaboracion"]));
                    db.AddInParameter(dbCommand, "@NumLote", DbType.String, Convert.ToString(item["NumLote"]));
                    db.AddInParameter(dbCommand, "@Cantidad", DbType.Int32, Convert.ToInt32(item["Cantidad"]));
                    db.AddInParameter(dbCommand, "@Rango", DbType.Int32, Convert.ToInt32(item["Rango"]));

                    db.ExecuteNonQuery(dbCommand);
                    DateTime dtime = Convert.ToDateTime(db.GetParameterValue(dbCommand, "@Update"));
                    String newNovedad = Convert.ToString(db.GetParameterValue(dbCommand, "@CodNewNovedad"));
                    DataRow row = dtnovedades.NewRow();
                    row["CodNovedad"] = item["CodNovedad"];

                    string dia = ((DateTime)dtime).Day.ToString().PadLeft(2, '0');
                    string mes = ((DateTime)dtime).Month.ToString().PadLeft(2, '0');
                    string anio = ((DateTime)dtime).Year.ToString();
                    string hora = ((DateTime)dtime).Hour.ToString().PadLeft(2, '0');
                    string minuto = ((DateTime)dtime).Minute.ToString().PadLeft(2, '0');
                    string segundo = ((DateTime)dtime).Second.ToString().PadLeft(2, '0');

                    row["FechaRegistro"] = anio + "-" + mes + "-" + dia + " " + hora + ":" + minuto + ":" + segundo;
                    row["CodNewNovedad"] = newNovedad;
                    dtnovedades.Rows.Add(row);
                }

                /*sincronizacion de ruteos*/
                dynamic dataruteos = jss.Deserialize<dynamic>(ruteos);
                DataTable dtruteos = new DataTable();
                dtruteos.Columns.Add("CodRuteo", typeof(String));
                dtruteos.Columns.Add("FechaRegistro", typeof(String));

                foreach (var item in dataruteos)
                {
                    Database db = DatabaseFactory.CreateDatabase("sinc");
                    string stSqlCommand = "dbo.RUT_AddRuteo";
                    DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

                    db.AddOutParameter(dbCommand, "@Update", DbType.DateTime, 200);
                    db.AddInParameter(dbCommand, "@CodProducto", DbType.String, item["CodProducto"]);
                    db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, item["CodRepositor"]);
                    db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, item["CodPuntoVenta"]);
                    db.AddInParameter(dbCommand, "@Observacion", DbType.String, item["Observacion"]);
                    db.AddInParameter(dbCommand, "@FechaCreacion", DbType.DateTime, Convert.ToDateTime(item["FechaCreacion"]));
                    db.AddInParameter(dbCommand, "@Coordenadas", DbType.String, item["Coordenadas"]);
                    db.AddInParameter(dbCommand, "@Precio", DbType.Double, Convert.ToDouble(item["Precio"]));
                    db.AddInParameter(dbCommand, "@Imagen", DbType.String, item["Imagen"]);
                    db.AddInParameter(dbCommand, "@Rango", DbType.String, item["Rango"]);
                    db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, item["CodSucursal"]);
                    db.ExecuteNonQuery(dbCommand);
                    DateTime dtime = Convert.ToDateTime(db.GetParameterValue(dbCommand, "@Update"));

                    DataRow row = dtruteos.NewRow();
                    row["CodRuteo"] = item["CodRuteo"];

                    string dia = ((DateTime)dtime).Day.ToString().PadLeft(2, '0');
                    string mes = ((DateTime)dtime).Month.ToString().PadLeft(2, '0');
                    string anio = ((DateTime)dtime).Year.ToString();
                    string hora = ((DateTime)dtime).Hour.ToString().PadLeft(2, '0');
                    string minuto = ((DateTime)dtime).Minute.ToString().PadLeft(2, '0');
                    string segundo = ((DateTime)dtime).Second.ToString().PadLeft(2, '0');

                    row["FechaRegistro"] = anio + "-" + mes + "-" + dia + " " + hora + ":" + minuto + ":" + segundo;
                    dtruteos.Rows.Add(row);
                }

                /*sincronizar productos*/

                Database dbp = DatabaseFactory.CreateDatabase("sinc");
                string stSqlCommandp = "[dbo].[SYNC_SincronizarArtMobile]";
                DbCommand dbCommandp = dbp.GetStoredProcCommand(stSqlCommandp);

                stSqlCommandp = "[dbo].[SYNC_SincronizarArtMobile]";
                dbCommandp = dbp.GetStoredProcCommand(stSqlCommandp);
                dbp.AddInParameter(dbCommandp, "@fechaUltAct", DbType.String, fechaUltAct);
                DataTable dtproductos = null;
                dtproductos = dbp.ExecuteDataSet(dbCommandp).Tables[0];

                Dictionary<string, DataTable> returndata = new Dictionary<string, DataTable>();
                returndata.Add("novedades", dtnovedades);
                returndata.Add("ruteos", dtruteos);
                returndata.Add("productos", dtproductos);

                return JsonConvert.SerializeObject(returndata, Formatting.Indented);
            }
            catch (Exception e)
            {
                return "false" + e.Message.ToString();
            }
        }
    }
}
