﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadFile.ascx.cs" Inherits="AppAndroid.Master.UploadFile" %>


            <!--MODAL-->
            <div class="modal fade" id="myModalArchivos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <!-- /.modal-content - listado de archivos -->
                    <div class="modal-content" id="modalContentSubirArchivos">
                        <div class="modal-header">
                            <input type='button' id='btnpopupFU' class='close' data-dismiss='modal' aria-hidden='true' value='x' style="display: none" />
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <br>
                            <i class="fa fa-file fa-3x"></i>

                            <h4 id="myModalLabel" class="semi-bold">Adjuntar Archivos</h4>
                            <%--<p class="no-margin">Cuando haya terminado de adjuntar los archivos presione Guardar</p>--%>
                            <br />

                        </div>
                        <div class="modal-body" id="bodyPopupFU">

                            <div>
                                <center><p style="font-size: 15px; margin-bottom: 10px; margin-left: 10px; font-weight: bold">
                                    Archivos Adjuntados
                                </p></center>
                                <div style="width: 500px; background-color: #F3F5F6; height: 25px; clear: both; margin-left: 25px">
                                    <div style="width: 175px; float: left; margin-left: 40px;">
                                        Nombre
                                    </div>
                                    <div style="width: 50px; float: left; margin-left: 80px">
                                        Acciones
                                    </div>
                                </div>
                                <div id="uploadedDiv" runat="server" style="width: 500px; clear: both;height: 250px;overflow-y: auto;overflow-x: hidden;margin-left: 25px" clientidmode="Static">
                                </div>
                                <asp:HiddenField ID="hdnFileFolder" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="hdnCountFiles" runat="server" Value="0" ClientIDMode="Static" />
                                <div style="padding-top: 10px; clear: both">
                                    <div style="float: left; padding-top: 40px;">
                                        <%--Show wrapper on above of fileUpload Control--%>
                                        <label class="file-upload">
                                            <%-- Set Text To be displayed inplace of Browse button--%>
                                            <%--<span><strong>Elegir Archivo</strong></span>--%>
                                            <%--Make clientID static if you are using Master Page--%>
                                            <button class="btn btn-primary" type="button" id="btnBuscarFoto" title="Subir archivo desde carpeta" data-backdrop="static" data-keyboard="false">
                                                <i class="fa fa-upload"></i>
                                            </button>
                                            <span class="btn btn-primary btn-file btn-mini" style="display: none"><i class="icon-custom-chart"></i>
                                                <asp:FileUpload ID="fileToUpload" runat="server" ClientIDMode="Static" AllowMultiple="true" />
                                            </span>
                                        </label>

                                        <input type="hidden" id="NoTienePreview" name="NoTienePreview" value="" />
                                        <button class="btn btn-primary" type="button" id="btnUsarCamara" title="Tomar foto desde webcam" style="margin-top: -67px; margin-left: 50px;">
                                            <i class="fa fa-camera"></i>
                                        </button>
                                    </div>
                                    <div style="float: left; padding-left: 10px">
                                        <span style="padding-left: 10px">
                                            <%--Progress bar--%>
                                            <img id="loading" src="loading.gif" style="display: none;"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <div id="botoneraFU">
                                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrarModalFU">Cerrar</button>
                                <%--<button type="button" class="btn btn-primary" id="btnConfirmarSubida">Confirmar</button>--%>
                            </div>

                        </div>
                    </div>
                    <!-- /.modal-content - listado de archivos -->

                    <!-- /.modal-content - tomar foto -->
                    <div class="modal-content" id="modalContentTomarFotos" style="display: none">
                        <div id="fSaveDatoRevisita">
                            <div class="modal-header">
                                <input type='button' id='btnpopup2' class='close' data-dismiss='modal' aria-hidden='true' value='x' style="display: none" />
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <br>
                                <i class="fa fa-camera fa-3x"></i>

                                <h4 id="myModalLabel" class="semi-bold">Tomar Foto Web Cam.</h4>
                                <p class="no-margin">Cuando este conforme, presione Confirmar para seleccionar la foto.</p>
                                <br />

                            </div>
                            <div class="modal-body" id="bodyPopup">
                                <div id="bodyTomarFoto">
                                    <div class="row form-row">
                                        <div class="col-md-6">
                                            <video id="video" width="260" height="280" autoplay></video>
                                        </div>
                                        <div class="col-md-6">
                                            <canvas id="canvas" style="padding-top: 40px;" width="260" height="200"></canvas>
                                            <canvas id="canvasHidden" style="display: none;" width="1024" height="768"></canvas>
                                            <img id='output' style="display: none;">
                                        </div>
                                    </div>
                                </div>
                                <div id="bodyEditarFoto" style="display: none">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div style="background-color: #f7f7f7; border: 1px solid #eee; box-shadow: inset 0 0 3px #f7f7f7; height: 480px; width: 100%; overflow: hidden">
                                                <img class="cropper" src="" alt="Picture" style="display: none">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="row eg-output">
                                            <div class="col-md-12">
                                                <div style="text-align: center;">
                                                    <button class="btn btn-primary btn-cons" id="getDataURL2" type="button" style="display: none">Aceptar</button>
                                                    <button class="btn btn-default" id="getDataURL" type="button" style="display: none">Cancelar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: center">
                                        <label class="form-label">Ingrese nombre de la foto</label>
                                        <input type="text" id="nombreFotoTomada" />
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div id="botoneraTomarFoto">
                                    <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrarModal">Cerrar</button>
                                    <button type="button" class="btn btn-primary" id="btnTomarFoto">Tomar Foto</button>
                                    <button type="button" class="btn btn-primary" id="btnConfirmarFoto">Confirmar</button>
                                </div>
                                <div id="botoneraEditarFoto" style="display: none">
                                    <%--<input class="btn btn-default" data-dismiss="modal" value="Cancelar" type="button" onclick="cerrarDialogoFoto()" />--%>
                                    <input value="Aceptar" class="btn btn-primary btn-sm" type="button" onclick="actualizarFoto()" />
                                </div>
                            </div>

                            <div>
                                <img id="imgCaptura" style="display: none;" src="" />
                            </div>
                        </div>
                    </div>
                    <!-- /.modal-content - tomar foto -->
                </div>
                <!-- /.modal-dialog -->
            </div>


<script>
    /**
        Definir
        [name=btnShowModalAdjubntos] -  boton que muestra el modal de Adjuntar Archivos
        [name=lstArchivosAdjuntos]   -  input que contiene la lista de archivos adjuntos
        [name=txtCantArchivosAdjuntos]- input que contiene la cantidad de archivos adjuntos (no necesario)

        Modal:
        #hdnCountFiles  -   Input Cantidad de Archivos
        #hdnFileFolder  -   Input Nombre de Carpeta donde se subira el Archivo Adjunto
        #fileToUpload   -   Input File para subirl los archivos
        .nroadjuntos    -   class para identificar donde se actualizara la cantidad de archivos a mostrar
    **/

    //Botón Adjunta Archivos
    $("[name=btnShowModalAdjubntos]").click(function () {
        //$("#hdnCountFiles").val("0");
        $('#myModalArchivos').modal({ keyboard: false, backdrop: "static" });
        cargarAdjuntos();
        
    });

    //Botón Examinar/Seleccionar Archivo
    $("#btnBuscarFoto").click(function () {
        
        $("[id$=fileToUpload]").click(); //mostrarDialogoBuscarFoto();
    });

    //Botón Usar Cámara Web
    $("#btnUsarCamara").click(function () {
        //tomarFotoCamara();
        $("#modalContentSubirArchivos").hide();
        $("#modalContentTomarFotos").show();
    });
    /*
    //Botón Tomar Foto
    $("#btnTomarFoto").click(function () {
        context.drawImage(video, 0, 0, 280, 220);
        //context.drawImage( video, 0, 0, 1024, 768);
        contextHdd.drawImage(video, 0, 0, 1024, 768);
    });

    //Botón Confirmar
    $("#btnConfirmarFoto").click(function () {
        if ($("#nombreFotoTomada").val().trim() != "") {
            var dataURL = canvasHdd.toDataURL("image/png");
            var dataURL_preview = canvasHdd.toDataURL("image/png");
            $("#dataURL").text(dataURL);
            $("#showDataURL").html('<img src="' + dataURL_preview + '">');
            cargarFoto(dataURL);
            salvarImagen(dataURL);

        } else {
            alert("Debe ingresar un nombre");
        }
    });
    */
    $("#hdnCountFiles").change(function() {
        var seccion = $("[id$=seccionseleccionada]").val();
        seccion = (seccion == "") ? "" : ("#" + seccion + " ");
        $(seccion + ".nroadjuntos").val(this.value).filter("span").text(this.value); //$(seccion + ".nroadjuntos").text(this.value);
    });



    //lee la extructura de la lista de archivos en [name=lstArchivosAdjuntos], lo interpreta y muestra en el modal
    function cargarAdjuntos() {
        var seccion = $("[id$=seccionseleccionada]").val();
        seccion = (seccion == "") ? "" : ("#" + seccion + " ");

        $("#modalContentSubirArchivos").show();
        $("#modalContentTomarFotos").hide();
        $("#uploadedDiv").html("");
        $("#hdnCountFiles").val("0");

        var count = 0;
        var dir = $('[id$=hdnFileFolder]').val();
        if ($(seccion + '[name=lstArchivosAdjuntos]').val() != "") {
            var listadoArchivos = $(seccion + '[name=lstArchivosAdjuntos]').val().split("|");
            for (var i = 0; i < listadoArchivos.length; i++) {
                count++;
                var hdnid = 'hdnDocId_' + count;
                var lblfilename = 'lblfilename_' + count;
                var filename = listadoArchivos[i].split(":")[0];
                var nombreFisico = listadoArchivos[i].split(":")[1];

                $("#uploadedDiv").append("<div id='" + hdnid + "' style='clear:both; background-color:#FCFCFC; padding-top:5px; height:25px; width:500px'>" +
                    "<span id='" + lblfilename + "' style='width:195px;float:left;margin-left:40px;overflow:hidden; white-space: nowrap; text-overflow: ellipsis;' value='" + nombreFisico + "'>" + filename + "</span>" +
                    "<input name='" + hdnid + "' id='" + hdnid + "' value='" + i + "' type='hidden'>" +
                    "<span style='float:left; margin-left:60px; width:40px;' ><a href='#' class='dellink' onclick='deleterow(\"#" + hdnid + "," + nombreFisico + "\")'>Borrar</a></span>" + // for deleting file
                    "<span style='float:left;' >&nbsp;&nbsp;<a class='dellink' href='../../Adjuntos/FileUpload.ashx?filepath=" + dir + "&file=" + nombreFisico + "' >Descargar</a></span>" + // for downloading file
                    "<span style='float:left;' >&nbsp;&nbsp;<a href='#' onclick=\"nuevaVentana('../../Adjuntos/FileUpload.ashx?filepath=" + dir + '&file=' + nombreFisico + "&preview=1', 'preview', 800, 400)\" class='' onclick=''>Ver</a></span>" +
                    "</div>"
                );

            }
            $("#hdnCountFiles").val(count).change();
        }

        subirCadena();
    }

    function subirCadena() {
        var seccion = $("[id$=seccionseleccionada]").val();
        seccion = (seccion == "") ? "" : ("#" + seccion + " ");

        $(seccion + '[name=lstArchivosAdjuntos]').val("");  //$('[id$=fSegListArch]').val("");
        
        count = parseInt($("#hdnCountFiles").val());
        var archivos = "";
        var i = 1;
        var cant = 0;
        while (cant < count) {
            var element = $("#lblfilename_" + i);
            var nombreFisico = $(element).attr("value");
            var nombremostrar = $(element).text();
            if (typeof element.attr("value") != typeof undefined) {
                archivos = archivos + nombremostrar + ":" + nombreFisico + "|";
                cant++;
            }
            i++;
        }

        $(seccion + '[name=lstArchivosAdjuntos]').val(archivos.substring(0, archivos.length - 1));  //$('[id$=fSegListArch]').val(archivos.substring(0, archivos.length - 1));

        archivos = $(seccion + '[name=lstArchivosAdjuntos]').val().trim();

        //Tiene adjunto: <asp:RadioButton ID="rbtntieneadjunto01"(COD_SI) y ID="rbtntieneadjunto02"(COD_NO) />
        if (archivos != "") {
            $(seccion + "[value=rbtntieneadjunto01]").click();
        } else {
            $(seccion + "[value=rbtntieneadjunto02]").click();
        }
    }

    // Asynchronous file upload process
    function ajaxFileUpload() {
        var codPersona = $('[id$=codigoPersona]').val();
        var codFormRealizado = $('[id$=codigoFormRealizado]').val();
        var codFormulario = $('[id$=codigoFormulario]').val();
        var codOrganizacion = $('[id$=codigoOrganizacion]').val();
        var codUsuario = $('[id$=codigoUsuario]').val();
        var FileFolder = $('#hdnFileFolder').val();


        /*preparar los archivos para subir
          el control fileupload tiene todos los archivos para subir.
          como es un control de entrada no se pueden borrar los elemtnos que ya existen,
          por lo que se envian todos los archivos. Primerante se hace un control de cuales 
          archivos ya existen, y se envian las posiciones en el array de esos elemento
          que no se insertaran.
        */
        var input = document.getElementById('fileToUpload');
        var duplicados = []; //duplicados ((y no admitidos))
        for (var x = 0; x < input.files.length; x++) {
            if (checkFileExtension(input.files[x].name)) {  //sube solo algunos tipos de archivos
            //if (true) {//sube cualquier tipo de archivo
                var filename = input.files[x].name.substr(0, (input.files[x].name.lastIndexOf('.')));
                var counter = $('#hdnCountFiles').val();
                for (var i = 1; i <= counter; i++) {
                    var hdnDocId = "#hdnDocId_" + i;
                    if ($(hdnDocId).length > 0) {
                        var mFileName = "#lblfilename_" + i;
                        if ($(mFileName).html().trim() == filename) {
                            duplicados.push(x);
                        }
                    }
                }
            } else {
                duplicados.push(x);
            }
        }

        blockUIIn("", "Subiendo. Aguarde por favor");
        //Subimos los archivos
        $.ajaxFileUpload({
            url: '../../Adjuntos/FileUpload.ashx?id=' + FileFolder + "&notSave=" + duplicados.join("|"),
            secureuri: false,
            fileElementId: 'fileToUpload',
            dataType: 'json',
            success: function (data, status) {
                console.log("resultado");
                console.log(data);
                console.log(status);
                console.log("___resultado");
                if (typeof (data.error) != 'undefined') {
                    if (data.upfile.trim() !== "") {
                        var filesSave = "";
                        var files = data.upfile.split("/");
                        for (var index = 0; index < files.length; index++) {
                            ini = files[index].indexOf('_') + 1;
                            fin = files[index].lastIndexOf('.');
                            ShowUploadedFiles(files[index], files[index].substring(ini, fin));//vamos mostrando los archivos
                            filesSave += FileFolder + "/" + files[index];//filseSave contiene la cadena de archivos que se guardaran en base e
                            if (index < files.length - 1) {
                                filesSave += "¬";
                            }
                        }
                        subirCadena();
                        /*Guardar en Base De Datos*/
                        var dataSave = {};
                        dataSave["CodPersona"] = codPersona;
                        dataSave["NameFile"] = filesSave;
                        dataSave["CodTipoMultimedia"] = "cod_archivo";
                        dataSave["CodFormRealizado"] = codFormRealizado;
                        dataSave["CodFormulario"] = codFormulario;
                        dataSave["Codlocalizacion"] = "";
                        dataSave["CodOrganizacion"] = codOrganizacion;
                        dataSave["CodUsuario"] = codUsuario;
                        $.ajax({
                            type: "POST",
                            url: '../WebServices/Formulario.asmx/GuardarAdjuntos',
                            data: JSON.stringify(dataSave),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (respuesta) {
                                unblockUIOut();
                            }
                        });
                    }

                    if (data.notUpload != '?') {
                        unblockUIOut();
                        alert("Los siguientes archivos no se subieron por que ya existen o el tipo de archivo no es admitido:\n\n" + (data.notUpload.split("/")).join("\n"));
                    }

                    $('#fileToUpload').val("");
                }
            },
            error: function (data, status, e) {
                console.log(e);
                console.log(data);
                console.log(status);
                unblockUIOut();
            }
        });
        return false;
    }

    /* ************************* */
    var cadena = "";
    function checkFileExtension(file) {
        var rechasar = /\.(exe|com|bat|dll)$/i.test(file);
        return !rechasar;
    }


    //get file path from client system
    function getNameFromPath(strFilepath) {

        var objRE = new RegExp(/([^\/\\]+)$/);
        var strName = objRE.exec(strFilepath);

        if (strName == null) {
            return null;
        }
        else {
            return strName[0];
        }

    }

    //show uploaded file 
    function ShowUploadedFiles(file, fileName) {

        count = parseInt($("#hdnCountFiles").val()) + 1;
        var hdnid = 'hdnDocId_' + count;
        var lblfilename = 'lblfilename_' + count;
        var path = $('[id$=hdnFileFolder]').val();

        $("#uploadedDiv").append("<div id='" + hdnid + "' style='clear:both; background-color:#FCFCFC; padding-top:5px; height:25px; width:500px'><span id='" + lblfilename + "' style='width:195px;float:left;margin-left:40px;overflow:hidden;white-space: nowrap; text-overflow: ellipsis;' value='" + file + "'>" + fileName +
            "</span><input name='" + hdnid + "' id='" + hdnid + "' value='" + count + "' type='hidden'></span><span style='float:left; margin-left:60px; width:40px;' >" +
            "<a href='#' class='dellink' onclick='deleterow(\"#" + hdnid + "," + file + "\")'>Borrar</a></span>" + // for deleting file
            "<span style='float:left;' >&nbsp;&nbsp;<a class='dellink' href='../../Adjuntos/FileUpload.ashx?filepath=" + path + "&file=" + file + "' >Descargar</a></span>" +// for downloading file
            "<span style='float:left;' >&nbsp;&nbsp;<a class='dellink' href='#' onclick=\"nuevaVentana('../../Adjuntos/FileUpload.ashx?filepath=" + path + '&file=' + file + "&preview=1', 'preview', 800, 400)\" class='' onclick=''>Ver</a></span>" + // for preview file
            "</div>"

        );

        //update file counter
        $("#hdnCountFiles").val(count).change();
        return false;

    }

    // delete existing file
    function deleterow(divrow) {
        var str = divrow.split(",");
        var row = str[0];
        var file = str[1];
        var path = $('#hdnFileFolder').val();
        if (confirm('Seguro qué desea eliminar?')) {
            $.ajax({
                url: "../../Adjuntos/FileUpload.ashx?path=" + path + "&file=" + file,
                type: "GET",
                cache: false,
                async: true,
                success: function (html) {
                    $("#hdnCountFiles").val(parseInt($("#hdnCountFiles").val()) - 1).change();
                    $(row).remove();
                    subirCadena();
                }
            });
        }
        //var nuevaCadena = $('[id$=archivosAdjuntos]').val().replace("" + file + "", "");
        //nuevaCadena = nuevaCadena.replace(";;", ";");
        //$('[id$=archivosAdjuntos]').val(nuevaCadena);
        var formRealizado = $('[id$=codigoFormRealizado]').val();
        var folder = $('#hdnFileFolder').val();
        var data = {};
        data["CodPersona"] = $('[id$=codigoPersona]').val();
        data["Archivo"] = folder + "/" + str[1];
        data["CodTipoMultimedia"] = "cod_archivo";
        data["CodFormRealizado"] = formRealizado;

        $.ajax({
            type: "POST",
            url: '../WebServices/Formulario.asmx/BorrarAdjuntos',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (respuesta) {
                console.log("borrado");
            }
        });
        return false;

    }

    function nuevaVentana(url, name, _width, _height) {
        var left = parseInt((screen.availWidth / 2) - (_width / 2));
        var top = parseInt((screen.availHeight / 2) - (_height / 2));
        var specs = "channelmode=yes, location=no, menubar=no, scrollbars=no, status=no, resizable=yes, titlebar=no, width=" + _width + ",height=" + _height + ",left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
        ventana = window.open(url, name, specs);
        ventana.focus();
    }

    function cargarFoto(url) {
        $("[id$=imagenmostrar]").attr("src", url);
        $("[id$=dataFoto]").attr("value", url);
    }

    function cerrarDialogoFoto() {
        $("[id$=botoneraTomarFoto]").show();
        $("[id$=botoneraEditarFoto]").hide();
        $("[id$=bodyTomarFoto]").show();
        $("[id$=bodyEditarFoto]").hide();
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.rect(0, 0, canvas.width, canvas.height);
        context.stroke();
        //$("#btnpopup1").trigger("click");
        //$("#btnpopup2").trigger("click");
    }

    window.addEventListener("DOMContentLoaded", function () {
        // Grab elements, create settings, etc.
        canvas = document.getElementById("canvas");
        canvasHdd = document.getElementById("canvasHidden");
        context = canvas.getContext("2d");
        context.rect(0, 0, canvas.width, canvas.height);
        context.stroke();
        contextHdd = canvasHdd.getContext("2d");

        video = document.getElementById("video");
        videoObj = { "video": true };
        errBack = function (error) {
            console.log("Video capture error: ", error.code);
        };

        // Put video listeners into place
        if (navigator.getUserMedia) { // Standard
            navigator.getUserMedia(videoObj, function (stream) {
                video.src = stream;
                video.play();
                console.log("Standard");
            }, errBack);
        } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
            navigator.webkitGetUserMedia(videoObj, function (stream) {
                video.src = window.webkitURL.createObjectURL(stream);
                video.play();
                console.log("WebKit-prefixed");
            }, errBack);
        } else if (navigator.mozGetUserMedia) { // WebKit-prefixed
            navigator.mozGetUserMedia(videoObj, function (stream) {
                video.src = window.URL.createObjectURL(stream);
                video.play();
                console.log("WebKit-prefixed 22");
            }, errBack);
        }

        
        // Trigger photo take
        document.getElementById("btnTomarFoto").addEventListener("click", function () {
            context.drawImage(video, 0, 0, 280, 220);
            //context.drawImage( video, 0, 0, 1024, 768);
            contextHdd.drawImage(video, 0, 0, 1024, 768);
        });
        document.getElementById("btnConfirmarFoto").addEventListener("click", function () {
           /*
            document.getElementById("imgCaptura").src = canvas.toDataURL();
            $("[id$=imagenmostrar]").attr("src", document.getElementById('imgCaptura').src);
    
            //console.log(canvas.toDataURL("image/png"));
            $image.cropper("replace", canvasHdd.toDataURL("image/png"));
            $image.cropper("reset", true);
    
            //$("#btnMostrarFotoRecortar").click();
            //$(window).trigger('resize');
    
            //document.getElementById("hidden").value = canvas.toDataURL("image/png").replace('data:image/png;base64,', '');
            $("[id$=botoneraTomarFoto]").hide();
            $("[id$=botoneraEditarFoto]").show();
            $("[id$=bodyTomarFoto]").hide();
            $("[id$=bodyEditarFoto]").show();
            */
            var adjuntos = $("#uploadedDiv div span:nth-child(1)").map(function () { return $(this).text(); });

            if ($("#nombreFotoTomada").val().trim() != "") {
                var dataURL = canvasHdd.toDataURL("image/png");
                var dataURL_preview = canvasHdd.toDataURL("image/png");
                $("#dataURL").text(dataURL);
                $("#showDataURL").html('<img src="' + dataURL_preview + '">');
                cargarFoto(dataURL);
                salvarImagen(dataURL);

            } else {
                alert("Debe ingresar un nombre");
            }
        });
    }, false);

    function salvarImagen(imagen, persona) {
        var codPersona = $('[id$=codigoPersona]').val();
        var codFormRealizado = $('[id$=codigoFormRealizado]').val();
        var codFormulario = $('[id$=codigoFormulario]').val();
        var codOrganizacion = $('[id$=codigoOrganizacion]').val();
        var codUsuario = $('[id$=codigoUsuario]').val();
        var FileFolder = $('#hdnFileFolder').val();
        var fileToUpload = getNameFromPath($('#nombreFotoTomada').val()) + ".jpg";

        var filename = fileToUpload.substr(0, (fileToUpload.lastIndexOf('.')));
        var flag = true;
        var counter = $('#hdnCountFiles').val();
        if (filename != "" && filename != null && FileFolder != "0") {
            //Check duplicate file entry
            for (var i = 1; i <= counter; i++) {
                var hdnDocId = "#hdnDocId_" + i;

                if ($(hdnDocId).length > 0) {
                    var mFileName = "#lblfilename_" + i;
                    if ($(mFileName).html() == filename) {
                        flag = false;
                        break;
                    }
                }
            }
            if (flag == true) {
                var time = new Date().getTime();
                var data = {};
                data["CodPersona"] = codPersona;
                data["NameFile"] = FileFolder + "/" + time + "_" + fileToUpload;
                data["CodTipoMultimedia"] = "cod_archivo";
                data["CodFormRealizado"] = codFormRealizado;
                data["CodFormulario"] = codFormulario;
                data["Codlocalizacion"] = "";
                data["CodOrganizacion"] = codOrganizacion;
                data["CodUsuario"] = codUsuario;
                var dataImagen = {};
                dataImagen["imagenBase64"] = imagen;
                dataImagen["nombreArchivo"] = time + "_" + fileToUpload;
                dataImagen["carpeta"] = FileFolder;
                $.ajax({
                    type: "POST",
                    url: 'F11.aspx/SalvarImagen',
                    data: JSON.stringify(dataImagen),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (respuesta) {
                        ShowUploadedFiles(time + "_" + fileToUpload, filename);
                        subirCadena();
                        cerrarDialogoFoto();
                        $("#modalContentSubirArchivos").show();
                        $("#modalContentTomarFotos").hide();
                        $("#nombreFotoTomada").val("");
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '../WebServices/Formulario.asmx/GuardarAdjuntos',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (respuesta) {
                        console.log(fileToUpload);
                    }
                });
            } else {
                alert('El archivo ' + filename + ' ya existe');
            }
        }
    }

    function tomarFotoCamara() {
        $("#modalContentSubirArchivos").hide();
        $("#modalContentTomarFotos").show();
    }


    /* FUNCIONES PARA EL MANEJO DE FOTOS - FIN */

    /* Si se mostrara la cantidad de archivos adjuntos es necesario agreagar en estas funciones el codigo siguiente
    $('#idDataTableByFile tbody').on('click', 'tr', function () {
        code.....
        var data = dataTableByFile.row(this).data();
        var respuestas = data[data.length - 1];
        var num = (respuestas['COD_FORM_XX_COD_LISTADO_DE_ARCHIVOS_ADJUNTO-'] == null) ? 0 : (respuestas['COD_FORM_XX_COD_LISTADO_DE_ARCHIVOS_ADJUNTO-'].split("|")).length;
        $(".nroadjuntos").val(0).filter("span").text("(" + num + ")");

        code....

    }

    $("#btnNuevoRegistro").on("click", function () {
        code...
        $(".nroadjuntos").val(0).filter("span").text("(0)");
        code...
    }

    function manejadorPostCargaControles(nombreSeccion) {
    $(".nroadjuntos").val(0).filter("span").text("(0)");

    }
*/

</script>




