﻿using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System.Text;
using System.Web.UI.HtmlControls;

namespace AppAndroid.Master
{
    public partial class Ruteo1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string codPantalla = "P_RUTEO";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
        }


        [WebMethod]
        public static string GetAllRepositores()
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.REP_getAllRepositoresByUser";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, Sesion.getCodUsuario());

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetAllPuntosVentas()
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.PV_getAllPuntosVenta";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        
        [WebMethod]
        public static string FiltrarRuteos(string CodRepositor, string CodPuntoVenta, string CodSucursal, string CodProducto, string FechaInicio, string FechaFin)
        {
            FechaInicio = FechaInicio.Replace("/", "-");
            FechaFin = FechaFin.Replace("/", "-");

            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.RUT_GetListRuteos");
            db.AddInParameter(dbCommand, "@Repositor", DbType.String, CodRepositor);
            db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, CodSucursal);
            db.AddInParameter(dbCommand, "@CodArticulo", DbType.String, CodProducto);
            db.AddInParameter(dbCommand, "@FechaIni", DbType.String, FechaInicio);
            db.AddInParameter(dbCommand, "@FechaFin", DbType.String, FechaFin);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            //Si retorna mas de 2000 registros tira error de aplicacion al deserealizar el json
            if (dt.Rows.Count <= 2000)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    var row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
            }
            else
            {
                var row = new Dictionary<string, object>();
                row.Add("CodRuteo", "Error");
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetDatosUsuario(String CodUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.USR_getUsuario";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, CodUsuario);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetDatosRuteo(String CodRuteo)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.RUT_GetDetalleRuteo";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodRuteo", DbType.String, CodRuteo);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        protected void btnExcelear_Click(object sender, EventArgs e)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            try
            {
                Style s = new Style();
                s.BackColor = System.Drawing.Color.Black;
                s.ForeColor = System.Drawing.Color.White;
                Style s2 = new Style();
                s2.BackColor = System.Drawing.Color.Gray;
                s2.ForeColor = System.Drawing.Color.White;

                String actualTime = DateTime.Now.ToString("ddMMyyyy_HHmmss");
                string tiempo = actualTime;
                
                string sqlCommand = "dbo.RUT_GetListRuteosExcel";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
                db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, hddCodPVRuteo.Value.ToString());
                db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, hddCodSucRuteo.Value.ToString());
                db.AddInParameter(dbCommand, "@CodArticulo", DbType.String, hddCodArticuloRuteo.Value.ToString());
                db.AddInParameter(dbCommand, "@Repositor", DbType.String, hddRepositorRuteo.Value.ToString());
                db.AddInParameter(dbCommand, "@FechaIni", DbType.String, hddFechaIniRuteo.Value.ToString());
                db.AddInParameter(dbCommand, "@FechaFin", DbType.String, hddFechaFinRuteo.Value.ToString());

                DataTable DtExcel = null;
                DtExcel = db.ExecuteDataSet(dbCommand).Tables[0];
                if (DtExcel == null || DtExcel.Columns.Count == 0)
                    throw new Exception("ExportToExcel: Nula o vacia la tabla.");
                DataTable dtExportar = DtExcel;
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page pagina = new Page();
                HtmlForm form = new HtmlForm();
                GridView dg = new GridView();
                dg.EnableViewState = false;
                dg.DataSource = dtExportar;
                dg.DataBind();
                dg.HeaderRow.ApplyStyle(s);

                pagina.EnableEventValidation = false;
                pagina.DesignerInitialize();
                pagina.Controls.Add(form);
                form.Controls.Add(dg);
                pagina.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;

                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=Listado-Relevamientos-" + tiempo + ".xls");
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Response.Write(sb.ToString());
                Response.End();
            }
            catch (Exception exp)
            {
                Response.Cookies.Remove("termino");
                Response.AppendCookie(new HttpCookie("termino", "termino"));
                string echo = exp.ToString();
            }

            /*try
            {
                DataTable dt = null;
                Database db = DatabaseFactory.CreateDatabase("ServerConnection");
                string sqlCommand = "dbo.RUT_GetListRuteosExcel";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
                db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, hddCodPVRuteo.Value.ToString());
                db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, hddCodSucRuteo.Value.ToString());
                db.AddInParameter(dbCommand, "@CodArticulo", DbType.String, hddCodArticuloRuteo.Value.ToString());
                db.AddInParameter(dbCommand, "@Repositor", DbType.String, hddRepositorRuteo.Value.ToString());
                db.AddInParameter(dbCommand, "@FechaIni", DbType.String, hddFechaIniRuteo.Value.ToString());
                db.AddInParameter(dbCommand, "@FechaFin", DbType.String, hddFechaFinRuteo.Value.ToString());
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                DataTable dtExportar = dt;
                string fechaHora = DateTime.Now.ToString("dd-MM-yyyy");
                string filename = "Listado-Relevamientos-" + fechaHora + ".xls";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();
                HSSFWorkbook workbook = new HSSFWorkbook();
                var style = workbook.CreateCellStyle();
                style.FillForegroundColor = HSSFColor.GREY_25_PERCENT.index;
                style.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuente = workbook.CreateFont();
                fuente.Color = HSSFColor.RED.index;
                fuente.FontName = "Calibri";
                fuente.FontHeightInPoints = 12;
                style.SetFont(fuente);
                var styleHeader = workbook.CreateCellStyle();
                styleHeader.FillForegroundColor = HSSFColor.GREY_80_PERCENT.index;
                styleHeader.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuenteHead = workbook.CreateFont();
                fuenteHead.Color = HSSFColor.WHITE.index;
                fuenteHead.FontName = "Calibri";
                fuenteHead.FontHeightInPoints = 12;
                styleHeader.SetFont(fuenteHead);
                var styleSpace = workbook.CreateCellStyle();
                styleSpace.FillForegroundColor = HSSFColor.WHITE.index;
                styleSpace.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleSpace.SetFont(fuenteHead);
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Listado de Ruteos");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);
                string rowTitle;
                for (int i = 0; i < dtExportar.Columns.Count; i++)
                {
                    rowTitle = "";
                    rowTitle = this.CapitalizeText(dtExportar.Columns[i].ColumnName);
                    filaEncabezado.CreateCell(i).SetCellValue(rowTitle);
                    filaEncabezado.GetCell(i).CellStyle = styleHeader;
                }
                for (int i = 0; i < dtExportar.Rows.Count; i++)
                {
                    Row filaRegistro = hoja1.CreateRow(i + 3);
                    for (int j = 0; j < dtExportar.Columns.Count; j++)
                    {
                        filaRegistro.CreateCell(j).SetCellValue(dtExportar.Rows[i][j].ToString());
                        hoja1.AutoSizeColumn(j);
                    }
                }
                MemoryStream file = new MemoryStream();
                workbook.Write(file);
                Response.BinaryWrite(file.GetBuffer());
                Response.Cookies.Remove("termino");
                Response.AppendCookie(new HttpCookie("termino", "termino"));
                Response.End();
            }
            catch (Exception exp)
            {
            }*/
        }

        public string CapitalizeText(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }
    }
}