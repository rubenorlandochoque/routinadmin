﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Buchon.Pantallas.Login.Login" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Routing Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/pace/pace-theme-flash.css")%>" rel="stylesheet" media="screen" />
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/boostrapv3/css/bootstrap.min.css")%>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/boostrapv3/css/bootstrap-theme.min.css")%>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/font-awesome/css/font-awesome.css")%>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Css/animate.min.css")%>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Css/style.css")%>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Css/responsive.css")%>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Css/custom-icon-set.css")%>" rel="stylesheet" />
</head>
<body class="error-body no-top lazy" data-original="<%= ResolveClientUrl("~/Recursos/Plantilla/Images/fondoDefault.png")%>" style="background-image: url('<%= ResolveClientUrl("~/Recursos/Plantilla/Images/fondoDefault.png")%>'); height: 100%; width: 100%">
    <div class="container">
        <div class="row login-container animated fadeInUp">
            <div class="col-md-7 col-md-offset-2 tiles white no-padding">
                <div class="p-t-30 p-l-40 p-b-20 xs-p-t-10 xs-p-l-10 xs-p-b-10" style="text-align: center">
                    <%--<img src="<%= ResolveClientUrl("~/Recursos/Plantilla/Images/LogoBuchonv5-color02.jpg")%>" alt="Buch.On" style="width: 100%; height: 60%" />--%>
                    <h2><b>ROUTING ADMIN</b></h2>
                </div>
                <div class="tiles grey p-t-20 p-b-20 text-black">
                    <form id="frm_login" class="animated fadeIn" runat="server">
                        <div class="row form-row m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                            <div class="col-md-6 col-sm-6 ">
                                <asp:TextBox type="text" ID="userid" class="form-control" placeholder="Usuario" runat="server" Text=""></asp:TextBox>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <asp:TextBox type="password" ID="password" CssClass="form-control" placeholder="Contraseña" runat="server" Text=""></asp:TextBox>
                            </div>
                        </div>
                        <div class="row p-t-10 m-l-20 m-r-20 xs-m-l-10 xs-m-r-10">
                            <div class="control-group  col-md-10">
                                <asp:Label ID="error" ForeColor="red" runat="server" Text=""></asp:Label><br />
                                <asp:Button ID="ingresar" runat="server" Text="Iniciar Sesión" CssClass="btn btn-success btn-cons" OnClick="ingresar_Click" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-1.8.3.min.js")%>" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/boostrapv3/js/bootstrap.min.js")%>" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/pace/pace.min.js")%>" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-validation/js/jquery.validate.min.js")%>" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-lazyload/jquery.lazyload.min.js")%>" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Js/login_v2.js")%>" type="text/javascript"></script>
</body>
</html>
