﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace AppAndroid.Master
{
    public class Usuario
    {
    #region Fields and Properties

        private int _IdUsuario;
        public int IdUsuario
        {
            get
            {
                return (int)_IdUsuario;
            }
            set
            {
                _IdUsuario = value;
            }
        }

        private string _CodUsuario;
        public string CodUsuario
        {
            get
            {
                return (string)_CodUsuario;
            }
            set
            {
                _CodUsuario = value;
            }
        }

        private string _CodDatoPersonalUsuario;
        public string CodDatoPersonalUsuario
        {
            get
            {
                return (string)_CodDatoPersonalUsuario;
            }
            set
            {
                _CodDatoPersonalUsuario = value;
            }
        }

        private string _CodTipoUsuario;
        public string CodTipoUsuario
        {
            get
            {
                return (string)_CodTipoUsuario;
            }
            set
            {
                _CodTipoUsuario = value;
            }
        }

        private string _Usuario1;
        public string Usuario1
        {
            get
            {
                return (string)_Usuario1;
            }
            set
            {
                _Usuario1 = value;
            }
        }

        private string _Pass;
        public string Pass
        {
            get
            {
                return (string)_Pass;
            }
            set
            {
                _Pass = value;
            }
        }

        private string _CodSexo;
        public string CodSexo
        {
            get
            {
                return (string)_CodSexo;
            }
            set
            {
                _CodSexo = value;
            }
        }

        private string _Foto;
        public string Foto
        {
            get
            {
                return (string)_Foto;
            }
            set
            {
                _Foto = value;
            }
        }

        private string _PathFoto;
        public string PathFoto
        {
            get
            {
                return (string)_PathFoto;
            }
            set
            {
                _PathFoto = value;
            }
        }

        private DateTime _FechaCreacionUsuario;
        public DateTime FechaCreacionUsuario
        {
            get
            {
                return (DateTime)_FechaCreacionUsuario;
            }
            set
            {
                _FechaCreacionUsuario = value;
            }
        }
        //--- Diego
        private string _CodOrganizacion;
        public string CodOrganizacion
        {
            get
            {
                return (string)_CodOrganizacion;
            }
            set
            {
                _CodOrganizacion = value;
            }
        }
        //---
        private bool _Activo;
        public bool Activo
        {
            get
            {
                return (bool)_Activo;
            }
            set
            {
                _Activo = value;
            }
        }
        #endregion

        public Usuario()
        {

        }

        #region Methods and Functions instanced

        public void Add()
        {
            // Create the Database object, using the default database service. The
            // default database service is determined through configuration.
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");

            try
            {

                string sqlCommand = "permisos.usp_UsuariosAdd";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

                // Add paramters
                // Output parameters specify the size of the return data
                db.AddOutParameter(dbCommand, "@CodUsuario", DbType.String, 50);

                // Input parameters can specify the input value
                db.AddInParameter(dbCommand, "@CodDatoPersonalUsuario", DbType.String, this.CodDatoPersonalUsuario);
                //db.AddInParameter(dbCommand,"@CodTipoUsuario",DbType.String,this.CodTipoUsuario);
                db.AddInParameter(dbCommand, "@Usuario", DbType.String, this.Usuario1);
                db.AddInParameter(dbCommand, "@Pass", DbType.String, this.Pass);
                db.AddInParameter(dbCommand, "@FechaCreacionUsuario", DbType.DateTime, this.FechaCreacionUsuario);
                //db.AddInParameter(dbCommand,"@Activo",DbType.Boolean,this.Activo);
                db.AddInParameter(dbCommand, "@CodOrganizacion", DbType.String, this.CodOrganizacion); //-- Diego


                db.ExecuteNonQuery(dbCommand);

                // Row of data is captured via output parameters
                this.CodUsuario = (db.GetParameterValue(dbCommand, "@CodUsuario").ToString());
            }
            catch (Exception ex)
            {
            }
        }

        public void Modify()
        {
            // Create the Database object, using the default database service. The
            // default database service is determined through configuration.
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");

            try
            {

                string sqlCommand = "permisos.usp_UsuariosModify";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

                // Add paramters
                // Input parameters can specify the input value
                db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, this.CodUsuario);
                db.AddInParameter(dbCommand, "@CodDatoPersonalUsuario", DbType.String, this.CodDatoPersonalUsuario);
                db.AddInParameter(dbCommand, "@CodTipoUsuario", DbType.String, this.CodTipoUsuario);
                db.AddInParameter(dbCommand, "@Usuario", DbType.String, this.Usuario1);
                db.AddInParameter(dbCommand, "@Pass", DbType.String, this.Pass);
                db.AddInParameter(dbCommand, "@FechaCreacionUsuario", DbType.DateTime, this.FechaCreacionUsuario);
                db.AddInParameter(dbCommand, "@Activo", DbType.Boolean, this.Activo);

                db.ExecuteNonQuery(dbCommand);

            }
            catch (Exception ex)
            {

            }
        }

        public void Get(string Cod)
        {
            DataRow dr = GetByCod(Cod);

            if (dr != null)
            {

                try { this.IdUsuario = Convert.ToInt32(dr["IdUsuario"]); }
                catch { this.IdUsuario = -1; }
                try { this.CodUsuario = dr["CodUsuario"].ToString(); }
                catch { this.CodUsuario = ""; }
                try { this.CodDatoPersonalUsuario = dr["CodDatoPersonalUsuario"].ToString(); }
                catch { this.CodDatoPersonalUsuario = ""; }
                try { this.CodTipoUsuario = dr["CodTipoUsuario"].ToString(); }
                catch { this.CodTipoUsuario = ""; }
                try { this.CodSexo = dr["CodSexo"].ToString(); }
                catch { this.CodSexo = ""; }
                try { this.PathFoto = dr["PathFoto"].ToString(); }
                catch { this.PathFoto = ""; }
                try { this.Foto = dr["Foto"].ToString(); }
                catch { this.Foto = ""; }
                try { this.Usuario1 = dr["Usuario"].ToString(); }
                catch { this.Usuario1 = ""; }
                try { this.Pass = dr["Pass"].ToString(); }
                catch { this.Pass = ""; }
                try { this.FechaCreacionUsuario = Convert.ToDateTime(dr["FechaCreacionUsuario"]); }
                catch { this.FechaCreacionUsuario = DateTime.Now; }
                //-- Diego
                try { this.CodOrganizacion = dr["CodOrganizacion"].ToString(); }
                catch { this.CodOrganizacion = ""; }
                try { this.Activo = Convert.ToBoolean(dr["Activo"]); }
                catch { this.Activo = false; }


            }
            else
            {
                this.IdUsuario = -1;
                this.CodUsuario = "";
                this.CodDatoPersonalUsuario = "";
                this.CodTipoUsuario = "";
                this.Usuario1 = "";
                this.Pass = "";
                this.FechaCreacionUsuario = DateTime.Now;
                this.CodOrganizacion = "";
                this.Activo = false;
            }
        }

        #endregion

        #region Methods and Functions not instanced

        public static DataTable GetAll()
        {
            // Create the Database object, using the default database service. The
            // default database service is determined through configuration.
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");

            string stSqlCommand = "permisos.usp_UsuariosGetAll";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);


            DataTable dt = null;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];

            return dt;
        }

		public static string GetPermisos(String codUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");

            string stSqlCommand = "dbo.SEG_GetPermisosPantallas";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, codUsuario);

            DataTable dt = null;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            string permisos = "|";
            foreach (DataRow dr in dt.Rows) {
                permisos += dr["CodPantalla"] + "|";
            }
            return permisos;
        }
        public static void Delete(string Cod)
        {
            // Create the Database object, using the default database service. The
            // default database service is determined through configuration.
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");

            string stSqlCommand = "permisos.usp_UsuariosDelete";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);


            // Add paramters
            // Input parameters can specify the input value
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, Cod);

            db.ExecuteNonQuery(dbCommand);

        }


        private static DataRow GetByCod(string Cod)
        {
            // Create the Database object, using the default database service. The
            // default database service is determined through configuration.
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");

            string stSqlCommand = "permisos.usp_UsuariosGetByCod";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            // Retrieve user from the specified userName.
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, Cod);

            // DataRow that will hold the returned results		
            DataRow dr = null;

            DataTable dt = null;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];

            if (dt.Rows.Count > 0)
                dr = dt.Rows[0];
            // Note: connection was closed by ExecuteDataSet method call 

            return dr;
        }

        public static DataRow GetDatosUsuario(string user, string pwd)
        {
            // Create the Database object, using the default database service. The
            // default database service is determined through configuration.
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");

            string stSqlCommand = "USR_validaUsuario";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            // Retrieve user from the specified userName.
            db.AddInParameter(dbCommand, "@usuario", DbType.String, user);
            db.AddInParameter(dbCommand, "@pass", DbType.String, pwd);

            // DataRow that will hold the returned results		
            DataRow dr = null;

            DataTable dt = null;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];

            if (dt.Rows.Count > 0)
                dr = dt.Rows[0];
            // Note: connection was closed by ExecuteDataSet method call 

            return dr;
        }

        

        #endregion

    }
}