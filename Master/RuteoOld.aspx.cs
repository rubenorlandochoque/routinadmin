﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.Record.Formula.Eval;
using NPOI.HSSF.Record.Formula.Functions;
using Master;

namespace AppAndroid.Master
{
    public partial class Ruteo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string codPantalla = "P_RUTEO";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
        }

        [WebMethod]
        public static string GetAllRepositores()
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.REP_getAllRepositoresByUser";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, Sesion.getCodUsuario());

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetAllPuntosVentas()
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.PV_getAllPuntosVenta";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetDetalleGeneralRuteo(string CodRepositor, string CodPuntoVenta, string FechaInicio, string FechaFin)
        {
            FechaInicio = FechaInicio.Replace("/", "-");
            FechaFin = FechaFin.Replace("/", "-");

            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.RUT_GetDetalleGral";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, CodRepositor);
            db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand, "@fechaInicio", DbType.String, FechaInicio);
            db.AddInParameter(dbCommand, "@fechaFin", DbType.String, FechaFin);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetRuteosDias(string CodRepositor, string CodPuntoVenta, string FechaInicio, string FechaFin)
        {
            FechaInicio = FechaInicio.Replace("/", "-");
            FechaFin = FechaFin.Replace("/", "-");

            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.RUT_GetDias";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, CodRepositor);
            db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand, "@FechaInicio", DbType.String, FechaInicio);
            db.AddInParameter(dbCommand, "@FechaFin", DbType.String, FechaFin);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetPuntosVentaRuteo(string CodRepositor, string Fecha)
        {
            //Fecha = Fecha.Replace("/", "-");

            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.RUT_GetPuntosVentas";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            db.AddInParameter(dbCommand, "@codRepositor", DbType.String, CodRepositor);
            db.AddInParameter(dbCommand, "@fecha", DbType.String, Fecha);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetListadoRuteos(string Fecha, string CodPuntoVenta, String CodRepositor) 
        {
            Fecha = Fecha.Replace("/", "-");
            
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.RUT_ListRuteos";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            db.AddInParameter(dbCommand, "@codPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand, "@codRepositor", DbType.String, CodRepositor);
            db.AddInParameter(dbCommand, "@fecha", DbType.String, Fecha);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetDatosUsuario(String CodUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.USR_getUsuario";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, CodUsuario);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetDatosRuteo(String CodRuteo)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.RUT_GetDetalleRuteo";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodRuteo", DbType.String, CodRuteo);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }
    }
}