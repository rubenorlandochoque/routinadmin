﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Asignaciones.aspx.cs" Inherits="AppAndroid.Master.Asignaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="<%= ResolveClientUrl("~/JS/Asignaciones.js")%>"></script>





    <script type="text/javascript">
        var table, usuarios;


        function clearFrmPersona() {
            //Reseteo formulario de datos personales
            $("#cntFrmPersona input").not("[type=submit], [type=button]").val("");
            $("#cntFrmPersona select").val("COD_SELECCIONE_UNA_OPCION").change();
            $("[id$=imagenmostrar]").attr("src", "../Images/silueta.png");

            //Reseteo datos del Usuario
            $("[id$=username]").prop("readonly", false).attr("value", "");
            $("[id$=cntUsuarios] input").not("[type=submit], [type=button]").val("");


            //Receteo Perfiles
            $("#cntPerfiles input").not("[type=submit], [type=button]").val("");

            // var n = $("[id$=userPerfil] option").size();
            $("[id$=userPerfil]").html("");
            $("[id$=allPerfiles]").html($("[id$=perfileshide]").html());
            /*
            for (var i = 0; i < n; i++) {
                $("[id$=userPerfil] option").eq(0).removeAttr("CodPerfilUsuario");
                var opt = $("[id$=userPerfil] option").eq(0).detach();
                $("[id$=allPerfiles]").prepend(opt);
            }
            */
            //limpiar titulo del grid
            $(".grid-title h4 span").text("");
        }

        function expandir(op) {
            $('.grid .tools .collapse').click();
            $("[id$=username]").prop("readonly", true);
            switch (op) {
                case 'datos':
                    //$('.grid .tools .collapse').click();
                    $('[id$=cntFrmPersona] .grid .tools .expand').click();
                    $("[id$=btnGuardarTodo]").css("display", "none");
                    $("[id$=GuardarDatosPersonales], [id$=btnGuardar]").css("display", "block");
                    break;
                case 'usuario':
                    //$('.grid .tools .collapse').click();
                    $('[id$=cntUsuarios] .grid .tools .expand').click();
                    $("[id$=btnGuardarTodo]").css("display", "none");
                    $("[id$=GuardarDatosPersonales], [id$=btnGuardar]").css("display", "block");
                    break;
                case 'perfil':
                    $('[id$=cntPerfiles] .grid .tools .expand').click();
                    $("[id$=btnGuardarTodo]").css("display", "none");
                    $("[id$=GuardarDatosPersonales], [id$=btnGuardar]").css("display", "block");
                    break;
                default:
                    //$('[id$=cntPersonas] .grid .tools .collapse').click();
                    $('[id$=cntFrmPersona] .grid .tools .expand').click();
                    $('[id$=cntUsuarios] .grid .tools .expand').click();
                    $('[id$=cntPerfiles] .grid .tools .expand').click();
                    $("[id$=username]").prop("readonly", false);
                    $("[id$=GuardarDatosPersonales], [id$=btnGuardar]").css("display", "none");


            }


        }

        $(document).ready(function () {
            $("body").on("click", "[id=btnNewPersona]", function () {
                clearFrmPersona();
                expandir();
                $("[id$=GuardarDatosPersonales], [id$=btnGuardar]").css("display", "none");
                $("[id$=btnGuardarTodo]").css("display", "block");
                $("[id$=apellidoPersona]").focus();
            });
        });

    </script>
    <style type="text/css">
        table.dataTable {
            border-collapse: collapse !important;
            width: 100% !important;
        }

        .filaSeleccionada {
            background-color: #e2e8eb !important;
        }

        th {
            background-color: #FFF !important;
        }

        .dataTables_wrapper .boton {
            float: right;
            padding-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <asp:HiddenField runat="server" ID="hddIdPuntoVenta" ClientIDMode="Static" />
    <section class="content-header">
        <h1>
            <i class="fa fa-external-link-square"></i> Asignación de Repositores a Puntos de Venta
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="#">Menu</a></li>
            <li class="active">Asignaciones</li>
        </ol>
    </section>
    <br />

    <div class="box">

        <div class="box-body">

            <div class="row">
                <div class="col-md-6">
                    <label>Búsqueda de Repositor</label>
                    <input type="text" class="form-control" id="textRepositorSearch">
                </div>
                <div class="col-md-6">
                    <label>Búsqueda de Punto de Venta</label>
                    <input type="text" class="form-control" id="textPVSearch" onkeyup="cargarListadoPV()">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-gray">
                            <!-- /.widget-user-image -->
                            <h4 style="margin-left: 0px; height: 28px; margin-bottom: 0px; margin-top: 0px; border-top-width: 0px;" class="widget-user-username">Repositores </h4>Encontrados: <label id="lblRepEncontrados"></label>
                        </div>
                        <div class="box-footer no-padding">
                            <div class="row" style="height: 500px">
                                <div class="col-md-12" style="height: 440px">
                                    <%--<div id="contentListadoRepositores" style="height: 440px; max-height: 440px !important; padding-right: 17px; padding-bottom: 10px" class="scrollbar-hidden">--%>
                                        <div class="scrollbar-inner" style="max-height: 490px; height: 490px; overflow: scroll; overflow-X: hidden;">
                                            <ul class="tableselsimple nav nav-stacked" id="listadoRepositores">
                                        </ul>
                                        </div>

                                    <%--</div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-user -->
                </div>
                <div class="col-md-6">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-gray">
                            <!-- /.widget-user-image -->
                            <h4 style="margin-left: 0px; height: 28px; margin-bottom: 0px; margin-top: 0px; border-top-width: 0px;" class="widget-user-username">Puntos de Venta <span style="margin-top: 3px; font-size: 20px; margin-right: 13px;" class="pull-right badge bg-blue" id="lblCantPV">0</span></h4> <label id="lblPVEncontrados"></label>
                        </div>
                        <div class="box-footer no-padding">
                            <div class="row" style="height: 500px">
                                <div class="col-md-12" style="height: 460px">
                                    <%--<div id="contentListadoPuntosVenta" style="height: 460px; max-height: 460px !important; padding-right: 17px; padding-bottom: 10px" class="scrollbar-hidden">--%>
                                        <div class="scrollbar-inner" style="max-height: 490px; height: 490px; overflow: scroll; overflow-x: hidden;">
                                            <ul class="table nav nav-stacked" id="listadoPV">
                                        </ul>
                                        </div>

                                    <%--</div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-user -->
                </div>
            </div>
            <br />
            <div class="box-footer">
                <button class="btn btn-primary pull-right" type="button" onclick="asignarRepositoresPV()">Asignar</button>
            </div>
        </div>
    </div>

    <!-- /.box -->

</asp:Content>
