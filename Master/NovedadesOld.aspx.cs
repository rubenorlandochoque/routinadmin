﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.Record.Formula.Eval;
using NPOI.HSSF.Record.Formula.Functions;
using Master;

namespace AppAndroid.Master
{
    public partial class Novedades : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string codPantalla = "P_NOVEDADES";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
        }

        [WebMethod]
        public static string GetAllRepositores()
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.REP_getAllRepositoresByUser";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, Sesion.getCodUsuario());

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetAllPuntosVentas()
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.PV_getAllPuntosVenta";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetDetalleGeneralNovedades(string CodUsuario, string CodPuntoVenta, string FechaInicio, string FechaFin)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.NOV_GetDetalleGral");
            db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, CodUsuario);
            db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand, "@fechaInicio", DbType.String, FechaInicio);
            db.AddInParameter(dbCommand, "@fechaFin", DbType.String, FechaFin);

            DbCommand dbCommand2 = db.GetStoredProcCommand("dbo.NOV_GetDetalleGralTiposNovedades");
            db.AddInParameter(dbCommand2, "@CodRepositor", DbType.String, CodUsuario);
            db.AddInParameter(dbCommand2, "@CodPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand2, "@fechaInicio", DbType.String, FechaInicio);
            db.AddInParameter(dbCommand2, "@fechaFin", DbType.String, FechaFin);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }

            dt = db.ExecuteDataSet(dbCommand2).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetNovedadesByDias(string CodUsuario, string CodPuntoVenta, string FechaInicio, string FechaFin)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.NOV_GetDias";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, CodUsuario);
            db.AddInParameter(dbCommand, "@codPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand, "@FechaInicio", DbType.String, FechaInicio);
            db.AddInParameter(dbCommand, "@FechaFin", DbType.String, FechaFin);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetPuntosVentaNovedad(string CodRepositor, string Fecha)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.NOV_GetPuntosVentas";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            db.AddInParameter(dbCommand, "@codRepositor", DbType.String, CodRepositor);
            db.AddInParameter(dbCommand, "@fecha", DbType.String, Fecha);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        /*
        [WebMethod]
        public static string GetPuntosVentaNovedad(string CodRepositor, string CodPuntoVenta, string Fecha)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.NOV_GetPuntosVentas";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            db.AddInParameter(dbCommand, "@codRepositor", DbType.String, CodRepositor);
            db.AddInParameter(dbCommand, "@codPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand, "@fecha", DbType.String, Fecha);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }*/


        [WebMethod]
        public static string GetTiposNovedadPV(string CodRepositor, string CodPuntoVenta, string Fecha)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.NOV_GetTiposNovedades";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, CodRepositor);
            db.AddInParameter(dbCommand, "@fecha", DbType.String, Fecha);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetListadoNovedades(string CodRepositor, string CodPuntoVenta, string Fecha, string CodTipoNovedad)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.NOV_ListNovedades";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, CodRepositor);
            db.AddInParameter(dbCommand, "@CodTipoNovedad", DbType.String, CodTipoNovedad);
            db.AddInParameter(dbCommand, "@fecha", DbType.String, Fecha);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetDatosUsuario(String CodUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.USR_getUsuario";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, CodUsuario);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetDatosNovedad(String CodNovedad)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.RUT_GetDetalleNovedad";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@codNovedad", DbType.String, CodNovedad);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }
    }
}