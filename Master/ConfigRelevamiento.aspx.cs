﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System.Web.UI.HtmlControls;

namespace AppAndroid.Master
{
    public partial class ConfigRelevamiento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string codPantalla = "P_RELEVAMIENTOS";
                string permisos = Sesion.getPermisos();
                if (permisos.IndexOf(codPantalla) == -1)
                {
                    Response.Redirect("~/Master/SinPermisos.aspx");
                }
            }
        }
    }
}