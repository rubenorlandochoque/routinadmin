﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Opciones.aspx.cs" Inherits="AppAndroid.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="<%= ResolveClientUrl("~/JS/Varios.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/Opciones.js")%>"></script>

<style>
    h3 {
        font-size: 18pt !important;
    }
</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6"  pant="O_P_PRODUCTOS" style="display: none;">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <a href="Productos.aspx" class="small-box-footer">
                    <div class="inner">
                        <h3>Productos</h3>
                        <div class="">
                            <i class="fa fa-3x fa-shopping-cart"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <strong> <ins>Propios </ins>:</strong> <span id="lblProdPropios"></span> | <strong> <ins>Competencia </ins>:</strong> <span id="lblProdCompetencia"></span>
                </a>
            </div>
        </div>
         <!-- ./col -->
        <div class="col-lg-3 col-xs-6" pant="O_P_TIPOPRODUCTO" style="display: none;">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <a href="TiposProductos.aspx" class="small-box-footer">
                    <div class="inner">
                        <h3>Tipos de Productos</h3>
                        <div class="">
                            <i class="fa fa-3x fa-th-large"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <strong> <ins>Tipos de Productos </ins>:</strong> <span id="lblTipoProducto"></span>
                </a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" pant="O_P_REPOSITORES" style="display: none;">
            <!-- small box -->
            <div class="small-box bg-green">
                <a href="Usuarios.aspx" class="small-box-footer">
                    <div class="inner">
                        <h3>Repositores</h3>
                        <div class="">
                            <i class="fa fa-3x fa-users"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <strong> <ins>Activos</ins>:</strong> <span id="lblCantRep"></span> | <strong> <ins>Sin sincronizar</ins>:</strong> <span id="lblCantRepNoSync"></span>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" pant="O_P_EMPRESAS" style="display: none;">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <a href="Empresas.aspx" class="small-box-footer">
                    <div class="inner">
                        <h3>Empresa</h3>
                        <div class="">
                            <i class="fa fa-3x fa-building "></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <strong> <ins>Empresas </ins>:</strong> <span id="lblEmpresas"></span>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" pant="O_P_PUNTOS_VENTAS" style="display: none;">
            <!-- small box -->
            <div class="small-box bg-red">
                <a href="PV.aspx" class="small-box-footer">
                    <div class="inner">
                        <h3>Puntos de Venta</h3>
                        <div class="">
                            <i class="fa fa-3x fa-truck"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <strong> <ins>Propios </ins>:</strong> <span id="lblPVPropios"></span> | <strong> <ins>Competencia </ins>:</strong> <span id="lblPVCompetencia"></span>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" pant="O_P_RUTEO" style="display: none;">
            <!-- small box -->
            <div class="small-box bg-maroon">
                <a href="Relevamiento.aspx" class="small-box-footer">
                    <div class="inner">
                        <h3>Relevamiento</h3>
                        <div class="">
                            <i class="fa fa-3x fa-location-arrow"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <strong> <ins>Relevamientos del día</ins>:</strong> <span id="lblCantRuteosHoy"></span>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6" pant="P_RELEVAMIENTOS" style="display: none;">
            <!-- small box -->
            <div class="small-box bg-maroon">
                <a href="ConfigRelevamiento.aspx" class="small-box-footer">
                    <div class="inner">
                        <h3>Config. Relevamiento</h3>
                        <div class="">
                            <i class="fa fa-3x fa-cog"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" pant="O_P_NOVEDADES" style="display: none;">
            <!-- small box -->
            <div class="small-box bg-orange">
                <a href="Novedades.aspx" class="small-box-footer">
                    <div class="inner">
                        <h3>Novedades</h3>
                        <div class="">
                            <i class="fa fa-3x fa-newspaper-o"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <strong> <ins>Novedades del día</ins>:</strong> <span id="lblCantNovHoy"></span>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" pant="O_P_TRAKING" style="display: none;">
            <!-- small box -->
            <div class="small-box bg-blue-gradient">
                <a href="Traking.aspx" class="small-box-footer">
                    <div class="inner">
                        <h3>Tracking</h3>
                        <div class="">
                            <i class="fa fa-3x fa-map-marker"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>

                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" pant="O_P_CONFIGURACION"  style="display: none;">
            <!-- small box -->
            <div class="small-box bg-light-blue">
                <a href="Asignaciones.aspx" class="small-box-footer">
                    <div class="inner">
                        <h3>Asignaciones</h3>
                        <div class="">
                            <i class="fa fa-3x fa-external-link-square"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>

                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6" pant="O_P_PUNTOS_VENTAS"  style="display: none;">
            <div class="small-box bg-red">
                <a href="Configuracion.aspx" class="small-box-footer"">
                    <div class="inner">
                        <h3>Configuración</h3>
                        <div class="">
                            <i class="fa fa-3x fa-cogs"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6" pant="P_EXCELCOMPARATIVO"  style="display: none;">
            <asp:Button ID="btnExcel" Text="text" OnClick="BtnExcelear_Click" runat="server" ClientIDMode="Static" Style="display: none" />
            <div class="small-box bg-red">
                <a href="#" onclick="excelDetalles()" class="small-box-footer"">
                    <div class="inner">
                        <h3>Excel Comparativo</h3>
                        <div class="">
                            <i class="fa fa-3x fa-file-excel-o"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    Descargar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>

        <%--<div class="col-lg-3 col-xs-6" pant="O_P_PUNTOS_VENTAS" style="display: none;">
            <div class="small-box bg-red">
                <a href="#" class="small-box-footer" onclick="sincronizar()">
                    <div class="inner">
                        <h3>Sincronizar</h3>
                        <div class="">
                            <i class="fa fa-3x fa-bolt"></i>
                        </div>
                        <p></p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    Últ. Sincronización: 12/10/2015
                </a>
            </div>
        </div>--%>


    </div>
    <!-- /.row -->


    <!-- SYNC windows -->
    <div id="winSync" class="modal fade" role="dialog">
        <div class="modal-dialog">

        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Sincronización</h4>
                </div>
                <div class="modal-body">
                    <p>¿Desea Iniciar la sincronización?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="onSync()">Sincronizar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="MenuMasterContent3" runat="server">
    <footer class="main-footer" style="display: none; height: 50px;" id="footerButton" pant="P_PUNTOS_VENTAS_" runat="server">
        <div class="row">
            <div class="col-xs-3">
                <button class="btn btn-block btn-danger" style="margin-top: -8px;" onclick="sincronizar()" type="button"><i class="fa fa-bolt"></i> Sincronizar</button></div>
            <div class="col-xs-9"><strong>Última fecha de sincronización:</strong>
                <label id="lblFchModif"></label>
            </div>
        </div>
    </footer>
</asp:Content>