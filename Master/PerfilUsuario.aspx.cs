﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Master;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace AppAndroid.Master
{
    public partial class PerfilUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			string codPantalla = "P_MI_CUENTA";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
            if (!Page.IsPostBack)
            {
                imagenmostrar.Attributes["onerror"] = "this.src='" + ResolveClientUrl("~/Recursos/Plantilla/Images/no_image.png") + "'";
                imagenmostrar.Attributes["src"] = ResolveClientUrl("~/Recursos/Plantilla/Images/no_image.png");
                //CargarTabla();
            }
        }

        [WebMethod]
        public static string GuardarUsuario(string _codUsuario, string _usuario, string _passOld, string _pass, string _legajo, string _apellido, string _nombres, string _imagen, string _dataFoto)
        {
            try
            {
                var _resultado = "";
                string stSqlCommand = "";
                Database db = DatabaseFactory.CreateDatabase("ServerConnection");

                if (_codUsuario == "")
                {
                    stSqlCommand = "dbo.REP_AddRepositor";
                }
                else
                {
                    stSqlCommand = "dbo.USR_EditPerfil";
                }
                DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

                db.AddInParameter(dbCommand, "@codUsuario", DbType.String, _codUsuario);
                //if (_codUsuario != "")
                //{

                //}
                //else
                //{
                //    db.AddOutParameter(dbCommand, "@codUsuario", DbType.String, 512);
                //}

                db.AddInParameter(dbCommand, "@usuario", DbType.String, _usuario);
                db.AddInParameter(dbCommand, "@passOld", DbType.String, _passOld);
                db.AddInParameter(dbCommand, "@passNew", DbType.String, _pass);
                //db.AddInParameter(dbCommand, "@legajo", DbType.String, _legajo);
                db.AddInParameter(dbCommand, "@apellido", DbType.String, _apellido);
                db.AddInParameter(dbCommand, "@nombres", DbType.String, _nombres);
                db.AddOutParameter(dbCommand, "@result", DbType.String, 512);
                if ((_dataFoto != "")) //si hay foto a subir envio nombre sino envio vacio. Para el caso de nuevo envio cualquier nombre y en stored comprueba != de vacio y actualiza el nombre con el codrepositor
                {
                    db.AddInParameter(dbCommand, "@fotos", DbType.String, _codUsuario + ".jpg");
                }
                else
                {
                    db.AddInParameter(dbCommand, "@fotos", DbType.String, "");
                }
                db.ExecuteNonQuery(dbCommand);
                _resultado = db.GetParameterValue(dbCommand, "@result").ToString();
                /*string retorno = "";
                if (_codUsuario == "")
                {
                    
                    if (_resultado.Equals("-1"))
                    {
                        return "[ERROR] : Se produjo un error en la insercion en fusion.";
                    }
                    else
                    {
                        //return "OK";
                    }
                }*/


                if (!_resultado.Equals("-1"))
                {
                    if (_dataFoto != "")
                    {
                        if (_codUsuario != "")  //Si edit el codigo usuario viene como
                        {
                            Proced.GuardarFotoPersona(_dataFoto, _codUsuario);
                        }
                        else
                        {
                            Proced.GuardarFotoPersona(_dataFoto, _resultado);
                        }
                    }
                }
                return _resultado;
            }
            catch (Exception ex)
            {
                return "[ERROR] : " + ex.Message;
            }
        }

        [WebMethod]
        public static string GetUsuarioByCod(string _codUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.USR_getUsuario";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, _codUsuario);

            DataTable dt = null;
            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string getTiposUsuarios()
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.USR_getTiposUsuarios";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            DataTable dt = null;
            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }
    }
}