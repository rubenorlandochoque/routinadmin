﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Master;
using Microsoft.Practices.EnterpriseLibrary.Data;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;

namespace AppAndroid.Master
{
    public partial class Repositores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                imagenmostrar.Attributes["onerror"] = "this.src='" + ResolveClientUrl("~/Recursos/Plantilla/Images/no_image.png") + "'";
                imagenmostrar.Attributes["src"] = ResolveClientUrl("~/Recursos/Plantilla/Images/no_image.png");
                //CargarTabla();
            }
        }

        public void CargarTabla()
        {
            DataTable dt = GetRepositores();
            //rptRowsPersona.DataSource = dt;
            //rptRowsPersona.DataBind();
        }

        public static DataTable GetRepositores()
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            //string sqlCommand = "permisos.usp_GetUsuariosPorServidor";
            string sqlCommand = "sp_repositores";

            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Input parameters can specify the input value            
            db.AddInParameter(dbCommand, "@Comando", DbType.String, "SelectAll");

            DataTable dt = null;
            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;

        }

        public static DataTable GetRepositor(string _codRepositor)
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            //string sqlCommand = "permisos.usp_GetUsuariosPorServidor";
            string sqlCommand = "sp_repositores";

            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Input parameters can specify the input value            
            db.AddInParameter(dbCommand, "@Comando", DbType.String, "SelectByCod");
            db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, _codRepositor);

            DataTable dt = null;
            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;

        }

        protected void verDatosUsuario_OnClick(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string GuardarRepositor(string _codRepositor, string _legajo, string _apellido, string _nombres, string _imagen, string _habilitado, string _dataFoto)
        {
            try
            {
                var _resultado = "";
                Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
                string stSqlCommand = "dbo.sp_repositores";
                DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
                db.AddOutParameter(dbCommand, "@output", DbType.String, 512);
                if (_codRepositor == "")
                {
                    db.AddInParameter(dbCommand, "@Comando", DbType.String, "Insert");
                    string fechaHora = DateTime.Now.ToString("ddmmyyyyddhmmsstt");
                    db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, fechaHora);
                }
                else
                {
                    db.AddInParameter(dbCommand, "@Comando", DbType.String, "Update");
                    db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, _codRepositor);
                }                
                
                db.AddInParameter(dbCommand, "@Legajo", DbType.String, _legajo);
                db.AddInParameter(dbCommand, "@Apellido", DbType.String, _apellido);
                db.AddInParameter(dbCommand, "@Nombres", DbType.String, _nombres);                
                db.AddInParameter(dbCommand, "@Imagen", DbType.String, _imagen);
                db.AddInParameter(dbCommand, "@Habilitado", DbType.String, _habilitado);
                db.ExecuteNonQuery(dbCommand);

                _resultado = db.GetParameterValue(dbCommand, "@output").ToString();

                if (_resultado.Equals("-1"))
                {
                    return "[ERROR] : Se produjo un error en la insercion en fusion.";
                }
                if (_dataFoto != "")
                {
                    Proced.GuardarFotoPersona(_dataFoto, _resultado);
                    dbCommand = db.GetStoredProcCommand(stSqlCommand);
                    db.AddOutParameter(dbCommand, "@output", DbType.String, 512);
                    db.AddInParameter(dbCommand, "@Comando", DbType.String, "UpdateImagen");
                    db.AddInParameter(dbCommand, "@CodRepositor", DbType.String, _resultado);
                    db.AddInParameter(dbCommand, "@Imagen", DbType.String, _resultado + ".jpg");
                    db.ExecuteNonQuery(dbCommand);
                }
                
                return _resultado;
            }
            catch (Exception ex)
            {
                return "[ERROR] : " + ex.Message;
            }
        }

        [WebMethod]
        public static string CargarTablaRepositores()
        {
            DataTable dt = GetRepositores();
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetRepositorByCod(string _codRepositor)
        {
            DataTable dt = GetRepositor(_codRepositor);
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        protected void btnExcelear_Click(object sender, EventArgs e)
        {
            try
            {
                /*string codRelevamiento = Request.QueryString["codRelevamiento"].ToString();
                string codIndicador = Request.QueryString["codIndicadorAlerta"].ToString();
                string codEstado = Request.QueryString["codEstado"].ToString();*/

                DataTable dt = null;
                Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
                string sqlCommand = "dbo.REP_getAllRepositores";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
                // Add paramters
                // Input parameters can specify the input value
                /*db.AddInParameter(dbCommand, "@CodSalida", DbType.String, codRelevamiento);
                db.AddInParameter(dbCommand, "@codIndicador", DbType.String, codIndicador);
                db.AddInParameter(dbCommand, "@codEstado", DbType.String, codEstado);
                db.AddInParameter(dbCommand, "@codUsuario", DbType.String, "");
                db.AddInParameter(dbCommand, "@codFormulario", DbType.String, "");*/
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                DataTable dtExportar = dt;
                /*dtExportar.Columns.Remove(dtExportar.Columns["Indicador"]);
                dtExportar.Columns.Remove(dtExportar.Columns["NumEncuesta"]);
                dtExportar.Columns.Remove(dtExportar.Columns["ocurrencias"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodRelevamiento"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodVivienda"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodTema"]);
                dtExportar.Columns.Remove(dtExportar.Columns["FechaInicioHdd"]);
                dtExportar.Columns.Remove(dtExportar.Columns["FechaFinHdd"]);
                dtExportar.Columns.Remove(dtExportar.Columns["DuracionEncu"]);*/
                string filename = "ListadoRepositores.xls";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();
                /*HSSFWorkbook workbook = new HSSFWorkbook();
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Reporte de Encuestas");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);*/
                HSSFWorkbook workbook = new HSSFWorkbook();
                var style = workbook.CreateCellStyle();
                style.FillForegroundColor = HSSFColor.GREY_25_PERCENT.index;
                style.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuente = workbook.CreateFont();
                fuente.Color = HSSFColor.RED.index;
                fuente.FontName = "Calibri";
                fuente.FontHeightInPoints = 12;
                style.SetFont(fuente);
                var styleHeader = workbook.CreateCellStyle();
                styleHeader.FillForegroundColor = HSSFColor.GREY_80_PERCENT.index;
                styleHeader.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuenteHead = workbook.CreateFont();
                fuenteHead.Color = HSSFColor.WHITE.index;
                fuenteHead.FontName = "Calibri";
                fuenteHead.FontHeightInPoints = 12;
                styleHeader.SetFont(fuenteHead);
                var styleSpace = workbook.CreateCellStyle();
                styleSpace.FillForegroundColor = HSSFColor.WHITE.index;
                styleSpace.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleSpace.SetFont(fuenteHead);
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Reporte de Revisitas");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);
                string rowTitle = "";
                for (int i = 0; i < dtExportar.Columns.Count; i++)
                {
                    rowTitle = "";
                    rowTitle = dtExportar.Columns[i].ColumnName.ToString();
                    //rowTitle = this.CapitalizeText(rowTitle) + "M";

                    filaEncabezado.CreateCell(i).SetCellValue(rowTitle);
                    filaEncabezado.GetCell(i).CellStyle = styleHeader;
                }
                for (int i = 0; i < dtExportar.Rows.Count; i++)
                {
                    Row filaRegistro = hoja1.CreateRow(i + 3);
                    for (int j = 0; j < dtExportar.Columns.Count; j++)
                    {
                        filaRegistro.CreateCell(j).SetCellValue(dtExportar.Rows[i][j].ToString());
                        //filaRegistro.GetCell(j).CellStyle = styleSpace;
                        filaRegistro.GetCell(j).CellStyle = style;
                        hoja1.AutoSizeColumn(j);
                    }
                }
                MemoryStream file = new MemoryStream();
                workbook.Write(file);
                Response.BinaryWrite(file.GetBuffer());
                Response.Cookies.Remove("termino");
                Response.AppendCookie(new HttpCookie("termino", "termino"));
                Response.End();
            }
            catch (Exception exp)
            {
            }
        }

    }
}