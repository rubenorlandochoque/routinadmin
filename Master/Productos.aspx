﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="AppAndroid.Master.ProductosMgmt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/jquery.dataTables.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.css")%>" rel="stylesheet" type="text/css" />

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jQuerypreviewmaster/jquery.preimage.js")%>"></script>
    <!--cropper.js-->
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.css")%>" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jquery.mask.js")%>"></script>

    <script src="<%= ResolveClientUrl("~/JS/RestAPI.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/Varios.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/Productos.js")%>"></script>

    <style type="text/css">
        table.dataTable {
            border-collapse: collapse !important;
            width: 100% !important;
        }

        .filaSeleccionada {
            background-color: #e2e8eb !important;
        }

        th {
            background-color: #FFF !important;
        }

        .dataTables_wrapper .boton {
            float: right;
            padding-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <asp:HiddenField runat="server" ID="hddCodRepositor" ClientIDMode="Static" />
    <section class="content-header">
        <h1>
            <i class="fa fa-shopping-cart"></i> Productos
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="#">Menu</a></li>
            <li class="active">Productos</li>
        </ol>
    </section>
    <br />

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Listado</b> de Productos</h3>
            <!-- div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
            </div -->
        </div>
        <div class="box-body" id="panelListadoProducto">
            <%--<h3>Productos de la Empresa</h3>--%>
            <div class="row">
                <div class="col-md-12">
                    <table id="tblProductos" class="table table-bordered table-striped table-hover table-condensed">
                        <thead>
                            <tr>
                                <th class="text-center">Codigo</th>
                                <th class="text-center">EAN</th>
                                <th class="text-center">Empresa</th>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Marca</th>
                                <th class="text-center">Presentación</th>
                                <th class="text-center">Familia</th>
                                <th class="text-center">Tipo Producto</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer text-right">
                <button type="button" class="btn btn-primary" onclick="agregarProducto()">Agregar Producto</button>
                <button class="btn btn-primary" type="button" onclick="generarExcel()">Exportar Excel</button>
            </div>
        </div>
        <asp:LinkButton ID="btnExcelear" runat="server" Style="display: block;" OnClick="btnExcelear_Click" ClientIDMode="Static"></asp:LinkButton>

    </div>

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title" id="titleEdit"><b>Nuevo</b> Producto</h3>
            <!-- div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
            </div -->
        </div>
        <div class="box-body" id="panelEditarProducto">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <!--
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="codigoProducto">Código del Producto</label>
                                <input type="text" class="form-control" id="txtCodProducto" placeholder="">
                                <input type="hidden" id="txtIdProducto">
                            </div>
                        </div>
                        -->
                        <input type="hidden" id="codProducto" value="">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="txtEAN">EAN</label>
                                <input type="text" class="form-control" id="txtEAN" placeholder="" maxlength="13">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textNombreProd">Nombre</label>
                                <input type="text" class="form-control" id="txtNombre" placeholder="" maxlength="101">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textMarca">Marca</label>
                                <input type="text" class="form-control" id="txtMarca" placeholder="" maxlength="50">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="txtPresentacion">Presentación</label>
                                <input type="text" class="form-control" id="txtPresentacion" placeholder="" maxlength="50">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" id="contCodEmpresa">
                                <label for="optIdFamilia">Familia:</label>
                                <select class="form-control" id="optIdFamilia">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" id="contCodTipoProducto">
                                <label for="optTipoProducto">Tipo Producto:</label>
                                <select class="form-control" id="optTipoProducto">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" id="contCodEmpresa">
                                <label for="optCodEmpresa">Compañía/Empresa</label>
                                <select class="form-control" id="optCodEmpresa">
                                </select>
                            </div>
                        </div>

                        <!--
                        <div class="col-md-4" id="contHabilitado">
                            <label>Habilitado</label>
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="rbtnHabilitado" class="minimal" value="true" id="rbtnHabilitadoSi">
                                    Sí
                                </label>
                                &nbsp;&nbsp;
                                <label>
                                    <input type="radio" name="rbtnHabilitado" class="minimal" value="false" id="rbtnHabilitadoNo">
                                    No
                                </label>
                            </div>
                        </div>
                         -->
                         <!--
                        <div class="col-md-4" style="display: none">
                            <label>Propio</label>
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="r1" class="minimal" value="true" id="rbtnPropio">
                                    Sí
                                </label>
                                &nbsp;&nbsp;
                                <label>
                                    <input type="radio" name="r1" class="minimal" value="false" id="rbtnPropio">
                                    No
                                </label>
                            </div>
                        </div>
                    -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: center">
                            <asp:Image alt="" class="img-thumbnail " ID="imagenmostrar" runat="server" />
                            <asp:HiddenField ID="dataFoto" runat="server" />
                            <div class="form-group">
                                <div style="margin-top: 5px; text-align: center">
                                    <button class="btn btn-primary" type="button" id="btnBuscarFoto" onclick=" mostrarDialogoBuscarFoto() " title="Subir foto desde archivo" data-backdrop="static" data-keyboard="false">
                                        <i class="fa fa-upload"></i>
                                    </button>

                                    <span class="btn btn-primary btn-file btn-mini" style="display: none"><i class="icon-custom-chart"></i>
                                        <asp:FileUpload ID="FileUploadFotoPerfil" accept="image/*;capture=camera" runat="server" />
                                    </span>

                                    <button class="btn btn-primary" type="button" id="btnTomarFoto" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false" title="Tomar foto desde webcam">
                                        <i class="fa fa-camera"></i>
                                    </button>
                                    <span class="btn btn-primary btn-file btn-mini" style="display: none"><i class="icon-custom-chart"></i>
                                        <asp:FileUpload ID="FileUploadFichero" accept="*;" runat="server" />
                                    </span>
                                </div>
                            </div>
                        </div>

                        <!-- Modal Tomar Foto -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div id="fSaveDatoRevisita">
                                        <div class="modal-header">
                                            <input type='button' id='btnpopup2' class='close' data-dismiss='modal' aria-hidden='true' value='x' style="display: none" />
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <br>
                                            <center>
                                            <h4 id="myModalLabel" class="semi-bold"><label id="lblTituloFoto"><b>Recortar Foto</b></label></h4>
                                            <p class="no-margin" id="lblTextoDescripcion">Recorte la foto y presione el botón 'Confirmar'.</p>
                                            <br />
                                            </center>
                                        </div>
                                        <div class="modal-body" id="bodyPopup">
                                            <div id="bodyTomarFoto">
                                                <div class="row form-row">
                                                    <div class="col-md-6">
                                                        <video id="video" width="260" height="280" autoplay></video>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <canvas id="canvas" style="padding-top: 40px;" width="260" height="200"></canvas>
                                                        <canvas id="canvasHidden" style="display: none;" width="1024" height="768"></canvas>
                                                        <img id='output' style="display: none;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="bodyEditarFoto" style="display: none">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div style="background-color: #f7f7f7; border: 1px solid #eee; box-shadow: inset 0 0 3px #f7f7f7; height: 350px; width: 100%; overflow: hidden">
                                                            <img class="cropper" src="" alt="Picture" style="display: none">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                    <div class="row eg-output">
                                                        <div class="col-md-12">
                                                            <div style="text-align: center;">
                                                                <button class="btn btn-primary btn-cons" id="getDataURL2" type="button" style="display: none">Aceptar</button>
                                                                <button class="btn btn-default" id="getDataURL" type="button" style="display: none">Cancelar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div id="botoneraTomarFoto">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrarModal">Cerrar</button>
                                                <button type="button" class="btn btn-primary" id="snap">Tomar Foto</button>
                                                <button type="button" class="btn btn-primary" id="btnConfirnarFoto">Confirmar</button>
                                            </div>
                                            <div id="botoneraEditarFoto" style="display: none">
                                                <input value="Aceptar" class="btn btn-primary btn-sm" type="button" onclick="actualizarFoto()" />
                                            </div>
                                        </div>
                                        <div>
                                            <img id="imgCaptura" style="display: none;" src="" />
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                </div>
                <div class="text-right" style="margin-right: 10px;">
                    <button class="btn btn-primary pull-right" type="button" id="btnGuardar" onclick="guardarProducto()">Guardar</button>
                    <button class="btn btn-white pull-right" type="button" onclick="$('#titleEdit').html('<b>Nuevo</b>  Producto');togglePanels('panelListadoProducto')">Volver a Listado</button>
                </div>
            </div>


            <!-- /.box-body -->

            <!-- /.box-footer-->
        </div>
        <!-- Modal -->

        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- /.box -->

    <div id="deleteDialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Deshabilitar Producto</h4>
                </div>
                <div class="modal-body">
                    <p>¿Esta seguro que desea deshabilitar el producto <span style="font-weight: bold;" id="lblNombre"></span>?</p>
                    <input type="hidden" name="" id="hhdCodProductoDel">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="onQuitarProducto()">Si</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
