﻿<%@ Page Title="Configurar Relevamientos" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="ConfigRelevamiento.aspx.cs" Inherits="AppAndroid.Master.ConfigRelevamiento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/jquery.dataTables.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.css")%>" rel="stylesheet" type="text/css" />
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jquery.mask.js")%>"></script>

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css")%>" rel="stylesheet" />

    <script src="<%= ResolveClientUrl("~/JS/RestAPI.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/Varios.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/ConfigRelevamiento.js")%>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <section class="content-header">
        <h1>
            <i class="fa fa fa-cog"></i>&nbsp;&nbsp;Configuración de Relevamientos
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="#">Menu</a></li>
            <li class="active">Configuración de Relevamientos</li>
        </ol>
    </section>
    <br>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Listado</b> de Relevamientos</h3>
        </div>
        <div class="box-body" id="panelListadoEmpresa">
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-hover table-condensed" id="tblData">
                    <thead>
                        <tr>
                            <!-- th>Codigo</th -->
                            <th class="text-center">Fecha Inicial</th>
                            <th class="text-center">Fecha Final</th>
                            <th class="text-center">Activo</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="text-right">
                <button type="button" class="btn btn-primary" onclick="onAgregar()">Agregar Relevamiento</button>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title" id="editTitle"><b>Nuevo</b> Relevamiento</h3>
            <input type="hidden" id="CodRelevavmiento">
        </div>
        <div class="box-body" id="panelEditarEmpresa">
            <div class="row">
                <div class="col-md-12">
                    <h4>Al agregar un nuevo relevamiento, se desasctivara el relevamiento activo actual</h4>
                </div>
            </div>
            <div class="row box-header with-border">
                 <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-5  form-group">
                            <label>Fecha Desde:</label>
                        </div>
                        <div class="col-md-7">
                            <input type="text" class="form-control" id="txtFechaDesde">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-5 form-group">
                            <label>Fecha Hasta:</label>
                        </div>
                        <div class="col-md-7">
                            <input type="text" class="form-control" id="txtFechaHasta">
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Buscar productos" id="textRepositorSearch">
                </div>
                <div class="col-md-6">
                    <br />
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-6">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-gray">
                            <!-- /.widget-user-image -->
                            <h4 style="margin-left: 0px; height: 28px; margin-bottom: 0px; margin-top: 0px; border-top-width: 0px;" class="widget-user-username">Productos </h4>Habilitados: <label id="lblRepEncontrados"></label>
                        </div>
                        <div class="box-footer no-padding">
                            <div class="row" style="height: 500px">
                                <div class="col-md-12" style="height: 440px">
                                        <div class="scrollbar-inner" style="max-height: 490px; height: 490px; overflow: scroll; overflow-X: hidden;">
                                            <ul class="tableselsimple nav nav-stacked" id="listadoRepositores">
                                            </ul>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-gray">
                            <!-- /.widget-user-image -->
                            <h4 style="margin-left: 0px; height: 28px; margin-bottom: 0px; margin-top: 0px; border-top-width: 0px;" class="widget-user-username">Productos asignados <span style="margin-top: 3px; font-size: 20px; margin-right: 13px;" class="pull-right badge bg-blue" id="lblCantPV">0</span></h4> <label id="lblPVEncontrados"></label>
                        </div>
                        <div class="box-footer no-padding">
                            <div class="row" style="height: 500px">
                                <div class="col-md-12" style="height: 460px">
                                    <%--<div id="contentListadoPuntosVenta" style="height: 460px; max-height: 460px !important; padding-right: 17px; padding-bottom: 10px" class="scrollbar-hidden">--%>
                                        <div class="scrollbar-inner" style="max-height: 490px; height: 490px; overflow: scroll; overflow-x: hidden;">
                                            <ul class="table nav nav-stacked" id="listadoPV">
                                        </ul>
                                        </div>

                                    <%--</div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-user -->
                </div>
            </div>
            <div class="box-footer text-right">
                <button type="button" class="btn btn-primary pull-right" onclick="Agregar()">Guardar</button>&nbsp;&nbsp;
                <button type="button" class="btn btn-white pull-right" onclick="$('#editTitle').html('<b>Nuevo</b> Relevamiento');togglePanels('panelListadoEmpresa')">Volver a Listado</button>&nbsp;&nbsp;
            </div>
        </div>
    </div>

    <div id="deleteDialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span id="actionTitle">Desactivar</span> Relevamiento</h4>
                </div>
                <div class="modal-body">
                    <p id="actionAlert"></p>
                    <input type="hidden" name="" id="hhdCodRelevamientoDel">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="onQuitar()">Si</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>