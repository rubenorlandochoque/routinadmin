﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Master;
using Microsoft.Practices.EnterpriseLibrary.Data;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;

namespace AppAndroid.Master
{
    public partial class ProductosMgmt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string codPantalla = "P_PRODUCTOS";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
            
            if (!Page.IsPostBack)
            {
                imagenmostrar.Attributes["onerror"] = "this.src='" + ResolveClientUrl("~/Recursos/Productos/no_producto.jpg") + "'";
                imagenmostrar.Attributes["src"] = ResolveClientUrl("~/Recursos/Productos/no_producto.jpg");
            }
        }

        public void CargarTabla()
        {
            DataTable dt = GetProductos();
            //rptRowsPersona.DataSource = dt;
            //rptRowsPersona.DataBind();
        }

        public static DataTable GetProductos()
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            //string sqlCommand = "permisos.usp_GetUsuariosPorServidor";
            string sqlCommand = "sp_productos";

            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Input parameters can specify the input value            
            db.AddInParameter(dbCommand, "@Comando", DbType.String, "SelectAll");

            DataTable dt = null;
            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;

        }

        [WebMethod]
        public static string GuardarProducto(string _codProducto, string _codEmpresa, string _EAN, string _nombre, string _marca, string _presentacion, string _imagen, string _propio, string _habilitado, string _idFamilia, string _codTipoProducto)
        {
            try
            {
                var _resultado = "";
                Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
                string stSqlCommand = "dbo.sp_productos";
                DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
                db.AddOutParameter(dbCommand, "@output", DbType.String, 512);
                db.AddInParameter(dbCommand, "@Comando", DbType.String, "Insert");
                db.AddInParameter(dbCommand, "@CodProducto", DbType.String, _codProducto);
                db.AddInParameter(dbCommand, "@CodEmpresa", DbType.String, _codEmpresa);
                db.AddInParameter(dbCommand, "@EAN", DbType.String, _EAN);
                db.AddInParameter(dbCommand, "@Nombre", DbType.String, _nombre);
                db.AddInParameter(dbCommand, "@Marca", DbType.String, _marca);
                db.AddInParameter(dbCommand, "@Presentacion", DbType.String, _presentacion);
                db.AddInParameter(dbCommand, "@Imagen", DbType.String, _imagen);
                db.AddInParameter(dbCommand, "@Propio", DbType.String, _propio);
                db.AddInParameter(dbCommand, "@Habilitado", DbType.String, _habilitado);
                db.AddInParameter(dbCommand, "@IdFamilia", DbType.String, _idFamilia);
                db.AddInParameter(dbCommand, "@CodTipoProducto", DbType.String, _codTipoProducto);

                db.ExecuteNonQuery(dbCommand);

                _resultado = db.GetParameterValue(dbCommand, "@output").ToString();

                if (_resultado.Equals("-1"))
                {
                    return "[ERROR] : Se produjo un error en la insercion en fusion.";
                }

                return _resultado;
            }
            catch (Exception ex)
            {
                return "[ERROR] : " + ex.Message;
            }
        }

        [WebMethod]
        public static string CargarTablaProductos()
        {
            DataTable dt = GetProductos();
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        protected void btnExcelear_Click(object sender, EventArgs e)
        {
            try
            {
                /*string codRelevamiento = Request.QueryString["codRelevamiento"].ToString();
                string codIndicador = Request.QueryString["codIndicadorAlerta"].ToString();
                string codEstado = Request.QueryString["codEstado"].ToString();*/

                DataTable dt = null;
                Database db = DatabaseFactory.CreateDatabase("ServerConnection");
                string sqlCommand = "dbo.sp_Productos";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
                db.AddInParameter(dbCommand, "@Comando", DbType.String, "SelectAllByExcel");
                // Add paramters
                // Input parameters can specify the input value
                /*db.AddInParameter(dbCommand, "@CodSalida", DbType.String, codRelevamiento);
                db.AddInParameter(dbCommand, "@codIndicador", DbType.String, codIndicador);
                db.AddInParameter(dbCommand, "@codEstado", DbType.String, codEstado);
                db.AddInParameter(dbCommand, "@codUsuario", DbType.String, "");
                db.AddInParameter(dbCommand, "@codFormulario", DbType.String, "");*/
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                DataTable dtExportar = dt;
                /*dtExportar.Columns.Remove(dtExportar.Columns["Indicador"]);
                dtExportar.Columns.Remove(dtExportar.Columns["NumEncuesta"]);
                dtExportar.Columns.Remove(dtExportar.Columns["ocurrencias"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodRelevamiento"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodVivienda"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodTema"]);
                dtExportar.Columns.Remove(dtExportar.Columns["FechaInicioHdd"]);
                dtExportar.Columns.Remove(dtExportar.Columns["FechaFinHdd"]);
                dtExportar.Columns.Remove(dtExportar.Columns["DuracionEncu"]);*/
                string fechaHora = DateTime.Now.ToString("dd-MM-yyyy");
                string filename = "Listado-de-Productos-" + fechaHora + ".xls";
                //string eTitulo = "Listado de Productos Propios y de la Competencia";
                string eTitulo = "Listado de Productos";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();
                /*HSSFWorkbook workbook = new HSSFWorkbook();
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Reporte de Encuestas");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);*/
                HSSFWorkbook workbook = new HSSFWorkbook();
                var style = workbook.CreateCellStyle();
                style.FillForegroundColor = HSSFColor.GREY_25_PERCENT.index;
                style.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuente = workbook.CreateFont();
                fuente.Color = HSSFColor.RED.index;
                fuente.FontName = "Calibri";
                fuente.FontHeightInPoints = 12;
                style.SetFont(fuente);
                var styleHeader = workbook.CreateCellStyle();
                styleHeader.FillForegroundColor = HSSFColor.GREY_80_PERCENT.index;
                styleHeader.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuenteHead = workbook.CreateFont();
                fuenteHead.Color = HSSFColor.WHITE.index;
                fuenteHead.FontName = "Calibri";
                fuenteHead.FontHeightInPoints = 12;
                styleHeader.SetFont(fuenteHead);
                var styleSpace = workbook.CreateCellStyle();
                styleSpace.FillForegroundColor = HSSFColor.WHITE.index;
                styleSpace.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleSpace.SetFont(fuenteHead);
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue(eTitulo);
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);
                for (int i = 0; i < dtExportar.Columns.Count; i++)
                {
                    filaEncabezado.CreateCell(i).SetCellValue(dtExportar.Columns[i].ColumnName);
                    filaEncabezado.GetCell(i).CellStyle = styleHeader;
                }
                for (int i = 0; i < dtExportar.Rows.Count; i++)
                {
                    Row filaRegistro = hoja1.CreateRow(i + 3);
                    for (int j = 0; j < dtExportar.Columns.Count; j++)
                    {
                        filaRegistro.CreateCell(j).SetCellValue(dtExportar.Rows[i][j].ToString());
                        //filaRegistro.GetCell(j).CellStyle = styleSpace;
                        //filaRegistro.GetCell(j).CellStyle = style;
                        hoja1.AutoSizeColumn(j);
                    }
                }
                MemoryStream file = new MemoryStream();
                workbook.Write(file);
                Response.BinaryWrite(file.GetBuffer());
                Response.Cookies.Remove("termino");
                Response.AppendCookie(new HttpCookie("termino", "termino"));
                Response.End();
            }
            catch (Exception exp)
            {
            }
        }
    }
}