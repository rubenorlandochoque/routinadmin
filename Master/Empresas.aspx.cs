﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;

namespace AppAndroid.Master
{
    public partial class Empresas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string codPantalla = "P_EMPRESAS";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
        }

        protected void btnExcelear_Click(object sender, EventArgs e)
        {
            try
            {
                /*string codRelevamiento = Request.QueryString["codRelevamiento"].ToString();
                string codIndicador = Request.QueryString["codIndicadorAlerta"].ToString();
                string codEstado = Request.QueryString["codEstado"].ToString();*/

                DataTable dt = null;
                Database db = DatabaseFactory.CreateDatabase("ServerConnection");
                string sqlCommand = "dbo.usp_Empresas";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
                db.AddInParameter(dbCommand, "@Comando", DbType.String, "SelectAllByExcel");
                // Add paramters
                // Input parameters can specify the input value
                /*db.AddInParameter(dbCommand, "@CodSalida", DbType.String, codRelevamiento);
                db.AddInParameter(dbCommand, "@codIndicador", DbType.String, codIndicador);
                db.AddInParameter(dbCommand, "@codEstado", DbType.String, codEstado);
                db.AddInParameter(dbCommand, "@codUsuario", DbType.String, "");
                db.AddInParameter(dbCommand, "@codFormulario", DbType.String, "");*/
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                DataTable dtExportar = dt;
                /*dtExportar.Columns.Remove(dtExportar.Columns["Indicador"]);
                dtExportar.Columns.Remove(dtExportar.Columns["NumEncuesta"]);
                dtExportar.Columns.Remove(dtExportar.Columns["ocurrencias"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodRelevamiento"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodVivienda"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodTema"]);
                dtExportar.Columns.Remove(dtExportar.Columns["FechaInicioHdd"]);
                dtExportar.Columns.Remove(dtExportar.Columns["FechaFinHdd"]);
                dtExportar.Columns.Remove(dtExportar.Columns["DuracionEncu"]);*/
                string fechaHora = DateTime.Now.ToString("dd-MM-yyyy");
                string filename = "ListadoEmpresas-" + fechaHora + ".xls";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();
                /*HSSFWorkbook workbook = new HSSFWorkbook();
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Reporte de Encuestas");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);*/
                HSSFWorkbook workbook = new HSSFWorkbook();
                var style = workbook.CreateCellStyle();
                style.FillForegroundColor = HSSFColor.GREY_25_PERCENT.index;
                style.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuente = workbook.CreateFont();
                fuente.Color = HSSFColor.RED.index;
                fuente.FontName = "Calibri";
                fuente.FontHeightInPoints = 12;
                style.SetFont(fuente);
                var styleHeader = workbook.CreateCellStyle();
                styleHeader.FillForegroundColor = HSSFColor.GREY_80_PERCENT.index;
                styleHeader.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuenteHead = workbook.CreateFont();
                fuenteHead.Color = HSSFColor.WHITE.index;
                fuenteHead.FontName = "Calibri";
                fuenteHead.FontHeightInPoints = 12;
                styleHeader.SetFont(fuenteHead);
                var styleSpace = workbook.CreateCellStyle();
                styleSpace.FillForegroundColor = HSSFColor.WHITE.index;
                styleSpace.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleSpace.SetFont(fuenteHead);
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Listado de Empresas");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);
                for (int i = 0; i < dtExportar.Columns.Count; i++)
                {
                    filaEncabezado.CreateCell(i).SetCellValue(dtExportar.Columns[i].ColumnName);
                    filaEncabezado.GetCell(i).CellStyle = styleHeader;
                }
                for (int i = 0; i < dtExportar.Rows.Count; i++)
                {
                    Row filaRegistro = hoja1.CreateRow(i + 3);
                    for (int j = 0; j < dtExportar.Columns.Count; j++)
                    {
                        filaRegistro.CreateCell(j).SetCellValue(dtExportar.Rows[i][j].ToString());
                        //filaRegistro.GetCell(j).CellStyle = styleSpace;
                        //filaRegistro.GetCell(j).CellStyle = style;
                        hoja1.AutoSizeColumn(j);
                    }
                }
                MemoryStream file = new MemoryStream();
                workbook.Write(file);
                Response.BinaryWrite(file.GetBuffer());
                Response.Cookies.Remove("termino");
                Response.AppendCookie(new HttpCookie("termino", "termino"));
                Response.End();
            }
            catch (Exception exp)
            {
            }
        }
    }
}