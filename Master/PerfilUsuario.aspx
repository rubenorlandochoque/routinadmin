﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="PerfilUsuario.aspx.cs" Inherits="AppAndroid.Master.PerfilUsuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/jquery.dataTables.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveClientUrl("~/LTE/dist/css/style.css")%>" rel="stylesheet" type="text/css" />

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jQuerypreviewmaster/jquery.preimage.js")%>"></script>
    <!--cropper.js-->
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.css")%>" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.js")%>"></script>


    <script src="<%= ResolveClientUrl("~/JS/PerfilUsuario.js")%>"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <asp:HiddenField runat="server" ID="hddCodRepositor" ClientIDMode="Static" />
    <section class="content-header">
        <h1>
            <i class="fa fa-user"></i> Perfil Usuario
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="#">Menu</a></li>
            <li class="active">Perfil Usuario</li>
        </ol>
    </section>
    <br />

    <div class="box collapsed-box" id="panelNuevoRep">
        <div class="box-header with-border">
            <h3 class="box-title"><label id="lblNuevoEdicion" >Datos</label> del Usuario</h3>
            <!--
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse" id="btnCollapseNuevo"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
            </div>
             -->
        </div>
        <div class="box-body" style="display:block">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textUsuario">Usuario</label>
                                <input type="text" class="form-control" id="textUsuario" placeholder="" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textApellido">Apellido</label>
                                <input type="text" class="form-control" id="textApellido" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textNombres">Nombre</label>
                                <input type="text" class="form-control" id="textNombres" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textPassOld">Contrase&ntilde;a anterior</label>
                                <input type="password" class="form-control" id="textPassOld" placeholder=""  autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textPass">Contrase&ntilde;a Nueva</label>
                                <input type="password" class="form-control" id="textPass" placeholder=""  autocomplete="off">
                                <span id="passstrength" ></span><br>
                                <span id="passmessage">La contraseña debe tener 8 caractéres como mínimo y contener al menos: mayúscula, número y símbolos</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textPassRepita">Confirmar Contrase&ntilde;a</label>
                                <input type="password" class="form-control" id="textPassRepita" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textLegajo">Legajo</label>
                                <input type="text" class="form-control" id="textLegajo" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: center">
                            <asp:Image alt="" class="img-thumbnail " ID="imagenmostrar" runat="server" />
                            <asp:HiddenField ID="dataFoto" runat="server" />
                            <asp:HiddenField ID="nombreFoto" runat="server" />
                            <div class="form-group">
                                <div style="margin-top: 5px; text-align: center">
                                    <button class="btn btn-primary" type="button" id="btnBuscarFoto" onclick=" mostrarDialogoBuscarFoto() " title="Subir foto desde archivo" data-backdrop="static" data-keyboard="false">
                                        <i class="fa fa-upload"></i>
                                    </button>

                                    <span class="btn btn-primary btn-file btn-mini" style="display: none"><i class="icon-custom-chart"></i>
                                        <asp:FileUpload ID="FileUploadFotoPerfil" accept="image/*;capture=camera" runat="server" />
                                    </span>

                                    <button class="btn btn-primary" type="button" id="btnTomarFoto" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false" title="Tomar foto desde webcam">
                                        <i class="fa fa-camera"></i>
                                    </button>
                                    <span class="btn btn-primary btn-file btn-mini" style="display: none"><i class="icon-custom-chart"></i>
                                        <asp:FileUpload ID="FileUploadFichero" accept="*;" runat="server" />
                                    </span>
                                </div>
                            </div>
                        </div>


                        <!-- ------------------------------->
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div id="fSaveDatoRevisita">
                                        <div class="modal-header">
                                            <input type='button' id='btnpopup2' class='close' data-dismiss='modal' aria-hidden='true' value='x' style="display: none" />
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <br>
                                            <center>
                                            <h4 id="myModalLabel" class="semi-bold"><label id="lblTituloFoto"><b>Recortar Foto</b></label></h4>
                                            <p class="no-margin" id="lblTextoDescripcion">Recorte la foto y presione el botón 'Confirmar'.</p>
                                            <br />
                                            </center>
                                        </div>
                                        <div class="modal-body" id="bodyPopup">
                                            <div id="bodyTomarFoto">
                                                <div class="row form-row">
                                                    <div class="col-md-6">
                                                        <video id="video" width="260" height="280" autoplay></video>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <canvas id="canvas" style="padding-top: 40px;" width="260" height="200"></canvas>
                                                        <canvas id="canvasHidden" style="display: none;" width="1024" height="768"></canvas>
                                                        <img id='output' style="display: none;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="bodyEditarFoto" style="display: none">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div style="background-color: #f7f7f7; border: 1px solid #eee; box-shadow: inset 0 0 3px #f7f7f7; height: 480px; width: 100%; overflow: hidden">
                                                            <img class="cropper" src="" alt="Picture" style="display: none">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                    <div class="row eg-output">
                                                        <div class="col-md-12">
                                                            <div style="text-align: center;">
                                                                <button class="btn btn-primary btn-cons" id="getDataURL2" type="button" style="display: none">Aceptar</button>
                                                                <button class="btn btn-default" id="getDataURL" type="button" style="display: none">Cancelar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div id="botoneraTomarFoto">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrarModal">Cerrar</button>
                                                <button type="button" class="btn btn-primary" id="snap">Tomar Foto</button>
                                                <button type="button" class="btn btn-primary" id="btnConfirnarFoto">Confirmar</button>
                                            </div>
                                            <div id="botoneraEditarFoto" style="display: none">
                                                <input value="Aceptar" class="btn btn-primary btn-sm" type="button" onclick="actualizarFoto()" />
                                            </div>
                                        </div>
                                        <div>
                                            <img id="imgCaptura" style="display: none;" src="" />
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>


                    </div>
                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer text-right" style="display:block">
                <button class="btn btn-primary" type="button" onclick="guardarDatos()">Guardar</button>
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- Modal -->

        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- /.box -->




</asp:Content>
