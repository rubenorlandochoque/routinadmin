﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Master;
using Microsoft.Practices.EnterpriseLibrary.Data;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;

namespace AppAndroid.Master
{
    public partial class PV : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			string codPantalla = "P_PUNTOS_VENTAS";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
            if (!Page.IsPostBack)
            {
                imagenmostrar.Attributes["onerror"] = "this.src='" + ResolveClientUrl("~/Recursos/PV/no_image.png") + "'";
                imagenmostrar.Attributes["src"] = ResolveClientUrl("~/Recursos/PV/no_image.png");
                //CargarTabla();
            }
        }

        [WebMethod]
        public static string GuardarPV(string _codPuntoVenta, string _nombre, string _codSucursal, string _direccion, string _ciudad, string _provincia, string _zona, string _coordenadas, string _tipoCliente, string _imagen, string _habilitado, string _dataFoto)
        {
            try
            {
                var _resultado = "";
                string stSqlCommand = "";
                Database db = DatabaseFactory.CreateDatabase("ServerConnection");

                if (_codPuntoVenta == "")
                {
                    stSqlCommand = "dbo.PV_AddPuntoVenta";
                }
                else
                {
                    stSqlCommand = "dbo.PV_EditPuntoVenta";
                }
                DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

                db.AddInParameter(dbCommand, "@codPuntoVenta", DbType.String, _codPuntoVenta);
                //if (_codUsuario != "")
                //{

                //}
                //else
                //{
                //    db.AddOutParameter(dbCommand, "@codUsuario", DbType.String, 512);
                //}

                db.AddInParameter(dbCommand, "@Nombre", DbType.String, _nombre);
                db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, _codSucursal);
                db.AddInParameter(dbCommand, "@Direccion", DbType.String, _direccion);
                db.AddInParameter(dbCommand, "@Ciudad", DbType.String, _ciudad);
                db.AddInParameter(dbCommand, "@Provincia", DbType.String, _provincia);
                db.AddInParameter(dbCommand, "@Zona", DbType.String, _zona);
                db.AddInParameter(dbCommand, "@Coodenadas", DbType.String, _coordenadas);
                db.AddInParameter(dbCommand, "@TipoCliente", DbType.String, _tipoCliente);                                                

                if ((_dataFoto != "")) //si hay foto a subir envio nombre sino envio vacio. Para el caso de nuevo envio cualquier nombre y en stored comprueba != de vacio y actualiza el nombre con el codrepositor
                {
                    db.AddInParameter(dbCommand, "@Imagen", DbType.String, _codPuntoVenta + ".jpg");
                }
                else
                {
                    db.AddInParameter(dbCommand, "@Imagen", DbType.String, "");
                }
                db.AddInParameter(dbCommand, "@Habilitado", DbType.String, _habilitado);
                db.AddOutParameter(dbCommand, "@result", DbType.String, 512);
                db.ExecuteNonQuery(dbCommand);

                _resultado = db.GetParameterValue(dbCommand, "@result").ToString();

                /*if (_codPuntoVenta == "")
                {
                    
                    if (_resultado.Equals("-1"))
                    {
                        return "[ERROR] : Se produjo un error en la insercion en fusion.";
                    }
                    else
                    {
                        return _resultado;
                    }
                }*/


                if ((!_resultado.Equals("-1")) && (!_resultado.Equals("duplicado")))
                {
                    if (_dataFoto != "")
                    {
                        if (_codPuntoVenta != "")  //Si edit el codigo usuario viene como
                        {
                            Proced.GuardarFotoPV(_dataFoto, _codPuntoVenta);
                        }
                        else
                        {
                            Proced.GuardarFotoPV(_dataFoto, _resultado);
                        }
                    }    
                }
                return _resultado;
            }
            catch (Exception ex)
            {
                return "[ERROR] : " + ex.Message;
            }
        }

        [WebMethod]
        public static string CargarTablaPV()
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.PV_getAllPuntosVenta";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            DataTable dt = null;
            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetPVByCod(string _codPV, string _codSucursal)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.PV_getPuntoVentaByCod";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, _codPV);
            db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, _codSucursal);

            DataTable dt = null;
            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string DeshabilitarPV(string CodPV, string CodSucursal)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            string stSqlCommand = "dbo.PV_DelPuntoVenta";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodPV", DbType.String, CodPV);
            db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, CodSucursal);
            db.ExecuteNonQuery(dbCommand);
            return "done";
        }

        //Agregado RicardoQ
        public string ToSiNo(string input)
        {
            if (input == "True")
            {
                return "Si";
            }
            else
            {
                return "No";
            }
        }

        //Creado RicardoQ
        public string CapitalizeText(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }


        protected void btnExcelear_Click(object sender, EventArgs e)
        {
            try
            {
                /*string codRelevamiento = Request.QueryString["codRelevamiento"].ToString();
                string codIndicador = Request.QueryString["codIndicadorAlerta"].ToString();
                string codEstado = Request.QueryString["codEstado"].ToString();*/

                DataTable dt = null;
                Database db = DatabaseFactory.CreateDatabase("ServerConnection");
                string sqlCommand = "dbo.PV_getAllPuntosVentaExcel";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
                // Add paramters
                // Input parameters can specify the input value
                /*db.AddInParameter(dbCommand, "@CodSalida", DbType.String, codRelevamiento);
                db.AddInParameter(dbCommand, "@codIndicador", DbType.String, codIndicador);
                db.AddInParameter(dbCommand, "@codEstado", DbType.String, codEstado);
                db.AddInParameter(dbCommand, "@codUsuario", DbType.String, "");
                db.AddInParameter(dbCommand, "@codFormulario", DbType.String, "");*/
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                DataTable dtExportar = dt;
                /*dtExportar.Columns.Remove(dtExportar.Columns["Indicador"]);
                dtExportar.Columns.Remove(dtExportar.Columns["NumEncuesta"]);
                dtExportar.Columns.Remove(dtExportar.Columns["ocurrencias"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodRelevamiento"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodVivienda"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodTema"]);
                dtExportar.Columns.Remove(dtExportar.Columns["FechaInicioHdd"]);
                dtExportar.Columns.Remove(dtExportar.Columns["FechaFinHdd"]);
                dtExportar.Columns.Remove(dtExportar.Columns["DuracionEncu"]);*/
                string fechaHora = DateTime.Now.ToString("dd-MM-yyyy");
                string filename = "Listado-de-Puntos-de-Venta-" + fechaHora + ".xls";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();
                /*HSSFWorkbook workbook = new HSSFWorkbook();
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Reporte de Encuestas");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);*/
                HSSFWorkbook workbook = new HSSFWorkbook();
                var style = workbook.CreateCellStyle();
                style.FillForegroundColor = HSSFColor.GREY_25_PERCENT.index;
                style.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuente = workbook.CreateFont();
                fuente.Color = HSSFColor.RED.index;
                fuente.FontName = "Calibri";
                fuente.FontHeightInPoints = 12;
                style.SetFont(fuente);
                var styleHeader = workbook.CreateCellStyle();
                styleHeader.FillForegroundColor = HSSFColor.GREY_80_PERCENT.index;
                styleHeader.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuenteHead = workbook.CreateFont();
                fuenteHead.Color = HSSFColor.WHITE.index;
                fuenteHead.FontName = "Calibri";
                fuenteHead.FontHeightInPoints = 12;
                styleHeader.SetFont(fuenteHead);
                var styleSpace = workbook.CreateCellStyle();
                styleSpace.FillForegroundColor = HSSFColor.WHITE.index;
                styleSpace.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleSpace.SetFont(fuenteHead);
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Listado de Puntos de Venta");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);
                for (int i = 0; i < dtExportar.Columns.Count; i++)
                {
                    filaEncabezado.CreateCell(i).SetCellValue(dtExportar.Columns[i].ColumnName);
                    filaEncabezado.GetCell(i).CellStyle = styleHeader;
                }
                for (int i = 0; i < dtExportar.Rows.Count; i++)
                {
                    Row filaRegistro = hoja1.CreateRow(i + 3);
                    for (int j = 0; j < dtExportar.Columns.Count; j++)
                    {
                        if (j == 9)
                        {
                            filaRegistro.CreateCell(j).SetCellValue(this.ToSiNo(dtExportar.Rows[i][j].ToString()));
                            //filaRegistro.GetCell(j).CellStyle = styleSpace;
                            //filaRegistro.GetCell(j).CellStyle = style;
                            hoja1.AutoSizeColumn(j);
                        }
                        else
                        {
                            filaRegistro.CreateCell(j).SetCellValue(dtExportar.Rows[i][j].ToString());
                            //filaRegistro.GetCell(j).CellStyle = styleSpace;
                            //filaRegistro.GetCell(j).CellStyle = style;
                            hoja1.AutoSizeColumn(j);
                        }

                    }
                }
                MemoryStream file = new MemoryStream();
                workbook.Write(file);
                Response.BinaryWrite(file.GetBuffer());
                Response.Cookies.Remove("termino");
                Response.AppendCookie(new HttpCookie("termino", "termino"));
                Response.End();
            }
            catch (Exception exp)
            {
            }
        }
    }
}