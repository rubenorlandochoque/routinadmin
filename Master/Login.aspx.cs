﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppAndroid.Master;

namespace Buchon.Pantallas.Login
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Sesion.InicializarVariables();    
                userid.Focus();
            }            
        }
        protected void ingresar_Click(object sender, EventArgs e)
        {
            if (userid.Text == "" || password.Text == "")
            {
                error.Text = "Debe ingresar todos los datos";
            }
            else
            {
                var usuario = new Usuarios();
                //usuario.IniciarSesion(userid.Text, password.Text);
                DataRow dr = Usuario.GetDatosUsuario(userid.Text, password.Text);
                if (dr != null)
                {
                    Sesion.setNombreUsuario(dr["nombres"].ToString().ToUpper());
                    Sesion.setApellidoUsuario(dr["Apellido"].ToString().ToUpper());
                    Sesion.setCodUsuario(dr["codUsuario"].ToString());
                    Sesion.setFoto((dr["Foto"].ToString()));
                    Sesion.setTipoUsuario((dr["CodTipoUsuario"].ToString()));
                    /*ruben*/
					Sesion.setPermisos(dr["CodUsuario"].ToString());
                    Response.Redirect("~/Master/Opciones.aspx");
                }
                else
                {
                    error.Text = "El usuario o contraseña están incorrectos";
                }
            }
        }
    }
}