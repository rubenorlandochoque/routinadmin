﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Configuracion.aspx.cs" Inherits="AppAndroid.Master.Configuracion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jquery.mask.js")%>"></script>

    <script src="<%= ResolveClientUrl("~/JS/Varios.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/Configuracion.js")%>"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <section class="content-header">
        <h1>
            <i class="fa fa-cogs"></i> Configuración
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="#">Menu</a></li>
            <li class="active">Configuración</li>
        </ol>
    </section>
    <br>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Configuración</b> General</h3>
            <!-- div class="box-tools pull-right">
                <button class="btn btn-box-tool"  id="btnContListado" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div -->
        </div>
        <div class="box-body" id="panelConfiguracion">
            <div class="row">
                <div class="col-md-7 form-group">
                    <label>Frecuencia de Tracking en minutos (1-60)</label>
                    <input type="text" id="frecuenciaTracking" runat="server" class="form-control"/>
                </div>

                <div class="col-md-6">
                    <br>
                </div>

                <div class="col-md-7 form-group">
                    <label>Frecuencia de sincronización de Tracking en minutos (1-60)</label>  
                    <input type="text" id="frecuenciaSincronizacion" runat="server" name="frecuenciaSincronizacion" class="form-control"/>
                </div>
                <div class="col-md-6">
                    <br>
                </div>
            </div>

            <div class="text-right">
                
                <button class="btn btn-primary" type="button" onclick="guardarConfiguracion()">Guardar</button>
            </div>


        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MenuMasterContent3" runat="server">
</asp:Content>
