﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Traking.aspx.cs" Inherits="AppAndroid.Master.Traking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css")%>" rel="stylesheet" />

    <!--
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-scrollbar/jquery.scrollbar.min.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-scrollbar/jquery.scrollbar.css")%>" rel="stylesheet" />
    -->

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/gmaps.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jquery.mask.js")%>"></script>

    <script src="<%= ResolveClientUrl("~/JS/Varios.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/Tracking.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/Styles/Traking.css")%>" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <section class="content-header">
        <h1>
            <i class="fa fa-map-marker"></i> Tracking
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="#">Menu</a></li>
            <li class="active">Tracking</li>
        </ol>
    </section>
    <br />

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Búsqueda de Tracking</h3>
        </div>
        <div class="box-body" id="panelFiltradoTracking">
            <div class="form-group col-md-6">
                <label>Repositor:</label>
                <select id="optCodRepositor" class="form-control">
                </select>
            </div>
            <div class="form-group col-md-4">
                <label>Fecha:</label>
                <input type="text" id="txtFecha" class="form-control">
            </div>
            <div class="col-md-2 text-center">
                <br>
                <button type="button" class="btn btn-primary btn-block" onclick="cargarListados()">Ver</button>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Ver Tracking</h3>
        </div>
        <div class="box-body" id="panelListadoTracking">
            <div class="text-center"><h4>Repositor: <strong id="lblNombreRepositor"></strong></h4></div>
            <div class="row">
                <div class="col-md-3 text-center">
                    <h4><img src="azul_24.png"> Tracking</h4>
                    <table class="table">
                        <thead>
                            <th><center>Hora</center></th>
                            <th><center>Pto.Venta</center></th>
                            <!--
                            <th><center>Precisión</center></th>
                            -->
                        </thead>
                    </table>
                    <div class="scrollbar-inner" style="max-height: 400px; overflow: scroll; overflow-x: hidden;">
                        <table class="table" id="tblTracking">
                            <tbody id="tablaTraking">
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <button type="button" class="btn btn-default btn-block" onclick="onDrawTrakingLine()" >Ver Recorrido</button>

                </div>

                <div class="col-md-6">
                    <div id="Mapa" style="height: 500px;">
                    </div>
                </div>

                <div class="col-md-3 text-center">
                    <h4><img src="market.png"> Ruteo</h4>
                    <table class="table">
                        <thead>
                            <th><center>Hora</center></th>
                            <th><center>Pto.Venta</center></th>
                            <!--
                            <th><center>Precisión</center></th>
                            -->
                        </thead>
                    </table>
                    <div class="scrollbar-inner" style="max-height: 400px; overflow: scroll;overflow-x: hidden;">
                        <table class="table" id="tblRuteo">
                            <tbody id="tablaRuteo">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <button type="button" class="btn btn-primary" onclick="onVolver()">Volver</button>
            </div>
        </div>
    </div>


</asp:Content>
