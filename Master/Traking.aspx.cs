﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.Record.Formula.Eval;
using NPOI.HSSF.Record.Formula.Functions;
using Master;

namespace AppAndroid.Master
{
    public partial class Traking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string codPantalla = "P_TRAKING";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
        }
 
        [WebMethod]
        public static string GetAllRepositores(string _codUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.REP_getAllRepositoresByUser";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, _codUsuario);
            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();
        
            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string VerTraking(string _codUsuario, string _fecha)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.TRK_GetTrakingByUser";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, _codUsuario);
            db.AddInParameter(dbCommand, "@Fecha", DbType.String, _fecha);
            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string VerPV(string _codUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.TRK_getPuntosVentaByRepositor";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, _codUsuario);            
            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string verRuteoByUserDay(string _codUsuario, string _fecha)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.RUT_GetRuteosByUserDay";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, _codUsuario);
            db.AddInParameter(dbCommand, "@Fecha", DbType.String, _fecha);
            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }
    }
}