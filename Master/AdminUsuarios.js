﻿var $image;

$(document).ready(function () {
    inicializarTablas();
    inicializarModuloFoto();
    inicializarOtrosModulos();
    cargarTablaRepositores();
    togglePanels("panelListado");
});

function inicializarOtrosModulos() {
    $("#textLegajo").mask('00000000');
    $("[id$=textPass]").attr('readonly', 'readonly');
    $("[id$=textPass]").attr('disabled', 'disabled');
    $("[id$=textPassRepetir]").attr('readonly', 'readonly');
    $("[id$=textPassRepetir]").attr('disabled', 'disabled');

    $("[id$=selectTipoUsuario]").attr('readonly', 'readonly');
    $("[id$=selectTipoUsuario]").attr('disabled', 'disabled');
    $("[id$=textLegajo]").attr('readonly', 'readonly');
    $("[id$=textLegajo]").attr('disabled', 'disabled');
    $("body").on("click", "[id=btnNewPersona]", function () {
        clearFrmPersona();
        expandir();
        $("[id$=GuardarDatosPersonales], [id$=btnGuardar]").css("display", "none");
        $("[id$=btnGuardarTodo]").css("display", "block");
        $("[id$=apellidoPersona]").focus();
    });
    /*$("a").click(function (e) {
        if (($(this).attr("href").indexOf("#") == -1) && ($(this).attr("href").indexOf("javascript") == -1)) {
            e.preventDefault();
            $("[id$=urlredireccionar]").val($(this).attr("href"));
            //alert( jQuery.type( $( "[id$=funcionejecutar]" ) ) !== "undefined" );
            if (($("[id$=funcionejecutar]").val() != "") && (jQuery.type($("[id$=funcionejecutar]").val()) !== "undefined")) {
                var tmp = $("[id$=funcionejecutar]").val();
                eval(tmp)(-1);
            } else {
            }
        }
    });*/

    //iCheck for checkbox and radio inputs
    //$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    //    checkboxClass: 'icheckbox_minimal-blue',
    //    radioClass: 'iradio_minimal-blue'
    //});
    $('#textPass').keyup(function (e) {
        if ($(this).val() != "") {
            retorno = verificarFuerzaContrasenia($(this).val());
            if (retorno == "-1") {
                $('#passstrength').html("Débil");
                $('#passstrength').removeClass("").addClass("passdebil");
            } else {
                if (retorno == "0") {
                    $('#passstrength').html("Media");
                    $('#passstrength').removeClass("").addClass("passmedia");
                } else {
                    if (retorno == "1") {
                        $('#passstrength').html("Fuerte");
                        $('#passstrength').removeClass("").addClass("passfuerte");
                    }
                }
            }
        } else {
            $('#passstrength').html('');
        }

        return true;
    });
    getTiposUsuario();
}

function verificarFuerzaContrasenia(pass) {
    var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    var enoughRegex = new RegExp("(?=.{6,}).*", "g");
    var retorno = "";
    if (false == enoughRegex.test(pass)) {
        //$('#passstrength').html('Débil');
        retorno = "-1";
    } else if (strongRegex.test(pass)) {
        //$('#passstrength').className = 'ok';
        //$('#passstrength').html('Fuerte!');
        retorno = "1";
    } else if (mediumRegex.test(pass)) {
        //$('#passstrength').className = 'alert';
        //$('#passstrength').html('Media!');
        retorno = "0";
    } else {
        //$('#passstrength').className = 'error';
        //$('#passstrength').html('Débil!');
        retorno = "-1";
    }
    return retorno;
}

function mostrarNuevoUsuario() {
    clearFrmPersona();
    mostrarOcultarPaneles("panel2");
    $("[id$=textPass]").removeAttr('readonly');
    $("[id$=textPass]").removeAttr('disabled');
    $("[id$=textPassRepetir]").removeAttr('readonly');
    $("[id$=textPassRepetir]").removeAttr('disabled');

    $("[id$=selectTipoUsuario]").removeAttr('readonly');
    $("[id$=selectTipoUsuario]").removeAttr('disabled');
    $("[id$=textLegajo]").removeAttr('readonly');
    $("[id$=textLegajo]").removeAttr('disabled');
    $('#textUsuario').focus();
}

function clearFrmPersona() {
    $("#lblNuevoEdicion").text("Nuevo");
    //Reseteo formulario de datos personales
    $('#divResetear').hide();
    //$('#rbtnResetearSi').removeAttr("checked");
    //$('#rbtnResetearNo').removeAttr("checked");
    $('#hddCodRepositor').val("");
    $('#textUsuario').val("");
    $('#textPass').val("");
    $('#textPassRepetir').val("");
    $('#textLegajo').val("");
    $('#textApellido').val("");
    $("#txtUsuario").val("");
    $("#txtPass").val("");
    $('#textNombres').val("");
    $('#selectTipoUsuario').val("select_opcion");
    $("[id$=imagenmostrar]").attr("src", $("[id$=hddURLPerfil]") + "no_image.png");
    $("[id$=dataFoto]").val("");
    $("[id$=nombreFoto]").val("");
    $(".grid-title h4 span").text("");
    $('#rbtnHabilitado').removeAttr("checked");
    $('#rbtnNoHabilitado').removeAttr("checked");
    $('#textUsuario').focus();
}

function mostrarOcultarPaneles(panelMostrar) {
    if (panelMostrar == "panel2") {
        togglePanels("panelNuevoRep");
        //$('#btnCollapseListado').click();
        //$('#btnCollapseNuevo').click();
    } else {
        //$('#btnCollapseListado').click();
        //$('#btnCollapseNuevo').click();
        togglePanels("panelListado");
    }
}

function expandir() {
    clearFrmPersona();
}

function inicializarModuloFoto() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }

    $("[id$=FileUploadFotoPerfil]").preimage();
    ejecutar();
    $("#btnTomarFoto").on("click", function () {
        $("[id$=botoneraTomarFoto]").show();
        $("[id$=botoneraEditarFoto]").hide();
        $("[id$=bodyTomarFoto]").show();
        $("[id$=bodyEditarFoto]").hide();
    });
}

var table, usuarios;

function inicializarTablas() {
    table = $('#tblPersonas').DataTable({
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Ultima",
                "sNext": "Siguiente",
                "sPrevious": "Atras",
            },
            "sProcessing": "Procesando...",
            "sSearch": "Buscar: ",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Registro _START_ al _END_ (_TOTAL_ registros en total)",
            "sInfoEmpty": "-",
            "sInfoFiltered": ", filtrado de un total de _MAX_ registros.",
            "sInfoPostFix": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando..."
        },
        "iDisplayLength": 25,
        "columnDefs": [
            {
                "targets": [0],
                "visible": false
                //"searchable": false
            }
        ]
    });

    $('#tblPersonas tbody').on('click', 'tr button', function () {

        console.log($(this).parent().parent());
        console.log(table.row($(this).parent().parent()).data());
        var data = table.row($(this).parent().parent().parent()).data();

        console.log($(this).val());

        if ($(this).val() == "datos") {
            console.log("asdfasdf");
            console.log(data);
            $("[id$=textPass]").attr('readonly', 'readonly');
            $("[id$=textPass]").attr('disabled', 'disabled');
            $("[id$=textPassRepetir]").attr('readonly', 'readonly');
            $("[id$=textPassRepetir]").attr('disabled', 'disabled');
            $("[id$=selectTipoUsuario]").attr('readonly', 'readonly');
            $("[id$=selectTipoUsuario]").attr('disabled', 'disabled');
            $("[id$=textLegajo]").attr('readonly', 'readonly');
            $("[id$=textLegajo]").attr('disabled', 'disabled');
            cargarDatos(data[0]);
        }
        /*$("[id$=CodigoPersonalUsuario]").val(data[4]);
        $("[id$=OpercionUsuario]").val($(this).val());

        $("[id$=verDatosUsuario]").click();*/
    });
}

//Funciones para fotos
function cargarFoto(url) {
    //alert( url );
    $("[id$=imagenmostrar]").attr("src", url);
    $("[id$=dataFoto]").attr("value", url);
}

function cerrarDialogoFoto() {
    $("[id$=botoneraTomarFoto]").show();
    $("[id$=botoneraEditarFoto]").hide();
    $("[id$=bodyTomarFoto]").show();
    $("[id$=bodyEditarFoto]").hide();
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.rect(0, 0, canvas.width, canvas.height);
    context.stroke();
    $("#btnpopup1").trigger("click");
    $("#btnpopup2").trigger("click");
}

function actualizarFoto() {
    cargarFotoCortada();
}

function ejecutar() {
    $image = $(".cropper");
    var $dataX = $("#dataX"),
    $dataY = $("#dataY"),
    $dataHeight = $("#dataHeight"),
    $dataWidth = $("#dataWidth"),
    console = window.console || { log: $.noop },
    cropper;
    $image.cropper({
        aspectRatio: 1 / 1,
        // autoCropArea: 1,
        data: {
            x: 520,
            y: 10,
            width: 1024,
            height: 768
        },
        preview: ".preview",

        // multiple: FALSE,
        // autoCrop: TRUE,
        // dragCrop: TRUE,
        // dashed: TRUE,
        // modal: TRUE,
        // movable: TRUE,
        // resizable: TRUE,
        // zoomable: TRUE,
        // rotatable: TRUE,

        // maxWidth: 480,
        // maxHeight: 270,
        // minWidth: 160,
        // minHeight: 90,

        done: function (data) {
            $dataX.val(data.x);
            $dataY.val(data.y);
            $dataHeight.val(data.height);
            $dataWidth.val(data.width);
        },

        build: function (e) {
            console.log(e.type);
        },

        built: function (e) {
            console.log(e.type);
        },

        dragstart: function (e) {
            console.log(e.type);
        },

        dragmove: function (e) {
            console.log(e.type);
        },

        dragend: function (e) {
            console.log(e.type);
        }
    });

    cropper = $image.data("cropper");

    $image.on({
        "build.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "built.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragstart.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragmove.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragend.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        }
    });

    $("#reset").click(function () {
        $image.cropper("reset");
    });

    $("#reset2").click(function () {
        $image.cropper("reset", true);
    });

    $("#clear").click(function () {
        $image.cropper("clear");
    });

    $("#destroy").click(function () {
        $image.cropper("destroy");
    });

    $("#enable").click(function () {
        $image.cropper("enable");
    });

    $("#disable").click(function () {
        $image.cropper("disable");
    });

    $("#zoom").click(function () {
        $image.cropper("zoom", $("#zoomWith").val());
    });

    $("#zoomIn").click(function () {
        $image.cropper("zoom", 0.9);
    });

    $("#zoomOut").click(function () {
        $image.cropper("zoom", -0.9);
    });

    $("#rotate").click(function () {
        $image.cropper("rotate", $("#rotateWith").val());
    });

    $("#rotateLeft").click(function () {
        $image.cropper("rotate", -90);
    });

    $("#rotateRight").click(function () {
        $image.cropper("rotate", 90);
    });

    $("#setAspectRatio").click(function () {
        $image.cropper("setAspectRatio", $("#aspectRatio").val());
    });

    $("#replace").click(function () {
        //$image.cropper( "replace", $( "#replaceWith" ).val() );
        $image.cropper("replace", $("#fotoMostrar").val());
    });

    $("#getImageData").click(function () {
        $("#showImageData").val(JSON.stringify($image.cropper("getImageData")));
    });

    $("#setData").click(function () {
        $image.cropper("setData", {
            x: $dataX.val(),
            y: $dataY.val(),
            width: $dataWidth.val(),
            height: $dataHeight.val()
        });
    });

    $("#getData").click(function () {
        $("#showData").val(JSON.stringify($image.cropper("getData")));
    });

    $("#getDataURL").click(function () {
        //var dataURL = $image.cropper( "getDataURL" );

        //$( "#dataURL" ).text( dataURL );
        //$( "#showDataURL" ).html( '<img src="' + dataURL + '">' );
        cerrarDialogoFoto();
    });

    $("#getDataURL2").click(function () {
        var dataURL = $image.cropper("getDataURL", "image/jpeg");

        $("#dataURL").text(dataURL);
        $("#showDataURL").html('<img src="' + dataURL + '">');
        cargarFoto(dataURL);
        cerrarDialogoFoto();
    });
};

function cargarURLFoto() {
    $("#imagenCortar").attr("src", $("#iframe12").attr("foto"));
    alert($("#iframe12").attr("foto"));
}

function agregarURL(url) {
    //$( "#imagenCortar" ).removeAttr( "src");
    $("#imagenCortar").attr("src", url);
    $("#fotoMostrar").attr("value", url);
    $("#replace").trigger("click");
    ejecutar();
}

function salir() {
    $("#btnpopup").trigger("click");
}

function cargarFotoCortada() {
    $("#getDataURL2").trigger("click");
}

//FIN Funciones para fotos

// Put event listeners into place
window.addEventListener("DOMContentLoaded", function () {
    // Grab elements, create settings, etc.
    canvas = document.getElementById("canvas");
    canvasHdd = document.getElementById("canvasHidden");
    context = canvas.getContext("2d");
    context.rect(0, 0, canvas.width, canvas.height);
    context.stroke();
    contextHdd = canvasHdd.getContext("2d");

    video = document.getElementById("video");
    videoObj = { "video": true };
    errBack = function (error) {
        console.log("Video capture error: ", error.code);
    };

    // Put video listeners into place
    if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function (stream) {
            video.src = stream;
            video.play();
            console.log("Standard");
        }, errBack);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function (stream) {
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
            console.log("WebKit-prefixed");
        }, errBack);
    } else if (navigator.mozGetUserMedia) { // WebKit-prefixed
        navigator.mozGetUserMedia(videoObj, function (stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
            console.log("WebKit-prefixed 22");
        }, errBack);
    }

    // Trigger photo take
    document.getElementById("snap").addEventListener("click", function () {
        context.drawImage(video, 0, 0, 280, 220);
        //context.drawImage( video, 0, 0, 1024, 768);
        contextHdd.drawImage(video, 0, 0, 1024, 768);
    });
    document.getElementById("btnConfirnarFoto").addEventListener("click", function () {
        document.getElementById("imgCaptura").src = canvas.toDataURL();
        $("[id$=imagenmostrar]").attr("src", document.getElementById('imgCaptura').src);

        //console.log(canvas.toDataURL("image/png"));
        $image.cropper("replace", canvasHdd.toDataURL("image/png"));
        $image.cropper("reset", true);

        //$("#btnMostrarFotoRecortar").click();
        //$(window).trigger('resize');

        //document.getElementById("hidden").value = canvas.toDataURL("image/png").replace('data:image/png;base64,', '');
        $("[id$=botoneraTomarFoto]").hide();
        $("[id$=botoneraEditarFoto]").show();
        $("[id$=bodyTomarFoto]").hide();
        $("[id$=bodyEditarFoto]").show();
    });
}, false);


function mostrarDialogoBuscarFoto() {
    $("[id$=FileUploadFotoPerfil]").click();
}
//Datos subir archivos - Inicio
function mostrarDialogoBuscarFichero() {
    $("[id$=FileUploadFichero]").click();
}
//Datos subir archivos - Fin



function modificarComportamientoSeccionLocalizacion() {
    try {
        document.getElementById('iframe1').contentWindow.cargarPuntoPorDefecto();
    }
    catch (err) {
        $('#iframe1').load(function () {
            document.getElementById('iframe1').contentWindow.cargarPuntoPorDefecto();
        });
    }
}

function centrarEnCentroConin() {
    //document.getElementById( 'iframe1' ).contentWindow.centrarMapa( -24.794682484311966, -65.4301929473877 );
}

function verificarCamposObligatorios() {
    var retorno = false;
    var mensaje = "";
    if ($("#textUsuario").val().trim() == "") {
        mensaje = "Debe completar el campo <b>Usuario</b>";
    } else {
        if ($("#textApellido").val().trim() == "") {
            mensaje = "Debe completar el campo Apellido";
        } else {
            if ($("#textNombres").val().trim() == "") {
                mensaje = "Debe completar el campo Nombres";
            } else {
                if ((!$("#textPass").val().trim() != "") && ($("#textPass").val().trim() != $("#textPassRepetir").val().trim())) {
                    mensaje = "La contraseña y la confirmación de la contraseña no coinciden";
                } else {
                    if ((!$("#textPass").is(":disabled")) && ($("#textPass").val().trim() != "") && ($("#textPass").val().trim() != $("#textPassRepetir").val().trim())) {
                        mensaje = "La contraseña y la confirmación de la contraseña no coinciden";
                    } else {
                        if ($("#textLegajo").val().trim() == "") {
                            mensaje = "Debe completar el campo Legajo";
                        } else {
                            if (($('#hddCodRepositor').val().trim() == "") && ($('#textPass').val().trim() == "")) {
                                mensaje = "Debe completar el campo Contraseña";
                            } else {
                                if (($("#textPass").val().trim() != "") && (verificarFuerzaContrasenia($("#textPass").val().trim()) == "-1")) {
                                    mensaje = "La contraseña es Débil";
                                } else {
                                    if ((($("#textPass").val().trim().length > 0)) && ($("#textPass").val().trim().length < 8)) {
                                        mensaje = "La contraseña debe tener 8 caractéres como mínimo";
                                    } else {
                                        if ($("#selectTipoUsuario").val() == "select_opcion") {
                                            mensaje = "Debe seleccionar el Tipo de Usuario";
                                        } else {
                                            retorno = true;
                                        }
                                    }
                                    //var resultado = existeLegajo($("#textLegajo").val());
                                    //alert("---------- " + $('[id$=hddExiste]').val());

                                }
                            }
                            /*if ((!$("#rbtnHabilitado").is(":checked")) && (!$("#rbtnNoHabilitado").is(":checked"))) {
                                mensaje = "Debe seleccionar si el repositor está habilitado";
                            } else {
                                retorno = true;
                            }*/
                        }
                    }
                }
            }
        }
    }
    if (!retorno) {
        showErrorMessage(mensaje);
    }
    return retorno;
}

function cargarDatos(codRep) {
    var data = {};
    data["_codUsuario"] = codRep.trim();
    $.ajax({
        type: "POST",
        url: 'AdminUsuarios.aspx/GetUsuarioByCod',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            var result = JSON.parse(respuesta.d);
            $.each(result, function () {
                $("#lblNuevoEdicion").text("Modificar");
                $('#divResetear').show();
                $('#hddCodRepositor').val(this.CodUsuario.trim());
                $('#textLegajo').val(this.Legajo.trim());
                $('#textApellido').val(this.Apellido.trim());
                $('#textNombres').val(this.Nombres.trim());
                $('#textUsuario').val(this.Usuario);
                $('#selectTipoUsuario').val(this.CodTipoUsuario.trim());
                $('#textPass').val("");
                $('#textPassRepetir').val("");
                console.log(this.Foto);
                console.log("imagen");
                if ((!this.Foto)) {
                    $("[id$=imagenmostrar]").attr("src", $("[id$=hddURLPerfil]").val() + "no_image.png");
                } else {
                    $("[id$=imagenmostrar]").attr("src", $("[id$=hddURLPerfil]").val() + this.Foto.trim() + "?id=" + randomID(7));
                    $("[id$=nombreFoto]").val(this.Foto.trim());
                }
                if (this.Habilitado == 1) {
                    $("#rbtnHabilitado").click();
                } else {
                    if (this.Habilitado == 0) {
                        $("#rbtnNoHabilitado").click();
                    }
                }

                mostrarOcultarPaneles("panel2");
                $('#textUsuario').focus();
            });
        }
    });
}

function guardarDatos() {

    if (verificarCamposObligatorios()) {
        blockUIIn();
        var data = {};
        data["_apellido"] = $('#textApellido').val();
        data["_nombres"] = $('#textNombres').val();
        data["_imagen"] = $("[id$=nombreFoto]").val();
        if ($("#rbtnHabilitado").is(":checked")) {
            data["_habilitado"] = "true";
        } else {
            data["_habilitado"] = "false";
        }
        data["_legajo"] = $('#textLegajo').val();
        data["_usuario"] = $('#textUsuario').val();
        data["_pass"] = $('#textPass').val();
        data["_codUsuario"] = $('#hddCodRepositor').val();
        data["_dataFoto"] = $("[id$=dataFoto]").val();
        data["_tipoUsuario"] = $("#selectTipoUsuario").val();
        //console.log(JSON.stringify(data, null, "  "));
        //unblockUIOut();
        $.ajax({
            type: "POST",
            url: 'AdminUsuarios.aspx/GuardarUsuario',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (respuesta) {
                console.log(respuesta.d);
                if (respuesta.d != "-1") {
                    if ($("[id$=hddCodUsuarioActivo]").val() == $('#hddCodRepositor').val()) {
                        if ($("[id$=nombreFoto]").val() != "") {
                            foteador($('#hddURLPerfil').val() + $('#hddCodRepositor').val() + ".jpg?id=" + Math.floor((Math.random() * 100) + 1));
                        } else {
                            foteador($('#hddURLPerfil').val() + "no_image.png?id=" + Math.floor((Math.random() * 100) + 1));
                        }
                    }
                    showSuccessMessage('Usuario guardado');
                    clearFrmPersona(true);
                    cargarTablaRepositores();
                    mostrarOcultarPaneles('panel1');
                } else {
                    showInfoMessage('Revise los datos. No se guardaron los cambios');
                }
                unblockUIOut();
            }
        });
    }
}

function cargarTablaRepositores() {
    blockUIIn();
    var data = {};
    $.ajax({
        type: "POST",
        url: 'AdminUsuarios.aspx/CargarTablaUsuarios',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            console.log(this.CodRepositor);
            table.clear().draw();
            var result = JSON.parse(respuesta.d);
            $.each(result, function () {
                var fila = new Array();
                if (this.Habilitado) {
                    fila.push(this.CodUsuario);
                    fila.push(this.Legajo);
                    fila.push(this.Apellido);
                    fila.push(this.Nombre);
                    fila.push(this.Usuario);
                    fila.push("<center>" + this.TipoUsuario + "</center>");
                    fila.push(
                        '<center><button type="button" class="btn btn-success btn-xs" value="datos" title="Modificar Datos"><span class="glyphicon glyphicon-pencil"></span></button>' +
                        '<button type="button" class="btn btn-success btn-xs" onclick="quitarUsuario(\'' + this.CodUsuario + '\',\'' + this.Apellido + " " + this.Nombre + '\')" style="margin-left: 5px;" title="Deshabilitar Usuario"><span class="glyphicon glyphicon-trash"></span></button>' +
                        '</center>');
                    //var filaInsertar = "<tr><td>" + this.CodProducto + "</td><td>" + this.EAN + "</td><td>" + this.Nombre + "</td><td>" + this.Marca + "</td><td>" + this.Presentacion + "</td></tr>";
                    //console.log(this.CodProducto);
                    table.row.add(fila);
                } else {
                    fila.push(this.CodUsuario);
                    fila.push(this.Legajo);
                    fila.push(this.Apellido);
                    fila.push(this.Nombre);
                    fila.push(this.Usuario);
                    fila.push("<center>" + this.TipoUsuario + "</center>");
                    fila.push(
                        '<center><button type="button" class="btn btn-success btn-xs" value="datos" title="Modificar Datos"><span class="glyphicon glyphicon-pencil"></span></button>' +
                        '<button type="button" class="btn btn-success btn-xs" onclick="habilitarUsuario(\'' + this.CodUsuario + '\',\'' + this.Apellido + " " + this.Nombre + '\')" style="margin-left: 5px;" title="Habilitar Usuario"><span class="glyphicon glyphicon-ok"></span></button>' +
                        '</center>');
                    //var filaInsertar = "<tr><td>" + this.CodProducto + "</td><td>" + this.EAN + "</td><td>" + this.Nombre + "</td><td>" + this.Marca + "</td><td>" + this.Presentacion + "</td></tr>";
                    //console.log(this.CodProducto);
                    table.row.add(fila);
                }

            });
            table.draw();
            unblockUIOut();


        }
    });
}

function getTiposUsuario() {
    var data = {};
    $.ajax({
        type: "POST",
        url: 'AdminUsuarios.aspx/GetTiposUsuario',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {

            var result = JSON.parse(respuesta.d);
            $.each(result, function () {
                $("#selectTipoUsuario").append("<option value='" + this.CodTipoUsuario + "'>" + this.descripcion + "</option>");
            });
            table.draw();



        }
    });
}


//funciones para generar excel
var cookie;
function generarExcel() {
    document.cookie = 'termino=error; path=/';
    blockUIIn();
    cookie = window.setInterval(function () {
        var cookieValue = getCookie('termino');
        if (cookieValue == "termino") {
            finishDownload();
        }
    }, 1000);
    document.getElementById('btnExcelear').click();
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function finishDownload() {
    window.clearInterval(cookie);
    document.cookie = 'termino=error; path=/';
    unblockUIOut();
}

//FIN funciones para generar excel

function getUsuarioByCod() {
    blockUIIn();
    var data = {};
    data["comando"] = 'GetAllSalidasByFormAndUsuario';
    data["codFormulario"] = $('#hddCodFormulario').val();
    data["nombreFormulario"] = $('#hddNombreFormulario').val();
    data["codUsuario"] = $('#hddUserSelected').val();
    data["codVersion"] = $('#hddVersion').val();
    $.ajax({
        type: "POST",
        url: '../../WebService/Filtros.asmx/cargarComboVersiones',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            if ($("#SalidasAsign").html() != "") {
                $("#SalidasAsign").fancytree("destroy")
            }
            var result = JSON.parse(respuesta.d);

            $('#hddCodPadre').val(result.key)

            $("#SalidasAsign").fancytree({
                checkbox: true,
                selectMode: 3,
                icons: false,
                source: result,
                extensions: ["filter"],
                quicksearch: true,
                filter: {
                    autoApply: false,
                    counter: false,
                    fuzzy: false,
                    hideExpandedCounter: false,
                    highlight: false,
                    mode: "hide"
                },
                select: function (event, data) {
                    selKeys = $.map(data.tree.getSelectedNodes(), function (node) {
                        return node.key;
                    });
                    var selRootNodes = data.tree.getSelectedNodes(true);
                    var selRootKeys = $.map(selRootNodes, function (node) {
                        return node.parent.key;
                    });
                    selKeys = _.union(selKeys, selRootKeys);
                },
                dblclick: function (event, data) {
                    data.node.toggleSelected();
                },
                keydown: function (event, data) {
                    if (event.which === 32) {
                        data.node.toggleSelected();
                        return false;
                    }
                },
                cookieId: "fancytree-Cb3",
                idPrefix: "fancytree-Cb3-"
            });
            var tree = $("#SalidasAsign").fancytree("getTree");
            $('#search').keyup(function (e) {
                var n,
                    opts = {
                        autoExpand: true
                    },
                    match = $(this).val();
                n = tree.filterNodes(match, opts);
            }).focus();
            unblockUIOut();
        }
    });
}

//Bloquea página.
function blockUIIn() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }
    $(parent.$("form")).block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}
//Desbloquea página.
function unblockUIOut(el) {
    $(parent.$("form")).unblock();
}

function quitarUsuario(codUsuario, nombre) {
    $("#hhdCodUsuarioDel").val(codUsuario);
    $("#lblNombre").html(nombre);
    $("#deleteDialog").modal("show");
}

function habilitarUsuario(codUsuario, nombre) {
    $("#hhdCodUsuarioHab").val(codUsuario);
    $("#lblNombreUsuarioHab").html(nombre);
    $("#habilitarDialog").modal("show");
}

function onQuitarUsuario() {
    var codUsuario = $("#hhdCodUsuarioDel").val();
    $.ajax({
        type: "POST",
        url: 'AdminUsuarios.aspx/DeshabilitarUsuario',
        data: JSON.stringify({ CodUsuario: codUsuario }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $("#deleteDialog").modal("hide");
            showSuccessMessage("Usuario deshabilitado");
            cargarTablaRepositores();
        }
    });
}

function onHabilitarUsuario() {
    var codUsuario = $("#hhdCodUsuarioHab").val();
    $.ajax({
        type: "POST",
        url: 'AdminUsuarios.aspx/HabilitarUsuario',
        data: JSON.stringify({ CodUsuario: codUsuario }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $("#habilitarDialog").modal("hide");
            showSuccessMessage("Usuario habilitado");
            cargarTablaRepositores();
        }
    });
}