﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="RuteoOld.aspx.cs" Inherits="AppAndroid.Master.Ruteo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="<%= ResolveClientUrl("~/js/plugins/datatable/js/jquery.dataTables.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/js/plugins/datatable/js/dataTables.bootstrap.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/js/plugins/datatable/css/dataTables.bootstrap.css")%>" rel="stylesheet" type="text/css" />

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jQuerypreviewmaster/jquery.preimage.js")%>"></script>
    <!--cropper.js-->
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.css")%>" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/Ruteo.js")%>"></script>

    <script src="<%= ResolveClientUrl("~/LTE/plugins/input-mask/jquery.inputmask.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/LTE/plugins/input-mask/jquery.inputmask.date.extensions.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/LTE/plugins/input-mask/jquery.inputmask.extensions.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/maps.js")%>"></script>

    <link href="../JS/plugins/pgwslider/pgwslider.css" rel="stylesheet" />
    <script src="../JS/plugins/pgwslider/pgwslider.js"></script>

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css")%>" rel="stylesheet" />

     <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/gmaps.js")%>"></script>

    <script src="<%= ResolveClientUrl("~/JS/Varios.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/RuteoOld.js")%>"></script>

    <style type="text/css">
        table.dataTable {
            border-collapse: collapse !important;
            width: 100% !important;
        }

        .filaSeleccionada {
            background-color: #e2e8eb !important;
        }

        th {
            background-color: #FFF !important;
        }

        .dataTables_wrapper .boton {
            float: right;
            padding-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <asp:HiddenField runat="server" ID="hddIdPuntoVenta" ClientIDMode="Static" />
    <section class="content-header">
        <h1>
            <i class="fa fa-location-arrow"></i> Ruteos
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="#">Menu</a></li>
            <li class="active">Ruteos</li>
        </ol>
    </section>
    <br />

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Búsqueda</b> de Ruteos</h3>
            <!--
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
            </div>
            -->
        </div>
        <div class="box-body" id="panelFiltrarRuteo">

            <div class="row" >
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Punto de Venta</label>
                        <select class="form-control" id="optCodPuntoVenta">
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Repositor</label>
                        <select class="form-control" id="optCodRepositor">
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Fecha desde:</label>
                        <div class="input-group">
                            <%--<div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>--%>
                            <input type="text" class="form-control" id="txtFechaDesde" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Fecha hasta:</label>
                        <div class="input-group">
                            <%--<div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>--%>
                            <input type="text" class="form-control" id="txtFechaHasta" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                </div>
            </div>
            <div class="box-footer">
                <button class="btn btn-primary pull-right" type="button" onclick="cargarRuteosDias()">Ver Ruteo</button>
            </div>
        </div>
    </div>
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h2 class="box-title">Ruteo Seleccionado</h2>
            <!--
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
            </div>
            -->
        </div>
        <div class="box-body" id="panelDetalleRuteo">
            <div class="row">

                <div class="col-md-12">

                    <div class="form-group col-md-10" style="text-align: center">
                        <label id="lblTituloRuteo">
                            <h3 style="margin-top: 1px;"><b>Ruteos:</b> <span id="fechaDesde"></span> al <span id="fechaHasta"></span></h3>
                        </label>
                    </div>
                    <div class="box-footer col-md-2">
                        <button class="btn btn-primary pull-right" type="button" onclick="onVolver()">Volver</button>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Widget: user widget style 1 -->
                                    <div class="box box-widget widget-user">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                        <div class="widget-user-header bg-aqua-active">
                                            <h3 class="widget-user-username" id="nombreRepositor"></h3>
                                            <h5 class="widget-user-desc">Repositor</h5>
                                        </div>
                                        <div class="widget-user-image">
                                            <img class="img-circle" id="imageRepositor" src="../Recursos/Profiles/no_image.png" alt="Repositor Avatar">
                                        </div>
                                        <div class="box-footer">
                                            <div class="row">
                                                <div class="col-sm-2 border-right">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Total Ruteos</h5>
                                                        <span class="description-text" id="dgTotalRuteos">0</span>
                                                    </div>
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-sm-2 border-right">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Ruteos Propios</h5>
                                                        <span class="description-text" id="dgRuteosPropios">0</span>
                                                    </div>
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-sm-3">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Ruteos Competencia</h5>
                                                        <span class="description-text" id="dgRuteosComp">0</span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Puntos Ventas Trabajados</h5>
                                                        <span class="description-text" id="dgTotalPV">0</span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-2">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Días Trabajados</h5>
                                                        <span class="description-text" id="dgTotalDias">0</span>
                                                    </div>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <!-- Widget: user widget style 1 -->
                            <div class="box box-widget widget-user-2">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-yellow">

                                    <!-- /.widget-user-image -->
                                    <h3 style="margin-left: 0px;" class="widget-user-username">Punto de Venta: <span id="nombrePV"></span></h3>
                                    <h5 style="margin-left: 0px;" class="widget-user-desc">Resultados: <span id="totalRuteos"></span></h5>
                                </div>
                                <div class="box-footer no-padding" style="max-height: 650px; min-height: 100px;">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <center><span style="text-align: center;font-size:18px"><b>Días</b></span></center>
                                            <div id="contentListadoDias" style="padding-right: 17px; padding-bottom: 10px; overflow-y: scroll;" class="scrollbar-hidden">
                                                <ul class="nav nav-stacked"  id="listadoRuteosDias">
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <center><span style="text-align: center;font-size:18px"><b>Puntos de Venta</b></span></center>
                                            <div id="contentListadoPuntosVentas" style="padding-right: 17px; padding-bottom: 10px; overflow-y: scroll;" class="scrollbar-hidden">
                                                <ul class="nav nav-stacked" id="listadoRuteosPV">
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <center><span style="text-align: center;font-size:18px"><b>Ruteos</b></span></center>
                                            <div id="contentListadoRuteos" style="padding-right: 17px; padding-bottom: 10px; overflow-y: scroll;" class="scrollbar-hidden">
                                                <ul class="nav nav-stacked" id="listadoRuteos">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.widget-user -->
                        </div>
                        <!-- /.col -->

                        <!-- datos producto -->
                        <div class="col-md-12" id="rContDatosProd">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Widget: user widget style 1 -->
                                    <div class="box box-widget widget-user">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                        <div class="widget-user-header bg-green-active">
                                            <h3 class="widget-user-username"> <span id="rNombre">Datos producto </span>  - <strong id="rMarca"></strong> </h3>
                                            <h5 class="widget-user-desc"> <span  id="rPresentacion"></span></h5>
                                        </div>
                                        <div class="widget-user-image">
                                            <img class="img-circle" id="rImagen" src="../Images/azucar.jpg" alt="Imagen del Producto">
                                        </div>
                                        <div class="box-footer">
                                            <div class="row">
                                                <div class="col-sm-4 border-right">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Dia</h5>
                                                        <span class="description-text" id="rDia"> --/--/--</span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4 border-right">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Empresa</h5>
                                                        <span class="description-text" id="rEmpresa">Empresa</span>
                                                    </div>
                                                </div>

                                                 <div class="col-sm-4 border-right">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Tipo Cliente</h5>
                                                        <span class="description-text" id="rTipoCliente">-- ----</span>
                                                    </div>
                                                </div>


                                                <div class="col-sm-4 border-right">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Producto</h5>
                                                        <span class="description-text" id="rPropio">Propio</span>
                                                    </div>
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-sm-4 border-right">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Precio</h5>
                                                        <span class="description-text" id="rPrecio">$ 0.0</span>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-sm-4">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Ubicación</h5>
                                                        <span class="description-text" id="rDireccion">Sin Definir</span> <br>
                                                        <span class="description-text" id="rCiudad">Sin Definir</span> <br>
                                                        <span class="description-text" id="rProvincia">Sin Definir</span> <br>
                                                        <span class="description-text" id="rZona">Sin Definir</span> <br>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="description-block">
                                                        <h5 class="description-header">Observación</h5>
                                                        <span class="description-text" style="text-align: left" id="rObservacion"></span></div>
                                                </div>
                                            </div>

                                            <div class="row">
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            </div>

                        <!-- /.col -->

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">Ubicación:</label>
                                    <div id="map_canvas" style="height: 260px"></div>
                                    <div>
                                        <br>
                                        <span>GPS: <strong id="rGPS"></strong></span> <br>
                                        <span>Presición: <strong id="rPresicion"></strong> </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="">Foto:</label>
                                    <div>
                                        <div class="img-thumbnail">
                                            <img src="" class="img-responsive" id="rFotoRuteo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
