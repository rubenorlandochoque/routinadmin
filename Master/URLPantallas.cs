﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAndroid.Master
{
    public class URLPantallas
    {
        private int _IdPantalla;
        private string _CodPantalla;
        private string _Pantalla;
        private string _Desccripcion;
        private bool _Activo;
        private bool _PermisoEscritura;
        private string _Icon;
        private string _Url;

        public int IdPantalla { get { return (int)_IdPantalla; } set { _IdPantalla = value; } }
        public string CodPantalla { get { return (string)_CodPantalla; } set { _CodPantalla = value; } }
        public string Pantalla { get { return (string)_Pantalla; } set { _Pantalla = value; } }
        public string Desccripcion { get { return (string)_Desccripcion; } set { _Desccripcion = value; } }
        public bool Activo { get { return (bool)_Activo; } set { _Activo = value; } }
        public bool PermisoEscritura { get { return (bool)_PermisoEscritura; } set { _PermisoEscritura = value; } }
        public string Icon { get { return (string)_Icon; } set { _Icon = value; } }
        public string Url { get { return (string)_Url; } set { _Url = value; } }

    }
}