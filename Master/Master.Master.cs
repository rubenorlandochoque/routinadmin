﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Master;
using NPOI.SS.UserModel;

namespace AppAndroid.Master
{
    public partial class Master : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblNombreUsuario.Text = Sesion.getNombreUsuario();
            lblNombreUsuario1.Text = Sesion.getNombreUsuario();
            lblNombreUsuario2.Text = Sesion.getNombreUsuario();
            lblApellido.Text = Sesion.getApellidoUsuario();
            lblTipoUsuario1.Text = Proced.FirstCharToUpper(Sesion.getTipoUsuario());
            lblTipoUsuario2.Text = Proced.FirstCharToUpper(Sesion.getTipoUsuario());
            
            string foto = Sesion.getFoto();
            if (foto == "")
            {
                foto = "no_image.png";
            }
            hddURLPerfil.Value = Sesion.getURLPerfil();
            hddURLProductos.Value = Sesion.getURLProductos();
            hddURLPV.Value = Sesion.getURLPV();
            hddCodUsuarioActivo.Value = Sesion.getCodUsuario();
			hddTipoUsuarioActivo.Value = Sesion.getTipoUsuario();
            hddPermisos.Value = Sesion.getPermisos();
            hddURLFotos.Value = Sesion.getURLFotos();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "initMap", "foteador('" + Sesion.getURLPerfil() + foto + "');", true);
        }
    }
}