﻿using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AppAndroid.Master
{
    public partial class Configuracion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string codPantalla = "P_CONFIGURACION";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
            else { 
                Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            
                const string stSqlCommand = "dbo.RUT_GetConfiguracion";
                DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
                db.AddOutParameter(dbCommand, "@FrecTrk", DbType.String, 10);
                db.AddOutParameter(dbCommand, "@FrecSync", DbType.String, 10);
                db.ExecuteNonQuery(dbCommand);

                frecuenciaTracking.Value = db.GetParameterValue(dbCommand, "@FrecTrk").ToString();
                frecuenciaSincronizacion.Value = db.GetParameterValue(dbCommand, "@FrecSync").ToString();
            }
        }


       [WebMethod]
        public static string GuardarConfiguracion(string FrecSync, string FrecTrk) {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");

            const string stSqlCommand = "dbo.RUT_SetConfiguracion";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@FrecTrk", DbType.String, FrecTrk);
            db.AddInParameter(dbCommand, "@FrecSync", DbType.String, FrecSync);
            db.ExecuteNonQuery(dbCommand);
            return "ok";
        }

    }
}