﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="PV.aspx.cs" Inherits="AppAndroid.Master.PV" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/jquery.dataTables.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.css")%>" rel="stylesheet" type="text/css" />

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jQuerypreviewmaster/jquery.preimage.js")%>"></script>
    <!--cropper.js-->
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.css")%>" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.js")%>"></script>
    
    <script src="<%= ResolveClientUrl("~/JS/Varios.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/PV.js")%>"></script>

    <style type="text/css">
        table.dataTable {
            border-collapse: collapse !important;
            width: 100% !important;
        }

        .filaSeleccionada {
            background-color: #e2e8eb !important;
        }

        th {
            background-color: #FFF !important;
        }

        .dataTables_wrapper .boton {
            float: right;
            padding-left: 10px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <asp:HiddenField runat="server" ID="hddCodPV" ClientIDMode="Static" />
    <section class="content-header">
        <h1>
            <i class="fa fa-truck"></i> Puntos de Venta
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="#">Menu</a></li>
            <li class="active">Puntos de Venta</li>
        </ol>
    </section>
    <br />

    <div class="box" id="panelListado">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Listado</b> de Puntos de Venta</h3>
            <div class="box-tools pull-right" style="display: none;">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse" id="btnCollapseListado"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">            

            <div class="row">
                <div class="col-xs-12">
                    <div class="">
                        <table id="tblPersonas" class="table table-bordered table-striped table-hover table-condensed" style="width: 100% !important">
                            <thead>
                                <tr>
                                    <th><center>CodPV</center></th>
                                    <th><center>Cliente</center></th>
                                    <th><center>Sucursal</center></th>
                                    <th><center>Dirección</center></th>
                                    <th><center>Ciudad</center></th>
                                    <th><center>Provincia</center></th>
                                    <th><center>Zona</center></th>
                                    <th><center>Coordenadas</center></th>
                                    <th><center>Tipo Cliente</center></th>
                                    
                                    <th style="width: 60px;"><center>Acciones</center></th>
                                </tr>
                            </thead>
                            <tbody id="listadoPV">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <asp:LinkButton ID="btnExcelear" runat="server" Style="display: block;" OnClick="btnExcelear_Click" ClientIDMode="Static"></asp:LinkButton>
        <div class="box-footer text-right">
            <button class="btn btn-primary" type="button" onclick="mostrarNuevoPV()">Agregar Punto de Venta</button>
            <%--<button class="btn btn-primary" type="button" onclick="verAsignaciones()">Asignar a Repositores</button>--%>
            <button class="btn btn-primary" type="button" onclick="generarExcel()">Exportar Excel</button>
        </div>
    </div>
    <!-- Default box -->
    <div class="box" >
        <div class="box-header with-border">
            <h3 class="box-title"><label id="lblNuevoEdicion" >Nuevo</label> Punto de Venta</h3>
            <div class="box-tools pull-right" style="display: none;">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse" id="btnCollapseNuevo"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body" id="panelNuevoRep">
            <div class="row">
                <div class="col-md-9">                    
                    <div class="row">                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textNombre">Cliente</label>
                                <input type="text" class="form-control" id="textNombre" placeholder="" maxlength="65">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textCodSuc">Sucursal</label>
                                <input type="text" class="form-control" id="textCodSuc" placeholder="" maxlength="50">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textDireccion">Dirección</label>
                                <input type="text" class="form-control" id="textDireccion" placeholder="" maxlength="61">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textCiudad">Ciudad</label>
                                <input type="text" class="form-control" id="textCiudad" placeholder="" maxlength="50">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textProvincia">Provincia</label>
                                <input type="text" class="form-control" id="textProvincia" placeholder="" maxlength="50">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textZona">Zona</label>
                                <input type="text" class="form-control" id="textZona" placeholder="" maxlength="50">
                            </div>
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textLatitud">Latitud</label>
                                <input type="text" class="form-control" id="textLatitud" placeholder="" >
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textLongitud">Longitud</label>
                                <input type="text" class="form-control" id="textLongitud" placeholder="" >
                            </div>
                        </div>
                    
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="textTipoCliente">Tipo de Cliente</label>
                                <input type="text" class="form-control" id="textTipoCliente" placeholder="" maxlength="65">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-primary" type="button" id="btnLimpiarCoordenadas" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                                        Limpiar coordenadas
                                    </button>
                        </div>
                        <div class="col-md-4" style="display: none">
                             <label >Habilitado</label>
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="r1" class="minimal" value="true" id="rbtnHabilitado"> Sí
                                </label> &nbsp;&nbsp;
                                <label>
                                    <input type="radio" name="r1" class="minimal" value="false" id="rbtnNoHabilitado"> No
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center">
                            <asp:Image alt="" class="img-thumbnail " ID="imagenmostrar" runat="server" />
                            <asp:HiddenField ID="dataFoto" runat="server" />
                            <asp:HiddenField ID="nombreFoto" runat="server" />
                            <div class="form-group">
                                <div style="margin-top: 5px; text-align: center">
                                    <button class="btn btn-primary" type="button" id="btnBuscarFoto" onclick=" mostrarDialogoBuscarFoto() " title="Subir foto desde archivo" data-backdrop="static" data-keyboard="false">
                                        <i class="fa fa-upload"></i>
                                    </button>

                                    <span class="btn btn-primary btn-file btn-mini" style="display: none"><i class="icon-custom-chart"></i>
                                        <asp:FileUpload ID="FileUploadFotoPerfil" accept="image/*;capture=camera" runat="server" />
                                    </span>

                                    <button class="btn btn-primary" type="button" id="btnTomarFoto" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false" title="Tomar foto desde webcam">
                                        <i class="fa fa-camera"></i>
                                    </button>
                                    <span class="btn btn-primary btn-file btn-mini" style="display: none"><i class="icon-custom-chart"></i>
                                        <asp:FileUpload ID="FileUploadFichero" accept="*;" runat="server" />
                                    </span>
                                </div>
                            </div>
                        </div>


                        <!-- ------------------------------->
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div id="fSaveDatoRevisita">
                                        <div class="modal-header">
                                            <input type='button' id='btnpopup2' class='close' data-dismiss='modal' aria-hidden='true' value='x' style="display: none" />
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <br>
                                            
                                            <center>
                                            <h4 id="myModalLabel" class="semi-bold"><label id="lblTituloFoto"><b>Recortar Foto</b></label></h4>
                                            <p class="no-margin" id="lblTextoDescripcion">Recorte la foto y presione el botón 'Confirmar'.</p>
                                            <br />
                                            </center>
                                            
                                        </div>
                                        <div class="modal-body" id="bodyPopup">
                                            <div id="bodyTomarFoto">
                                                <div class="row form-row">
                                                    <div class="col-md-6">
                                                        <video id="video" width="260" height="280" autoplay></video>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <canvas id="canvas" style="padding-top: 40px;" width="260" height="200"></canvas>
                                                        <canvas id="canvasHidden" style="display: none;" width="1024" height="768"></canvas>
                                                        <img id='output' style="display: none;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="bodyEditarFoto" style="display: none">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div style="background-color: #f7f7f7; border: 1px solid #eee; box-shadow: inset 0 0 3px #f7f7f7; height: 480px; width: 100%; overflow: hidden">
                                                            <img class="cropper" src="" alt="Picture" style="display: none">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                    <div class="row eg-output">
                                                        <div class="col-md-12">
                                                            <div style="text-align: center;">
                                                                <button class="btn btn-primary btn-cons" id="getDataURL2" type="button" style="display: none">Aceptar</button>
                                                                <button class="btn btn-default" id="getDataURL" type="button" style="display: none">Cancelar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div id="botoneraTomarFoto">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrarModal">Cerrar</button>
                                                <button type="button" class="btn btn-primary" id="snap">Tomar Foto</button>
                                                <button type="button" class="btn btn-primary" id="btnConfirnarFoto">Confirmar</button>
                                            </div>
                                            <div id="botoneraEditarFoto" style="display: none">
                                                <input value="Aceptar" class="btn btn-primary btn-sm" type="button" onclick="actualizarFoto()" />
                                            </div>
                                        </div>
                                        <div>
                                            <img id="imgCaptura" style="display: none;" src="" />
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>


                    </div>
                </div>
            </div>            
            <div class="row">                
                <div class="col-xs-12">
                    <label for="">Mapa:</label>
                    <iframe id="iframe1" name="mapa" src="<%= ResolveClientUrl("~/js/plugins/map/index.html")%>" width="100%" height="400"></iframe>
                </div>                            
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button class="btn btn-primary pull-right" type="button" onclick="guardarDatos()" id="btnGuardar">Guardar</button>
                <button class="btn btn-white pull-right" type="button" onclick="$('#lblNuevoEdicion').text('Nuevo');mostrarOcultarPaneles('panel1')">Volver a Listado</button>
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- Modal -->

        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- /.box -->
    <div id="deleteDialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Deshabilitar Sucursal del Punto de Venta</h4>
                </div>
                <div class="modal-body">
                    <p>¿Está seguro que desea deshabilitar la Sucursal <span style="font-weight: bold;" id="lblNombre"></span> perteneciente al Punto de Venta <span style="font-weight: bold;" id="lblNombrePV"></span>?</p>
                    <input type="hidden" name="" id="hhdCodPVDel">
                    <input type="hidden" name="" id="hhdCodSucursalDel">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="onQuitarPV()">Si</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
