﻿using System.Diagnostics;
using System.Drawing;
using System.IO;
using AppAndroid.Master;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace Master
{
    public class Proced
    {

        public static DataTable getData(String _sp, object[,] _parametros)
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            string stSqlCommand = _sp;
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            dbCommand.CommandTimeout = 360;
            for (int i = 0; i < (_parametros.Length / 3); i++)
            {
                String tipo = _parametros[i, 1].ToString();
                TypeToDbType(tipo);
                db.AddInParameter(dbCommand,
                                _parametros[i, 0].ToString(),
                                TypeToDbType(tipo),
                                _parametros[i, 2]
                                );
            }
            DataTable dt = null;
            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;
        }

        public static DbType TypeToDbType(String T)
        {
            switch (T)
            {
                case "DbType.Int64":
                    return DbType.Int64;
                    break;
                case "DbType.Int32":
                    return DbType.Int32;
                    break;
                case "DbType.Int16":
                    return DbType.Int16;
                    break;
                case "DbType.Decimal":
                    return DbType.Decimal;
                    break;
                case "DbType.Double":
                    return DbType.Double;
                    break;
                case "DbType.Boolean":
                    return DbType.Boolean;
                    break;
                case "DbType.String":
                    return DbType.String;
                    break;
                case "DbType.DateTime":
                    return DbType.DateTime;
                    break;
                default:
                    return DbType.Object;
                    break;
            }
        }

        public static string FromDataRow(DataRow row)
        {
            DataColumnCollection cols = row.Table.Columns;
            string colDelimiter = "";
            StringBuilder result = new StringBuilder("{");
            for (int i = 0; i < cols.Count; i++)
            {
                result.Append(colDelimiter).Append("\"")
                      .Append(cols[i].ColumnName).Append("\":")
                      .Append(JSONValueFromDataRowObject(row[i], cols[i].DataType));
                colDelimiter = ",";
            }
            result.Append("}");
            return result.ToString();
        }

        private static Type[] numeric = new Type[] {typeof(byte), typeof(decimal), typeof(double), 
                                     typeof(Int16), typeof(Int32), typeof(SByte), typeof(Single),
                                     typeof(UInt16), typeof(UInt32), typeof(UInt64)};
        private static long EpochTicks = new DateTime(1970, 1, 1).Ticks;
        private static string JSONValueFromDataRowObject(object value, Type DataType)
        {
            if (value == DBNull.Value) return "null";
            if (Array.IndexOf(numeric, DataType) > -1)
                return value.ToString();
            if (DataType == typeof(bool))
                return ((bool)value) ? "true" : "false";
            if (DataType == typeof(DateTime))
                return "\"\\/Date(" + new TimeSpan(((DateTime)value).ToUniversalTime().Ticks - EpochTicks).TotalMilliseconds.ToString() + ")\\/\"";
            return "\"" + value.ToString().Replace(@"\", @"\\").Replace(Environment.NewLine, @"\n").Replace("\"", @"\""") + "\"";
        }
        public static string convertDate(string fecha, int limite)/*Limite = 0 fecha desde --- Limite = 1 fecha hasta*/
        {
            string fechaConvert = "";
            if (fecha == "")
            {
                if (limite == 0)
                {
                    fechaConvert = "1900-01-01";
                }
                else
                {
                    object[,] parametros = new object[0, 0] { };
                    DataTable dt = Proced.getData("Control.dbo.getFechActual", parametros);
                    DataRow dr = dt.Rows[0];
                    fechaConvert = dt.Rows[0]["fecha"].ToString();
                }
            }
            else
            {
                fechaConvert = fecha.Split('/')[2] + '-' + fecha.Split('/')[1] + '-' + fecha.Split('/')[0];
            }
            return fechaConvert;
        }

        public static void GuardarFotoPersona(string imagenBase64, string codPersona)
        {
            var array = imagenBase64.Split(',');
            System.Drawing.Image image = Base64ToImage(array[1]);
            Size newSize = CalculateDimensions(image.Size, 640);
            //image.Save("c://dddddd.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
            try
            {
                using (System.Drawing.Bitmap tempImage = new System.Drawing.Bitmap(image, newSize))
                //using (System.Drawing.Bitmap tempImage = new System.Drawing.Bitmap(image))
                {
                    /*
                    EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 5);
                    //create a collection of all parameters that we will pass to the encoder
                    EncoderParameters encoderParams = new EncoderParameters(1);
                    //set the quality parameter for the codec
                    encoderParams.Param[0] = qualityParam;
                    */
                    tempImage.Save(HttpContext.Current.Server.MapPath("~/Recursos/Profiles/" + codPersona + ".jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);

                }
                //Formulario.GuardarImagenPersona(codPersona + ".png", codPersona, codFormRealizado, codFormulario, codLocalizacion, codOrganizacion, codUsuario);
            }
            catch (Exception e)
            {
                Debug.WriteLine("DEBUG::LoadImages()::Error attempting to create image::" + e.Message);
            }

        }

		public static void GuardarFotoProducto(string imagenBase64, string codProducto)
        {
            var array = imagenBase64.Split(',');
            string path = Sesion.getURLProductosServer();

            System.Drawing.Image image = Base64ToImage(array[1]);
            Size newSize = CalculateDimensions(image.Size, 640);
            try
            {
                using (System.Drawing.Bitmap tempImage = new System.Drawing.Bitmap(image, newSize))
                {
                    //tempImage.Save(HttpContext.Current.Server.MapPath("~/Recursos/Productos/" + codProducto + ".jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);
                    tempImage.Save(HttpContext.Current.Server.MapPath(path + codProducto + ".jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("DEBUG::LoadImages()::Error attempting to create image::" + e.Message);
            }

        }

        public static void GuardarFotoPV(string imagenBase64, string codPersona)
        {
            var array = imagenBase64.Split(',');
            System.Drawing.Image image = Base64ToImage(array[1]);
            Size newSize = CalculateDimensions(image.Size, 640);
            //image.Save("c://dddddd.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
            try
            {
                using (System.Drawing.Bitmap tempImage = new System.Drawing.Bitmap(image, newSize))
                //using (System.Drawing.Bitmap tempImage = new System.Drawing.Bitmap(image))
                {
                    /*
                    EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 5);
                    //create a collection of all parameters that we will pass to the encoder
                    EncoderParameters encoderParams = new EncoderParameters(1);
                    //set the quality parameter for the codec
                    encoderParams.Param[0] = qualityParam;
                    */
                    tempImage.Save(HttpContext.Current.Server.MapPath("~/Recursos/PV/" + codPersona + ".jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);

                }
                //Formulario.GuardarImagenPersona(codPersona + ".png", codPersona, codFormRealizado, codFormulario, codLocalizacion, codOrganizacion, codUsuario);
            }
            catch (Exception e)
            {
                Debug.WriteLine("DEBUG::LoadImages()::Error attempting to create image::" + e.Message);
            }

        }
        public static Size CalculateDimensions(Size oldSize, int targetSize)
        {
            Size newSize = new Size();
            if (oldSize.Height > oldSize.Width)
            {
                newSize.Width = (int)(oldSize.Width * ((float)targetSize / (float)oldSize.Height));
                newSize.Height = targetSize;
            }
            else
            {
                newSize.Width = targetSize;
                newSize.Height = (int)(oldSize.Height * ((float)targetSize / (float)oldSize.Width));
            }
            return newSize;
        }
        public static System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        public static string FirstCharToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1).ToLower();

            return str.ToUpper();
        }
    }
}