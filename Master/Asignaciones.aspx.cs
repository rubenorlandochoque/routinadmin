﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace AppAndroid.Master
{
    public partial class Asignaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			string codPantalla = "P_PUNTOS_VENTAS";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");    
            }
        }

        public static DataTable GetRepositores(string _filtro)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            //string sqlCommand = "permisos.usp_GetUsuariosPorServidor";
            string sqlCommand = "dbo.REP_getAllRepositoresSearch";

            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Input parameters can specify the input value            
            db.AddInParameter(dbCommand, "@habilitado", DbType.String, "1");
            db.AddInParameter(dbCommand, "@search", DbType.String, _filtro);

            DataTable dt = null;
            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;

        }

        public static DataTable GetPuntosVenta(string _filtro)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            //string sqlCommand = "permisos.usp_GetUsuariosPorServidor";
            string sqlCommand = "sp_puntosventa";

            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Input parameters can specify the input value            
            db.AddInParameter(dbCommand, "@Comando", DbType.String, "SelectByNombre");
            db.AddInParameter(dbCommand, "@Filtro", DbType.String, _filtro);
            DataTable dt = null;
            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;

        }

        public static DataTable GetPuntosVentaByCod(string _filtro)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            //string sqlCommand = "permisos.usp_GetUsuariosPorServidor";
            string sqlCommand = "sp_puntosventa";

            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Input parameters can specify the input value            
            db.AddInParameter(dbCommand, "@Comando", DbType.String, "SelectByCod");
            db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, _filtro);
            DataTable dt = null;
            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;

        }

        public static DataTable GetPuntosVentaByRepositor(string _codUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            //string sqlCommand = "permisos.usp_GetUsuariosPorServidor";
            string sqlCommand = "dbo.PV_getSucursalesAsignadasByRepositor";

            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Input parameters can specify the input value                        
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, _codUsuario);
            DataTable dt = null;
            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;

        }

        [WebMethod]
        public static string GetListadoRepositoresByFilter(string _filtro)
        {
            DataTable dt = GetRepositores(_filtro);
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetListadoPuntosVentaByFilter(string _filtro)
        {
            DataTable dt = GetPuntosVenta(_filtro);
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetListadoPuntosVentaByCod(string _filtro)
        {
            DataTable dt = GetPuntosVentaByCod(_filtro);
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetListadoPVByRepositor(string _codUsuario)
        {
            DataTable dt = GetPuntosVentaByRepositor(_codUsuario);
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string AsignarRepositoresPV(string _codRepositor, string _listadoPVAsignar, string _listadoSucAsignar)
        {
            try
            {
                var _resultado = "";
                Database db = DatabaseFactory.CreateDatabase("ServerConnection");
                string stSqlCommand = "dbo.PV_AsignarRep_Sucursal";
                DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);                
                
                db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, _codRepositor);
                db.AddInParameter(dbCommand, "@CodPuntosVenta", DbType.String, _listadoPVAsignar);
                db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, _listadoSucAsignar);
                db.ExecuteNonQuery(dbCommand);

                _resultado = db.GetParameterValue(dbCommand, "@CodUsuario").ToString();
                                
                if (_resultado.Equals("-1"))
                {
                    return "[ERROR] : Se produjo un error en la insercion en fusion.";
                }
                return _resultado;
            }
            catch (Exception ex)
            {
                return "[ERROR] : " + ex.Message;
            }
        }
    }
}