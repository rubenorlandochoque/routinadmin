﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace AppAndroid.Master
{
    public class Sesion
    {
        public static void InicializarVariables()
        {
            HttpContext.Current.Session["Foto"] = "";
            HttpContext.Current.Session["CodUsuario"] = "";
            HttpContext.Current.Session["NombreUsuario"] = "";
            HttpContext.Current.Session["ApellidoUsuario"] = "";
            HttpContext.Current.Session["User"] = "";
            HttpContext.Current.Session["UrlActual"] = "";
            HttpContext.Current.Session["Encabezado"] = "";
            HttpContext.Current.Session["RelSeleccionado"] = "";
            HttpContext.Current.Session["ListaSecciones"] = "";
            HttpContext.Current.Session["TipoUsuario"] = "";
            HttpContext.Current.Session["CodRelevamiento"] = "";
            HttpContext.Current.Session["CodZona"] = "";
            HttpContext.Current.Session["CodGrupo"] = "";
            HttpContext.Current.Session["CodSistema"] = "";
			HttpContext.Current.Session["Permisos"] = "";
        }

        public static string getNombreSistema()
        {
            return "Ruteo de Precios";
        }
        public static void setCodUsuario(string codUsuario)
        {
            HttpContext.Current.Session["CodUsuario"] = codUsuario;
        }
        public static string getCodUsuario()
        {
            if (HttpContext.Current.Session["CodUsuario"] != null)
                return HttpContext.Current.Session["CodUsuario"].ToString();
            else
                return "";
        }
        public static void setFoto(string foto)
        {
            HttpContext.Current.Session["Foto"] = getAmbiente() + foto;
        }

        public static string getFoto()
        {
            if (HttpContext.Current.Session["Foto"] != null)
                return HttpContext.Current.Session["Foto"].ToString();
            else
                return "";
        }
        public static string getAmbiente()
        {

            string ambiente = System.Configuration.ConfigurationManager.AppSettings.Get("ambiente");
            string pathImagenes = "";

            if (ambiente == "Local")
                pathImagenes = System.Configuration.ConfigurationManager.AppSettings.Get("carpetaImagenLocal");
            else
                pathImagenes = System.Configuration.ConfigurationManager.AppSettings.Get("carpetaImagenNube");
            return pathImagenes;
        }
        public static string getAmbienteEditor()
        {
            string ambienteEditor = System.Configuration.ConfigurationManager.AppSettings.Get("base");
            string pathBase = "";
            switch (ambienteEditor)
            {
                case "Desarrollo":
                    pathBase = System.Configuration.ConfigurationManager.AppSettings.Get("editorDesarrollo");
                    break;
                case "Calidad":
                    pathBase = System.Configuration.ConfigurationManager.AppSettings.Get("editorCalidad");
                    break;
                case "Produccion":
                    pathBase = System.Configuration.ConfigurationManager.AppSettings.Get("editorProduccion");
                    break;
            }
            return pathBase;
        }

        public static string getURLPerfil()
        {
            string retorno = System.Configuration.ConfigurationManager.AppSettings.Get("urlPerfil");
            return retorno;
        }

        public static string getURLProductos()
        {
            string retorno = System.Configuration.ConfigurationManager.AppSettings.Get("urlProductos");
            return retorno;
        }

        public static string getURLProductosServer()
        {
            string retorno = System.Configuration.ConfigurationManager.AppSettings.Get("urlProductosServer");
            return retorno;
        }

        public static string getURLFotos()
        {
            string retorno = System.Configuration.ConfigurationManager.AppSettings.Get("pathFiles");
            return retorno;
        }

        public static string getURLFotosServer()
        {
            string retorno = System.Configuration.ConfigurationManager.AppSettings.Get("pathFilesServer");
            return retorno;
        }

        public static string getURLPV()
        {
            string retorno = System.Configuration.ConfigurationManager.AppSettings.Get("urlPV");
            return retorno;
        }
        public static string getUrlVirtual()
        {
            string urlVirtual = "";
            urlVirtual = System.Configuration.ConfigurationManager.AppSettings.Get("urlVirtual");
            return urlVirtual;
        }

        public static void setNombreUsuario(string nombreUsuario)
        {
            HttpContext.Current.Session["NombreUsuario"] = nombreUsuario;
        }
        public static string getNombreUsuario()
        {
            if (HttpContext.Current.Session["NombreUsuario"] != null)
                return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(HttpContext.Current.Session["NombreUsuario"].ToString().ToLower());
            else
                return "";
        }
        public static void setUser(string user)
        {
            HttpContext.Current.Session["User"] = user;
        }
        public static string getuser()
        {
            if (HttpContext.Current.Session["User"] != null)
                return HttpContext.Current.Session["User"].ToString();
            else
                return "";
        }
        public static void setApellidoUsuario(string apellidoUsuario)
        {
            HttpContext.Current.Session["ApellidoUsuario"] = apellidoUsuario;
        }
        public static string getApellidoUsuario()
        {
            if (HttpContext.Current.Session["ApellidoUsuario"] != null)
                return HttpContext.Current.Session["ApellidoUsuario"].ToString();
            return "";
        }
        public static void setUrlActual(string urlActual)
        {
            HttpContext.Current.Session["UrlActual"] = urlActual;
        }
        public static string getUrlActual()
        {
            if (HttpContext.Current.Session["UrlActual"] != null)
                return HttpContext.Current.Session["UrlActual"].ToString();
            else
                return "";
        }
        public static void setSeleccion(string seleccion)
        {
            HttpContext.Current.Session["RelSeleccionado"] = seleccion;
        }
        public static string getSeleccion()
        {
            if (HttpContext.Current.Session["RelSeleccionado"] != null)
                return HttpContext.Current.Session["RelSeleccionado"].ToString();
            else
                return "";
        }
        public static void setEncabezado(string encabezado)
        {
            HttpContext.Current.Session["Encabezado"] = encabezado;
        }
        public static string getEncabezado()
        {
            if (HttpContext.Current.Session["Encabezado"] != null)
                return HttpContext.Current.Session["Encabezado"].ToString();
            else
                return "";
        }
        public static void setListaSecciones(List<URLPantallas> listaSecciones)
        {
            HttpContext.Current.Session["ListaSecciones"] = listaSecciones;
        }
        public static List<URLPantallas> getListaSecciones()
        {
            if (HttpContext.Current.Session["ListaSecciones"] != null)

                return (List<URLPantallas>)HttpContext.Current.Session["ListaSecciones"];
            else
                return null;
        }
        public static void setTipoUsuario(string tipoUsuario)
        {
            HttpContext.Current.Session["TipoUsuario"] = tipoUsuario;
        }
        public static string getTipoUsuario()
        {
            if (HttpContext.Current.Session["TipoUsuario"] != null)
                return HttpContext.Current.Session["TipoUsuario"].ToString();
            else
                return "";
        }
        public static void setCodRelevamiento(string codRelevamiento)
        {
            HttpContext.Current.Session["CodRelevamiento"] = codRelevamiento;
        }
        public static string getCodRelevamiento()
        {
            if (HttpContext.Current.Session["CodRelevamiento"] != null)
                return HttpContext.Current.Session["CodRelevamiento"].ToString();
            else
                return "";
        }
        public static void setCodZona(string codZona)
        {
            HttpContext.Current.Session["CodZona"] = codZona;
        }
        public static string getCodZona()
        {
            if (HttpContext.Current.Session["CodZona"] != null)
                return HttpContext.Current.Session["CodZona"].ToString();
            else
                return "";
        }
        public static void setCodGrupo(string codGrupo)
        {
            HttpContext.Current.Session["CodGrupo"] = codGrupo;
        }
        public static string getCodGrupo()
        {
            if (HttpContext.Current.Session["CodGrupo"] != null)
                return HttpContext.Current.Session["CodGrupo"].ToString();
            else
                return "";
        }
        public static void setCodSistema(string codSistema)
        {
            HttpContext.Current.Session["CodSistema"] = codSistema;
        }
        public static string getCodSistema()
        {
            if (HttpContext.Current.Session["CodSistema"] != null)
                return HttpContext.Current.Session["CodSistema"].ToString();
            else
                return "";
        }
        public static void setCodAplicacion(string codAplicacion)
        {
            HttpContext.Current.Session["CodAplicacion"] = codAplicacion;
        }
        public static string getCodAplicacion()
        {
            if (HttpContext.Current.Session["CodAplicacion"] != null)
                return HttpContext.Current.Session["CodAplicacion"].ToString();
            else
                return "";
        }

		public static void setPermisos(string codUsuario)
        {
            HttpContext.Current.Session["Permisos"] = Usuario.GetPermisos(codUsuario);
        }

        public static string getPermisos()
        {
            //if (HttpContext.Current.Session != null)
            if (HttpContext.Current.Session["Permisos"] != null) 
            {
                return HttpContext.Current.Session["Permisos"].ToString();
            }
            else
            {
                return "";
            }
        }

        public static bool isLogin() {
            return false;
        }

        public static bool isAccess(string pantalla)
        {
            if (Sesion.getPermisos().IndexOf(pantalla) != -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}