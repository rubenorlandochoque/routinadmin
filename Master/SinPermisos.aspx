﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="SinPermisos.aspx.cs" Inherits="AppAndroid.Master.SinPermisos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Acceso Denegado
          </h1>
        
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"><img src="../Recursos/Images/notAllowed.png"></h2>
            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i>Usted no tiene acceso a esta página.</h3>
                <p>
                    Comuniquese con el Administrador del Sitio
             
                </p>
                <form class="search-form">
                    <%--<div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    --%><!-- /.input-group -->
                </form>
            </div>
            <!-- /.error-content -->
        </div>
        <!-- /.error-page -->
    </section>
    <!-- /.content -->


</asp:Content>
