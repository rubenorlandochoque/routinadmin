﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.Record.Formula.Eval;
using NPOI.HSSF.Record.Formula.Functions;
using Master;
using AppAndroid.Master;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;

namespace AppAndroid
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Sesion.getTipoUsuario().ToUpper() != "ADMIN")
            {
                footerButton.InnerHtml = "";
            }
        }

        [WebMethod]
        public static string Sincronizar()
        {
            
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.SYNC_Sincronizar";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.ExecuteNonQuery(dbCommand);
            
            return "done";
        }

        [WebMethod]
        public static string GetFechaSincronizacion()
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.SYNC_GetUltimaSincronizacion";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);            

            DataTable dt = null;
            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetTablero()
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.MAIN_GetTablero";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            DataTable dt = null;
            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }
        protected void BtnExcelear_Click(object sender, EventArgs e)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            try
            {
                Style s = new Style();
                s.BackColor = System.Drawing.Color.Black;
                s.ForeColor = System.Drawing.Color.White;
                Style s2 = new Style();
                s2.BackColor = System.Drawing.Color.Gray;
                s2.ForeColor = System.Drawing.Color.White;

                String actualTime = DateTime.Now.ToString("ddMMyyyy_HHmmss");
                string tiempo = actualTime;
                string sqlCommand = "dbo.usp_Reporte_Comparativo";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
                DataTable DtExcel = null;
                DtExcel = db.ExecuteDataSet(dbCommand).Tables[0];
                if (DtExcel == null || DtExcel.Columns.Count == 0)
                    throw new Exception("ExportToExcel: Nula o vacia la tabla.");
                DataTable dtExportar = DtExcel;
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page pagina = new Page();
                HtmlForm form = new HtmlForm();
                GridView dg = new GridView();
                dg.EnableViewState = false;
                dg.DataSource = dtExportar;
                dg.DataBind();
                dg.HeaderRow.ApplyStyle(s);
                foreach ( GridViewRow r in dg.Rows)
                {
                    if (r.Cells[1].Text.Replace("&nbsp;","") != "")
                        r.ApplyStyle(s2);
                }
                //dg.Columns[0].ItemStyle.BackColor= System.Drawing.Color.Black;
                //dg.Columns[0].ControlStyle.ForeColor=System.Drawing.Color.White;
                pagina.EnableEventValidation = false;
                pagina.DesignerInitialize();
                pagina.Controls.Add(form);
                form.Controls.Add(dg);
                pagina.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=ReporteComparativo_" + tiempo + ".xls");
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Response.Write(sb.ToString());
                Response.End();
            }
            catch (Exception exp)
            {
                Response.Cookies.Remove("termino");
                Response.AppendCookie(new HttpCookie("termino", "termino"));
                string echo = exp.ToString();
            }
        }
    }
}