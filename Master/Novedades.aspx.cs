﻿using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;

namespace AppAndroid.Master
{
    public partial class Novedades1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string codPantalla = "P_NOVEDADES";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
        }


        [WebMethod]
        public static string GetAllRepositores()
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.REP_getAllRepositoresByUser";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, Sesion.getCodUsuario());

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetAllPuntosVentas()
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            const string stSqlCommand = "dbo.PV_getAllPuntosVenta";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string FiltrarNovedades(string CodRepositor, string CodPuntoVenta, string CodSucursal, string CodProducto, string CodTipoNovedad, string FechaInicio, string FechaFin)
        {
            FechaInicio = FechaInicio.Replace("/", "-");
            FechaFin = FechaFin.Replace("/", "-");

            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.NOV_GetListNovedades");
            db.AddInParameter(dbCommand, "@Repositor", DbType.String, CodRepositor);
            db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, CodPuntoVenta);
            db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, CodSucursal);
            db.AddInParameter(dbCommand, "@CodArticulo", DbType.String, CodProducto);
            db.AddInParameter(dbCommand, "@CodTipoNovedad", DbType.String, CodTipoNovedad);
            db.AddInParameter(dbCommand, "@FechaIni", DbType.String, FechaInicio);
            db.AddInParameter(dbCommand, "@FechaFin", DbType.String, FechaFin);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetDatosNovedad(String CodNovedad)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.RUT_GetDetalleNovedad";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@codNovedad", DbType.String, CodNovedad);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        [WebMethod]
        public static string GetTiposNovedad()
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.NOV_ListTiposNovedad");
            
            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetFotosNovedad(string CodNovedad)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.NOV_GetFotosNovedad");
            db.AddInParameter(dbCommand, "@CodNovedad", DbType.String, CodNovedad);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        protected void btnExcelear_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = null;
                Database db = DatabaseFactory.CreateDatabase("ServerConnection");
                string sqlCommand = "dbo.NOV_GetListNovedadesExcel";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
                // Add paramters
                // Input parameters can specify the input value               
                db.AddInParameter(dbCommand, "@CodPuntoVenta", DbType.String, hddCodPVNovedad.Value.ToString());
                db.AddInParameter(dbCommand, "@CodSucursal", DbType.String, hddCodSucNovedad.Value.ToString());
                db.AddInParameter(dbCommand, "@CodArticulo", DbType.String, hddCodArticuloNovedad.Value.ToString());
                db.AddInParameter(dbCommand, "@Repositor", DbType.String, hddRepositorNovedad.Value.ToString());
                db.AddInParameter(dbCommand, "@CodTipoNovedad", DbType.String, hddCodTipoNovedad.Value.ToString());
                db.AddInParameter(dbCommand, "@FechaIni", DbType.String, hddFechaIniNovedad.Value.ToString());
                db.AddInParameter(dbCommand, "@FechaFin", DbType.String, hddFechaFinNovedad.Value.ToString());
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                DataTable dtExportar = dt;
                string fechaHora = DateTime.Now.ToString("dd-MM-yyyy");
                string filename = "Listado-Novedades-" + fechaHora + ".xls";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();
                /*HSSFWorkbook workbook = new HSSFWorkbook();
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Reporte de Encuestas");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);*/
                HSSFWorkbook workbook = new HSSFWorkbook();
                var style = workbook.CreateCellStyle();
                style.FillForegroundColor = HSSFColor.GREY_25_PERCENT.index;
                style.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuente = workbook.CreateFont();
                fuente.Color = HSSFColor.RED.index;
                fuente.FontName = "Calibri";
                fuente.FontHeightInPoints = 12;
                style.SetFont(fuente);
                var styleHeader = workbook.CreateCellStyle();
                styleHeader.FillForegroundColor = HSSFColor.GREY_80_PERCENT.index;
                styleHeader.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuenteHead = workbook.CreateFont();
                fuenteHead.Color = HSSFColor.WHITE.index;
                fuenteHead.FontName = "Calibri";
                fuenteHead.FontHeightInPoints = 12;
                styleHeader.SetFont(fuenteHead);
                var styleSpace = workbook.CreateCellStyle();
                styleSpace.FillForegroundColor = HSSFColor.WHITE.index;
                styleSpace.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleSpace.SetFont(fuenteHead);
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Listado de Novedades");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);
                for (int i = 0; i < dtExportar.Columns.Count; i++)
                {
                    filaEncabezado.CreateCell(i).SetCellValue(dtExportar.Columns[i].ColumnName);
                    filaEncabezado.GetCell(i).CellStyle = styleHeader;
                }
                for (int i = 0; i < dtExportar.Rows.Count; i++)
                {
                    Row filaRegistro = hoja1.CreateRow(i + 3);
                    for (int j = 0; j < dtExportar.Columns.Count; j++)
                    {
                        filaRegistro.CreateCell(j).SetCellValue(dtExportar.Rows[i][j].ToString());
                        hoja1.AutoSizeColumn(j);
                    }
                }
                MemoryStream file = new MemoryStream();
                workbook.Write(file);
                Response.BinaryWrite(file.GetBuffer());
                Response.Cookies.Remove("termino");
                Response.AppendCookie(new HttpCookie("termino", "termino"));
                Response.End();
            }
            catch (Exception exp)
            {
            }
        }


        public string ToSiNo(string input)
        {
            if (input == "True")
            {
                return "Si";
            }
            else
            {
                return "No";
            }
        }
    }
}