﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Relevamiento.aspx.cs" Inherits="AppAndroid.Master.Ruteo1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/jquery.dataTables.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.css")%>" rel="stylesheet" type="text/css" />

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jQuerypreviewmaster/jquery.preimage.js")%>"></script>
    <!--cropper.js-->
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.css")%>" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jquery.mask.js")%>"></script>

    <script src="<%= ResolveClientUrl("~/JS/maps.js")%>"></script>

    <!--
    <link href="../JS/plugins/pgwslider/pgwslider.css" rel="stylesheet" />
    <script src="../JS/plugins/pgwslider/pgwslider.js"></script>
    -->

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css")%>" rel="stylesheet" />

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/Plugins/gmaps.js")%>"></script>

    <link rel="stylesheet" href="<%= ResolveClientUrl("~/JS/Plugins/fancybox/jquery.fancybox.css?v=2.1.5 ")%>" type="text/css" media="screen" />
    <script type="text/javascript" src="<%= ResolveClientUrl("~/JS/Plugins/fancybox/jquery.fancybox.pack.js?v=2.1.5")%>"></script>

    <script src="<%= ResolveClientUrl("~/JS/RestAPI.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/Varios.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/Ruteo.js")%>"></script>

    <style>
        .description-block {
            min-height: 90px !important;
        }
    </style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <asp:HiddenField runat="server" ID="hddCodPVRuteo" Value="" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hddCodSucRuteo" Value="" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hddCodArticuloRuteo" Value="" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hddRepositorRuteo" Value="" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hddFechaIniRuteo" Value="" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hddFechaFinRuteo" Value="" ClientIDMode="Static" />
    <section class="content-header">
        <h1>
            <i class="fa fa-location-arrow"></i> Relevamientos
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="#">Menu</a></li>
            <li class="active">Relevamientos</li>
        </ol>
    </section>
    <br />

    <div class="box" id="filtrarRuteo">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Búsqueda</b> de Relevamientos</h3>
        </div>
        <div class="box-body" id="panelFiltrarRuteo">
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        <label>Repositor:</label>
                        <select class="form-control" id="optCodRepositor">
                        </select>
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="form-group">
                        <label>Punto de Venta:</label>
                        <div class="row">
                            <div class="col-md-11">
                                <input type="text" id="txtNamePV" class="form-control" disabled>
                                <input type="hidden" id="txtCodPuntoVenta" value="">
                                <input type="hidden" id="txtCodSucursal" value="">
                            </div>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-primary" id="btnOpenSearchPV" onclick="onLoadWinSearchPV()"><i class="fa fa-search"></i></button>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5  form-group">
                                    <label>Fecha Desde:</label>
                                </div>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" id="txtFechaDesde">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 form-group">
                                    <label>Fecha Hasta:</label>
                                </div>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" id="txtFechaHasta">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <label>Articulo:</label>
                        <div class="row">
                            <div class="col-md-11">
                                <input type="text" id="txtNameArt" class="form-control" disabled>
                                <input type="hidden" id="txtCodArticulo" value="-1">
                            </div>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-primary" id="btnOpenSearchArt" onclick="onLoadWinSearchArt()"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <button type="button" class="btn btn-primary" onclick="filtrarRuteos();">Buscar</button>
                <button id="btnExportarExcel" class="btn btn-primary" type="button" onclick="generarExcel()" disabled="disabled">Exportar Excel</button>
            </div>

            <br>

            <div>
                <table class="table" id="tblRuteos">
                    <thead>
                        <tr>
                            <th>Repositor</th>
                            <th>Pto. Vta.</th>
                            <th>Fecha</th>
                            <th>EAN</th>
                            <th>Articulo</th>
                            <th>Precio</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="box" id="rContDatosProd">
        <div class="box box-widget widget-user">
        <div class="box-body" id="panelFiltrarRuteo">
            <div class="row">
                <div class="form-group col-md-8 text-right">
                    <label id="lblTituloRuteo">
                        <h3 style="margin-top: 1px;" class="text-left"><strong>Detalle Ruteo</strong></h3>
                    </label>
                </div>
                <div class="col-md-4 text-right">
                    <button type="button" class="btn btn-primary" onclick="onVolver()">Volver</button>
                </div>
            </div>

        <!-- datos producto -->
        <div class="col-md-12" id="">
            <div class="row">
                <div class="col-md-12">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="row widget-user-header bg-green-active">
                            <div class="col-md-10">
                                <h3 class="widget-user-username"> <span id="rNombre">Datos producto </span>  - <strong id="rMarca"></strong> </h3>
                                <h5 class="widget-user-desc"> <span  id="rPresentacion"></span></h5>
                            </div>
                            <div class="col-md-2">
                                <h3 class="widget-user-username"><span id="rFamilia">----</span></h3>
                            </div>
                        </div>
                        <div class="widget-user-image">
                            <img class="img-circle" id="rImagen" src="../Images/azucar.jpg" alt="Imagen del Producto">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-12 row">
                                            <div class="col-sm-6 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">Repositor</h5>
                                                    <span class="description-text" id="rRepositor">--------</span>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">Codigo Interno</h5>
                                                    <span class="description-text" id="rCodProducto"> xxxxx </span>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">EAN</h5>
                                                    <span class="description-text" id="rEAN"> xxxxx </span>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">Dia</h5>
                                                    <span class="description-text" id="rDia"> --/--/--</span>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">Punto de Venta</h5>
                                                    <span class="description-text" id="rPuntoVenta">PuntoDeVenta</span>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">Codigo Sucursal</h5>
                                                    <span class="description-text" id="rCodSucursal">CodSucursal</span>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">Tipo Cliente</h5>
                                                    <span class="description-text" id="rTipoCliente">-- ----</span>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">Empresa</h5>
                                                    <span class="description-text" id="rEmpresa">Empresa</span>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">Producto</h5>
                                                    <span class="description-text" id="rPropio">Propio</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">Precio</h5>
                                                    <span>$ <span class="description-text" id="rPrecio"> 0.0</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="description-block">
                                        <h5 class="description-header">Ubicación</h5>
                                        <span class="description-text" id="rDireccion">Sin Definir</span> <br>
                                        <span class="description-text" id="rCiudad">Sin Definir</span> <br>
                                        <span class="description-text" id="rProvincia">Sin Definir</span> <br>
                                        <span class="description-text" id="rZona">Sin Definir</span> <br>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="description-block">
                                        <h5 class="description-header">Observación</h5>
                                        <span class="description-text" style="text-align: left" id="rObservacion"></span></div>
                                </div>
                            </div>

                            <div class="row">
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    <!-- /.widget-user -->
                </div>
            </div>

        <!-- /.col -->

            <div class="row">
                <!--
                <div class="col-md-6">
                    <label for="">Ubicación:</label>
                    <div id="map_canvas" style="height: 260px"></div>
                    <div>
                        <br>
                        <span>GPS: <strong id="rGPS"></strong></span> <br>
                        <span>Presición: <strong id="rPresicion"></strong> </span>
                    </div>
                </div>
                -->
                <div class="col-md-12">
                    <label for="">Foto:</label>
                    <div>
                        <a href="" id="lFotoRuteo">
                        <div class="img-thumbnail" style="max-width: 100px">
                            <img src="" class="img-responsive" id="rFotoRuteo">
                        </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        </div>
        </div>
    </div>

    <!-- Ventana de busqueda de Puntos de Venta -->
    <div id="winSearchPV" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Puntos de Venta</h4>
                </div>
                <div class="modal-body">
                    <table class="table" id="tblListadoPV">
                        <thead>
                            <tr>
                                <th>Cliente</th>
                                <th>Sucursal</th>
                                <th>Dirección</th>
                                <th>Ciudad</th>
                                <th>Provincia</th>
                                <th>Zona</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>


                </div>
                <asp:LinkButton ID="btnExcelear" runat="server" Style="display: block;" OnClick="btnExcelear_Click" ClientIDMode="Static"></asp:LinkButton>
                <!--
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
                -->
            </div>
        </div>
    </div>

    <!-- Ventana de seleccion de articulo -->
    <div id="winSearchArt" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Articulos</h4>
                </div>
                <div class="modal-body">
                    <table class="table" id="tblListadoArt">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>EAN</th>
                                <th>Empresa</th>
                                <th>Nombre</th>
                                <th>Marca</th>
                                <th>Presentación</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>


                </div>
                <!--
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
                -->
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MenuMasterContent3" runat="server">
</asp:Content>
