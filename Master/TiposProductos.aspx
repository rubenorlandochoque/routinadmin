﻿<%@ Page Title="Tipo Producto" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="TiposProductos.aspx.cs" Inherits="AppAndroid.Master.TiposProductos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/jquery.dataTables.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.js")%>"></script>
    <link href="<%= ResolveClientUrl("~/LTE/plugins/datatables/dataTables.bootstrap.css")%>" rel="stylesheet" type="text/css" />

    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/jQuerypreviewmaster/jquery.preimage.js")%>"></script>
    <!--cropper.js-->
    <link href="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.css")%>" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/Recursos/Plantilla/plugins/cropper/cropper.js")%>"></script>

    <script src="<%= ResolveClientUrl("~/JS/RestAPI.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/Varios.js")%>"></script>
    <script src="<%= ResolveClientUrl("~/JS/tipoproducto.js")%>"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuMasterContent2" runat="server">
    <section class="content-header">
        <h1>
            <i class="fa fa-th-large "></i> Tipos de Productos
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li><a href="#">Menu</a></li>
            <li class="active">Tipos de Productos</li>
        </ol>
    </section>
    <br>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Listado</b> de Tipos de Productos</h3>
        </div>
        <div class="box-body" id="panelListadoEmpresa">
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-hover table-condensed" id="tblEmpresas">
                    <thead>
                        <tr>
                            <th class="text-center">Nombre</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="text-right">
                <button type="button" class="btn btn-primary" onclick="onAgregarEmpresa()">Agregar Tipo</button>
                <button class="btn btn-primary" type="button" onclick="generarExcel()">Exportar Excel</button>
            </div>
        </div>
        <asp:LinkButton ID="btnExcelear" runat="server" Style="display: block;" OnClick="btnExcelear_Click" ClientIDMode="Static"></asp:LinkButton>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title" id="editTitle"><b>Nuevo</b> Tipo</h3>
        </div>
        <div class="box-body" id="panelEditarEmpresa">
            <div class="row">
                <div class="col-md-6 form-group">
                    <label>Nombre:</label>
                    <input type="text" class="form-control" id="nombreEmpresa" maxlength="128">
                    <input type="hidden" id="codEmpresa">
                </div>
            </div>
            <div class="box-footer text-right">
                <button type="button" class="btn btn-primary pull-right" onclick="agregarEmpresa()">Guardar</button>&nbsp;
                <button type="button" class="btn btn-white pull-right" onclick="$('#editTitle').html('<b>Nuevo</b> Tipo');togglePanels('panelListadoEmpresa')">Volver a Listado</button>
            </div>
        </div>
    </div>
    <div id="deleteDialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Eliminar Tipos de Productos</h4>
                </div>
                <div class="modal-body">
                    <p>¿Esta seguro que desea quitar el Tipo de Producto <span style="font-weight: bold;" id="lblNombre"></span>?</p>
                    <input type="hidden" name="" id="hhdCodTipoProductoDel">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="onQuitarTipoProducto()">Si</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
