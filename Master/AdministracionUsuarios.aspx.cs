﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Master;
using Microsoft.Practices.EnterpriseLibrary.Data;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System.Web.Script.Serialization;

namespace AppAndroid.Master
{
    public partial class AdministracionUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string codPantalla = "P_REPOSITORES";
            string permisos = Sesion.getPermisos();
            if (permisos.IndexOf(codPantalla) == -1)
            {
                Response.Redirect("~/Master/SinPermisos.aspx");
            }
            if (!Page.IsPostBack)
            {
                imagenmostrar.Attributes["onerror"] = "this.src='" + ResolveClientUrl("~/Recursos/Plantilla/Images/no_image.png") + "'";
                imagenmostrar.Attributes["src"] = ResolveClientUrl("~/Recursos/Plantilla/Images/no_image.png");
                //CargarTabla();
            }
        }

        [WebMethod]
        public static string GuardarUsuario(string _codUsuario, string _usuario, string _pass, string _legajo, string _apellido, string _nombres, string _imagen, string _habilitado, string _dataFoto, string _tipoUsuario)
        {
            try
            {
                var _resultado = "";
                string stSqlCommand = "";
                Database db = DatabaseFactory.CreateDatabase("ServerConnection");

                if (_codUsuario == "")
                {
                    stSqlCommand = "dbo.USR_AddUsuario2";
                }
                else
                {
                    stSqlCommand = "dbo.USR_EditUsuario2";
                }
                DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);

                db.AddInParameter(dbCommand, "@codUsuario", DbType.String, _codUsuario);
                db.AddInParameter(dbCommand, "@usuario", DbType.String, _usuario);
                db.AddInParameter(dbCommand, "@pass", DbType.String, _pass);
                db.AddInParameter(dbCommand, "@legajo", DbType.String, _legajo);
                db.AddInParameter(dbCommand, "@apellido", DbType.String, _apellido);
                db.AddInParameter(dbCommand, "@nombres", DbType.String, _nombres);
                db.AddOutParameter(dbCommand, "@result", DbType.String, 512);

                if (_codUsuario == "")
                {
                    db.AddInParameter(dbCommand, "@codTipoUsuario", DbType.String, _tipoUsuario);
                }

                if ((_dataFoto != "")) //si hay foto a subir envio nombre sino envio vacio. Para el caso de nuevo envio cualquier nombre y en stored comprueba != de vacio y actualiza el nombre con el codrepositor
                {
                    db.AddInParameter(dbCommand, "@fotos", DbType.String, _codUsuario + ".jpg");
                }
                else
                {
                    db.AddInParameter(dbCommand, "@fotos", DbType.String, "");
                }
                //db.AddInParameter(dbCommand, "@habilitado", DbType.String, _habilitado);
                db.ExecuteNonQuery(dbCommand);

                _resultado = db.GetParameterValue(dbCommand, "@result").ToString();

                /*if (_codUsuario == "")
                {
                    _resultado = db.GetParameterValue(dbCommand, "@codUsuario").ToString();
                    if (_resultado.Equals("-1"))
                    {
                        return "[ERROR] : Se produjo un error en la insercion en fusion.";
                    }
                    else
                    {
                        return "OK";
                    }
                }*/

                if ((!_resultado.Equals("-1")) && (!_resultado.Equals("lduplicado")))
                {
                    if (_dataFoto != "")
                    {
                        if (_codUsuario != "")  //Si edit el codigo usuario viene como
                        {
                            Proced.GuardarFotoPersona(_dataFoto, _codUsuario);
                        }
                        else
                        {
                            Proced.GuardarFotoPersona(_dataFoto, _resultado);
                        }
                    }
                }
                return _resultado;
            }
            catch (Exception ex)
            {
                return "[ERROR] : " + ex.Message;
            }
        }

        [WebMethod]
        public static string CargarTablaUsuarios()
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.USR_getAllUsuarios";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            DataTable dt = null;
            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string GetUsuarioByCod(string _codUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.USR_getUsuario";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, _codUsuario);

            DataTable dt = null;
            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string ExisteLegajo(string _legajo)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.REP_ExisteLegajo";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@Legajo", DbType.String, _legajo);

            DataTable dt = null;
            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }        

        [WebMethod]
        public static string getTiposUsuarios()
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.USR_getTiposUsuarios";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            DataTable dt = null;
            JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            dt = db.ExecuteDataSet(dbCommand).Tables[0];

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        [WebMethod]
        public static string DeshabilitarUsuario(string CodUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            string stSqlCommand = "dbo.REP_DelRepositor";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, CodUsuario);
            db.ExecuteNonQuery(dbCommand);
            return "done";
        }

        [WebMethod]
        public static string HabilitarUsuario(string CodUsuario)
        {
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            string stSqlCommand = "dbo.REP_HabilitarRepositor";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@CodUsuario", DbType.String, CodUsuario);
            db.ExecuteNonQuery(dbCommand);
            return "done";
        }

        protected void verDatosUsuario_OnClick(object sender, EventArgs e)
        {

        }

        public string CapitalizeText(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        //Agregado RicardoQ
        public string ToSiNo(string input)
        {
            if (input == "True")
            {
                return "Si";
            }
            else
            {
                return "No";
            }
        }

        protected void btnExcelear_Click(object sender, EventArgs e)
        {
            try
            {
                /*string codRelevamiento = Request.QueryString["codRelevamiento"].ToString();
                string codIndicador = Request.QueryString["codIndicadorAlerta"].ToString();
                string codEstado = Request.QueryString["codEstado"].ToString();*/

                DataTable dt = null;
                Database db = DatabaseFactory.CreateDatabase("ServerConnection");
                string sqlCommand = "dbo.USR_getAllUsuarios";
                DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
                // Add paramters
                // Input parameters can specify the input value
                /*db.AddInParameter(dbCommand, "@CodSalida", DbType.String, codRelevamiento);
                db.AddInParameter(dbCommand, "@codIndicador", DbType.String, codIndicador);
                db.AddInParameter(dbCommand, "@codEstado", DbType.String, codEstado);
                db.AddInParameter(dbCommand, "@codUsuario", DbType.String, "");
                db.AddInParameter(dbCommand, "@codFormulario", DbType.String, "");*/
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                DataTable dtExportar = dt;
                /*dtExportar.Columns.Remove(dtExportar.Columns["Indicador"]);
                dtExportar.Columns.Remove(dtExportar.Columns["NumEncuesta"]);
                dtExportar.Columns.Remove(dtExportar.Columns["ocurrencias"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodRelevamiento"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodVivienda"]);
                dtExportar.Columns.Remove(dtExportar.Columns["CodTema"]);
                dtExportar.Columns.Remove(dtExportar.Columns["FechaInicioHdd"]);
                dtExportar.Columns.Remove(dtExportar.Columns["FechaFinHdd"]);
                dtExportar.Columns.Remove(dtExportar.Columns["DuracionEncu"]);*/
                string fechaHora = DateTime.Now.ToString("dd-MM-yyyy");
                string filename = "ListadoUsuarios-" + fechaHora + ".xls";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();
                /*HSSFWorkbook workbook = new HSSFWorkbook();
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Reporte de Encuestas");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);*/
                HSSFWorkbook workbook = new HSSFWorkbook();
                var style = workbook.CreateCellStyle();
                style.FillForegroundColor = HSSFColor.GREY_25_PERCENT.index;
                style.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuente = workbook.CreateFont();
                fuente.Color = HSSFColor.RED.index;
                fuente.FontName = "Calibri";
                fuente.FontHeightInPoints = 12;
                style.SetFont(fuente);
                var styleHeader = workbook.CreateCellStyle();
                styleHeader.FillForegroundColor = HSSFColor.GREY_80_PERCENT.index;
                styleHeader.FillPattern = FillPatternType.SOLID_FOREGROUND;
                Font fuenteHead = workbook.CreateFont();
                fuenteHead.Color = HSSFColor.WHITE.index;
                fuenteHead.FontName = "Calibri";
                fuenteHead.FontHeightInPoints = 12;
                styleHeader.SetFont(fuenteHead);
                var styleSpace = workbook.CreateCellStyle();
                styleSpace.FillForegroundColor = HSSFColor.WHITE.index;
                styleSpace.FillPattern = FillPatternType.SOLID_FOREGROUND;
                styleSpace.SetFont(fuenteHead);
                Sheet hoja1 = workbook.CreateSheet("Reporte");
                Row filaTitulo = hoja1.CreateRow(0);
                filaTitulo.CreateCell(0).SetCellValue("Listado de Usuarios");
                Row filaEspacio = hoja1.CreateRow(1);
                filaEspacio.CreateCell(0).SetCellValue("");
                Row filaEncabezado = hoja1.CreateRow(2);
                for (int i = 0; i < dtExportar.Columns.Count; i++)
                {
                    filaEncabezado.CreateCell(i).SetCellValue(dtExportar.Columns[i].ColumnName);
                    filaEncabezado.GetCell(i).CellStyle = styleHeader;
                }
                for (int i = 0; i < dtExportar.Rows.Count; i++)
                {
                    Row filaRegistro = hoja1.CreateRow(i + 3);
                    for (int j = 0; j < dtExportar.Columns.Count; j++)
                    {
                        if (j != dtExportar.Columns.Count - 1)
                        {
                            filaRegistro.CreateCell(j).SetCellValue(dtExportar.Rows[i][j].ToString());
                            hoja1.AutoSizeColumn(j);
                        }
                        else
                        {
                            filaRegistro.CreateCell(j).SetCellValue(this.ToSiNo(dtExportar.Rows[i][j].ToString()));
                            hoja1.AutoSizeColumn(j);
                        }
                    }
                }
                MemoryStream file = new MemoryStream();
                workbook.Write(file);
                Response.BinaryWrite(file.GetBuffer());
                Response.Cookies.Remove("termino");
                Response.AppendCookie(new HttpCookie("termino", "termino"));
                Response.End();
            }
            catch (Exception exp)
            {
            }
        }

    }
}