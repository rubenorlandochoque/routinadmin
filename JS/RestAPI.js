/*
    Autor: Ricardo Quiroga
*/


var RestAPI = RestAPI || {};


function RestAPI() {
    this.apiUrl = '../RestAPI.asmx/';
}


RestAPI.prototype.scall = function(object, jsonData, callback, async_) {
    if (typeof(async_) == "undefined") { async_ = true; }
    if (typeof(handler) == "undefined") { handler = function(data) { console.log(data); }; }
    var jsXHR = $.ajax({
        type: "POST",
        url: this.apiUrl + object,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: async_,
        data: JSON.stringify({
            JSONData: JSON.stringify(jsonData),
        }),
        success: function(response) {
            callback(response.d);
        }
    });
}


RestAPI.prototype.call = function(object, jsonData, async_) {
    if (typeof(async_) == "undefined") { async_ = true; }
    var jsXHR = $.ajax({
        type: "POST",
        url: this.apiUrl + object,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: async_,
        data: JSON.stringify({
            JSONData: JSON.stringify(jsonData),
        }),
    });
    return jsXHR;
}

/*
//Sample Use
var api = new RestAPI();
var jsXHR = api.call("Empresas", {Comando:"SelectAll"});
jsXHR.then(function(response) {
    var empresas = JSON.parse(response.d);
    $.each(empresas, function(idx, emp) {
        console.log(emp.Nombre);
    })
});
*/
