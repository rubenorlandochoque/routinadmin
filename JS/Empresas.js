var $tablaEmpresas = null;

$(document).ready(function() {
    configurarTblEmpresas();
    loadTblEmpresas();
    togglePanels("panelListadoEmpresa");
});


function configurarTblEmpresas() {
    if ($tablaEmpresas != null) {
        $tablaEmpresas.destroy();
    }

    $tablaEmpresas = $("#tblEmpresas").DataTable({
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Ultima",
                "sNext": "Siguiente",
                "sPrevious": "Atras",
            },
            "sProcessing": "Procesando...",
            "sSearch": "Buscar: ",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Registro _START_ al _END_ (_TOTAL_ registros en total)",
            "sInfoEmpty": "-",
            "sInfoFiltered": ", filtrado de un total de _MAX_ registros.",
            "sInfoPostFix": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando..."
        },
        "iDisplayLength": 25,
        "columnDefs": [
            {
                "targets": [1],
                //"visible": false
                "searchable": false
            }
        ]
    });
}


function loadTblEmpresas() {
    blockUIIn();
    var api = new RestAPI();
    jsXHR = api.call("Empresas", {Comando:"SelectAll"});
    jsXHR.then(function(response) {
        var empresas = JSON.parse(response.d);
        $tablaEmpresas.clear().draw();
        $.each(empresas, function(index, emp) {
            var fila = new Array()
            //fila.push(emp.CodEmpresa);
            fila.push(emp.Nombre);
            fila.push(
                '<button type="button" class="btn btn-success btn-xs" onclick="modificarEmpresa(\'{0}\')"><span class="fa fa-pencil"></span></button> '.format(emp.CodEmpresa)
                //'<button type="button" class="btn btn-default btn-sm" onclick="productosEmpresa(\'{0}\')"><span class="fa fa-user"></span></button> '.format(emp.CodEmpresa) +
                //'<button type="button" class="btn btn-default btn-sm" onclick="borrarEmpresa(\'{0}\')"><span class="fa fa-trash"></span></button>'.format(emp.CodEmpresa)
            );
            var currentRow = $tablaEmpresas.row.add(fila).node();
            filas = $(currentRow).find("td");
            //$(filas[0]).attr("class", "col-md-2");
            $(filas[0]).attr("class", "col-md-8");
            $(filas[1]).attr("class", "col-md-2 text-center");
        })
        $tablaEmpresas.draw();
        unblockUIOut();
    });
}


function borrarEmpresa(codEmpresa) {

}


function onAgregarEmpresa() {
    togglePanels('panelEditarEmpresa');
    $("#nombreEmpresa").val("");
    $("#codEmpresa").val("");
}

function agregarEmpresa() {
    $("#editTitle").html("<b>Nueva</b> Empresa");
    nombre = $("#nombreEmpresa").val();
    codEmpresa = $("#codEmpresa").val();

    if (codEmpresa != "") {
        comando = "Update";
    }
    else {
        comando = "Insert";
    }

    if ($.trim(nombre) != "") {
        api = new RestAPI();
        jsXHR = api.call("Empresas", {
            Comando: comando,
            Nombre: nombre,
            CodEmpresa: codEmpresa
        });

        jsXHR.then(function(response) {
            console.log(response.d);
            $("#nombreEmpresa").val("");
            $("#codEmpresa").val("");
            loadTblEmpresas();
            togglePanels("panelListadoEmpresa");
            if (codEmpresa != "") {
                showSuccessMessage("Empresa actualizada");
            }
            else {
                showSuccessMessage("Empresa agregada");
            }
        });
    }
    else {
        showErrorMessage("Debe completar el campo Nombre");
    }
}


function modificarEmpresa(codEmpresa) {
    $("#editTitle").html("<b>Modificar</b> Empresa");
    blockUIIn();
    togglePanels("panelEditarEmpresa");
    api = new RestAPI();
    jsXHR = api.call("Empresas", {
        Comando: "SelectByCodEmpresa",
        CodEmpresa: codEmpresa
    });
    jsXHR.then(function(response) {
        empresa = JSON.parse(response.d)[0];
        $("#nombreEmpresa").val(empresa.Nombre);
        $("#codEmpresa").val(empresa.CodEmpresa);
        unblockUIOut();
    });
}

//Bloquea página.
function blockUIIn() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }
    $(parent.$("form")).block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}
//Desbloquea página.
function unblockUIOut(el) {
    $(parent.$("form")).unblock();
}

//funciones para generar excel
var cookie;
function generarExcel() {
    document.cookie = 'termino=error; path=/';
    blockUIIn();
    cookie = window.setInterval(function () {
        var cookieValue = getCookie('termino');
        if (cookieValue == "termino") {
            finishDownload();
        }
    }, 1000);

    document.getElementById('btnExcelear').click();

}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function finishDownload() {
    window.clearInterval(cookie);
    document.cookie = 'termino=error; path=/';
    unblockUIOut();
}

//FIN funciones para generar excel


