﻿
$(document).ready(function () {

    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }

    $('.tableselsimple').on('click', 'li', function () {

        $("#listadoRepositores").find("i").removeClass("fa fa-fw fa-check");
        $("#listadoRepositores").find("i").addClass("fa fa-fw fa-circle-o");
        $("#listadoRepositores").find("li").removeClass('active');
        $("#listadoRepositores").find("li").css("background", "#FFFFFF");
        $(this).addClass('active');
        //cursor: pointer;background:#B3D4E7
        $(this).css("background", "#B3D4E7");
        $(this).find("i").removeClass("fa fa-fw fa-circle-o");
        $(this).find("i").addClass("fa fa-fw fa-check");
        if (jQuery.isEmptyObject($.getURLParam("pv"))) {
            visualizarPVAsignados($(this).attr("codrep"));
        }

    });

    $('.table').on('click', 'li', function () {
        if (!$(this).hasClass('active')) {
            //$(this).css('background-color', 'yellow');
            //$('.table tbody tr.row_selected').removeClass('row_selected');
            $(this).addClass('active');
            $(this).find("i").removeClass("fa fa-fw fa-circle-o");
            $(this).find("i").addClass("fa fa-fw fa-check");
            $(this).css("background", "#B3D4E7");
        } else {
            $(this).removeClass('active');
            $(this).find("i").removeClass("fa fa-fw fa-check");
            $(this).find("i").addClass("fa fa-fw fa-circle-o");
            $(this).css("background", "#FFFFFF");
        }
        //alert($("#listadoRepositores").find("i").filter("[class~='fa-check']"));
        //console.log($("#listadoRepositores").find("i").filter("[class~='fa-check']").length);
        //$("#lblCantRep").html($("#listadoRepositores").find("i").filter("[class~='fa-check']").length);
        $("#lblCantPV").html($("#listadoPV").find("i").filter("[class~='fa-check']").length);
    });

    $(document).keypress(function (e) {

        if (e.which == 13) {
            cargarListadoRepositores();
            e.preventDefault();
        }
    });

    cargarListadoRepositores();
    //cargarListadoPV();
});

function asignarRepositoresPV() {
    blockUIIn();
    var listadoCodRepSel = "";
    var listadoCodPVSel = "";
    var listadoCodSucSel = "";
    var listadoRepSel = $("#listadoRepositores").find("li").filter("[class~='active']");
    var listadoPVSel = $("#listadoPV").find("li").filter("[class~='active']");

    for (i = 0; i < listadoRepSel.length; i++) {
        console.log(listadoRepSel[i]);
        console.log($(listadoRepSel[i]).attr("codrep"));
        listadoCodRepSel += $(listadoRepSel[i]).attr("codrep").trim() + "";
    }

    for (j = 0; j < listadoPVSel.length; j++) {
        console.log(listadoPVSel[j]);
        console.log($(listadoPVSel[j]).attr("codrep"));
        listadoCodPVSel += $(listadoPVSel[j]).attr("codpv").trim() + "#";
        //console.log($(listadoPVSel[j]));
        //alert($(listadoPVSel[j]).attr("codsuc").trim());
        listadoCodSucSel += $(listadoPVSel[j]).attr("codsuc").trim() + "#";
    }
    console.log("listado rep sel = " + listadoCodRepSel);
    console.log("listado rep pv = " + listadoCodPVSel);

    if (listadoCodRepSel != "" && listadoCodPVSel != "-1") {
        var data = {};
        data["_codRepositor"] = listadoCodRepSel;
        data["_listadoPVAsignar"] = listadoCodPVSel;
        data["_listadoSucAsignar"] = listadoCodSucSel;

        $.ajax({
            type: "POST",
            url: 'Asignaciones.aspx/AsignarRepositoresPV',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (respuesta) {
                unblockUIOut();
                showSuccessMessage('Se guardaron las asignaciones');

            },
            error: function () {
                unblockUIOut();
                showErrorMessage('Error al guardar las asignaciones. Revise su conexión a internet');
            }
        });
    } else {
        unblockUIOut();
        showInfoMessage('Falta seleccionar Repositor y/o Puntos de Venta');
        //showErrorMessage('Falta seleccionar Repositor y/o Puntos de Venta');
        //showSuccessMessage('Falta seleccionar Repositor y/o Puntos de Venta');
    }

    $("#lblCantPV").html($("#listadoPV").find("i").filter("[class~='fa-check']").length);
}

function visualizarPVAsignados(codRepositor) {
    blockUIIn();
    console.log(codRepositor);
    var data = {};
    data["_codUsuario"] = codRepositor;
    $.ajax({
        type: "POST",
        url: 'Asignaciones.aspx/GetListadoPVByRepositor',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            $("#listadoPV").html("");
            var result = JSON.parse(respuesta.d);
            $.each(result, function () {
                if (this.Asignado) {
                    $("#listadoPV").append('<li class="active" style="cursor: pointer; background: rgb(179, 212, 231) none repeat scroll 0% 0%;" codpv="' + this.CodPuntoVenta + '" codsuc="' + this.CodSucursal + '"><a>' + this.PuntoVenta + '<span class="pull-right badge bg-blue"><i class="fa fa-fw fa-check"></i></span></a></li>');
                } else {
                    $("#listadoPV").append('<li style="cursor: pointer" codpv="' + this.CodPuntoVenta + '" codsuc="' + this.CodSucursal + '"><a>' + this.PuntoVenta + '<span class="pull-right badge bg-blue"><i class="fa fa-fw fa-circle-o"></i></span></a></li>');
                }

            });
            $("#lblCantPV").html($("#listadoPV").find("i").filter("[class~='fa-check']").length);
            unblockUIOut();
        }
    });
}

function modificarComportamientoSeccionLocalizacion() {
    try {
        document.getElementById('iframe1').contentWindow.cargarPuntoPorDefecto();
    }
    catch (err) {
        $('#iframe1').load(function () {
            document.getElementById('iframe1').contentWindow.cargarPuntoPorDefecto();
        });
    }
}

function centrarEnCentroConin() {
    //document.getElementById( 'iframe1' ).contentWindow.centrarMapa( -24.794682484311966, -65.4301929473877 );
}

function cargarPunto(latitud, longitud) {
    $("[id$=textLatitud]").val(latitud);
    $("[id$=textLatitud]").prop("value", latitud);
    $("[id$=textLongitud]").val(longitud);
    $("[id$=textLongitud]").prop("value", longitud);
}
function getLatitud() {
    return $("[id$=textLatitud]").val();
}

function getLongitud() {
    return $("[id$=textLongitud]").val();
}

function verificarCamposObligatorios() {
    return false;
}

function guardarDatos() {
    LATITUD = "-24.720453";
    LONGITUD = "-65.518722";
    if (LATITUD != "NONE" && LONGITUD != "NONE") {
        addMarkerCenter(LATITUD, LONGITUD, 'verde_16.png', 1, 'Nº Encuesta = ' + "", 11);
    }
    //blockUIIn();
    if (verificarCamposObligatorios()) {
//        showErrorMessage('Complete los campos obligatorios', 'error');
    } else {
        var data = {};
        data["CodRepositor"] = $('#hddCodRepositor').val();
        data["Legajo"] = $('#textCodigoCliente').val();
        data["Apellido"] = $('#textApellido').val();
        data["Nombres"] = $('#textNombres').val();
        data["Imagen"] = "";
        data["Habilitado"] = "";
        $.ajax({
            type: "POST",
            url: 'Repositores.aspx/GuardarRepositor',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(respuesta) {
                //showErrorMessage('Zonas asignadas con éxito', 'success')

                unblockUIOut();
            }
        });
    }
}

function cargarListadoRepositores() {
    blockUIIn();
    var data = {};
    data["_filtro"] = $("#textRepositorSearch").val();
    $.ajax({
        type: "POST",
        url: 'Asignaciones.aspx/GetListadoRepositoresByFilter',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            $("#listadoRepositores").html("");
            var result = JSON.parse(respuesta.d);
            $("#lblRepEncontrados").text(result.length);
            $.each(result, function () {
                $("#listadoRepositores").append('<li style="cursor: pointer" codrep="' + this.CodUsuario + '"><a>' + this.legajo.trim() + " - " + this.apellido + ' ' + this.nombres + '</a></li>');
            });
            unblockUIOut();
        }
    });
    cargarListadoPV();
}

function cargarListadoPV() {
    //blockUIIn();

    filtro = $.trim($("#textPVSearch").val()).toLowerCase();
    puntosVentas = $("#listadoPV li");
    $.each(puntosVentas, function(index, pv) {
        content = $(pv).html().toLowerCase();
        if (content.indexOf(filtro) != -1 || filtro == "" || $(pv).attr("class") == "active") {
            $(pv).show();
        }
        else {
            $(pv).hide();
        }
    });
    //unblockUIOut();

    /*
    var data = {};
    var metodo = "";
    if (jQuery.isEmptyObject($.getURLParam("pv"))) {
        data["_filtro"] = $("#textPVSearch").val();
        metodo = "GetListadoPuntosVentaByFilter";
    } else {
        data["_filtro"] = "" + $.getURLParam("pv");
        metodo = "GetListadoPuntosVentaByCod";
    }

    $.ajax({
        type: "POST",
        url: 'Asignaciones.aspx/' + metodo,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            $("#listadoPV").html("");
            //alert("asdf");
            var result = JSON.parse(respuesta.d);
            $("#lblPVEncontrados").text(result.length);
            $.each(result, function () {
                $("#listadoPV").append('<li style="cursor: pointer" codpv="' + this.CodPuntoVenta + '"><a>' + this.Nombre + '<span class="pull-right badge bg-blue"><i class="fa fa-fw fa-circle-o"></i></span></a></li>');
            });

            unblockUIOut();
        }
    });
    */
}

//Bloquea página.
function blockUIIn() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }
    $(parent.$("form")).block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}
//Desbloquea página.
function unblockUIOut(el) {
    $(parent.$("form")).unblock();
}
