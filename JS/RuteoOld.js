var mapa = null;
var gpsPoint = null;
$(document).ready(function () {
    cargarRepositores();
    cargarPuntosVenta();

    togglePanels("panelFiltrarRuteo");

    $("#txtFechaDesde").datepicker({
        dateFormat: "dd/mm/yy",
    });

    $("#txtFechaHasta").datepicker({
        dateFormat: "dd/mm/yy",
    });
});



function cargarRepositores() {
    $.ajax({
        type: "POST",
        url: 'Ruteo2.aspx/GetAllRepositores',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            repositores = JSON.parse(response.d);
            content = '<option value="-1">Seleccione una opción</option>';
            $.each(repositores, function(index, usr) {
                content += '<option value="{0}">{1}</option>'.format(usr.CodUsuario, usr.apellido + ", " + usr.nombres);
            });
            console.log(content);
            $("#optCodRepositor").html(content);
        }
    });
}


function cargarPuntosVenta() {
    $.ajax({
        type: "POST",
        url: 'Ruteo2.aspx/GetAllPuntosVentas',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            puntosVenta = JSON.parse(response.d);
            content = '<option value="-1">Seleccione una opción</option>';
            $.each(puntosVenta, function(index, pv) {
                content += '<option value="{0}">{1}</option>'.format(pv.CodPuntoVenta, pv.Nombre);
            })
            content += '<option value="">Todos</option>';
            $("#optCodPuntoVenta").html(content);
        }
    });
}


function translateFecha(fecha) {
    var data = fecha.split("/");
    return data[2] + data[1] + data[0];
}


function colorearElemento(cont, idx) {
    $("#" + cont + " li").css("background-color", "#FFFFFF");
    $("#" + cont + " [codigo=" + idx + "]").css("background-color", "#74D1FF");
}


function cargarRuteosDias() {
    $("#rContDatosProd").hide();
    codRepositor = $("#optCodRepositor").val();
    codPuntoVenta = $("#optCodPuntoVenta").val();
    fechaDesde = translateFecha($("#txtFechaDesde").val());
    fechaHasta = translateFecha($("#txtFechaHasta").val());

    if (codRepositor != "-1" && codPuntoVenta != "-1" && $.trim(fechaDesde) != "" && $.trim(fechaHasta) != "") {
        blockUIIn();
        $.ajax({
            type: "POST",
            url: 'Ruteo2.aspx/GetDatosUsuario',
            data: JSON.stringify({
                CodUsuario: codRepositor
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function(response) {
                $("#fechaDesde").html(fechaDesde);
                $("#fechaHasta").html(fechaHasta);
                $("#nombrePV").html($("#optCodPuntoVenta option:selected").html());

                usr = JSON.parse(response.d)[0];
                $("#nombreRepositor").html("{0} {1}".format(usr.Apellido, usr.Nombres));
                path = $("[id$=hddURLPerfil]").val();
                console.log(usr.Foto);
                if (usr.Foto == "") {
                    $("#imageRepositor").attr("src", path + "no_image.png");
                }
                else {
                    $("#imageRepositor").attr("src", path + usr.Foto);
                }
            }
        });

        //datos generales del ruteo
        $.ajax({
            type: "POST",
            url: 'Ruteo2.aspx/GetDetalleGeneralRuteo',
            data: JSON.stringify({
                CodRepositor: codRepositor,
                CodPuntoVenta: codPuntoVenta,
                FechaInicio: fechaDesde,
                FechaFin: fechaHasta
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function(response) {
                datos = JSON.parse(response.d)[0];
                $("#dgRuteosPropios").html(datos.PropioRuteos);
                $("#dgRuteosComp").html(datos.CompetenciaRuteos);
                $("#dgTotalRuteos").html(datos.TotalRuteos);
                $("#dgTotalPV").html(datos.PuntosVentasTrabajados);
                $("#dgTotalDias").html(datos.CantDiasTrabajados);
            }
        });

        //ruteos por dia
        $.ajax({
            type: "POST",
            url: 'Ruteo2.aspx/GetRuteosDias',
            data: JSON.stringify({
                CodRepositor: codRepositor,
                CodPuntoVenta: codPuntoVenta,
                FechaInicio: fechaDesde,
                FechaFin: fechaHasta
            }),
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var ruteos = JSON.parse(response.d);
                var nresult = ruteos.lenght;
                var content = "";
                var suma = 0;
                var idx = 1;
                $.each(ruteos, function(index, ruteo){
                    content += '<li style="cursor: pointer" onclick="cargarPVDia(\'{0}\', \'{1}\', \'{2}\')" codigo="{2}">'.format(codRepositor ,ruteo.dia, idx) +
                    '<a>{0} <span class="badge pull-right bg-blue">{1}</span></a></li>'.format(ruteo.dia, ruteo.cantPuntosVenta);
                    suma += ruteo.cantPuntosVenta;
                    idx += 1;
                })
                $("#totalRuteos").html(suma);
                $("#listadoRuteosDias").html(content);
                $("#listadoRuteosPV").html("");
                $("#listadoRuteos").html("");
            }
        });

        togglePanels("panelDetalleRuteo");
        unblockUIOut();
        //togglePanels("panelDetalleRuteo");
    }
    else {
        ShowErrorMessage("Hay campos sin completar..");
    }
    //cargar DatosRepositor

}


function cargarPVDia(codRepositor, fecha, idx) {
    colorearElemento("listadoRuteosDias", idx);
    $("#rContDatosProd").hide();
    blockUIIn();
    fecha = translateFecha(fecha);
    console.log(fecha);
    $.ajax({
        type: "POST",
        url: 'Ruteo2.aspx/GetPuntosVentaRuteo',
        data: JSON.stringify({
            CodRepositor: codRepositor,
            Fecha: fecha
        }),
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            var puntosventa = JSON.parse(response.d);
            var content = "";
            var idx = 1;
            $.each(puntosventa, function(index, pv) {
                content += '<li style="cursor: pointer" onclick="cargarRuteos(\'{0}\', \'{1}\', \'{2}\', \'{3}\')" codigo="{3}">'.format(codRepositor ,pv.CodPuntoVenta, fecha, idx) +
                    '<a>{0} <span class="badge bg-blue pull-right">{1}</span></a></li>'.format(pv.puntoVenta, pv.CantRuteos);
                idx += 1;
            });
            $("#listadoRuteosPV").html(content);
            $("#listadoRuteos").html("");
            unblockUIOut();
        }
    });
}


function cargarRuteos(codRepositor, codPuntoVenta, fecha, idx) {
    colorearElemento("listadoRuteosPV", idx);
    $("#rContDatosProd").hide();
    blockUIIn();
    $.ajax({
        type: "POST",
        url: 'Ruteo2.aspx/GetListadoRuteos',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
            CodRepositor: codRepositor,
            CodPuntoVenta: codPuntoVenta,
            Fecha: fecha,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            var ruteos = JSON.parse(response.d);
            var content = "";
            var idx = 1;
            $.each(ruteos, function(index, ruteo){
                content += '<li style="cursor: pointer" onclick="mostrarRuteo(\'{0}\', \'{1}\')" codigo="{1}">'.format(ruteo.CodRuteo, idx) +
                    '<a>{0} <span class="badge pull-right bg-blue">{1}</span></a></li>'.format(ruteo.Nombre1, ruteo.Hora);
                    idx += 1;
            });
            $("#listadoRuteos").html(content);
            unblockUIOut();
        }
    })
}


function toPropio(propio) {
    if (propio == true) {
        return "Propio";
    }
    else {
        return "Competencia";
    }
}


function ShowErrorMessage(msj) {
    return showErrorMessage(msj);
}


function mostrarRuteo(codRuteo, idx) {
    blockUIIn();
    colorearElemento("listadoRuteos", idx)
    $("#rContDatosProd").show();
    mapa = new GMaps({
          div: '#map_canvas',
          lat: -24.7761747,
          lng: -65.4115812
    });

    $.ajax({
        type: "POST",
        url: 'Ruteo2.aspx/GetDatosRuteo',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
            CodRuteo: codRuteo,
        }),
        success: function(response) {
            ruteo = JSON.parse(response.d)[0];
            if (typeof(ruteo) != "undefined") {
                $("#rNombre").html(ruteo.producto);
                $("#rMarca").html(ruteo.Marca);
                $("#rPropio").html(toPropio(ruteo.Propio));
                $("#rPresentacion").html(ruteo.Presentacion);
                path = $("[id$=hddURLProductos]").val();
                if (ruteo.Imagen != null) {
                    $("#rImagen").attr("src", path + ruteo.Imagen);
                }
                else {
                    $("#rImagen").attr("src", path + "no_producto.jpg");
                }

                //path fotos
                //path = "../Recursos/Fotos/";
                var pathFotos = $("[id$=hddURLFotos]").val();
                if ($.trim(ruteo.FotoRuteo) != "" && ruteo.FotoRuteo != null) {
                    $("#rFotoRuteo").attr("src", pathFotos + ruteo.FotoRuteo);
                }
                else {
                    $("#rFotoRuteo").attr("src", pathFotos + "no_image.png");
                }

                $("#rEmpresa").html(ruteo.empresa);
                $("#rTipoCliente").html(ruteo.TipoCliente);
                $("#rDia").html(ruteo.dia);
                $("#rObservacion").html(ruteo.Observacion);
                $("#rPrecio").html(ruteo.Precio);
                $("#rDireccion").html(ruteo.Direccion);
                $("#rCiudad").html(ruteo.Ciudad);
                $("#rProvincia").html(ruteo.Provincia);
                $("#rZona").html(ruteo.Zona);

                $("#rGPS").html(ruteo.Coordenadas);
                console.log(ruteo.Coordenadas);
                $("#rPresicion").html(ruteo.rango);

                pos = ruteo.Coordenadas.split(";");
                _lat = parseFloat(pos[0]);
                _lng = parseFloat(pos[1]);

                gpsPoint = mapa.addMarker({
                    lat: _lat,
                    lng: _lng
                });

                mapa.map.setCenter(gpsPoint.getPosition());
                unblockUIOut();

            }
            else {
                unblockUIOut();
                ShowErrorMessage("No se obtuvieron Datos");
            }
        },
        fail: function(xhr, err) {
            unblockUIOut();
        }
    });
}


function onVolver() {
    togglePanels("panelFiltrarRuteo");
}
