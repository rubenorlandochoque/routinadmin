﻿var $tablaEmpresas = null;

$(document).ready(function () {
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: 'Anterior',
        nextText: 'Siguiente',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    configurarTblEmpresas();
    loadTblData();
    togglePanels("panelListadoEmpresa");
    $("#txtFechaDesde").datepicker();
    $("#txtFechaDesde").mask('00/00/0000');

    $("#txtFechaHasta").datepicker();
    $("#txtFechaHasta").mask('00/00/0000');
    cargarListadoProductos();

    $('#textRepositorSearch').keyup(function (e) {     
        var filter = $(this).val();
        Buscador(filter);
    });
});

function configurarTblEmpresas() {
    if ($tablaEmpresas != null) {
        $tablaEmpresas.destroy();
    }

    $tablaEmpresas = $("#tblData").DataTable({
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Ultima",
                "sNext": "Siguiente",
                "sPrevious": "Atras",
            },
            "sProcessing": "Procesando...",
            "sSearch": "Buscar: ",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Registro _START_ al _END_ (_TOTAL_ registros en total)",
            "sInfoEmpty": "-",
            "sInfoFiltered": ", filtrado de un total de _MAX_ registros.",
            "sInfoPostFix": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando..."
        },
        "iDisplayLength": 25
    });
}

function loadTblData() {
    blockUIIn();
    var api = new RestAPI();
    jsXHR = api.call("Relevamientos", { comando: "SelectAll" });
    jsXHR.then(function (response) {
        var empresas = JSON.parse(response.d);
        $tablaEmpresas.clear().draw();
        $.each(empresas, function (index, emp) {
            var fila = new Array()
            fila.push(emp.FechaInicial);
            fila.push(emp.FechaFinal);
            fila.push(emp.Activo?"Activo":"Inactivo");
            fila.push(
                '<button type="button" title="Editar" class="btn btn-success btn-xs" onclick="Modificar(\'{0}\')"><span class="fa fa-pencil"></span></button> '.format(emp.CodRelevamiento) 
                //+'<button type="button" class="btn btn-default btn-sm" onclick="Desactivar(\'{0}\',\'{1}\')"><span class="fa fa-trash"></span></button>'.format(emp.CodRelevamiento, emp.Activo)
            );
            var currentRow = $tablaEmpresas.row.add(fila).node();
            filas = $(currentRow).find("td");
            $(filas[0]).attr("class", "col-md-3 text-center");
            $(filas[1]).attr("class", "col-md-3 text-center");
            $(filas[2]).attr("class", "col-md-3 text-center");
            $(filas[3]).attr("class", "col-md-3 text-center");
        })
        $tablaEmpresas.draw();
        unblockUIOut();
    });
}

function onAgregar() {
    togglePanels('panelEditarEmpresa');
    $("#txtFechaDesde").val("");
    $("#txtFechaHasta").val("");
    $("#CodRelevavmiento").val("");
}

function Agregar() {
    $("#editTitle").html("<b>Nuevo</b> Relevamiento");
    fechaDesde = $("#txtFechaDesde").val();
    fechaHasta = $("#txtFechaHasta").val();
    codRelevavmiento = $("#CodRelevavmiento").val();

    //Validamos el rango de fechas
    var d = fechaDesde.split('/');
    var h = fechaHasta.split('/');
    fecha1 = d[1] + '/' + d[0] + '/' + d[2];
    fecha2 = h[1] + '/' + h[0] + '/' + h[2];
    
    if (new Date(fecha1) > new Date(fecha2)) {
        showErrorMessage("La fecha Hasta debe ser mayor o igual que la fecha Desde.");
    }
    else {
        if (codRelevavmiento != "") {
            comando = "Update";
        }
        else {
            comando = "Insert";
        }

        if ($.trim(fechaDesde) != "" && $.trim(fechaHasta) != "") {
            var codProductos = "";
            $(".prodAsignado").each(function () {
                codProductos = codProductos + $(this).attr("data-codProd")+"#";                  
            });

            api = new RestAPI();
            jsXHR = api.call("Relevamientos", {
                comando: comando,
                fechaDesde: fechaDesde,
                fechaHasta: fechaHasta,
                codRelevamiento: codRelevavmiento,
                codProductos: codProductos
            });

            jsXHR.then(function (response) {
                var result = JSON.parse(response.d);
                var accionResult = "";
                $.each(result, function () {
                    accionResult = this.CodRelevamiento
                })

                if (accionResult.length > 0) {
                    $("#txtFechaDesde").val("");
                    $("#txtFechaHasta").val("");
                    $("#CodRelevavmiento").val("");
                    loadTblData();
                    togglePanels("panelListadoEmpresa");

                    if (codRelevavmiento != "") {
                        showSuccessMessage("Relevamiento actualizado");
                    }
                    else {
                        showSuccessMessage("Relevamiento agregado");
                    }
                }
                else {
                    showErrorMessage("Se produjo un error, revise los datos o comuniquese con el Administrador");
                }
            });
        }
        else {
            showErrorMessage("Debe completar ambas fechas");
        }
    }
}

function Modificar(CodRelevamiento) {
    $("#editTitle").html("<b>Modificar</b> Relevamiento");
    blockUIIn();
    togglePanels("panelEditarEmpresa");
    api = new RestAPI();
    jsXHR = api.call("Relevamientos", {
        comando: "SelectByCod",
        codRelevamiento: CodRelevamiento
    });
    jsXHR.then(function (response) {
        entidad = JSON.parse(response.d)[0];
        $("#txtFechaDesde").val(entidad.FechaInicial);
        $("#txtFechaHasta").val(entidad.FechaFinal);
        $("#CodRelevavmiento").val(entidad.CodRelevamiento);
        CargaProductosAsignados(CodRelevamiento);
        unblockUIOut();
    });
}

//Bloquea página.
function blockUIIn() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }
    $(parent.$("form")).block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}
//Desbloquea página.
function unblockUIOut(el) {
    $(parent.$("form")).unblock();
}

function Desactivar(codRelevamiento, activo) {
    //Le cambiamos el estado
    if (activo == "true") { activo = "false"; title = "Desactivar"; alert = "¿Está seguro que desea Desactivar el Relevamiento?"; }
    else { activo = "true"; title = "Activar"; alert = "¿Está seguro que desea Activar el Relevamiento? Esta accion 'DESACTIVARA' los otros Relevamientos activos si los hubiera";}

    $("#hhdCodRelevamientoDel").val(codRelevamiento + "#" + activo);
    $("#actionTitle").text(title);
    $("#actionAlert").text(alert);
    $("#deleteDialog").modal("show");
}

function onQuitar() {
    var CodRelevamiento = $("#hhdCodRelevamientoDel").val();
    blockUIIn();
    var api = new RestAPI();
    jsXHR = api.call("Relevamientos", {
        comando: "IsActive",
        codRelevamiento: CodRelevamiento.split('#')[0],
        activo: CodRelevamiento.split('#')[1]
    });
    jsXHR.then(function (response) {
        var result = JSON.parse(response.d);
        var accionResult = "";
        $.each(result, function () {
            accionResult = this.CodRelevamiento
        })

        if (accionResult.length > 0) {
            $("#deleteDialog").modal("hide");
            loadTblData();
            showSuccessMessage("Relevamiento Actualizado");
        }
        else {
            showErrorMessage("Se produjo un error, revise los datos o comuniquese con el Administrador");
        }
    });
    unblockUIOut();
}

function cargarListadoProductos() {
    _filtro = $("#textRepositorSearch").val();
    var api = new RestAPI();
    jsXHR = api.call("Relevamientos", {
        comando: "SelectAllProductos",
        filtro: _filtro
    });
    jsXHR.then(function (response) {
        $("#listadoRepositores").html("");
        var result = JSON.parse(response.d);
        $("#lblRepEncontrados").text(result.length);
        $.each(result, function () {
            $("#listadoRepositores").append('<li style="cursor: pointer" class="buscador" title="Doble click para asignar" ondblclick="onClickAsignar(\'' + this.CodProducto + '\',\'' + this.Producto+ '\')"><a>' + this.Producto+ '</a></li>');
        });
    });    
}
function CargaProductosAsignados(codRelevamiento) {
    var api = new RestAPI();
    jsXHR = api.call("Relevamientos", {
        comando: "SelectAllProductosAsignados",
        codRelevamiento: codRelevamiento
    });
    jsXHR.then(function (response) {
        $("#listadoPV").html("");
        var result = JSON.parse(response.d);
        $("#lblCantPV").text(result.length);
        $.each(result, function () {
            $("#listadoPV").append('<li title="Doble click para quitar" ondblclick="QuitarProd(this)" style="cursor: pointer" class="prodAsignado" data-codProd="' + this.CodProducto + '"><a>' + this.Producto + '</a></li>');
        });
    });
}
function onClickAsignar(CodProducto, producto) {
    var existe = false;
    $(".prodAsignado").each(function () {
        if ($(this).attr("data-codProd") == CodProducto) {
            existe = true;
        }
    });
    if (!existe) {
        $("#listadoPV").prepend('<li title="Doble click para quitar" ondblclick="QuitarProd(this)" style="cursor: pointer" class="prodAsignado" data-codProd="' + CodProducto + '"><a>' + producto + '</a></li>');
        var total = $("#lblCantPV").text();
        total = parseInt(total) + 1;
        $("#lblCantPV").text(total);
    }
}
function QuitarProd(elemnt) {
    $(elemnt).remove();
    var total = $("#lblCantPV").text();
    total = parseInt(total) - 1;
    $("#lblCantPV").text(total);
}
function Buscador(filter) {
    $(".buscador").each(function () {
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
}
