
/*
    Formateo Parametros en un String
    "hola {1} cuantos años tienes {0}".format(23, "Juan")
    >>> hola Juan cuantos años tienes 23
 */
String.prototype.format = function() {
    var formatted = this;
    for( var idx in arguments) {
      formatted = formatted.replace(new RegExp("\\{" + idx + "\\}", 'g'),  arguments[idx]);
    }
    return formatted;
};


var panelList = [
    "panelListado",
    "panelNuevoRep",

    "panelListadoProducto",
    "panelEditarProducto",

    "panelEditarEmpresa",
    "panelListadoEmpresa",

    "panelFiltrarRuteo",
    "panelDetalleRuteo",

	"panelFiltradoTracking",
	"panelListadoTracking",

    "panelFiltrarNovedades",
    "panelDetalleNovedad",

]

function togglePanel(idPanel, action) {
    if (action == "hide") {
        $("#" + idPanel).hide("slow");
    }
    else {
        $("#" + idPanel).show("slow");
    }

}


function togglePanels(idPanel) {
    $.each(panelList, function(index, name) {
        if (name != idPanel) {
            togglePanel(name, "hide");
        }
    });
    togglePanel(idPanel, "show");
}


// wrapper function to  un-block element(finish loading)
function unblockUIOut(el) {
    $("#form1").unblock();
}

function blockUIIn(el) {
    $("#form1").block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
   // $('.blockUI.blockMsg').center();
}

