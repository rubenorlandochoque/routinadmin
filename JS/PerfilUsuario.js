var $image;

$(document).ready(function () {
    inicializarModuloFoto();
    inicializarOtrosModulos();
});

function inicializarOtrosModulos() {
    $("[id$=textLegajo]").attr('readonly', 'readonly');
    cargarDatos($("[id$=hddCodUsuarioActivo]").val());
    /*$("a").click(function (e) {
        if (($(this).attr("href").indexOf("#") == -1) && ($(this).attr("href").indexOf("javascript") == -1)) {
            e.preventDefault();
            $("[id$=urlredireccionar]").val($(this).attr("href"));
            //alert( jQuery.type( $( "[id$=funcionejecutar]" ) ) !== "undefined" );
            if (($("[id$=funcionejecutar]").val() != "") && (jQuery.type($("[id$=funcionejecutar]").val()) !== "undefined")) {
                var tmp = $("[id$=funcionejecutar]").val();
                eval(tmp)(-1);
            } else {
            }
        }
    });*/

    //iCheck for checkbox and radio inputs
    //$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    //    checkboxClass: 'icheckbox_minimal-blue',
    //    radioClass: 'iradio_minimal-blue'
    //});
    $('#textPass').keyup(function (e) {
        if ($(this).val() != "") {
            retorno = verificarFuerzaContrasenia($(this).val());
            if (retorno == "-1") {
                $('#passstrength').html("Débil");
                $('#passstrength').removeClass("").addClass("passdebil");
            } else {
                if (retorno == "0") {
                    $('#passstrength').html("Media");
                    $('#passstrength').removeClass("").addClass("passmedia");
                } else {
                    if (retorno == "1") {
                        $('#passstrength').html("Fuerte");
                        $('#passstrength').removeClass("").addClass("passfuerte");
                    }
                }
            }
        } else {
            $('#passstrength').html('');
        }

        return true;
    });
}

function verificarFuerzaContrasenia(pass) {
    var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    var enoughRegex = new RegExp("(?=.{6,}).*", "g");
    var retorno = "";
    if (false == enoughRegex.test(pass)) {
        //$('#passstrength').html('Débil');
        retorno = "-1";
    } else if (strongRegex.test(pass)) {
        //$('#passstrength').className = 'ok';
        //$('#passstrength').html('Fuerte!');
        retorno = "1";
    } else if (mediumRegex.test(pass)) {
        //$('#passstrength').className = 'alert';
        //$('#passstrength').html('Media!');
        retorno = "0";
    } else {
        //$('#passstrength').className = 'error';
        //$('#passstrength').html('Débil!');
        retorno = "-1";
    }
    return retorno;
}

function mostrarNuevoUsuario() {
    clearFrmPersona();
    mostrarOcultarPaneles("panel2");
}

function clearFrmPersona() {
    //Reseteo formulario de datos personales
    $('#hddCodRepositor').val("");
    $('#textUsuario').val("");
    $('#textPass').val("");
    $('#textPassOld').val("");
    $('#textPassRepita').val("");
    $('#textLegajo').val("");
    $('#textApellido').val("");
    $("#txtUsuario").val("");
    $("#txtPass").val("");
    $('#textNombres').val("");
    $("[id$=imagenmostrar]").attr("src", $("[id$=hddURLPerfil]") + "no_image.png");
    $("[id$=dataFoto]").val("");
    $("[id$=nombreFoto]").val("");
    $(".grid-title h4 span").text("");
    $('#textLegajo').focus();
}

function mostrarOcultarPaneles(panelMostrar) {
    if (panelMostrar == "panel2") {
        $('#btnCollapseListado').click();
        $('#btnCollapseNuevo').click();
    } else {
        $('#btnCollapseListado').click();
        $('#btnCollapseNuevo').click();

    }
}

function expandir() {
    clearFrmPersona();
}

function inicializarModuloFoto() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }

    $("[id$=FileUploadFotoPerfil]").preimage();
    ejecutar();
    $("#btnTomarFoto").on("click", function () {
        $("[id$=botoneraTomarFoto]").show();
        $("[id$=botoneraEditarFoto]").hide();
        $("[id$=bodyTomarFoto]").show();
        $("[id$=bodyEditarFoto]").hide();
    });
}

var table, usuarios;


function cargarDatos(codRep) {
    clearFrmPersona();
    var data = {};
    data["_codUsuario"] = codRep.trim();
    $.ajax({
        type: "POST",
        url: 'Usuarios.aspx/GetUsuarioByCod',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            var result = JSON.parse(respuesta.d);
            $.each(result, function () {
                $('#hddCodRepositor').val(this.CodUsuario.trim());
                $('#textLegajo').val(this.Legajo.trim());
                $('#textApellido').val(this.Apellido.trim());
                $('#textNombres').val(this.Nombres.trim());
                $('#textUsuario').val(this.Usuario);
                //$('#textPass').val(this.Pass);
                console.log(this.Foto);
                console.log("imagen");
                if (this.Foto == "") {
                    $("[id$=imagenmostrar]").attr("src", $("[id$=hddURLPerfil]").val() + "no_image.png");
                } else {
                    $("[id$=imagenmostrar]").attr("src", $("[id$=hddURLPerfil]").val() + this.Foto.trim() + "?id=" + randomID(7));
                    $("[id$=nombreFoto]").val(this.Foto.trim());
                }
                if (this.Habilitado == 1) {
                    $("#rbtnHabilitado").click();
                } else {
                    if (this.Habilitado == 0) {
                        $("#rbtnNoHabilitado").click();
                    }
                }
                $('#textLegajo').focus();
                mostrarOcultarPaneles("panel2");
            });
        }
    });
}
//Funciones para fotos
function cargarFoto(url) {
    //alert( url );
    $("[id$=imagenmostrar]").attr("src", url);
    $("[id$=dataFoto]").attr("value", url);
}

function cerrarDialogoFoto() {
    $("[id$=botoneraTomarFoto]").show();
    $("[id$=botoneraEditarFoto]").hide();
    $("[id$=bodyTomarFoto]").show();
    $("[id$=bodyEditarFoto]").hide();
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.rect(0, 0, canvas.width, canvas.height);
    context.stroke();
    $("#btnpopup1").trigger("click");
    $("#btnpopup2").trigger("click");
}

function actualizarFoto() {
    cargarFotoCortada();
}

function ejecutar() {
    $image = $(".cropper");
    var $dataX = $("#dataX"),
    $dataY = $("#dataY"),
    $dataHeight = $("#dataHeight"),
    $dataWidth = $("#dataWidth"),
    console = window.console || { log: $.noop },
    cropper;
    $image.cropper({
        aspectRatio: 1 / 1,
        // autoCropArea: 1,
        data: {
            x: 520,
            y: 10,
            width: 1024,
            height: 768
        },
        preview: ".preview",

        // multiple: FALSE,
        // autoCrop: TRUE,
        // dragCrop: TRUE,
        // dashed: TRUE,
        // modal: TRUE,
        // movable: TRUE,
        // resizable: TRUE,
        // zoomable: TRUE,
        // rotatable: TRUE,

        // maxWidth: 480,
        // maxHeight: 270,
        // minWidth: 160,
        // minHeight: 90,

        done: function (data) {
            $dataX.val(data.x);
            $dataY.val(data.y);
            $dataHeight.val(data.height);
            $dataWidth.val(data.width);
        },

        build: function (e) {
            console.log(e.type);
        },

        built: function (e) {
            console.log(e.type);
        },

        dragstart: function (e) {
            console.log(e.type);
        },

        dragmove: function (e) {
            console.log(e.type);
        },

        dragend: function (e) {
            console.log(e.type);
        }
    });

    cropper = $image.data("cropper");

    $image.on({
        "build.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "built.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragstart.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragmove.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragend.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        }
    });

    $("#reset").click(function () {
        $image.cropper("reset");
    });

    $("#reset2").click(function () {
        $image.cropper("reset", true);
    });

    $("#clear").click(function () {
        $image.cropper("clear");
    });

    $("#destroy").click(function () {
        $image.cropper("destroy");
    });

    $("#enable").click(function () {
        $image.cropper("enable");
    });

    $("#disable").click(function () {
        $image.cropper("disable");
    });

    $("#zoom").click(function () {
        $image.cropper("zoom", $("#zoomWith").val());
    });

    $("#zoomIn").click(function () {
        $image.cropper("zoom", 0.9);
    });

    $("#zoomOut").click(function () {
        $image.cropper("zoom", -0.9);
    });

    $("#rotate").click(function () {
        $image.cropper("rotate", $("#rotateWith").val());
    });

    $("#rotateLeft").click(function () {
        $image.cropper("rotate", -90);
    });

    $("#rotateRight").click(function () {
        $image.cropper("rotate", 90);
    });

    $("#setAspectRatio").click(function () {
        $image.cropper("setAspectRatio", $("#aspectRatio").val());
    });

    $("#replace").click(function () {
        //$image.cropper( "replace", $( "#replaceWith" ).val() );
        $image.cropper("replace", $("#fotoMostrar").val());
    });

    $("#getImageData").click(function () {
        $("#showImageData").val(JSON.stringify($image.cropper("getImageData")));
    });

    $("#setData").click(function () {
        $image.cropper("setData", {
            x: $dataX.val(),
            y: $dataY.val(),
            width: $dataWidth.val(),
            height: $dataHeight.val()
        });
    });

    $("#getData").click(function () {
        $("#showData").val(JSON.stringify($image.cropper("getData")));
    });

    $("#getDataURL").click(function () {
        //var dataURL = $image.cropper( "getDataURL" );

        //$( "#dataURL" ).text( dataURL );
        //$( "#showDataURL" ).html( '<img src="' + dataURL + '">' );
        cerrarDialogoFoto();
    });

    $("#getDataURL2").click(function () {
        var dataURL = $image.cropper("getDataURL", "image/jpeg");

        $("#dataURL").text(dataURL);
        $("#showDataURL").html('<img src="' + dataURL + '">');
        cargarFoto(dataURL);
        cerrarDialogoFoto();
    });
};

function cargarURLFoto() {
    $("#imagenCortar").attr("src", $("#iframe12").attr("foto"));
    alert($("#iframe12").attr("foto"));
}

function agregarURL(url) {
    //$( "#imagenCortar" ).removeAttr( "src");
    $("#imagenCortar").attr("src", url);
    $("#fotoMostrar").attr("value", url);
    $("#replace").trigger("click");
    ejecutar();
}

function salir() {
    $("#btnpopup").trigger("click");
}

function cargarFotoCortada() {
    $("#getDataURL2").trigger("click");
}

//FIN Funciones para fotos

// Put event listeners into place
window.addEventListener("DOMContentLoaded", function () {
    // Grab elements, create settings, etc.
    canvas = document.getElementById("canvas");
    canvasHdd = document.getElementById("canvasHidden");
    context = canvas.getContext("2d");
    context.rect(0, 0, canvas.width, canvas.height);
    context.stroke();
    contextHdd = canvasHdd.getContext("2d");

    video = document.getElementById("video");
    videoObj = { "video": true };
    errBack = function (error) {
        console.log("Video capture error: ", error.code);
    };

    // Put video listeners into place
    if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function (stream) {
            video.src = stream;
            video.play();
            console.log("Standard");
        }, errBack);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function (stream) {
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
            console.log("WebKit-prefixed");
        }, errBack);
    } else if (navigator.mozGetUserMedia) { // WebKit-prefixed
        navigator.mozGetUserMedia(videoObj, function (stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
            console.log("WebKit-prefixed 22");
        }, errBack);
    }

    // Trigger photo take
    document.getElementById("snap").addEventListener("click", function () {
        context.drawImage(video, 0, 0, 280, 220);
        //context.drawImage( video, 0, 0, 1024, 768);
        contextHdd.drawImage(video, 0, 0, 1024, 768);
    });
    document.getElementById("btnConfirnarFoto").addEventListener("click", function () {
        document.getElementById("imgCaptura").src = canvas.toDataURL();
        $("[id$=imagenmostrar]").attr("src", document.getElementById('imgCaptura').src);

        //console.log(canvas.toDataURL("image/png"));
        $image.cropper("replace", canvasHdd.toDataURL("image/png"));
        $image.cropper("reset", true);

        //$("#btnMostrarFotoRecortar").click();
        //$(window).trigger('resize');

        //document.getElementById("hidden").value = canvas.toDataURL("image/png").replace('data:image/png;base64,', '');
        $("[id$=botoneraTomarFoto]").hide();
        $("[id$=botoneraEditarFoto]").show();
        $("[id$=bodyTomarFoto]").hide();
        $("[id$=bodyEditarFoto]").show();
    });
}, false);


function mostrarDialogoBuscarFoto() {
    $("[id$=FileUploadFotoPerfil]").click();
}
//Datos subir archivos - Inicio
function mostrarDialogoBuscarFichero() {
    $("[id$=FileUploadFichero]").click();
}
//Datos subir archivos - Fin



function modificarComportamientoSeccionLocalizacion() {
    try {
        document.getElementById('iframe1').contentWindow.cargarPuntoPorDefecto();
    }
    catch (err) {
        $('#iframe1').load(function () {
            document.getElementById('iframe1').contentWindow.cargarPuntoPorDefecto();
        });
    }
}

function centrarEnCentroConin() {
    //document.getElementById( 'iframe1' ).contentWindow.centrarMapa( -24.794682484311966, -65.4301929473877 );
}

function buscarDireccion() {
    $("[id$=buscarDireccion]").on("click", function () {
        var direccion = "";
        var barrio = $("[id$=textBarrio]").val();
        var calle = $("[id$=textCalle]").val();
        var nroPuerta = $("[id$=textNroPuerta]").val();
        var pais = $("[id$=selectnacionalidades] option:selected").val() != "COD_SELECCIONE_UNA_OPCION" ? $("[id$=selectnacionalidades] option:selected").text() : "";
        var provincia = $("[id$=selectprovincia] option:selected").val() != "COD_SELECCIONE_UNA_OPCION" ? $("[id$=selectprovincia] option:selected").text() : "";;
        var localidad = $("[id$=selectlocalidades] option:selected").val() != "COD_SELECCIONE_UNA_OPCION" ? $("[id$=selectlocalidades] option:selected").text() : "";;
        direccion = (calle.trim() != "" ? calle + (nroPuerta.trim() != "" ? " " + nroPuerta + "," : ",") : "") + (localidad.trim() != "" ? localidad : "") + (provincia.trim() != "" ? "," + provincia : "") + (pais.trim() != "" ? "," + pais : "");
        $('#iframe1').contents().find('#pac-input').val(direccion);
        document.getElementById('iframe1').contentWindow.buscar();
        $('html, body').animate({ scrollTop: $("#iframe1").offset().top - 100 }, 1000);

    });
}

function verificarCamposObligatorios() {
    var retorno = false;
    var mensaje = "";
    if ($("#textUsuario").val().trim() == "") {
        mensaje = "Debe completar el campo Usuario";
    } else {
        if ($("#textApellido").val().trim() == "") {
            mensaje = "Debe completar el campo Apellido";
        } else {
            if ($("#textNombres").val().trim() == "") {
                mensaje = "Debe completar el campo Nombres";
            } else {
                if (($("#textPass").val().trim() != "") && ($("#textPassRepita").val().trim() != "") && ($("#textPassOld").val().trim() != "")) {
                    if ($("#textPass").val().trim() != $("#textPassRepita").val().trim()) {
                        mensaje = "La contraseña y la confirmación de la contraseña no coinciden";
                    } else {
                        if (($("#textPass").val().trim() != "") && (verificarFuerzaContrasenia($("#textPass").val().trim()) == "-1")) {
                            mensaje = "La contraseña es Débil";
                        } else {
                            if ($("#textPass").val().trim().length < 8) {
                                mensaje = "La contraseña debe tener 8 caractéres como mínimo";
                            } else {
                                retorno = true;
                            }
                        }
                    }
                } else {
                    var cont = 3;
                    if ($("#textPassOld").val() == "") {
                        cont--;
                    }
                    if ($("#textPass").val() == "") {
                        cont--;
                    }
                    if ($("#textPassRepita").val() == "") {
                        cont--;
                    }
                    if (cont != 0) {
                        mensaje = "Si desea cambiar la contraseña, debe completar todos los campos relacionados a la misma";
                    } else {
                        retorno = true;
                    }
                }
            }
        }
    }
    if (!retorno) {
        showErrorMessage(mensaje);
    }
    return retorno;
}

function guardarDatos() {

    if (verificarCamposObligatorios()) {
        blockUIIn();
        var data = {};
        data["_apellido"] = $('#textApellido').val();
        data["_nombres"] = $('#textNombres').val();
        data["_imagen"] = $("[id$=nombreFoto]").val();
        data["_legajo"] = $('#textLegajo').val();
        data["_usuario"] = $('#textUsuario').val();
        data["_passOld"] = $('#textPassOld').val();
        data["_pass"] = $('#textPass').val();
        data["_codUsuario"] = $("[id$=hddCodUsuarioActivo]").val();
        data["_dataFoto"] = $("[id$=dataFoto]").val();
        //console.log(JSON.stringify(data, null, "  "));
        //unblockUIOut();
        $.ajax({
            type: "POST",
            url: 'PerfilUsuario.aspx/GuardarUsuario',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (respuesta) {
                if (respuesta.d != "-1") {
                    //alert($("[id$=nombreFoto]").val());
                    if ($("[id$=nombreFoto]").val().trim() != "") {
                        foteador($('#hddURLPerfil').val() + $('#hddCodRepositor').val() + ".jpg?id=" + Math.floor((Math.random() * 100) + 1));
                    } else {
                        foteador($('#hddURLPerfil').val() + "no_image.png?id=" + Math.floor((Math.random() * 100) + 1));
                    }
                    $("#lblNombreUsuario").text($('#textNombres').val());
                    $("#lblNombreUsuario1").text($('#textNombres').val());
                    $("#lblNombreUsuario2").text($('#textNombres').val());
                    $("#lblApellido").text($('#textApellido').val());
                    showSuccessMessage('Usuario guardado');
                    //clearFrmPersona();
                } else {
                    showInfoMessage('Revise los datos. No se guardaron los cambios');
                }
                unblockUIOut();
            },
            error: function () {
                showErrorMessage('Error al guardar las asignaciones. Revise su conexión a internet');
            }
        });
    }
}


//funciones para generar excel
var cookie;
function generarExcel() {
    document.cookie = 'termino=error; path=/';
    blockUIIn();
    cookie = window.setInterval(function () {
        var cookieValue = getCookie('termino');
        if (cookieValue == "termino") {
            finishDownload();
        }
    }, 1000);
    alert("antes");
    document.getElementById('btnExcelear').click();
    alert("despues");
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function finishDownload() {
    window.clearInterval(cookie);
    document.cookie = 'termino=error; path=/';
    unblockUIOut();
}

//FIN funciones para generar excel

function getUsuarioByCod() {
    blockUIIn();
    var data = {};
    data["comando"] = 'GetAllSalidasByFormAndUsuario';
    data["codFormulario"] = $('#hddCodFormulario').val();
    data["nombreFormulario"] = $('#hddNombreFormulario').val();
    data["codUsuario"] = $('#hddUserSelected').val();
    data["codVersion"] = $('#hddVersion').val();
    $.ajax({
        type: "POST",
        url: '../../WebService/Filtros.asmx/cargarComboVersiones',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            if ($("#SalidasAsign").html() != "") {
                $("#SalidasAsign").fancytree("destroy")
            }
            var result = JSON.parse(respuesta.d);

            $('#hddCodPadre').val(result.key)

            $("#SalidasAsign").fancytree({
                checkbox: true,
                selectMode: 3,
                icons: false,
                source: result,
                extensions: ["filter"],
                quicksearch: true,
                filter: {
                    autoApply: false,
                    counter: false,
                    fuzzy: false,
                    hideExpandedCounter: false,
                    highlight: false,
                    mode: "hide"
                },
                select: function (event, data) {
                    selKeys = $.map(data.tree.getSelectedNodes(), function (node) {
                        return node.key;
                    });
                    var selRootNodes = data.tree.getSelectedNodes(true);
                    var selRootKeys = $.map(selRootNodes, function (node) {
                        return node.parent.key;
                    });
                    selKeys = _.union(selKeys, selRootKeys);
                },
                dblclick: function (event, data) {
                    data.node.toggleSelected();
                },
                keydown: function (event, data) {
                    if (event.which === 32) {
                        data.node.toggleSelected();
                        return false;
                    }
                },
                cookieId: "fancytree-Cb3",
                idPrefix: "fancytree-Cb3-"
            });
            var tree = $("#SalidasAsign").fancytree("getTree");
            $('#search').keyup(function (e) {
                var n,
                    opts = {
                        autoExpand: true
                    },
                    match = $(this).val();
                n = tree.filterNodes(match, opts);
            }).focus();
            unblockUIOut();
        }
    });
}

//Bloquea página.
function blockUIIn() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }
    $(parent.$("form")).block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}
//Desbloquea página.
function unblockUIOut(el) {
    $(parent.$("form")).unblock();
}

