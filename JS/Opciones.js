$(document).ready(function() {
    $("#footerButton").show();
    getFechaSync();
    getTablero();
});

function sincronizar() {
    $("#winSync").modal("show");
}

function onSync() {
    $("#winSync").modal("hide");
    blockUIIn();
    $.ajax({
        type: "POST",
        url: 'Opciones.aspx/Sincronizar',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            unblockUIOut();
            showInfoMessage("Sincronización Completada....");
        },
        fail: function(xhr, err) {
            unblockUIOut();
            showErrorMessage("Fallo la sincronización");
        }
    });
    getFechaSync();
}

function getFechaSync() {
    var data = {};
    $.ajax({
        type: "POST",
        url: 'Opciones.aspx/GetFechaSincronizacion',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            var result = JSON.parse(respuesta.d);
            $('#lblFchModif').text("");
            $('#lblFchModif').text("");
            $.each(result, function () {
                $('#lblFchModif').text(this.FechaSincro);
            });
        },
        error: function (respuesta) {
            //showErrorMessage("No se pudo cargar el Punto de Venta Seleccionado. Revise su conexión a Internet");
        }
    });
}

function getTablero() {
    blockUIIn();
    var data = {};
    $.ajax({
        type: "POST",
        url: 'Opciones.aspx/GetTablero',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            var result = JSON.parse(respuesta.d);
            $.each(result, function () {
                $('#lblProdPropios').text(this.CantProdPropios);
                $('#lblProdCompetencia').text(this.CantProdNoPropio);
                $('#lblPVPropios').text(this.CantPVPropios);
                $('#lblPVCompetencia').text(this.CantPVNoPropio);
                $('#lblEmpresas').text(this.CantEmpresas);
                $('#lblCantRep').text(this.CantRepActivos);
                $('#lblCantRuteosHoy').text(this.CantRuteosHoy);
                $('#lblCantNovHoy').text(this.CantNovHoy);
                $('#lblCantRepNoSync').text(this.CantRepSinSincro);
                $('#lblTipoProducto').text(this.CantTipoProducto);
            });
            unblockUIOut();
        },
        error: function (respuesta) {
            unblockUIOut();
            //showErrorMessage("No se pudo cargar el Punto de Venta Seleccionado. Revise su conexión a Internet");
        }
    });
}

//Bloquea página.
function blockUIIn() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }
    $(parent.$("form")).block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}
//Desbloquea página.
function unblockUIOut(el) {
    $(parent.$("form")).unblock();
}



var cookie;
function excelDetalles() {
    document.cookie = 'termino=error; path=/';
    blockUIIn();
    cookie = window.setInterval(function () {
        var cookieValue = getCookie('termino');
        if (cookieValue == "termino") {
            finishDownload();
        }
    }, 1000);
    document.getElementById('btnExcel').click();

}

function finishDownload() {
    window.clearInterval(cookie);
    document.cookie = 'termino=error; path=/';
    unblockUIOut();
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
    }
    return "";
}