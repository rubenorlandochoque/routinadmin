var $image;
var $tablaProductos;
api = null;

$(document).ready(function () {
    inicializarModuloFoto();
    api = new RestAPI();
    loadEmpresas();
    loadFamilias();
    loadTipoProducto();
    togglePanels("panelListadoProducto");
    configurarTblProductos();
    loadTblProductos();

    $("#txtEAN").mask('000000000000000');
});


function loadEmpresas() {
    var jsXHR = api.call("Empresas", {Comando: "SelectAll"});
    jsXHR.then(function(response) {
        var empresas = JSON.parse(response.d);
        var content = "<option value='-1'>Seleccione una opcion</option>";
        $.each(empresas, function(idx, emp) {
            content += "<option value='{0}'>{1}</option>".format(emp.CodEmpresa, emp.Nombre);
        });
        $("#optCodEmpresa").html(content);
    });
}

function loadFamilias() {
    var jsXHR = api.call("Familias", {Comando: "SelectAll"});
    jsXHR.then(function(response) {
        var familias = JSON.parse(response.d);
        var content = "<option value='-1'>Seleccione una opcion</option>";
        $.each(familias, function(idx, fam) {
            content += "<option value='{0}'>{1}</option>".format(fam.CodFamilia, fam.Descripcion);
        });
        $("#optIdFamilia").html(content);
    });
}

function loadTipoProducto() {
    var jsXHR = api.call("TiposProductos", { Comando: "SelectAll" });
    jsXHR.then(function (response) {
        var familias = JSON.parse(response.d);
        var content = "";
        $.each(familias, function (idx, fam) {
            content += "<option value='{0}'>{1}</option>".format(fam.CodTipoProducto, fam.Nombre);
        });
        $("#optTipoProducto").html(content);
    });
}
function configurarTblProductos() {
    if ($tablaProductos != null) {
        $tablaProductos.destroy();
    }

    $tablaProductos = $("#tblProductos").DataTable({
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Ultima",
                "sNext": "Siguiente",
                "sPrevious": "Atras",
            },
            "sProcessing": "Procesando...",
            "sSearch": "Buscar: ",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Registro _START_ al _END_ (_TOTAL_ registros en total)",
            "sInfoEmpty": "-",
            "sInfoFiltered": ", filtrado de un total de _MAX_ registros.",
            "sInfoPostFix": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando..."
        },
        "iDisplayLength": 25,
        "columnDefs": [
            {
                //"targets": [0],
                //"visible": false
                //"searchable": false
            }
        ]
    });
}


function loadTblProductos() {
    blockUIIn();
    var jsXHR = api.call("Productos", {Comando: "SelectAll"});
    jsXHR.then(function(response) {
        $tablaProductos.clear().draw();
        productos = JSON.parse(response.d);
        $.each(productos, function(idx, prod) {
            var fila = new Array();
            fila.push(prod.CodProducto);
            fila.push(prod.EAN);
            fila.push(prod.Empresa);
            fila.push(prod.Nombre);
            fila.push(prod.Marca);
            fila.push(prod.Presentacion);
            fila.push(prod.Familia);
            fila.push(prod.Descripcion);
            if (prod.Propio == false || prod.Propio == null) {
                fila.push(
                    "<button type='button' class='btn btn-xs btn-success' style='margin: 0px 5px;' onclick='modificarProducto(\"{0}\")'><span class='fa fa-pencil'></span></button>".format(prod.CodProducto) +
                    "<button type='button' class='btn btn-xs btn-success' style='margin: 0px 5px;' onclick='quitarProducto(\"{0}\",\"{1}\")'><span class='fa fa-trash'></span></button>".format(prod.CodProducto, prod.Nombre)
                );
            }
            else {
                fila.push(
                    "<button type='button' class='btn btn-xs btn-success' style='margin: 0px 5px;' onclick='mostrarProducto(\"{0}\")'><span class='fa fa-eye'></span></button>".format(prod.CodProducto)
                );
            }

            var currentRow = $tablaProductos.row.add(fila).node();
            columnas = $(currentRow).find("td");
            $(columnas[3]).attr("class", "col-md-4");
            $(columnas[5]).attr("class", "col-md-2");
            $(columnas[6]).attr("class", "col-md-1 text-center");
        });
        $tablaProductos.draw();
        unblockUIOut();
    });
}


function agregarProducto() {
    $("#titleEdit").html("<b>Nuevo</b>  Producto");
    habilitarEdicion();
    togglePanels("panelEditarProducto");
    $("#codProducto").val("");
    $("#txtNombre").val("");
    $("#txtEAN").val("");
    $("#txtCodProducto").val("");
    $("#txtMarca").val("");
    $("#txtPresentacion").val("");
    $("#optCodEmpresa").val("-1");
    $("#optIdFamilia").val("-1");
    $("#optTipoProducto").val("TP_1");
    $("#rbtnHabilitadoSi").prop("checked", true);
    $("[id$=imagenmostrar]").attr("src", "../Recursos/Productos/no_producto.jpg");
}


function modificarProducto(codProducto) {
    $("#titleEdit").html("<b>Modificar</b>  Producto");
    blockUIIn();
    $("#codProducto").val(codProducto);
    api.call("Productos", {
        Comando: "SelectByCodProducto",
        CodProducto: codProducto
    }).then(function(response) {
        producto = JSON.parse(response.d)[0];
        //$("#txtIdProducto").val(producto.IdProducto);
        $("#txtNombre").val(producto.Nombre);
        $("#txtEAN").val(producto.EAN);
        //$("#txtCodProducto").val(producto.CodProducto);
        $("#txtMarca").val(producto.Marca);
        $("#optCodEmpresa").val(producto.CodEmpresa);
        $("#optIdFamilia").val(producto.CodFamilia);
        $("#txtPresentacion").val(producto.Presentacion);
        if (producto.Habilitado) {
            $("#rbtnHabilitadoSi").prop("checked", true);
        }
        else {
            $("#rbtnHabilitadoNo").prop("checked", true);
        }
        if (producto.Imagen != "") {
            path = $("[id$=hddURLProductos]").val();
            $("[id$=imagenmostrar]").attr("src", path + producto.Imagen);
        }
        else {
           $("[id$=imagenmostrar]").attr("src", "../Recursos/Productos/no_producto.jpg");
        }

    });
    togglePanels("panelEditarProducto");
    habilitarEdicion();
    $("#txtEAN").prop("disabled", true);
    unblockUIOut();
}


function deshabilitarEdicion() {
    $("#txtEAN").prop("disabled", true);
    $("#txtNombre").prop("disabled", true);
    $("#txtMarca").prop("disabled", true);
    $("#txtPresentacion").prop("disabled", true);
    $("#optCodEmpresa").prop("disabled", true);
    $("#optIdFamilia").prop("disabled", true);
    $("#optTipoProducto").prop("disabled", true);
    $("#btnBuscarFoto").hide();
    $("#btnTomarFoto").hide();
    $("#btnGuardar").hide();
    //$("#optCodEmpresa option[value=SS]").show();
}


function habilitarEdicion() {
    $("#txtEAN").prop("disabled", false);
    $("#txtNombre").prop("disabled", false);
    $("#txtMarca").prop("disabled", false);
    $("#txtPresentacion").prop("disabled", false);
    $("#optCodEmpresa").prop("disabled", false);
    $("#optIdFamilia").prop("disabled", false);
    $("#optTipoProducto").prop("disabled", false);
    $("#btnBuscarFoto").show();
    $("#btnTomarFoto").show();
    $("#btnGuardar").show();
    //$("#optCodEmpresa option[value=SS]").hide();
}


function mostrarProducto(codProducto) {
    $("#titleEdit").html("<b>Detalle</b>  Producto");
    api.call("Productos", {
        Comando: "SelectByCodProducto",
        CodProducto: codProducto
    }).then(function(response) {
        producto = JSON.parse(response.d)[0];
        //$("#txtIdProducto").val(producto.IdProducto);
        $("#txtNombre").val(producto.Nombre);
        $("#txtEAN").val(producto.EAN);
        $("#txtMarca").val(producto.Marca);
        $("#optCodEmpresa").val(producto.CodEmpresa);
        $("#optIdFamilia").val(producto.CodFamilia);
        $("#optTipoProducto").val(producto.CodTipoProducto);
        $("#txtPresentacion").val(producto.Presentacion);
        if (producto.Imagen != null && producto.Imagen != "") {
            path = $("[id$=hddURLProductos]").val();
            $("[id$=imagenmostrar]").attr("src", path + producto.Imagen);
        }
        else {
           $("[id$=imagenmostrar]").attr("src", "/Recursos/Productos/no_producto.jpg");
        }
    });
    togglePanels("panelEditarProducto");
    deshabilitarEdicion();
}


function guardarProducto() {
    var codProducto = $("#codProducto").val();
    if (codProducto == "") {
        cmd = "Insert";
    }
    else {
        cmd = "Update";
    }
    var nombre = $.trim($("#txtNombre").val());
    var ean = $.trim($("#txtEAN").val());
    //codProducto = $("#txtCodProducto").val();
    var marca = $.trim($("#txtMarca").val());
    var presentacion = $.trim($("#txtPresentacion").val());
    var codEmpresa = $.trim($("#optCodEmpresa").val());
    var codFamilia = $("#optIdFamilia").val();
    var codTipoProducto = $("#optTipoProducto").val();
    /*
    if ($("input[name=rbtnHabilitado]:checked").val()) {
        habilitado = 1;
    } else {
        habilitado  = 0;
    }
    */
    imageD = $("[id$=dataFoto]").val();
    if (imageD != "") {
        imagen = "x";
    }
    else {
        imagen = "";
    }

    //propio = "true";
    jsonData = {
        Comando: cmd,
        CodProducto: codProducto,
        EAN: ean,
        Nombre: nombre,
        Marca: marca,
        Presentacion: presentacion,
        CodEmpresa: codEmpresa,
        CodFamilia: codFamilia,
        Imagen: imagen,
        //Habilitado: habilitado,
        Habilitado: 1,
        //Propio: propio,
        ImageData: imageD,
        CodTipoProducto: codTipoProducto
    }

    if ($.trim(nombre) != "" &&
        $.trim(ean) != "" &&
        $.trim(ean).length == 13 &&
        $.trim(marca) != "" &&
        codFamilia != '-1' && $.trim(codFamilia) != '' && codFamilia != null &&
        $.trim(presentacion) != "") {
        api.call("Productos", jsonData).then(function(response) {

            if (codProducto == "") {
                if (response.d == "ERROR EAN YA EXISTE") {
                    showErrorMessage("EAN duplicado, Producto ya registrado");
                    $("#txtEAN").focus();
                }
                else {
                    loadTblProductos();
                    togglePanels("panelListadoProducto");
                    showSuccessMessage("Producto agregado");
                }
            }
            else {
                loadTblProductos();
                togglePanels("panelListadoProducto");
                showSuccessMessage("Producto actualizado");
            }

        });
    }
    else {
        if (ean.length < 13) {
            showErrorMessage("Debe completar con un EAN correcto");
        }
        else if (nombre == "") {
           showErrorMessage("Debe completar el campo Nombre");
        }
        else if (marca == "") {
            showErrorMessage("Debe completar el campo Marca");
        }
        else if (codFamilia == '-1' || codFamilia == null || $.trim(codFamilia) == '') {
            showErrorMessage("Debe seleccionar una familia para el producto");
        }
        else if (presentacion == "") {
            showErrorMessage("Debe completar el campo Presentación");
        }
    }


}


function quitarProducto(codProducto, nombre) {
    $("#hhdCodProductoDel").val(codProducto);
    $("#lblNombre").html(nombre);
    $("#deleteDialog").modal("show");
}


function onQuitarProducto() {
    var codProducto = $("#hhdCodProductoDel").val();
    api.call("Productos", {
        Comando: "Delete",
        CodProducto: codProducto,
        Imagen: "",
        ImageData: "",
    }).then(function(response) {
        $("#deleteDialog").modal("hide");
        showSuccessMessage("Producto deshabilitado");
        loadTblProductos();
    });
}





/**********  FOTOS ***********/
function inicializarModuloFoto() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }

    $("[id$=FileUploadFotoPerfil]").preimage();
    ejecutar();
    $("#btnTomarFoto").on("click", function () {
        $("[id$=botoneraTomarFoto]").show();
        $("[id$=botoneraEditarFoto]").hide();
        $("[id$=bodyTomarFoto]").show();
        $("[id$=bodyEditarFoto]").hide();
    });
}


function cargarFoto(url) {
    //alert( url );
    $("[id$=imagenmostrar]").attr("src", url);
    $("[id$=dataFoto]").attr("value", url);
}

function cerrarDialogoFoto() {
    $("[id$=botoneraTomarFoto]").show();
    $("[id$=botoneraEditarFoto]").hide();
    $("[id$=bodyTomarFoto]").show();
    $("[id$=bodyEditarFoto]").hide();
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.rect(0, 0, canvas.width, canvas.height);
    context.stroke();
    $("#btnpopup1").trigger("click");
    $("#btnpopup2").trigger("click");
}

function actualizarFoto() {
    cargarFotoCortada();
}

function ejecutar() {
    $image = $(".cropper");
    var $dataX = $("#dataX"),
    $dataY = $("#dataY"),
    $dataHeight = $("#dataHeight"),
    $dataWidth = $("#dataWidth"),
    console = window.console || { log: $.noop },
    cropper;
    $image.cropper({
        aspectRatio: 1 / 1,
        // autoCropArea: 1,
        data: {
            x: 520,
            y: 10,
            width: 1024,
            height: 768
        },
        preview: ".preview",

        // multiple: FALSE,
        // autoCrop: TRUE,
        // dragCrop: TRUE,
        // dashed: TRUE,
        // modal: TRUE,
        // movable: TRUE,
        // resizable: TRUE,
        // zoomable: TRUE,
        // rotatable: TRUE,

        // maxWidth: 480,
        // maxHeight: 270,
        // minWidth: 160,
        // minHeight: 90,

        done: function (data) {
            $dataX.val(data.x);
            $dataY.val(data.y);
            $dataHeight.val(data.height);
            $dataWidth.val(data.width);
        },

        build: function (e) {
            console.log(e.type);
        },

        built: function (e) {
            console.log(e.type);
        },

        dragstart: function (e) {
            console.log(e.type);
        },

        dragmove: function (e) {
            console.log(e.type);
        },

        dragend: function (e) {
            console.log(e.type);
        }
    });

    cropper = $image.data("cropper");

    $image.on({
        "build.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "built.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragstart.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragmove.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragend.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        }
    });

    $("#reset").click(function () {
        $image.cropper("reset");
    });

    $("#reset2").click(function () {
        $image.cropper("reset", true);
    });

    $("#clear").click(function () {
        $image.cropper("clear");
    });

    $("#destroy").click(function () {
        $image.cropper("destroy");
    });

    $("#enable").click(function () {
        $image.cropper("enable");
    });

    $("#disable").click(function () {
        $image.cropper("disable");
    });

    $("#zoom").click(function () {
        $image.cropper("zoom", $("#zoomWith").val());
    });

    $("#zoomIn").click(function () {
        $image.cropper("zoom", 0.9);
    });

    $("#zoomOut").click(function () {
        $image.cropper("zoom", -0.9);
    });

    $("#rotate").click(function () {
        $image.cropper("rotate", $("#rotateWith").val());
    });

    $("#rotateLeft").click(function () {
        $image.cropper("rotate", -90);
    });

    $("#rotateRight").click(function () {
        $image.cropper("rotate", 90);
    });

    $("#setAspectRatio").click(function () {
        $image.cropper("setAspectRatio", $("#aspectRatio").val());
    });

    $("#replace").click(function () {
        //$image.cropper( "replace", $( "#replaceWith" ).val() );
        $image.cropper("replace", $("#fotoMostrar").val());
    });

    $("#getImageData").click(function () {
        $("#showImageData").val(JSON.stringify($image.cropper("getImageData")));
    });

    $("#setData").click(function () {
        $image.cropper("setData", {
            x: $dataX.val(),
            y: $dataY.val(),
            width: $dataWidth.val(),
            height: $dataHeight.val()
        });
    });

    $("#getData").click(function () {
        $("#showData").val(JSON.stringify($image.cropper("getData")));
    });

    $("#getDataURL").click(function () {
        //var dataURL = $image.cropper( "getDataURL" );

        //$( "#dataURL" ).text( dataURL );
        //$( "#showDataURL" ).html( '<img src="' + dataURL + '">' );
        cerrarDialogoFoto();
    });

    $("#getDataURL2").click(function () {
        var dataURL = $image.cropper("getDataURL", "image/jpeg");

        $("#dataURL").text(dataURL);
        $("#showDataURL").html('<img src="' + dataURL + '">');
        cargarFoto(dataURL);
        cerrarDialogoFoto();
    });
};

function cargarURLFoto() {
    $("#imagenCortar").attr("src", $("#iframe12").attr("foto"));
    alert($("#iframe12").attr("foto"));
}

function agregarURL(url) {
    //$( "#imagenCortar" ).removeAttr( "src");
    $("#imagenCortar").attr("src", url);
    $("#fotoMostrar").attr("value", url);
    $("#replace").trigger("click");
    ejecutar();
}

function salir() {
    $("#btnpopup").trigger("click");
}

function cargarFotoCortada() {
    $("#getDataURL2").trigger("click");
}

//FIN Funciones para fotos

// Put event listeners into place
window.addEventListener("DOMContentLoaded", function () {
    // Grab elements, create settings, etc.
    canvas = document.getElementById("canvas");
    canvasHdd = document.getElementById("canvasHidden");
    context = canvas.getContext("2d");
    context.rect(0, 0, canvas.width, canvas.height);
    context.stroke();
    contextHdd = canvasHdd.getContext("2d");

    video = document.getElementById("video");
    videoObj = { "video": true };
    errBack = function (error) {
        console.log("Video capture error: ", error.code);
    };

    // Put video listeners into place
    if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function (stream) {
            video.src = stream;
            video.play();
            console.log("Standard");
        }, errBack);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function (stream) {
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
            console.log("WebKit-prefixed");
        }, errBack);
    } else if (navigator.mozGetUserMedia) { // WebKit-prefixed
        navigator.mozGetUserMedia(videoObj, function (stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
            console.log("WebKit-prefixed 22");
        }, errBack);
    }

    // Trigger photo take
    document.getElementById("snap").addEventListener("click", function () {
        context.drawImage(video, 0, 0, 280, 220);
        //context.drawImage( video, 0, 0, 1024, 768);
        contextHdd.drawImage(video, 0, 0, 1024, 768);
    });
    document.getElementById("btnConfirnarFoto").addEventListener("click", function () {
        document.getElementById("imgCaptura").src = canvas.toDataURL();
        $("[id$=imagenmostrar]").attr("src", document.getElementById('imgCaptura').src);

        //console.log(canvas.toDataURL("image/png"));
        $image.cropper("replace", canvasHdd.toDataURL("image/png"));
        $image.cropper("reset", true);

        //$("#btnMostrarFotoRecortar").click();
        //$(window).trigger('resize');

        //document.getElementById("hidden").value = canvas.toDataURL("image/png").replace('data:image/png;base64,', '');
        $("[id$=botoneraTomarFoto]").hide();
        $("[id$=botoneraEditarFoto]").show();
        $("[id$=bodyTomarFoto]").hide();
        $("[id$=bodyEditarFoto]").show();
    });
}, false);



function mostrarDialogoBuscarFoto() {
    $("[id$=FileUploadFotoPerfil]").click();
}
//Datos subir archivos - Inicio
function mostrarDialogoBuscarFichero() {
    $("[id$=FileUploadFichero]").click();
}


/*********** FOTOS END *********/

//Bloquea página.
function blockUIIn() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }
    $(parent.$("form")).block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}
//Desbloquea página.
function unblockUIOut(el) {
    $(parent.$("form")).unblock();
}

//funciones para generar excel
var cookie;
function generarExcel() {
    document.cookie = 'termino=error; path=/';
    blockUIIn();
    cookie = window.setInterval(function () {
        var cookieValue = getCookie('termino');
        if (cookieValue == "termino") {
            finishDownload();
        }
    }, 1000);

    document.getElementById('btnExcelear').click();

}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function finishDownload() {
    window.clearInterval(cookie);
    document.cookie = 'termino=error; path=/';
    unblockUIOut();
}

//FIN funciones para generar excel