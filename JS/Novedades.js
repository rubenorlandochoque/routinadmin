var tblPuntosVenta = null;
var tblProductos = null;
var tblNovedades = null;
var api = null;
//var mapa = null;
var gpsPoint = null;
var tiposNovedad = {};

$(document).ready(function() {
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: 'Anterior',
        nextText: 'Siguiente',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    $("#txtFechaDesde").datepicker();
    $("#txtFechaDesde").mask('00/00/0000');;

    $("#txtFechaHasta").datepicker();
    $("#txtFechaHasta").mask('00/00/0000');

    $("#optCodRepositor").select2({
        "language": {
            "noResults": function(){
                return "No se han encontrado resultados";
            }
        },
        escapeMarkup: function (markup) {
            return markup;
        }
    });
    /*
    mapa = new GMaps({
          div: '#map_canvas',
          lat: -24.7761747,
          lng: -65.4115812
    });
    */
    api = new RestAPI();

    cargarTiposNovedad();
    blockUIIn();
    configurarTblPV();
    configurarTblProductos();
    configurarTblNovedades();
    cargarTblPuntosVenta();
    cargarTblProductos();
    cargarRepositores();
    unblockUIOut();

    $("#rContDatosProd").hide();
});



function configurarTblPV() {
    if (tblPuntosVenta  != null) {
        tblPuntosVenta.destroy();
    }

    tblPuntosVenta  = $("#tblListadoPV").DataTable({
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Ultima",
                "sNext": "Siguiente",
                "sPrevious": "Atras",
            },
            "sProcessing": "Procesando...",
            "sSearch": "Buscar: ",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Registro _START_ al _END_ (_TOTAL_ registros en total)",
            "sInfoEmpty": "-",
            "sInfoFiltered": ", filtrado de un total de _MAX_ registros.",
            "sInfoPostFix": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando..."
        },
        "iDisplayLength": 10,
        "bLengthChange": false,
        "columnDefs": [
            {
                //"targets": [0],
                //"visible": false
                //"searchable": false
            }
        ]
    });
}


function configurarTblProductos() {
    if (tblProductos != null) {
        tblProductos.destroy();
    }

    tblProductos = $("#tblListadoArt").DataTable({
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Ultima",
                "sNext": "Siguiente",
                "sPrevious": "Atras",
            },
            "sProcessing": "Procesando...",
            "sSearch": "Buscar: ",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Registro _START_ al _END_ (_TOTAL_ registros en total)",
            "sInfoEmpty": "-",
            "sInfoFiltered": ", filtrado de un total de _MAX_ registros.",
            "sInfoPostFix": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando..."
        },
        "iDisplayLength": 10,
        "bLengthChange": false,
        "columnDefs": [
            {
                //"targets": [0],
                //"visible": false
                //"searchable": false
            }
        ]
    });
}


function configurarTblNovedades() {
    if (tblNovedades != null) {
        tblNovedades.destroy();
    }

    tblNovedades= $("#tblNovedades").DataTable({
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Ultima",
                "sNext": "Siguiente",
                "sPrevious": "Atras",
            },
            "sProcessing": "Procesando...",
            "sSearch": "Buscar: ",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Registro _START_ al _END_ (_TOTAL_ registros en total)",
            "sInfoEmpty": "-",
            "sInfoFiltered": ", filtrado de un total de _MAX_ registros.",
            "sInfoPostFix": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando..."
        },
        "iDisplayLength": 10,
        "bLengthChange": false,
        "columnDefs": [
            {
                //"targets": [0],
                //"visible": false
                //"searchable": false
            }
        ]
    });
}



/* Metodos Especificos */

function onLoadWinSearchPV() {
    $("#winSearchPV").modal("show");
}


function onLoadWinSearchArt() {
    $("#winSearchArt").modal("show");
}


function cargarTiposNovedad() {
    $.ajax({
        type: "POST",
        url: 'Novedades.aspx/GetTiposNovedad',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            var novedades = JSON.parse(response.d);
            tiposNovedad = {};
            var content = "<option value=''>Seleccione una opción</option>";
            $.each(novedades, function(index, nov) {
                tiposNovedad[nov.CodTipoNovedad] = nov.Descripcion;
                content += "<option value='{0}'>{1}</option>".format(nov.CodTipoNovedad, nov.Descripcion);
            });
            $("#optTipoNovedad").html(content);
        }
    });
}

function cargarTblPuntosVenta() {
    $.ajax({
        type: "POST",
        url: 'Novedades.aspx/GetAllPuntosVentas',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function(response) {
            puntosVenta = JSON.parse(response.d);
            tblPuntosVenta.clear().draw();
            $.each(puntosVenta, function(index, pv) {
                var fila = new Array();
                fila.push($.trim(pv.Nombre));
                fila.push($.trim(pv.CodSucursal));
                fila.push($.trim(pv.Direccion));
                fila.push($.trim(pv.Ciudad));
                fila.push($.trim(pv.Provincia));
                fila.push($.trim(pv.Zona));
                fila.push('<button type="button" class="btn btn-success btn-xs" style="margin: 0px 5px;" onclick="onSeleccionarPV(\'{0}\', \'{1}\', \'{2}\')"><i class="fa fa-external-link-square"></i></button>'.format(pv.CodPuntoVenta, pv.Nombre, pv.CodSucursal));
                var currentRow = tblPuntosVenta.row.add(fila).node();
                //columnas = $(currentRow).find("td");
            });

            //Agregar la opcion Todos
            var fila = new Array();
            fila.push("<span style='color:#fff'>-</span>Todos");
            fila.push("Todos");
            fila.push("");
            fila.push("");
            fila.push("");
            fila.push("");
            fila.push('<button type="button" class="btn btn-success btn-xs" style="margin: 0px 5px;" onclick="onSeleccionarPV(\'{0}\', \'{1}\', \'{2}\')"><i class="fa fa-external-link-square"></i></button>'.format("", "", ""));
            var currentRow = tblPuntosVenta.row.add(fila).node();
            tblPuntosVenta.draw();
        }
    });
}


function cargarTblProductos() {
    console.log("tbl productos");
    var jsXHR = api.call("Productos", {Comando: "SelectAllAlt"});
    jsXHR.then(function(response) {
        var productos = JSON.parse(response.d);
        tblProductos.clear().draw();
        $.each(productos, function(index, art) {
            var fila = new Array();
            fila.push($.trim(art.CodProducto));
            fila.push($.trim(art.EAN));
            fila.push($.trim(art.Empresa));
            fila.push($.trim(art.Nombre));
            fila.push($.trim(art.Marca));
            fila.push($.trim(art.Presentacion));
            fila.push('<button type="button" class="btn btn-success btn-xs" style="margin: 0px 5px;" onclick="onSeleccionarArt(\'{0}\', \'{1}\', \'{2}\')"><i class="fa fa-external-link-square"></i></button>'.format(art.CodProducto, art.Nombre, art.Marca));
            var currentRow = tblProductos.row.add(fila).node();
        });
        var fila = new Array();
        fila.push("<span style='color:#fff'>-</span>Todos");
        fila.push("Todos");
        fila.push("");
        fila.push("");
        fila.push("");
        fila.push("");
        fila.push('<button type="button" class="btn btn-success btn-xs" style="margin: 0px 5px;" onclick="onSeleccionarArt(\'{0}\', \'{1}\', \'{2}\')"><i class="fa fa-external-link-square"></i></button>'.format("", "", ""));
        var currentRow = tblProductos.row.add(fila).node();
        tblProductos.draw();
    });
}


function cargarRepositores() {
    $.ajax({
        type: "POST",
        url: 'Novedades.aspx/GetAllRepositores',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            repositores = JSON.parse(response.d);
            content = '<option value="-1">Seleccione una opción</option>';
            $.each(repositores, function(index, usr) {
                content += '<option value="{0}">{1}</option>'.format(usr.CodUsuario, usr.apellido + ", " + usr.nombres);
            });
            //console.log(content);
            $("#optCodRepositor").html(content);
            $("#optCodRepositor").html(content);
            if ($("#optCodRepositor option").length == 2) {
                var activo = $("#optCodRepositor option").eq(1).val();
                $("#optCodRepositor").select2('destroy');
                $("#optCodRepositor").val(activo);
                $("#optCodRepositor").prop("disabled", true);
            }
        }
    });
}


function onSeleccionarPV(codPV, nombre, sucursal) {
    console.log(codPV, nombre, sucursal);
    if (nombre != "" && sucursal != "") {
        var nombrePV = "{0} ({1})".format($.trim(nombre), $.trim(sucursal));
    }
    else {
        nombrePV = "";
    }
    $("#txtNamePV").val(nombrePV);
    $("#txtCodPuntoVenta").val($.trim(codPV));
    $("#txtCodSucursal").val($.trim(sucursal));
    $("#winSearchPV").modal("hide");
}


function onSeleccionarArt(codArt, nombre, presentacion) {
    console.log(codArt, nombre, presentacion);
    if (nombre != "" && presentacion != "") {
        var nombreArt = "{0} ({1})".format($.trim(nombre), $.trim(presentacion));
    }
    else {
        nombreArt = "";
    }
    $("#txtNameArt").val(nombreArt);
    $("#txtCodArticulo").val($.trim(codArt));
    $("#winSearchArt").modal("hide");
}


function filtrarNovedades() {
    var codPuntoVenta = $.trim($("#txtCodPuntoVenta").val());
    var codSucursal = $.trim($("#txtCodSucursal").val());
    var codRepositor = $.trim($("#optCodRepositor").val());
    var codProducto = $.trim($("#txtCodArticulo").val());
    var fechaIni = $.trim($("#txtFechaDesde").val());
    var fechaFin = $.trim($("#txtFechaHasta").val());
    var codTipoNovedad = $.trim($("#optTipoNovedad").val());
    var _fechaIni = fechaIni.split("/")[2] + fechaIni.split("/")[1] + fechaIni.split("/")[0];
    var _fechaFin = fechaFin.split("/")[2] + fechaFin.split("/")[1] + fechaFin.split("/")[0];

    var errFlag = false;
    if (codPuntoVenta == "-1") {
        //errFlag = true;
        //showErrorMessage("Debe especificar el Punto de Venta");
    }

    if (codRepositor == "" || codRepositor == "-1") {
        //errFlag = true;
        //showErrorMessage("Debe especificar el Repositor");
    }

    if (codProducto == "-1") {
        //errFlag = true;
        //showErrorMessage("Debe especificar el Articulo");
    }

    if (fechaIni != "" || fechaFin != "") {
        if (fechaIni != "") {
            if (!validateDate(fechaIni)) {
                errFlag = true;
                showErrorMessage("La Fecha Desde es invalida");
                $("#txtFechaDesde").focus();
            }
        }
        if (fechaFin != "")  {
            if (!validateDate(fechaFin)) {
                errFlag = true;
                showErrorMessage("La Fecha Hasta es invalida");
                $("#txtFechaHasta").focus();
            }
        }
        //errFlag = true;
        //showErrorMessage("Debe especificar el rango de Fechas");
    }


    if (_fechaFin < _fechaIni && fechaFin != "" && fechaIni != "") {
        errFlag = true;
        showErrorMessage("La Fecha de Desde no puede ser mayor que la Fecha Hasta");
    }

    if (codPuntoVenta == "-1") {
        codPuntoVenta = '';
    }

    if (codRepositor == "-1") {
        codRepositor = '';
    }

    if (codProducto == "-1") {
        codProducto = '';
    }


    if (!errFlag) {
        blockUIIn();

        $("#hddCodPVNovedad").val(codPuntoVenta);
        $("#hddCodSucNovedad").val(codSucursal);
        $("#hddRepositorNovedad").val(codRepositor);
        $("#hddCodArticuloNovedad").val(codProducto);
        $("#hddCodTipoNovedad").val(codTipoNovedad);
        $("#hddFechaIniNovedad").val(fechaIni.replace("/", "-").replace("/", "-"));
        $("#hddFechaFinNovedad").val(fechaFin.replace("/", "-").replace("/", "-"));

        $.ajax({
            type: "POST",
            url: 'Novedades.aspx/FiltrarNovedades',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                CodRepositor: codRepositor,
                CodPuntoVenta: codPuntoVenta,
                CodSucursal: codSucursal,
                CodProducto: codProducto,
                CodTipoNovedad: codTipoNovedad,
                FechaInicio: fechaIni,
                FechaFin: fechaFin,
            }),
            success: function(response) {
                var novedades = JSON.parse(response.d);
                tblNovedades.clear().draw()
                $.each(novedades, function(index, rut) {
                    var fila = new Array();
                    var repositor = "{0} - {1} {2}".format($.trim(rut.Legajo), $.trim(rut.Apellido), $.trim(rut.Nombres));
                    fila.push(repositor);
                    fila.push("{0} ({1})".format($.trim(rut.NombrePV), $.trim(rut.CodSucursal)));
                    fila.push(convertDate(rut.FechaCreacion));
                    fila.push($.trim(rut.EAN));
                    fila.push($.trim(rut.Nombre));
                    //fila.push("$ {0}".format($.trim(rut.Precio)));
                    tNov = $.trim(rut.CodTipoNovedad);
                    if (tNov != "") {
                        txt = tiposNovedad[tNov];
                    }
                    else {
                        txt = "No Especificada";
                    }
                    fila.push(txt);

                    fila.push('<button type="button" class="btn btn-success btn-xs" style="margin: 0px 5px; width: 20px;" onclick="mostrarNovedad(\'{0}\',\'{1}\')"><i class="fa fa-info"></i></button>'.format($.trim(rut.CodNovedad), repositor));
                    var currentRow = tblNovedades.row.add(fila).node();
                });
                tblNovedades.draw();
                if (novedades.length > 0) {
                    $("#btnExportarExcel").removeAttr("disabled");
                } else {
                    $("#btnExportarExcel").attr("disabled", "disabled");
                }
                unblockUIOut();
            },
            fail: function (xhr, err, cerr) {
                $("#btnExportarExcel").attr("disabled", "disabled");
                showErrorMessage("Error coneccion al servidor, verifique su conexión a internet");
                unblockUIOut();
            },
        });


    } else {
        $("#btnExportarExcel").attr("disabled", "disabled");
    }

    //console.log(codPuntoVenta,",", codProducto, ",", codRepositor, fechaIni, fechaFin);
}


function traerFotosNovedad(codNovedad) {
    $.ajax({
        type: "POST",
        url: 'Novedades.aspx/GetFotosNovedad',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        data: JSON.stringify({
            CodNovedad: codNovedad,
        }),
        success: function(response) {
            var pathFotos = $("[id$=hddURLFotos]").val();
            var tpl = '<div class="col-md-4">' +
            '<a href="{0}" class="fancybox" id="lightfoto">' +
            '<div class="img-thumbnail"><img src="{0}" class="img-responsive" id="rFotoNovedad">' +
            '</div></a></div>';
            var fotos = JSON.parse(response.d);
            var content = "";
            $.each(fotos, function(index, foto) {
                var path = pathFotos + foto.Imagen;
                $.ajax({
                    url: path,
                    type:'HEAD',
                    async: false,
                    error: function() {
                        path = pathFotos + "no_image.png";
                    },
                    success: function(){}
                });
                content += tpl.format(path);
            });
            $("#fotosNovedad").html(content);
            $(".fancybox").fancybox();
        }
    });
}

function mostrarNovedad(codNovedad, repositor) {
    blockUIIn();
    $.ajax({
        type: "POST",
        url: 'Novedades.aspx/GetDatosNovedad',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
            CodNovedad: codNovedad,
        }),
        success: function(response) {
            $("#panelFiltrarRuteo").hide();
            $("#rContDatosProd").show();
            //mapa.refresh();
            //mapa.removeMarkers();

            novedad = JSON.parse(response.d)[0];
            if (typeof(novedad) != "undefined") {
                traerFotosNovedad(codNovedad);
                $("#rNombre").html(novedad.producto);
                $("#rMarca").html(novedad.Marca);
                $("#rPropio").html(toPropio(novedad.Propio));
                $("#rPresentacion").html(novedad.Presentacion);
                path = $("[id$=hddURLProductos]").val();
                console.log(novedad.Imagen);

                if (novedad.Imagen != null  &&  $.trim(novedad.Imagen) != '') {
                    $.ajax({
                        url: path + novedad.Imagen,
                        type:'HEAD',
                        async: false,
                        error: function() {
                            $("#rImagen").attr("src", path + novedad.Imagen);
                        },
                        success: function(){
                            $("#rImagen").attr("src", path + "no_producto.jpg");
                        }
                    });
                }
                else {
                    $("#rImagen").attr("src", path + "no_producto.jpg");
                }

                $("#rFamilia").html(novedad.Familia);
                $("#rRepositor").html(repositor);
                $("#rCodProducto").html(novedad.CodProducto);
                $("#rEAN").html(novedad.EAN);
                $("#rCodTipoNovedad").html(tiposNovedad[novedad.CodTipoNovedad]);
                $("#rPuntoVenta").html(novedad.NombrePV);
                $("#rCodSucursal").html(novedad.CodSucursal);
                $("#rEmpresa").html(novedad.empresa);
                $("#rTipoCliente").html(novedad.TipoCliente);
                $("#rDia").html(novedad.dia);
                $("#rObservacion").html(novedad.Observacion);
                $("#rPrecio").html("{0}".format(parseFloat(novedad.Precio).toFixed(2)));
                $("#rDireccion").html(novedad.Direccion);
                $("#rCiudad").html(novedad.Ciudad);
                $("#rProvincia").html(novedad.Provincia);
                $("#rZona").html(novedad.Zona);

                $("#rFechaElab").html(novedad.fechaElavoracion);
                $("#rHoraElab").html(novedad.HoraElaboracion);
                $("#rNumLote").html(novedad.NumLote);
                $("#rCantidad").html(novedad.Cantidad);

                $("#rGPS").html(novedad.Coordenadas);
                $("#rPresicion").html(novedad.rango);
                /*
                pos = novedad.Coordenadas.split(";");
                _lat = (parseFloat(pos[0]));
                _lng = (parseFloat(pos[1]));

                if (_lat == 0 && _lng == 0) {
                    showErrorMessage("Coordenadas de ruteo no validas");
                    _lat = -24.7896132;
                    _lng = -65.4104367;
                }

                gpsPoint = mapa.addMarker({
                    lat: _lat,
                    lng: _lng,
                    icon: "mark1.png",
                });

                mapa.map.setCenter(gpsPoint.getPosition());
                */
            }
            else {
                unblockUIOut();
                ShowErrorMessage("No se obtuvieron Datos");
            }

            unblockUIOut();
        },
        fail: function(xhr, err) {
            showErrorMessage("Error al traer datos, revise su coneccion a internet");
            unblockUIOut();
        }
    });
}


function onVolver() {
    $("#panelFiltrarRuteo").show();
    $("#rContDatosProd").hide();
}




/* Funciones Genericas */

function translateFecha(fecha) {
    var data = fecha.split("/");
    return data[2] + data[1] + data[0];
}


function ShowErrorMessage(msj) {
    return showErrorMessage(msj);
}


Date.prototype.dmy = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = this.getDate().toString();
    console.log(dd,mm,yyyy);
    return "{2}/{1}/{0}".format(yyyy ,(mm[1]?mm:"0"+mm[0]), (dd[1]?dd:"0"+dd[0])); // padding
};


function convertDate(str_) {
    var dn = str_.substring(6, 19);
    var fecha = new Date(parseInt(dn));
    return fecha.dmy();
}

function isValidDate(day, month, year)
{
    var dteDate;
    month=month-1;
    dteDate = new Date(year, month, day);
    return ((day==dteDate.getDate()) && (month==dteDate.getMonth()) && (year==dteDate.getFullYear()));
}

function validateDate(_fecha)
{
    var fecha = "{2}-{1}-{0}".format(_fecha.split("/")[0], _fecha.split("/")[1], _fecha.split("/")[2]);
    var patron = new RegExp("^(19|20)+([0-9]{2})([-])([0-9]{1,2})([-])([0-9]{1,2})$");

    if(fecha.search(patron)==0)
    {
        var values=fecha.split("-");
        if(isValidDate(values[2],values[1],values[0]))
        {
            return true;
        }
    }
    return false;
}

function toPropio(propio) {
    if (propio == true) {
        return "Propio";
    }
    else {
        return "Competencia";
    }
}


//Bloquea página.
function blockUIIn() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }
    $(parent.$("form")).block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}


//Desbloquea página.
function unblockUIOut() {
    $(parent.$("form")).unblock();
}

//funciones para generar excel
var cookie;
function generarExcel() {
    document.cookie = 'termino=error; path=/';
    blockUIIn();
    cookie = window.setInterval(function () {
        var cookieValue = getCookie('termino');
        if (cookieValue == "termino") {
            finishDownload();
        }
    }, 1000);
    //alert("antes");
    document.getElementById('btnExcelear').click();
    //alert("despues");
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function finishDownload() {
    window.clearInterval(cookie);
    document.cookie = 'termino=error; path=/';
    unblockUIOut();
}

//FIN funciones para generar excel
