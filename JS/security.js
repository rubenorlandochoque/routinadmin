$(document).ready(function() {
    validarPantallas();
});


function validarPantallas() {
    pantallas = [
        'P_REPOSITORES',
        'P_PUNTOS_VENTAS',
        'P_PRODUCTOS',
        'P_RUTEO',
        'P_NOVEDADES',
        'P_EXPORTAR',
        'P_TRAKING',
        'P_EMPRESAS',
        'P_CONFIGURACION',
        'P_MI_CUENTA',
        'P_RELEVAMIENTOS',
        'P_TIPOPRODUCTO'
    ];
    var permisos = $("[id$=hddPermisos]").val();
    $.each(pantallas, function (index, pant) {
        if (permisos.indexOf(pant) != -1) {
            leftMenu = $("[pant=" + pant + "]");
            oMenu = $("[pant=O_" + pant + "]");
            $.each(leftMenu, function (idx, obj) {
                $(obj).show();
            });
            $.each(oMenu, function (idx, obj) {
                $(obj).show();
            });
        }
        else {
            leftMenu = $("[pant=" + pant + "]");
            oMenu = $("[pant=O_" + pant + "]");
            $.each(leftMenu, function (idx, obj) {
                $(obj).hide();
            });
            $.each(oMenu, function (idx, obj) {
                $(obj).hide();
            });
        }
    });
}