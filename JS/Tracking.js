var mapa = null;
var mapPVPoints = [];
var mapRuteoPoints = {};
var trackingPoints = {};
var drawLine = false;
var counter = 10;

$(document).ready(function() {
    cargarListadoRepositores();

    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: 'Anterior',
        nextText: 'Siguiente',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);


    $("#txtFecha").datepicker();
    $("#txtFecha").mask('00/00/0000');

    mapa = new GMaps({
        div: '#Mapa',
        lat: -24.7780049,
        lng: -65.4123518
    });

    //togglePanels("panelFiltradoTracking");
    mapa.refresh();

    $("#panelListadoTracking").hide();

});

function cargarListadoRepositores() {
    var data = {};
    data["_codUsuario"] = $("[id$=hddCodUsuarioActivo]").val();
    $.ajax({
        type: "POST",
        url: 'Traking.aspx/GetAllRepositores',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            $("#listadoRepositores").html("");
            $("#optCodRepositor").html("<option value='-1'>Seleccione una opción</option>");
            var result = JSON.parse(respuesta.d);
            $.each(result, function () {
                $("#optCodRepositor").append('<option value="' + this.CodUsuario + '">' + this.apellido + ", " + this.nombres + '</option>');
                //$("#listadoRepositores").append('<li style="cursor: pointer" codrep="' + this.CodUsuario + '"><a>' + this.apellido + ' ' + this.nombres + '<span class="pull-right badge bg-blue"><i class="fa fa-fw fa-circle-o"></i></span></a></li>');
                $("#optCodRepositor").select2();
            });

        }
    });
}

function drawPV() {
    $.each(mapPVPoints, function(index, pv) {
        counter += 1;
        mark = mapa.addMarker({
            lat: pv.lat,
            lng: pv.lng,
            icon: "townhouse.png",
            label: "",
            zIndex: 1,
            infoWindow: {
                content: '<p>' + pv.nombre + '</p>'
            }
        });
    });
}


function drawTraking(all) {
    if (typeof(all) == "undefined") {
        all = false;
    }

    var puntos = $("#tblTracking tbody").find("tr");
    var i = 0;
    $.each(puntos, function(index, punto) {
        counter += 1;
        var activo = $(punto).attr("activo");
        var cod = $(punto).attr("codigo");
        var pt = trackingPoints[cod];
        if (all) {
            if (i == 0) {
                icon_ = "celeste_24.png";
            }
            else if (i == puntos.length - 1) {
                icon_ = "rojo_24.png";
            }
            else {
                icon_ = "azul_24.png";
            }
            var mark = mapa.addMarker({
                lat: pt.lat,
                zIndex: counter,
                lng: pt.lng,
                icon: icon_,
                infoWindow: {
                    content: pt.content
                }
            });
            i += 1;
        }

        else if (activo == "1") {
            var mark = mapa.addMarker({
                lat: pt.lat,
                zIndex: counter,
                lng: pt.lng,
                icon: "azul_24.png",
                infoWindow: {
                    content: pt.content
                }
            });
        }

        else {
            //marca.setMap(null);
        }
    });
}

function drawRuteo() {
    //ya no se muestran los puntos de ruteo en el mapa pero se deja
    //la referencia a la funcion por tema de compatibilidad
    /*
    var puntos = $("#tblRuteo tbody").find("tr");
    $.each(puntos, function (index, punto) {
        counter += 1;
        var activo = $(punto).attr("activo");
        var cod = $(punto).attr("codigo");
        var rt = mapRuteoPoints[cod];
        if (activo == "1") {
            mark = mapa.addMarker({
                lat: rt.lat,
                lng: rt.lng,
                zIndex: counter,
                icon: "market.png",
                infoWindow: {
                    content: rt.content,
                }
            });
        }
    });
    */
}


function drawTrakingLine() {
    var markets = [];
    $.each(trackingPoints, function(index, pt) {
        markets.push([pt.lat, pt.lng]);
    });

    mapa.drawPolyline({
        path: markets,
        strokeColor: '#131540',
        strokeOpacity: 0.6,
        strokeWeight: 3
    });

    cod = $("#tblTracking tr").first().attr("codigo");
    if (typeof(cod) != "undefined") {
        mapa.map.setCenter(trackingPoints[cod]);
    }
}


function clearMap() {
    mapa.removeMarkers();
    mapa.removePolylines();
    counter = 10;
}


function drawMap() {
    clearMap();
    drawPV();
    counter += 100;
    drawRuteo();
    drawTraking(drawLine);
    if (drawLine) {
        drawTrakingLine();
    }
}


function cargarListados() {
    var errorFlag = false;
    if ($.trim($("#txtFecha").val()) == "") {
        showErrorMessage("Debe indicar una fecha");
        errorFlag = true;
    }

    else if (!validateDate($.trim($("#txtFecha").val()))) {
        showErrorMessage("La Fecha es incorrecta");
        errorFlag = true;
    }

    else if ($("#optCodRepositor").val() == "-1") {
        showErrorMessage("Debe especificar un repositor");
        errorFlag = true;
    }

    if (!errorFlag) {
        togglePanels("panelListadoTracking");
        $("#lblNombreRepositor").html($("#optCodRepositor option:checked").html());
        blockUIIn();

        cargarTraking();
        cargarRuteos();
        cargarPV();
        clearMap();
        setTimeout(function() {
            mapa.refresh();
            clearMap();
            drawMap();
        }, 500);
    }
}


function cargarTraking() {
    var data = {};
    data["_codUsuario"] = $("#optCodRepositor").val();
    data["_fecha"] = $("#txtFecha").val().replace("/", "-").replace("/", "-");
    $.ajax({
        type: "POST",
        url: 'Traking.aspx/VerTraking',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            $("#tablaTraking").html("");
            trackingPoints = {};
            var result = JSON.parse(respuesta.d);
            var filas = "";
            var idx = 0;
            var repositor = $("#optCodRepositor option:selected").html();
            $.each(result, function () {
                idx +=  1;
                filas += '<tr activo="0" style="cursor: pointer" orden="' + idx + '"  codigo="' + this.codTraking + '" pv="' + this.puntoVenta + '" coordenadas="' + this.Coordenadas + '"><td>' + this.Hora + '</td><td>' + this.puntoVenta + '</td></tr>';
                markerCont = "<div><p><strong>" + repositor + "</strong></p>" +
                             "<p> Numero: " +  idx + "</p>" +'<p> Hora:'  + this.Hora + "</p></div>";
                partes = this.Coordenadas.replace(",", ";").split(";");
                _lat = parseFloat(partes[0]);
                if (_lat == 0) {_lat = -24.789211; }
                _lng = parseFloat(partes[1]);
                if (_lng == 0) {_lng = -65.410323; }
                trackingPoints[this.codTraking] = { lat: _lat, lng: _lng, content: markerCont };
            });
            console.log(idx);
            $("#tablaTraking").html(filas);
            $("#tablaTraking tr").click(function () {
                var activo = $(this).attr("activo");
                if (activo == "1") {
                    $(this).css("background-color", "#FFFFFF");
                    var activo = $(this).attr("activo", "0");
                } else {
                    $(this).css("background-color", "#66CCFF");
                    var activo = $(this).attr("activo", "1");
                    cod = $(this).attr("codigo");
                    mapa.map.setCenter(trackingPoints[cod]);
                }
                drawMap();
            });
        }
    });
}


function cargarPV() {
    var data = {};
    data["_codUsuario"] = $("#optCodRepositor").val();
    $.ajax({
        type: "POST",
        url: 'Traking.aspx/VerPV',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            var result = JSON.parse(respuesta.d);
            mapPVPoints = new Array();
            $.each(result, function () {
                partes = this.Coordenadas.replace(",", ";").split(";");
                _lat = parseFloat(partes[0]);
                if (_lat == 0) {_lat = -24.789211; }
                _lng = parseFloat(partes[1]);
                if (_lng == 0) {_lng = -65.410323; }
                mapPVPoints.push({lat: _lat, lng: _lng, nombre: this.PuntoVenta});
            });
            //drawPV();
        }
    });
}

function cargarRuteos() {
    var data = {};
    data["_codUsuario"] = $("#optCodRepositor").val();
    data["_fecha"] = $("#txtFecha").val().replace("/", "-").replace("/", "-");
    $.ajax({
        type: "POST",
        url: 'Traking.aspx/verRuteoByUserDay',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            //console.log(this.CodRepositor);
            $("#tablaRuteo").html("");
            mapRuteoPoints = {};
            var result = JSON.parse(respuesta.d);
            var filas = "";
            $.each(result, function () {
                filas += '<tr activo="0"  style="cursor: pointer" codigo="' + this.CodRuteo + '" pv="' + this.puntoVenta + '" coordenadas="' + this.Coodenadas + '"><td>' + this.Hora + '</td><td>' + this.puntoVenta + '</td></tr>';
                var markerCont = "<div>" +
                    "<strong>" + this.puntoVenta + "</strong>" + '<p>' + this.Hora + '</p>' + "</div>";
                var partes = this.Coodenadas.replace(",", ";").split(";");
                var _lat = parseFloat(partes[0]);
                if (_lat == 0) {_lat = -24.789211; }
                var _lng = parseFloat(partes[1]);
                if (_lng == 0) {_lng = -65.410323; }
                mapRuteoPoints[this.CodRuteo] = { lat: _lat, lng: _lng, content: markerCont };

            });
            $("#tablaRuteo").html(filas);

            /*
            $("#tablaRuteo tr").click(function () {
                //alert("asdf");
                var activo = $(this).attr("activo");
                if (activo == "1") {
                    $(this).css("background-color", "#FFFFFF");
                    var activo = $(this).attr("activo", "0");
                } else {
                    $(this).css("background-color", "#66CCFF");
                    var activo = $(this).attr("activo", "1");
                    cod = $(this).attr("codigo");
                    mapa.map.setCenter(mapRuteoPoints[cod]);
                }
                drawMap();
            });
            */
            unblockUIOut();
        }
    });
}


function onDrawTrakingLine() {
    drawLine = !drawLine;
    drawMap();
}

function onVolver() {
    var mapPVPoints = [];
    var mapRuteoPoints = {};
    var trackingPoints = {};
    drawLine = false;
    togglePanels('panelFiltradoTracking');
}



function isValidDate(day, month, year)
{
    var dteDate;
    month=month-1;
    dteDate = new Date(year, month, day);
    return ((day==dteDate.getDate()) && (month==dteDate.getMonth()) && (year==dteDate.getFullYear()));
}

function validateDate(_fecha)
{
    var fecha = "{2}-{1}-{0}".format(_fecha.split("/")[0], _fecha.split("/")[1], _fecha.split("/")[2]);
    var patron = new RegExp("^(19|20)+([0-9]{2})([-])([0-9]{1,2})([-])([0-9]{1,2})$");

    if(fecha.search(patron)==0)
    {
        var values=fecha.split("-");
        if(isValidDate(values[2],values[1],values[0]))
        {
            return true;
        }
    }
    return false;
}

//Bloquea p?ina.
function blockUIIn() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }
    $(parent.$("form")).block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}

//Desbloquea p?ina.
function unblockUIOut(el) {
    $(parent.$("form")).unblock();
}