﻿var mapa = null;
var gpsPoint = null;
$(document).ready(function () {
    cargarRepositores();
    cargarPuntosVenta();

    togglePanels("panelFiltrarNovedades");

    $("#txtFechaDesde").datepicker({
        dateFormat: "dd/mm/yy",
    });

    $("#txtFechaHasta").datepicker({
        dateFormat: "dd/mm/yy",
    });
});

function translateFecha(fecha) {
    var data = fecha.split("/");
    return data[2] + data[1] + data[0];
}


function colorearElemento(cont, idx) {
    $("#" + cont + " li").css("background-color", "#FFFFFF");
    $("#" + cont + " [codigo=" + idx + "]").css("background-color", "#74D1FF");
}


function cargarRepositores() {
    $.ajax({
        type: "POST",
        url: 'NovedadesOld.aspx/GetAllRepositores',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            repositores = JSON.parse(response.d);
            content = '<option value="-1">Seleccione una opción</option>';
            $.each(repositores, function(index, usr) {
                content += '<option value="{0}">{1}</option>'.format(usr.CodUsuario, usr.apellido + ", " + usr.nombres);
            });
            console.log(content);
            $("#optCodRepositor").html(content);
        }
    });
}


function cargarPuntosVenta() {
    $.ajax({
        type: "POST",
        url: 'NovedadesOld.aspx/GetAllPuntosVentas',
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            puntosVenta = JSON.parse(response.d);
            content = '<option value="-1">Seleccione una opción</option>';
            $.each(puntosVenta, function(index, pv) {
                content += '<option value="{0}">{1}</option>'.format(pv.CodPuntoVenta, pv.Nombre);
            })
            content += '<option value="">Todos</option>';
            $("#optCodPuntoVenta").html(content);
        }
    });
}


function cargarNovedadesDia() {
    $("#rContDatosNovedad").hide();
    codRepositor = $("#optCodRepositor").val();
    codPuntoVenta = $("#optCodPuntoVenta").val();
    fechaDesde = translateFecha($("#txtFechaDesde").val());
    fechaHasta = translateFecha($("#txtFechaHasta").val());

    if (codRepositor != "-1" && codPuntoVenta != "-1" && $.trim(fechaDesde) != "" && $.trim(fechaHasta) != "") {
        blockUIIn();
        //datos usuario
        $.ajax({
            type: "POST",
            url: 'NovedadesOld.aspx/GetDatosUsuario',
            data: JSON.stringify({
                CodUsuario: codRepositor
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function(response) {
                $("#fechaDesde").html(fechaDesde);
                $("#fechaHasta").html(fechaHasta);
                $("#nombrePV").html($("#optCodPuntoVenta option:selected").html());

                usr = JSON.parse(response.d)[0];
                $("#nombreRepositor").html("{0} {1}".format(usr.Apellido, usr.Nombres));
                path = $("[id$=hddURLPerfil]").val();
                console.log(usr.Foto);
                if (usr.Foto == "") {
                    $("#imageRepositor").attr("src", path + "no_image.png");
                }
                else {
                    $("#imageRepositor").attr("src", path + usr.Foto);
                }
            }
        });

        //datos generales
        $.ajax({
            type: "POST",
            url: 'NovedadesOld.aspx/GetDetalleGeneralNovedades',
            data: JSON.stringify({
                CodUsuario: codRepositor,
                CodPuntoVenta: codPuntoVenta,
                FechaInicio: fechaDesde,
                FechaFin: fechaHasta
            }),
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var datos = JSON.parse(response.d);

                $("#dgTotalNov").html(datos[0].TotalNovedades);
                $("#dgNovPropias").html(datos[0].PropioNovedades);
                $("#dgNovComp").html(datos[0].CompetenciaNovedades);
                $("#dgPVT").html(datos[0].PuntosVentasTrabajados);
                $("#dgDias").html(datos[0].CantDiasTrabajados);
                $("#dgTipoNov").html(datos[0].CantTiposNovedades);

                cont = "";
                $.each(datos, function(index, tnov) {
                    if (index != 0) {
                        cont += '<span class="pull-right badge bg-blue">{0}</span>  {1}<br>'.format(tnov.cantidad, tnov.Descripcion)
                    }
                });
                console.log(cont);
                $("#dgTiposNovs").html(cont);
            },
        });

        //novedades por dia
        $.ajax({
            type: "POST",
            url: 'NovedadesOld.aspx/GetNovedadesByDias',
            data: JSON.stringify({
                CodUsuario: codRepositor,
                CodPuntoVenta: codPuntoVenta,
                FechaInicio: fechaDesde,
                FechaFin: fechaHasta
            }),
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                var dias = JSON.parse(response.d);
                var nresult = dias.lenght;
                var content = "";
                var suma = 0;
                var idx = 1;
                $.each(dias, function(index, dia) {
                    content += '<li style="cursor: pointer" onclick="cargarPVDia(\'{0}\', \'{1}\', \'{2}\')" codigo="{2}">'.format(codRepositor ,dia.dia, idx) +
                    '<a>{0} <span class="badge pull-right bg-blue">{1}</span></a></li>'.format(dia.dia, dia.cantPuntosVenta);
                    suma += dia.cantPuntosVenta;

                    idx += 1;
                });

                $("#totalNovedades").html(suma);
                $("#listadoDias").html(content);
            }
        })
        $("#listadoPuntosVenta").html("");
        $("#listadoTiposNovedades").html("");
        $("#listadoNovedades").html("");
        togglePanels("panelDetalleNovedad");
        unblockUIOut();
    }
    else {
        ShowErrorMessage("Hay campos sin completar..");
    }
    //cargar DatosRepositor
    togglePanels("panelDetalleNovedad");
}


function cargarPVDia(codRepositor, fecha, idx) {
    colorearElemento("listadoDias", idx);
    $("#rContDatosNovedad").hide();
    blockUIIn();
    fecha = translateFecha(fecha);
    console.log(fecha);
    $.ajax({
        type: "POST",
        url: 'NovedadesOld.aspx/GetPuntosVentaNovedad',
        data: JSON.stringify({
            CodRepositor: codRepositor,
            Fecha: fecha
        }),
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            var puntosventa = JSON.parse(response.d);
            var content = "";
            var idx = 1;
            $.each(puntosventa, function(index, pv) {
                content += '<li style="cursor: pointer" onclick="cargarTiposNovedades(\'{0}\', \'{1}\', \'{2}\', \'{3}\')" codigo="{3}">'.format(codRepositor ,pv.CodPuntoVenta, fecha, idx) +
                    '<a>{0} <span class="badge bg-blue pull-right">{1}</span></a></li>'.format(pv.puntoVenta, pv.CantRuteos);
                idx += 1;
            });
            $("#listadoPuntosVenta").html(content);
            $("#listadoTiposNovedades").html("");
            $("#listadoNovedades").html("");
            unblockUIOut();
        }
    });
}


function cargarTiposNovedades(codRepositor, codPuntoVenta, fecha, idx) {
    colorearElemento("listadoPuntosVenta", idx);
    $("#rContDatosNovedad").hide();
    blockUIIn();
    $.ajax({
        type: "POST",
        url: 'NovedadesOld.aspx/GetTiposNovedadPV',
        data: JSON.stringify({
            CodRepositor: codRepositor,
            CodPuntoVenta: codPuntoVenta,
            Fecha: fecha
        }),
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            var tiposNovedad = JSON.parse(response.d);
            var content = "";
            var idx = 1;
            $.each(tiposNovedad, function(index, tn) {
                content += '<li style="cursor: pointer" onclick="cargarNovedades(\'{0}\', \'{1}\', \'{2}\', \'{3}\', \'{4}\')" codigo="{4}">'.format(codRepositor ,codPuntoVenta, tn.CodTipoNovedad, fecha, idx) +
                    '<a>{0} <span class="badge bg-blue pull-right">{1}</span></a></li>'.format(tn.Descripcion, tn.cantidad);
                idx += 1;
            });
            $("#listadoTiposNovedades").html(content);
            $("#listadoNovedades").html("");
            unblockUIOut();
        }
    });
    // body...
}

function cargarNovedades(codRepositor, codPuntoVenta, codTipoNovedad, fecha, idx) {
    colorearElemento("listadoTiposNovedades", idx);
    $("#rContDatosNovedad").hide();
    blockUIIn();
    $.ajax({
        type: "POST",
        url: 'NovedadesOld.aspx/GetListadoNovedades',
        data: JSON.stringify({
            CodRepositor: codRepositor,
            CodPuntoVenta: codPuntoVenta,
            CodTipoNovedad: codTipoNovedad,
            Fecha: fecha
        }),
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
            var novedades = JSON.parse(response.d);
            var content = "";
            var idx = 1;
            $.each(novedades, function(index, nov) {
                content += '<li style="cursor: pointer" onclick="mostrarNovedad(\'{0}\', \'{1}\')" codigo="{1}">'.format(nov.CodNovedad, idx) +
                    '<a>{0} <span class="badge bg-blue pull-right">{1}</span></a></li>'.format(nov.Empresa, nov.HoraNovedad);
                idx += 1;
            });
            $("#listadoNovedades").html(content);
            unblockUIOut();
        }
    });
}


function mostrarNovedad(codNovedad, idx) {
    colorearElemento("listadoNovedades", idx);
    blockUIIn();
    $("#rContDatosNovedad").show();
    mapa = new GMaps({
          div: '#map_canvas',
          lat: -24.7761747,
          lng: -65.4115812
    });

    $.ajax({
        type: "POST",
        url: 'NovedadesOld.aspx/GetDatosNovedad',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
            CodNovedad: codNovedad,
        }),
        success: function(response) {
            novedad = JSON.parse(response.d)[0];


            if (typeof(novedad) != "undefined") {
                $("#rNombre").html(novedad.producto);
                $("#rMarca").html(novedad.Marca);
                $("#rPropio").html(toPropio(novedad.Propio));
                $("#rPresentacion").html(novedad.Presentacion);
                path = $("[id$=hddURLProductos]").val();
                if (novedad.Imagen != null) {
                    $("#rImagen").attr("src", path + novedad.Imagen);
                }
                else {
                    $("#rImagen").attr("src", path + "no_producto.jpg");
                }

                //path = "../Recursos/Fotos/";
                var pathFotos = $("[id$=hddURLFotos]").val();
                if ($.trim(novedad.Fotonovedad) != "" && novedad.Fotonovedad != null) {
                    $("#rFotoNovedad").attr("src", pathFotos + novedad.Fotonovedad);
                }
                else {
                    $("#rFotoNovedad").attr("src", pathFotos + "no_image.png");
                }


                $("#rEmpresa").html(novedad.empresa);
                $("#rTipoCliente").html(novedad.TipoCliente);
                $("#rDia").html(novedad.dia);
                $("#rObservacion").html(novedad.Observacion);
                $("#rPrecio").html(novedad.Precio);
                $("#rDireccion").html(novedad.Direccion);
                $("#rCiudad").html(novedad.Ciudad);
                $("#rProvincia").html(novedad.Provincia);
                $("#rZona").html(novedad.Zona);

                $("#rFechaElab").html(novedad.fechaElavoracion);
                $("#rHoraElab").html(novedad.HoraElaboracion);
                $("#rNumLote").html(novedad.NumLote);
                $("#rCantidad").html(novedad.Cantidad);

                $("#rGPS").html(novedad.Coordenadas);
                $("#rPresicion").html(novedad.rango);

                pos = novedad.Coordenadas.split(";");
                _lat = (parseFloat(pos[0]));
                _lng = (parseFloat(pos[1]));

                gpsPoint = mapa.addMarker({
                    lat: _lat,
                    lng: _lng
                });

                mapa.map.setCenter(gpsPoint.getPosition());
            }
            else {
                unblockUIOut();
                ShowErrorMessage("No se obtuvieron Datos");
            }

            unblockUIOut();
        },
        fail: function(xhr, err) {
            unblockUIOut();
        }
    });
}



function toPropio(propio) {
    if (propio == true) {
        return "Propio";
    }
    else {
        return "Competencia";
    }
}


function ShowErrorMessage(msj) {
    return showErrorMessage(msj);
}



function onVolver() {
    togglePanels("panelFiltrarNovedades");
}
