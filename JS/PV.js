﻿var $image;
var map;
$(document).ready(function () {
    inicializarTablas();
    inicializarModuloFoto();
    inicializarOtrosModulos();
    cargarTablaPV();
    togglePanels("panelListado");
});

function inicializarOtrosModulos() {
    $("#textLatitud").inputmask("-99.999999");
    $("#textLongitud").inputmask("-99.999999");
    $("[id$=textLatitud]").attr('readonly', 'readonly');
    $("[id$=textLongitud]").attr('readonly', 'readonly');

    $("body").on("click", "[id=btnNewPersona]", function () {
        clearFrmPV();
        expandir();
        $("[id$=GuardarDatosPersonales], [id$=btnGuardar]").css("display", "none");
        $("[id$=btnGuardarTodo]").css("display", "block");
        $("[id$=apellidoPersona]").focus();
    });
}

function verAsignaciones() {
    window.location.href = "Asignaciones.aspx";
}

function mostrarNuevoPV() {
    clearFrmPV();
    mostrarOcultarPaneles("panel2");

    //Habilita el modo edicion
    $('#iframe1').contents().find("input").show();
    $('#iframe1')[0].contentWindow.eFlag = true;
    var plat = -24.789211;
    var plng = -65.410323;
    setTimeout(function() {
        map = $('#iframe1')[0].contentWindow.map;
        map.setZoom(15);
        $('#iframe1')[0].contentWindow.cargarPuntoMapa(plat, plng);
        map.setCenter({lat: plat, lng: plng});
    }, 500);
}

function clearFrmPV() {
    $("#btnBuscarFoto").show();
    $("#btnTomarFoto").show();
    $("#btnGuardar").removeAttr("disabled");
    $("#btnGuardar").show();
    $("#textNombre").removeAttr("readonly");
    $("#textCodSuc").removeAttr("readonly");
    $("#textDireccion").removeAttr("readonly");
    $("#textCiudad").removeAttr("readonly");
    $("#textProvincia").removeAttr("readonly");
    $("#textZona").removeAttr("readonly");
    $("#textTipoCliente").removeAttr("readonly");
    $("#lblNuevoEdicion").text("Nuevo");
    //Reseteo formulario de datos personales
    $('#hddCodPV').val("");
    $('#textNombre').val("");
    $('#textCodSuc').val("");
    $('#textDireccion').val("");
    $('#textCiudad').val("");
    $("#textProvincia").val("");
    $("#textZona").val("");
    $('#textLatitud').val("");
    $('#textLongitud').val("");
    $('#textTipoCliente').val("");
    $("[id$=imagenmostrar]").attr("src", $("[id$=hddURLPV]").val() + "no_image.png");
    $("[id$=dataFoto]").val("");

    $("[id$=nombreFoto]").val("");
    $(".grid-title h4 span").text("");
    $('#textNombre').focus();
    $("#rbtnHabilitado").removeAttr("checked");
    $("#rbtnNoHabilitado").removeAttr("checked");
}

function mostrarOcultarPaneles(panelMostrar) {
    if (panelMostrar == "panel2") {
        togglePanels("panelNuevoRep");
        //$('#btnCollapseListado').click();
        //$('#btnCollapseNuevo').click();
    } else {
        togglePanels("panelListado");
        //$('#btnCollapseListado').click();
        //$('#btnCollapseNuevo').click();

    }
}

function expandir() {
    clearFrmPV();
}

function inicializarModuloFoto() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }

    $("[id$=FileUploadFotoPerfil]").preimage();
    ejecutar();
    $("#btnTomarFoto").on("click", function () {
        $("[id$=botoneraTomarFoto]").show();
        $("[id$=botoneraEditarFoto]").hide();
        $("[id$=bodyTomarFoto]").show();
        $("[id$=bodyEditarFoto]").hide();
    });
    $("#btnLimpiarCoordenadas").on("click", function () {
        $("#textLatitud").val("");
        $("#textLongitud").val("");
        $('#iframe1')[0].contentWindow.clearMap();
    });
}

var table, usuarios;

function inicializarTablas() {
    table = $('#tblPersonas').DataTable({
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primer",
                "sLast": "Ultima",
                "sNext": "Siguiente",
                "sPrevious": "Atras",
            },
            "sProcessing": "Procesando...",
            "sSearch": "Buscar: ",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Registro _START_ al _END_ (_TOTAL_ registros en total)",
            "sInfoEmpty": "-",
            "sInfoFiltered": ", filtrado de un total de _MAX_ registros.",
            "sInfoPostFix": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando..."
        },
        "iDisplayLength": 25,
        "columnDefs": [
            {
                "targets": [0],
                "visible": false
                //"searchable": false
            },
            {
                "targets": [4],
                "visible": false
                //"searchable": false
            },
            {
                "targets": [7],
                "visible": false
                //"searchable": false
            }
        ]
    });

    $('#tblPersonas tbody').on('click', 'tr button', function () {

        //console.log($(this).parent().parent());
        //console.log(table.row($(this).parent().parent()).data());
        var data = table.row($(this).parent().parent().parent()).data();

        console.log($(this).val());

        if ($(this).val() == "datos") {
            //console.log("asdfasdf");
            //console.log(data);
            cargarDatos(data[0], data[2]);
        }
        if ($(this).val() == "asignar") {
            //console.log("asdfasdf");
            //console.log(data);
            asignarRepositores(data[0]);
        }
        /*$("[id$=CodigoPersonalUsuario]").val(data[4]);
        $("[id$=OpercionUsuario]").val($(this).val());

        $("[id$=verDatosUsuario]").click();*/
    });
}

function asignarRepositores(codRep) {
    blockUIIn();
    window.location.href = "Asignaciones.aspx?pv=" + codRep;

}

function cargarDatos(codPV,codSucursal) {
    clearFrmPV();
    blockUIIn();
    var data = {};
    data["_codPV"] = codPV.trim();
    data["_codSucursal"] = codSucursal.trim();
    $.ajax({
        type: "POST",
        url: 'PV.aspx/GetPVByCod',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            var result = JSON.parse(respuesta.d);
            $.each(result, function () {

                $("#lblNuevoEdicion").text("Modificar");
                $('#hddCodPV').val(this.CodPuntoVenta.trim());
                $('#textNombre').val(this.Nombre.trim());
                $('#textCodSuc').val(this.CodSucursal.trim());
                $('#textDireccion').val(this.Direccion.trim());
                $('#textCiudad').val(this.Ciudad.trim());
                $('#textProvincia').val(this.Provincia);
                $('#textZona').val(this.Zona);
                if (this.Coodenadas.trim() != "") {
                    var partes = this.Coodenadas.split(";");
                    $('#textLatitud').val(partes[0]);
                    $('#textLongitud').val(partes[1]);
                    //document.getElementById('iframe1').contentWindow.centrarMapa(partes[0], partes[1]);
                    map = $('#iframe1')[0].contentWindow.map;

                    //Codigo Ricardo valida que el punto sea valido, sino pone la 9 de julio por defecto
                    var plat = parseFloat($("#textLatitud").val());
                    var plng = parseFloat($("#textLongitud").val());


                    if (plat == 0 || plng == 0 || isNaN(plat) || isNaN(plng)) {
                        console.log("Punto Invalido");
                        isValidPto = false;
                        //9 julio punto por defecto
                        plat = -24.789211;
                        plng = -65.410323;
                    }
                    else {
                        console.log("Cargando punto Punto");
                        isValidPto = true;
                    }
                    setTimeout(function() { //retraza porque se llama con otro punto antes
                        map.setZoom(15);
                        if (isValidPto) {
                            $('#iframe1')[0].contentWindow.cargarPuntoMapa(plat, plng);
                        }
                        map.setCenter({ lat: plat, lng: plng });
                    }, 500);
                }

                $('#textTipoCliente').val(this.TipoCliente);
                $('#textZona').val(this.Zona);
                console.log(this.Imagen);
                console.log("imagen");
                if ((!this.Imagen)) {
                    $("[id$=imagenmostrar]").attr("src", $("[id$=hddURLPV]").val() + "no_image.png");
                } else {
                    $("[id$=imagenmostrar]").attr("src", $("[id$=hddURLPV]").val() + this.Imagen.trim() + "?id=" + randomID(7));
                    $("[id$=nombreFoto]").val(this.Imagen.trim());
                }
                if (this.Habilitado == 1) {
                    $("#rbtnHabilitado").click();
                } else {
                    if (this.Habilitado == 0) {
                        $("#rbtnNoHabilitado").click();
                    }
                }
                $('#textNombre').focus();
                mostrarOcultarPaneles("panel2");
                /*if ($('#textLatitud').val().indexOf("_") == -1) {
                    document.getElementById('iframe1').contentWindow.cargarPuntoPorDefecto();
                }*/

                if (this.propio) { //si es producto propio
                    $("#btnBuscarFoto").hide();
                    $("#btnTomarFoto").hide();
                    $("#lblNuevoEdicion").text("Detalle");
                    $("#btnGuardar").attr("disabled", "disabled");
                    $("#btnGuardar").hide();

                    $("#textDireccion").attr("readonly", "readonly");
                    $("#textCiudad").attr("readonly", "readonly");
                    $("#textProvincia").attr("readonly", "readonly");
                    $("#textZona").attr("readonly", "readonly");
                    $("#textTipoCliente").attr("readonly", "readonly");

                    //codigo ricardo
                    $('#iframe1').contents().find("input").hide();
                    $('#iframe1')[0].contentWindow.eFlag = false;

                } else {
                    $("#btnBuscarFoto").show();
                    $("#btnTomarFoto").show();
                    $("#textDireccion").removeAttr("readonly");
                    $("#textCiudad").removeAttr("readonly");
                    $("#textProvincia").removeAttr("readonly");
                    $("#textZona").removeAttr("readonly");
                    $("#textTipoCliente").removeAttr("readonly");
                    $("#btnGuardar").removeAttr("disabled");
                    $("#btnGuardar").show();

                    //codigo ricardo
                    $('#iframe1').contents().find("input").show();
                    $('#iframe1')[0].contentWindow.eFlag = true;

                }
                $("#textNombre").attr("readonly", "readonly");
                $("#textCodSuc").attr("readonly", "readonly");
                unblockUIOut();
            });
        },
        error: function (respuesta) {
            showErrorMessage("No se pudo cargar el Punto de Venta Seleccionado. Revise su conexión a Internet");
        }
    });
}
//Funciones para fotos
function cargarFoto(url) {
    //alert( url );
    $("[id$=imagenmostrar]").attr("src", url);
    $("[id$=dataFoto]").attr("value", url);
}

function cerrarDialogoFoto() {
    $("[id$=botoneraTomarFoto]").show();
    $("[id$=botoneraEditarFoto]").hide();
    $("[id$=bodyTomarFoto]").show();
    $("[id$=bodyEditarFoto]").hide();
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.rect(0, 0, canvas.width, canvas.height);
    context.stroke();
    $("#btnpopup1").trigger("click");
    $("#btnpopup2").trigger("click");
}

function actualizarFoto() {
    cargarFotoCortada();
}

function ejecutar() {
    $image = $(".cropper");
    var $dataX = $("#dataX"),
    $dataY = $("#dataY"),
    $dataHeight = $("#dataHeight"),
    $dataWidth = $("#dataWidth"),
    console = window.console || { log: $.noop },
    cropper;
    $image.cropper({
        aspectRatio: 1 / 1,
        // autoCropArea: 1,
        data: {
            x: 520,
            y: 10,
            width: 1024,
            height: 768
        },
        preview: ".preview",

        // multiple: FALSE,
        // autoCrop: TRUE,
        // dragCrop: TRUE,
        // dashed: TRUE,
        // modal: TRUE,
        // movable: TRUE,
        // resizable: TRUE,
        // zoomable: TRUE,
        // rotatable: TRUE,

        // maxWidth: 480,
        // maxHeight: 270,
        // minWidth: 160,
        // minHeight: 90,

        done: function (data) {
            $dataX.val(data.x);
            $dataY.val(data.y);
            $dataHeight.val(data.height);
            $dataWidth.val(data.width);
        },

        build: function (e) {
            console.log(e.type);
        },

        built: function (e) {
            console.log(e.type);
        },

        dragstart: function (e) {
            console.log(e.type);
        },

        dragmove: function (e) {
            console.log(e.type);
        },

        dragend: function (e) {
            console.log(e.type);
        }
    });

    cropper = $image.data("cropper");

    $image.on({
        "build.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "built.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragstart.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragmove.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        },
        "dragend.cropper": function (e) {
            console.log(e.type);
            // e.preventDefault();
        }
    });

    $("#reset").click(function () {
        $image.cropper("reset");
    });

    $("#reset2").click(function () {
        $image.cropper("reset", true);
    });

    $("#clear").click(function () {
        $image.cropper("clear");
    });

    $("#destroy").click(function () {
        $image.cropper("destroy");
    });

    $("#enable").click(function () {
        $image.cropper("enable");
    });

    $("#disable").click(function () {
        $image.cropper("disable");
    });

    $("#zoom").click(function () {
        $image.cropper("zoom", $("#zoomWith").val());
    });

    $("#zoomIn").click(function () {
        $image.cropper("zoom", 0.9);
    });

    $("#zoomOut").click(function () {
        $image.cropper("zoom", -0.9);
    });

    $("#rotate").click(function () {
        $image.cropper("rotate", $("#rotateWith").val());
    });

    $("#rotateLeft").click(function () {
        $image.cropper("rotate", -90);
    });

    $("#rotateRight").click(function () {
        $image.cropper("rotate", 90);
    });

    $("#setAspectRatio").click(function () {
        $image.cropper("setAspectRatio", $("#aspectRatio").val());
    });

    $("#replace").click(function () {
        //$image.cropper( "replace", $( "#replaceWith" ).val() );
        $image.cropper("replace", $("#fotoMostrar").val());
    });

    $("#getImageData").click(function () {
        $("#showImageData").val(JSON.stringify($image.cropper("getImageData")));
    });

    $("#setData").click(function () {
        $image.cropper("setData", {
            x: $dataX.val(),
            y: $dataY.val(),
            width: $dataWidth.val(),
            height: $dataHeight.val()
        });
    });

    $("#getData").click(function () {
        $("#showData").val(JSON.stringify($image.cropper("getData")));
    });

    $("#getDataURL").click(function () {
        //var dataURL = $image.cropper( "getDataURL" );

        //$( "#dataURL" ).text( dataURL );
        //$( "#showDataURL" ).html( '<img src="' + dataURL + '">' );
        cerrarDialogoFoto();
    });

    $("#getDataURL2").click(function () {
        var dataURL = $image.cropper("getDataURL", "image/jpeg");

        $("#dataURL").text(dataURL);
        $("#showDataURL").html('<img src="' + dataURL + '">');
        cargarFoto(dataURL);
        cerrarDialogoFoto();
    });
};

function cargarURLFoto() {
    $("#imagenCortar").attr("src", $("#iframe12").attr("foto"));
    alert($("#iframe12").attr("foto"));
}

function agregarURL(url) {
    //$( "#imagenCortar" ).removeAttr( "src");
    $("#imagenCortar").attr("src", url);
    $("#fotoMostrar").attr("value", url);
    $("#replace").trigger("click");
    ejecutar();
}

function salir() {
    $("#btnpopup").trigger("click");
}

function cargarFotoCortada() {
    $("#getDataURL2").trigger("click");
}

//FIN Funciones para fotos

// Put event listeners into place
window.addEventListener("DOMContentLoaded", function () {
    // Grab elements, create settings, etc.
    canvas = document.getElementById("canvas");
    canvasHdd = document.getElementById("canvasHidden");
    context = canvas.getContext("2d");
    context.rect(0, 0, canvas.width, canvas.height);
    context.stroke();
    contextHdd = canvasHdd.getContext("2d");

    video = document.getElementById("video");
    videoObj = { "video": true };
    errBack = function (error) {
        console.log("Video capture error: ", error.code);
    };

    // Put video listeners into place
    if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function (stream) {
            video.src = stream;
            video.play();
            console.log("Standard");
        }, errBack);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function (stream) {
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
            console.log("WebKit-prefixed");
        }, errBack);
    } else if (navigator.mozGetUserMedia) { // WebKit-prefixed
        navigator.mozGetUserMedia(videoObj, function (stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
            console.log("WebKit-prefixed 22");
        }, errBack);
    }

    // Trigger photo take
    document.getElementById("snap").addEventListener("click", function () {
        context.drawImage(video, 0, 0, 280, 220);
        //context.drawImage( video, 0, 0, 1024, 768);
        contextHdd.drawImage(video, 0, 0, 1024, 768);
    });
    document.getElementById("btnConfirnarFoto").addEventListener("click", function () {
        document.getElementById("imgCaptura").src = canvas.toDataURL();
        $("[id$=imagenmostrar]").attr("src", document.getElementById('imgCaptura').src);

        //console.log(canvas.toDataURL("image/png"));
        $image.cropper("replace", canvasHdd.toDataURL("image/png"));
        $image.cropper("reset", true);

        //$("#btnMostrarFotoRecortar").click();
        //$(window).trigger('resize');

        //document.getElementById("hidden").value = canvas.toDataURL("image/png").replace('data:image/png;base64,', '');
        $("[id$=botoneraTomarFoto]").hide();
        $("[id$=botoneraEditarFoto]").show();
        $("[id$=bodyTomarFoto]").hide();
        $("[id$=bodyEditarFoto]").show();
    });
}, false);


function mostrarDialogoBuscarFoto() {
    $("[id$=FileUploadFotoPerfil]").click();
}
//Datos subir archivos - Inicio
function mostrarDialogoBuscarFichero() {
    $("[id$=FileUploadFichero]").click();
}
//Datos subir archivos - Fin



function modificarComportamientoSeccionLocalizacion() {
    try {
        document.getElementById('iframe1').contentWindow.cargarPuntoPorDefecto();
    }
    catch (err) {
        $('#iframe1').load(function () {
            document.getElementById('iframe1').contentWindow.cargarPuntoPorDefecto();
        });
    }
}

function centrarEnCentro() {
    document.getElementById( 'iframe1' ).contentWindow.centrarMapa( -24.794682484311966, -65.4301929473877 );
}

function verificarCamposObligatorios() {
    var retorno = false;
    var mensaje = "";
    var plat = "-24.789211"; //gps
    var plng = "-65.410323";
    if ($("#textNombre").val().trim() == "") {
        mensaje = "Debe completar el campo Cliente";
    } else {
        if ($("#textCodSuc").val().trim() == "") {
            mensaje = "Debe completar el campo Sucursal";
        } else {
            if ($("#textDireccion").val().trim() == "") {
                mensaje = "Debe completar el campo Dirección";
            } else {
                if ($("#textCiudad").val().trim() == "") {
                    mensaje = "Debe completar el campo Ciudad";
                } else {
                    if ($("#textProvincia").val().trim() == "") {
                        mensaje = "Debe completar el campo Provincia";
                    } else {
                        if ($("#textZona").val().trim() == "") {
                            mensaje = "Debe completar el campo Zona";
                        } else {
                            if ($("#textTipoCliente").val().trim() == "") {
                                mensaje = "Debe completar el campo Tipo de Cliente";
                            } else {
                                retorno = true;
                                /*if ($("#textLatitud").val().trim() == "" || $("#textLatitud").val().trim() == plat ||
                                    $("#textLongitud").val().trim() == "" || $("#textLongitud").val().trim() == plng) {
                                    mensaje = "Debe indicar la ubicación del Punto de Venta en el mapa";
                                } else {
                                    retorno = true;
                                    //if ((!$("#rbtnHabilitado").is(":checked")) && (!$("#rbtnNoHabilitado").is(":checked"))) {
                                    //    mensaje = "Debe seleccionar si el Punto de Venta está habilitado";
                                    //} else {
                                    //    retorno = true;
                                    //}
                                }*/
                            }
                        }
                    }
                }
            }
        }
    }
    if (!retorno) {
        showErrorMessage(mensaje);
    }
    return retorno;
}

function guardarDatos() {
    if (verificarCamposObligatorios()) {
        blockUIIn();
        var data = {};
        data["_nombre"] = $('#textNombre').val();
        data["_codSucursal"] = $('#textCodSuc').val();
        data["_direccion"] = $('#textDireccion').val();
        data["_ciudad"] = $('#textCiudad').val();
        data["_provincia"] = $('#textProvincia').val();
        data["_zona"] = $('#textZona').val();
        data["_coordenadas"] = $('#textLatitud').val() + ";" + $('#textLongitud').val();
        data["_tipoCliente"] = $('#textTipoCliente').val();
        data["_imagen"] = $("[id$=nombreFoto]").val();
        if ($("#rbtnHabilitado").is(":checked")) {
            data["_habilitado"] = "true";
        } else {
            data["_habilitado"] = "false";
        }
        data["_codPuntoVenta"] = $('#hddCodPV').val();
        data["_dataFoto"] = $("[id$=dataFoto]").val();
        //console.log(JSON.stringify(data, null, "  "));
        //unblockUIOut();
        $.ajax({
            type: "POST",
            url: 'PV.aspx/GuardarPV',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (respuesta) {
                if ((respuesta.d != "-1") && (respuesta.d != "duplicado")) {
                    showSuccessMessage('Punto de Venta actualizado');
                    clearFrmPV(true);
                    cargarTablaPV();
                    mostrarOcultarPaneles('panel1');
                } else {
                    if (respuesta.d == "duplicado") {
                        showErrorMessage('Los campos Cliente y Sucursal no se deben repetir');
                    } else {
                        showErrorMessage('Revise los datos. No se guardaron los cambios');
                    }
                }
                unblockUIOut();
            }
        });
    }
}

function cargarTablaPV() {
    blockUIIn();
    var data = {};
    $.ajax({
        type: "POST",
        url: 'PV.aspx/CargarTablaPV',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            console.log(this.CodPuntoVenta);
            table.clear().draw();
            var result = JSON.parse(respuesta.d);
            $.each(result, function () {
                var fila = new Array();
                fila.push(this.CodPuntoVenta);
                fila.push(this.Nombre);
                fila.push(this.CodSucursal);
                fila.push(this.Direccion);
                fila.push(this.Ciudad);
                fila.push(this.Provincia);
                fila.push(this.Zona);
                fila.push(this.Coodenadas);
                fila.push(this.TipoCliente);

                if (this.propio == false || this.propio == null) {
                    fila.push('<center><button type="button" class="btn btn-success btn-xs" value="datos" title="Modificar Datos"><span class="glyphicon glyphicon-pencil"></span></button>' +
                        '<button type="button" class="btn btn-success btn-xs" onclick="quitarPV(\'' + this.CodPuntoVenta + '\',\'' + this.Nombre + '\',\'' + this.CodSucursal + '\')" style="margin-left: 5px;" title="Deshabilitar Sucursal del Punto de Venta"><span class="glyphicon glyphicon-trash"></span></button>' +
                        '</center>');
                } else {
                    fila.push('<center><button type="button" class="btn btn-success btn-xs" value="datos" title="Visualizar Datos"><span class="fa fa-eye"></span></button></center>');

                }

                //var filaInsertar = "<tr><td>" + this.CodProducto + "</td><td>" + this.EAN + "</td><td>" + this.Nombre + "</td><td>" + this.Marca + "</td><td>" + this.Presentacion + "</td></tr>";
                //console.log(this.CodProducto);
                table.row.add(fila);
            });
            table.draw();
            unblockUIOut();


        }
    });
}


//funciones para generar excel
var cookie;
function generarExcel() {
    document.cookie = 'termino=error; path=/';
    blockUIIn();
    cookie = window.setInterval(function () {
        var cookieValue = getCookie('termino');
        if (cookieValue == "termino") {
            finishDownload();
        }
    }, 1000);

    document.getElementById('btnExcelear').click();

}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function finishDownload() {
    window.clearInterval(cookie);
    document.cookie = 'termino=error; path=/';
    unblockUIOut();
}

//FIN funciones para generar excel

function getUsuarioByCod() {
    blockUIIn();
    var data = {};
    data["comando"] = 'GetAllSalidasByFormAndUsuario';
    data["codFormulario"] = $('#hddCodFormulario').val();
    data["nombreFormulario"] = $('#hddNombreFormulario').val();
    data["codUsuario"] = $('#hddUserSelected').val();
    data["codVersion"] = $('#hddVersion').val();
    $.ajax({
        type: "POST",
        url: '../../WebService/Filtros.asmx/cargarComboVersiones',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (respuesta) {
            if ($("#SalidasAsign").html() != "") {
                $("#SalidasAsign").fancytree("destroy")
            }
            var result = JSON.parse(respuesta.d);

            $('#hddCodPadre').val(result.key)

            $("#SalidasAsign").fancytree({
                checkbox: true,
                selectMode: 3,
                icons: false,
                source: result,
                extensions: ["filter"],
                quicksearch: true,
                filter: {
                    autoApply: false,
                    counter: false,
                    fuzzy: false,
                    hideExpandedCounter: false,
                    highlight: false,
                    mode: "hide"
                },
                select: function (event, data) {
                    selKeys = $.map(data.tree.getSelectedNodes(), function (node) {
                        return node.key;
                    });
                    var selRootNodes = data.tree.getSelectedNodes(true);
                    var selRootKeys = $.map(selRootNodes, function (node) {
                        return node.parent.key;
                    });
                    selKeys = _.union(selKeys, selRootKeys);
                },
                dblclick: function (event, data) {
                    data.node.toggleSelected();
                },
                keydown: function (event, data) {
                    if (event.which === 32) {
                        data.node.toggleSelected();
                        return false;
                    }
                },
                cookieId: "fancytree-Cb3",
                idPrefix: "fancytree-Cb3-"
            });
            var tree = $("#SalidasAsign").fancytree("getTree");
            $('#search').keyup(function (e) {
                var n,
                    opts = {
                        autoExpand: true
                    },
                    match = $(this).val();
                n = tree.filterNodes(match, opts);
            }).focus();
            unblockUIOut();
        }
    });
}

//Bloquea página.
function blockUIIn() {
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
        return this;
    }
    $(parent.$("form")).block({
        message: '<i class="fa fa-spinner fa-spin fa fa-4x"></i>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}
//Desbloquea página.
function unblockUIOut(el) {
    $(parent.$("form")).unblock();
}

function cargarPunto(latitud, longitud) {
    $("[id$=textLatitud]").val(latitud);
    $("[id$=textLatitud]").prop("value", latitud);
    $("[id$=textLongitud]").val(longitud);
    $("[id$=textLongitud]").prop("value", longitud);
}
function getLatitud() {
    return $("#textLatitud").val();
}

function getLongitud() {
    return $("#textLongitud").val();
}

function quitarPV(codPV, nombre, codSucursal) {
    $("#hhdCodPVDel").val(codPV);
    $("#hhdCodSucursalDel").val(codSucursal);

    $("#lblNombre").html(codSucursal);
    $("#lblNombrePV").html(nombre);
    $("#deleteDialog").modal("show");
}


function onQuitarPV() {
    var codPV = $("#hhdCodPVDel").val();
    var codSucursal = $("#hhdCodSucursalDel").val();
    $.ajax({
        type: "POST",
        url: 'PV.aspx/DeshabilitarPV',
        data: JSON.stringify({ CodPV: codPV, CodSucursal: codSucursal }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $("#deleteDialog").modal("hide");
            showSuccessMessage("Sucursal deshabilitada");
            cargarTablaPV();
        }
    });
}
