﻿var markersAll = [];
var map;
var toggleState = 1;
var ctaLayer;
var selectedShape;
var oms;
function addMarkers(_puntos) {
    var puntos = _puntos.split("#");
    clearMap();
    for (var index = 0; index < puntos.length; index++) {
        var partes = puntos[index].split("@");
        if (partes[0] != 'NONE' && partes[1] != 'NONE') {
            addMarkerTablero(partes[0], partes[1], partes[2], partes[3], partes[4], partes[5]);
        }
    }
}

function clearMap() {
    for (i = 0; i < markersAll.length; i++) {
        markersAll[i].setMap(null);
    }
}

function addMarker(_x, _y, _image, n, _title, _info) {
    var myLatlng = new google.maps.LatLng(_x, _y);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: _title,
        icon: _image
    });
    if (_info != '') {
        var infowindow = new google.maps.InfoWindow({
            content: _info
        });
        oms.addListener('click', function () {
            infowindow.open(map, marker);
        });
    }
    markersAll.push(marker);
}

function addMarkersTablero(_puntos) {
    var puntos = _puntos.split("#");
    clearMap();
    for (var index = 0; index < puntos.length; index++) {
        var partes = puntos[index].split("@");
        if (partes[0] != 'NONE' && partes[1] != 'NONE') {
            addMarkerTablero(partes[0], partes[1], partes[2], partes[3], partes[4], partes[5]);
        }
    }
}

function addMarkerTablero(_x, _y, _image, n, _title, _info) {

    var myLatlng = new google.maps.LatLng(_x, _y);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: _title,
        animation: google.maps.Animation.DROP,
        icon: _image,
        info: _info

    });
    if (_info != '') {

    }
    oms.addMarker(marker);
    markersAll.push(marker);
}

function addMarkerCenter(_x, _y, _image, n, _title, _zoom) {
    var myLatlng = new google.maps.LatLng(_x, _y);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: _title,
        icon: _image
    });
    map.setCenter(marker.getPosition());
    map.setZoom(_zoom);
    markersAll.push(marker);
    $("#flechaResultado").attr("class", "expand");
    $("#listaResultado").css("display", "none");
    $("#flechaBusqueda").attr("class", "expand");
    $("#listaBusqueda").css("display", "none");
}

function addMarker2() {
    alert("si funciona sin parametros...");
    var myLatlng = new google.maps.LatLng(-24.837148, -65.391357);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'TITULO',
        icon: 'mark1.png',
        snippet: '<input type:"button" style="with:50px"/>'
    });
    markersAll.push(marker);
}
var newShape
var poligonoSalida = new google.maps.Polygon();
var arraySalidas = [];
var drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: null,
    drawingControl: true,
    drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: [google.maps.drawing.OverlayType.POLYGON, google.maps.drawing.OverlayType.MARKER, ]

    },
    polygonOptions: {
        strokeWeight: 1,
        editable: false
    }
});

var selectedShape;
function startMap(mapa) {
    if ($('#hddLatitud').val() == '' || $('#hddLongitud').val() == '') {
        var myLatlng = new google.maps.LatLng(-24.837148, -65.391357);
    } else {
        var myLatlng = new google.maps.LatLng($('#hddLatitud').val(), $('#hddLongitud').val());
    }


    var mapOptions = {
        zoom: 12,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    switch (mapa) {
        case 'Control':
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            oms = new OverlappingMarkerSpiderfier(map, { markersWontMove: true, markersWontHide: true, keepSpiderfied: true, legWeight: 1.5 });
            oms.addListener('click', function (marker) {
                VerInfoEncuestaNew(marker.info);
            });
            google.maps.event.addDomListener(document.getElementById('btnPoliEncuesta'), 'click', function () {
                var data = {};
                data["codVisita"] = $("[id$=lblNumEncuSelec]").html();
                $.ajax({
                    type: "POST",
                    url: '../../WebService/Mapa.asmx/GetPoligonoEncuesta',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (respuesta) {
                        var result = JSON.parse(respuesta.d);
                        var poligono = "";
                        var arrayPoligono = [];
                        drawingManager.setMap(null);
                        if (result[0]) {
                            centro = new google.maps.LatLng(parseFloat(result[0]["PuntosPoligono"].split(';')[0]), parseFloat(result[0]["PuntosPoligono"].split(';')[1]))
                            map.setCenter(centro);
                            $.each(result, function () {
                                if (this.PuntosPoligono != null && this.PuntosPoligono != "") {
                                    poligono = new google.maps.LatLng(parseFloat(this.PuntosPoligono.split(';')[0]), parseFloat(this.PuntosPoligono.split(';')[1]))
                                    arrayPoligono.push(poligono);
                                }
                            });
                            poligonoSalida = new google.maps.Polygon({
                                paths: arrayPoligono,
                                strokeColor: '#FF0000',
                                strokeOpacity: 0.8,
                                strokeWeight: 1,
                                fillColor: '#FF0000',
                                fillOpacity: 0.25
                            });
                            poligonoSalida.setMap(map);
                            arrayPoligono = [];
                        } else {
                            showErrorMessage('No hay polígono cargado', 'error');
                        }
                    }
                })
            })
            break;
        case 'Admin':
            map2 = new google.maps.Map(document.getElementById("map_canvas2"), mapOptions);
            //ctaLayer = new google.maps.KmlLayer('http://mpi-web.cloudapp.net:8089/kml/CORDOBA.kml');
            google.maps.event.addDomListener(document.getElementById('btnBorrar'), 'click', function () {
                //var triangleCoords = [
                //new google.maps.LatLng (3560848.823,7258160.2316,true),
                //new google.maps.LatLng (3560819.5087,7258329.9972,true),
                //new google.maps.LatLng(3560763.0713,7258419.0436,true),
                //new google.maps.LatLng(3560716.5318,7258474.3633,true),
                //new google.maps.LatLng(3560698.8017999995,7258493.9567,true),
                //new google.maps.LatLng(3560848.823,7258160.2316,true)
                //];
                //// Construct the polygon.
                //console.log(triangleCoords);
                //bermudaTriangle = new google.maps.Polygon({
                //    paths: triangleCoords,
                //    strokeColor: '#FF0000',
                //    strokeOpacity: 0.8,
                //    strokeWeight: 2,
                //    fillColor: '#FF0000',
                //    fillOpacity: 0.35
                //});
                //bermudaTriangle.setMap(map2);
                var data = {};
                data["codSalida"] = $("#hddCodSalida").val();
                $.ajax({
                    type: "POST",
                    url: 'zonas.aspx/GetPoligono',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (respuesta) {
                        var result = JSON.parse(respuesta.d);
                        var poligono = "";
                        var arrayPoligono = [];
                        drawingManager.setMap(null);
                        for (i = 0; i < arraySalidas.length; i++) {
                            arraySalidas[i].setMap(null);
                        }
                        $.each(result, function () {
                            if (this.PuntosPoligono != null && this.PuntosPoligono != "") {
                                puntos = this.PuntosPoligono.split(',');
                                for (i = 0; i < puntos.length - 1; i++) {
                                    poligono = new google.maps.LatLng(parseFloat(puntos[i].split(' ')[1]), parseFloat(puntos[i].split(' ')[0]))
                                    arrayPoligono.push(poligono);
                                }
                                poligonoSalida = new google.maps.Polygon({
                                    paths: arrayPoligono,
                                    strokeColor: '#FF0000',
                                    strokeOpacity: 0.8,
                                    strokeWeight: 1,
                                    fillColor: '#FF0000',
                                    fillOpacity: 0.35
                                });
                                arraySalidas.push(poligonoSalida);
                                poligonoSalida.setMap(map2);
                                google.maps.event.addListener(poligonoSalida, 'click', function (e) {
                                    poligonoSalida.setOptions({
                                        editable: true
                                    })
                                    google.maps.event.addListener(poligonoSalida.getPath(), 'set_at', function () {
                                        poli = "";
                                        inicial = "";
                                        path = poligonoSalida.getPath();
                                        for (var i = 0; i < path.length; i++) {
                                            poli += path.getAt(i);
                                        }
                                        inicial = path.getAt(0);
                                        $('#hddPoligono').val(poli + inicial);
                                    });
                                    google.maps.event.addListener(poligonoSalida.getPath(), 'insert_at', function () {
                                    });
                                });
                                arrayPoligono = [];
                            }
                        });
                    }
                });
            })
            google.maps.event.addDomListener(document.getElementById('btnAddZona'), 'click', function () {
                for (i = 0; i < arraySalidas.length; i++) {
                    arraySalidas[i].setMap(null);
                }
                arraySalidas = [];
                drawingManager.setMap(map2);
            })
            google.maps.event.addDomListener(drawingManager, 'polygoncomplete', function (e) {
                poli = "";
                inicial = "";
                path = e.getPath();
                for (var i = 0; i < path.length; i++) {
                    inicial = path.getAt(0);
                    poli += path.getAt(i);
                    drawingManager.setOptions({
                        drawingControl: false,
                        drawingMode: null
                    });
                    inicial = path.getAt(0);
                    $('#hddPoligono').val(poli + inicial);
                }
            });
            google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
                if (e.type != google.maps.drawing.OverlayType.MARKER) {
                    drawingManager.setDrawingMode(null);
                    var newShape = e.overlay;
                    newShape.type = e.type;
                    google.maps.event.addListener(newShape, 'click', function () {
                        setSelection(newShape);
                    });
                    setSelection(newShape);
                }
            });
            function clearSelection() {
                if (selectedShape) {
                    selectedShape.setEditable(false);
                    selectedShape = null;
                }
            }
            function setSelection(shape) {
                clearSelection();
                selectedShape = shape;
            }

            function deleteSelectedShape() {
                if (selectedShape) {
                    selectedShape.setMap(null);
                }
                $('#hddPoligono').val('');
                drawingManager.setOptions({
                    drawingControl: true,
                    drawingMode: null
                });
            }
            google.maps.event.addDomListener(document.getElementById('btnReset'), 'click', deleteSelectedShape);
            break;
    }
}

function startMap2(lat, long) {
    var myLatlng = new google.maps.LatLng(-24.720453, -65.518722);
    var mapOptions = {
        zoom: 9,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    ctaLayer = new google.maps.KmlLayer('http://saltaconin.cloudapp.net:8080/Buchon/Recursos/kml/SUDESTE.kml');
    ctaLayer.setMap(null);
}

function pintarBarrios() {
    if (toggleState == 1) {
        ctaLayer.setMap(map2);
        toggleState = 0;
    }
    else {
        ctaLayer.setMap(null);
        toggleState = 1;
    }
}

function getMap() {
    return map;
}