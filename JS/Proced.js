﻿function getRandomNumber(range) {
    return Math.floor(Math.random() * range);
}

function getRandomChar() {
    var chars = "0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ";
    return chars.substr(getRandomNumber(62), 1);
}

function randomID(size) {
    var str = "";
    for (var i = 0; i < size; i++) {
        str += getRandomChar();
    }
    return str;
}
