

$(document).ready(function() {
    $("[id$=frecuenciaTracking]").mask('00');
    $("[id$=frecuenciaSincronizacion]").mask('00');

    $("[id$=frecuenciaTracking]").blur(function(e) {
        validarRangoValores("[id$=frecuenciaTracking]");
    });

    $("[id$=frecuenciaSincronizacion]").blur(function(e) {
        validarRangoValores("[id$=frecuenciaSincronizacion]");
    });
});


function validarRangoValores(input) {
    var value = parseInt($(input).val());
    if (isNaN(value)) {
        showErrorMessage("Error - El valor ingresado no es un numero");
        $(input).val("");
        $(input).focus();
    }
    else if (value < 1 || value > 60) {
        showErrorMessage("Error - El valor debe ser mayor o igual que 1 y menor o igual que 60");
        $(input).val("");
        $(input).focus();
    }
}


function guardarConfiguracion() {
    var trk = parseInt($("[id$=frecuenciaTracking]").val());
    var sync = parseInt($("[id$=frecuenciaSincronizacion]").val());
    if (!isNaN(trk) && !isNaN(sync)) {
        if (trk < sync) {
            $.ajax({
                url: 'Configuracion.aspx/GuardarConfiguracion',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    FrecSync: sync,
                    FrecTrk: trk
                }),
                success: function(response) {
                    showSuccessMessage("Configuración actualizada");
                },
                fail: function(xhr, err, cerr) {
                    showErrorMessage('Error de conección con el servidor, revise su conección de Internet');
                },
            });
        }
        else {
            showErrorMessage("Error- La frecuencia de sincronización de tracking debe ser mayor a la frecuencia de tracking. Por favor, modifique los valores");
        }
    }
    else {
        showErrorMessage("Error - El valor ingresado no es un numero");
    }
}