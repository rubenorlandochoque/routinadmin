﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.Record.Formula.Eval;
using NPOI.HSSF.Record.Formula.Functions;
using Master;
using AppAndroid.Master;

namespace AppAndroid
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class RestAPI : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public string Empresas(String JSONData)
        {
            if (!Sesion.isAccess("P_EMPRESAS"))
            {
                return "peticion invalida";
            }

            var data = JObject.Parse(JSONData);
            String Comando = (String)data["Comando"];
            String CodEmpresa = (String)data["CodEmpresa"];
            String Nombre = (String)data["Nombre"];
            String output = "";

            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.usp_Empresas";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddOutParameter(dbCommand, "@output", DbType.String, 255);
            db.AddInParameter(dbCommand, "@Comando", DbType.String, Comando);
            db.AddInParameter(dbCommand, "@CodEmpresa", DbType.String, CodEmpresa);
            db.AddInParameter(dbCommand, "@Nombre", DbType.String, Nombre);
            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            switch (Comando)
            {
                case "SelectAll":
                case "SelectByCodEmpresa":
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        var row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    return serializer.Serialize(rows);
                    break;

                case "Insert":
                    db.ExecuteNonQuery(dbCommand);
                    output = db.GetParameterValue(dbCommand, "@output").ToString();
                    return output;
                    break;

                case "Update":
                case "Delete":
                    db.ExecuteNonQuery(dbCommand);
                    return "done";
                    break;
            }

            return "undefined comand";
        }

        [WebMethod(EnableSession = true)]
        public string LoginTest()
        {
            return Sesion.isAccess("P_PRODUCTOS").ToString();
            
        }

        [WebMethod(EnableSession = true)]
        public string Productos(String JSONData)
        {
            var data = JObject.Parse(JSONData);
            String Comando = (String)data["Comando"];       

            if (!Sesion.isAccess("P_PRODUCTOS") && Comando != "SelectAllAlt")
            {
                return "peticion invalida";
            }

            String IdProducto = (String)data["IdProducto"];
            String CodProducto = (String)data["CodProducto"];
            String CodEmpresa = (String)data["CodEmpresa"];
            String EAN = (String)data["EAN"];
            String Nombre = (String)data["Nombre"];
            String Marca = (String)data["Marca"];
            String Presentacion = (String)data["Presentacion"];
            String Imagen = (String)data["Imagen"];
            //String Propio = (String)data["Propio"];
            String Habilitado = (String)data["Habilitado"];
            String CodFamilia = (String)data["CodFamilia"];
            String CodTipoProducto = (String)data["CodTipoProducto"];
            String ImageData = (String)data["ImageData"];
            String output = "";

            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.sp_Productos";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddOutParameter(dbCommand, "@output", DbType.String, 255);
            db.AddInParameter(dbCommand, "@Comando", DbType.String, Comando);
            db.AddInParameter(dbCommand, "@CodEmpresa", DbType.String, CodEmpresa);
            db.AddInParameter(dbCommand, "@IdProducto", DbType.String, IdProducto);
            db.AddInParameter(dbCommand, "@CodProducto", DbType.String, CodProducto);
            db.AddInParameter(dbCommand, "@Nombre", DbType.String, Nombre);
            db.AddInParameter(dbCommand, "@EAN", DbType.String, EAN);
            db.AddInParameter(dbCommand, "@Marca", DbType.String, Marca);
            db.AddInParameter(dbCommand, "@Presentacion", DbType.String, Presentacion);
            db.AddInParameter(dbCommand, "@Imagen", DbType.String, Imagen);
            //db.AddInParameter(dbCommand, "@Propio", DbType.String, Propio);
            db.AddInParameter(dbCommand, "@CodFamilia", DbType.String, CodFamilia);
            db.AddInParameter(dbCommand, "@CodTipoProducto", DbType.String, CodTipoProducto);
            db.AddInParameter(dbCommand, "@Habilitado", DbType.String, Habilitado);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            switch (Comando)
            {
                case "SelectAll":
                case "SelectAllAlt":
                case "SelectByIdProducto":
                case "SelectByCodProducto":
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        var row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    return serializer.Serialize(rows);
                    break;

                case "Insert":
                    db.ExecuteNonQuery(dbCommand);
                    output = db.GetParameterValue(dbCommand, "@output").ToString();
                    if (ImageData != "")
                    {
                        Proced.GuardarFotoProducto(ImageData, output);
                    }
                    return output;
                    break;

                case "Update":
                case "Delete":
                    db.ExecuteNonQuery(dbCommand);
                    if (ImageData != "")
                    {
                        Proced.GuardarFotoProducto(ImageData, CodProducto);
                    }
                    return "done";
                    break;
            }

            return "undefined comand";
        }


        [WebMethod(EnableSession = true)]
        public string Familias(String JSONData)
        {
            var data = JObject.Parse(JSONData);
            String Comando = (String)data["Comando"];

            if (!Sesion.isAccess("P_PRODUCTOS") && Comando != "SelectAllAlt")
            {
                return "peticion invalida";
            }

            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.usp_Familias";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddInParameter(dbCommand, "@Comando", DbType.String, Comando);

            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            switch (Comando)
            {
                case "SelectAll":
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        var row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    return serializer.Serialize(rows);
                    break;
            }

            return "undefined comand";
        }

        [WebMethod(EnableSession = true)]
        public string Relevamientos(String JSONData)
        {
            if (!Sesion.isAccess("P_RELEVAMIENTOS"))
            {
                return "peticion invalida";
            }

            var data = JObject.Parse(JSONData);
            String Comando = (String)data["comando"];
            String fechaDesde = (String)data["fechaDesde"];
            String fechaHasta = (String)data["fechaHasta"];
            String codRelevamiento = (String)data["codRelevamiento"];
            String activo = (String)data["activo"];
            String filtro = (String)data["filtro"];
            String codProductos = (String)data["codProductos"];
            String output = "";
            filtro = filtro == null ? "" : filtro;
            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.usp_Relevamientos";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddOutParameter(dbCommand, "@output", DbType.String, 255);
            db.AddInParameter(dbCommand, "@Comando", DbType.String, Comando);
            db.AddInParameter(dbCommand, "@CodRelevamiento", DbType.String, codRelevamiento);
            db.AddInParameter(dbCommand, "@FechaInicial", DbType.String, fechaDesde);
            db.AddInParameter(dbCommand, "@FechaFinal", DbType.String, fechaHasta);
            db.AddInParameter(dbCommand, "@Filtro", DbType.String, filtro);
            db.AddInParameter(dbCommand, "@Activo", DbType.Boolean, activo=="true"?true:false);
            db.AddInParameter(dbCommand, "@CodProductos", DbType.String, codProductos);
            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();
            

            switch (Comando)
            {
                case "SelectAll":
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        var row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    break;

                case "Update":
                case "Insert":
                    db.ExecuteNonQuery(dbCommand);
                    output = db.GetParameterValue(dbCommand, "@output").ToString();
                    var row1 = new Dictionary<string, object>();
                    row1.Add("CodRelevamiento", output);
                    rows.Add(row1);
                    break;
                case "SelectByCod":
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        var row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    break;
                case "IsActive":
                    db.ExecuteNonQuery(dbCommand);
                    output = db.GetParameterValue(dbCommand, "@output").ToString();
                    var row3 = new Dictionary<string, object>();
                    row3.Add("CodRelevamiento", output);
                    rows.Add(row3);
                    break;
                case "SelectAllProductos":
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        var row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    break;
                case "SelectAllProductosAsignados":
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        var row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    break;                    
                default:
                    var row2 = new Dictionary<string, object>();
                    row2.Add("Undefined Comand", Comando);
                    rows.Add(row2);
                    break;
            }
            return serializer.Serialize(rows);
        }

        [WebMethod(EnableSession = true)]
        public string TiposProductos(String JSONData)
        {
            if (!Sesion.isAccess("P_TIPOPRODUCTO"))
            {
                return "peticion invalida";
            }

            var data = JObject.Parse(JSONData);
            String Comando = (String)data["Comando"];
            String CodTipoProducto = (String)data["CodEmpresa"];
            String Nombre = (String)data["Nombre"];
            String output = "";

            Database db = DatabaseFactory.CreateDatabase("ServerConnection");
            const string stSqlCommand = "dbo.usp_TipoProducto";
            DbCommand dbCommand = db.GetStoredProcCommand(stSqlCommand);
            db.AddOutParameter(dbCommand, "@output", DbType.String, 255);
            db.AddInParameter(dbCommand, "@Comando", DbType.String, Comando);
            db.AddInParameter(dbCommand, "@CodTipoProducto", DbType.String, CodTipoProducto);
            db.AddInParameter(dbCommand, "@Nombre", DbType.String, Nombre);
            DataTable dt = null;
            var rows = new List<Dictionary<string, object>>();
            var serializer = new JavaScriptSerializer();

            switch (Comando)
            {
                case "SelectAll":
                case "SelectByCodTipoProducto":
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        var row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    return serializer.Serialize(rows);
                    break;

                case "Insert":
                    db.ExecuteNonQuery(dbCommand);
                    output = db.GetParameterValue(dbCommand, "@output").ToString();
                    return output;
                    break;

                case "Update":
                case "Delete":
                    db.ExecuteNonQuery(dbCommand);
                    return "done";
                    break;
            }

            return "undefined comand";
        }
    }
}
